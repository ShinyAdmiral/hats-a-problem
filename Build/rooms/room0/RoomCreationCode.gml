randomize();
instance_create_layer(-24, 108, "Instances", oCamera);
instance_create_layer(-24, 108, "Instances", obj_scoreHolder);
instance_create_layer(120, 184, "Player", obj_player);
instance_create_layer(80, 128, "Cards", obj_CardHelp);
instance_create_layer(120, 128, "Cards", obj_CardPlay);
instance_create_layer(160, 128, "Cards", obj_CardCredits);
instance_create_layer(x,y,"Fire", obj_cursor);