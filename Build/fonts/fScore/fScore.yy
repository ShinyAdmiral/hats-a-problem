{
    "id": "f14c47b2-12d4-466a-850d-71831ed7f602",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fScore",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "dd928127-6469-4d82-822d-dddee4309a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3d1e2b36-0ef5-428d-aa06-d5cd2b315c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 12,
                "y": 35
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "67f5e6f6-ee03-44d7-a8ba-5ff4ef3540d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 8,
                "y": 35
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3247f87d-7593-4611-b601-17be9adbb8ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "88703309-df60-4e48-8d8e-28d2baf14bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 120,
                "y": 24
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8a4ece77-17a6-482e-8bb4-2288d868a0bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 111,
                "y": 24
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4a89af11-88cb-47b0-85a5-f8925f620b1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 104,
                "y": 24
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c8512285-b8f7-4e7e-865d-67b8b1884569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 100,
                "y": 24
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d3901c59-7ab6-4f3f-82ad-16146564df4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 96,
                "y": 24
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2bc22a21-e137-4e51-aff0-e261eb2ba2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4337086e-4fc9-484f-b588-b6218cdde7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 16,
                "y": 35
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7b897a4a-f60f-4548-bf71-c3a9afa5374a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 24
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5e6d7994-047b-4f54-8e23-ce58e8aeac8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 76,
                "y": 24
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a3d7b91d-86c2-4a09-97a4-3bad3018406f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ecb5719d-a831-41ed-8b51-8970065d2aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 68,
                "y": 24
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "95f47191-18fd-43d1-8237-c713436cefd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 64,
                "y": 24
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "37cde766-b9e0-42bd-a0bc-937563a5603f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 58,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d8c70ba4-3053-49f5-b787-ca3997980ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 54,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1a8e387c-9ffa-4175-856a-c8decdd09078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 48,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a81defbf-7879-4e86-ac64-cc817cb2ae8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 42,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d33e7468-6c4c-4b11-83de-791ed094d42d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 36,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "649d5b75-087d-480d-a67d-7ef457722461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 80,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b6c0e382-d8a7-4482-9dae-aedeae5d30ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 35
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "81dbe992-592c-4f60-a4f5-361ea2a4def7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 27,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "645a20ba-46eb-4589-97bd-074d7ca55c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 33,
                "y": 35
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5666f08c-1be0-4cd4-8251-e431713550c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 44,
                "y": 46
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "63ab6314-4eec-4b40-9314-6d04223d73e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 40,
                "y": 46
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "18022636-3e03-44cf-83fe-0c3685799678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 36,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2a23c57f-0d5c-4407-a128-c3a0c1469be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 30,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2e01729e-7929-4283-b12a-5bf2e643fdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 24,
                "y": 46
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "158ad4e8-2548-42fe-925f-776c3941f8e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3a005e26-502f-494b-9a50-6061de270279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "252af783-bb48-4bf9-87b5-8b64250eac83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4ffcfc0c-882f-4a08-b9b8-ed479c38930e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 111,
                "y": 35
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f691840b-50b1-477f-884a-251610f16369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 105,
                "y": 35
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3d996ef2-e9dd-4902-b0a9-5f7570fa8cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 98,
                "y": 35
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2a400bfe-c728-47c6-9fdd-2ed63b9e3ce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 92,
                "y": 35
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1cb38777-f659-48ab-892b-37ad103f0123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 35
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2fd4fab9-47b4-4e52-b303-b3a77afebbb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 35
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "dc85f37b-588c-4495-9e32-12c307ca702b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 72,
                "y": 35
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1c9ff05e-0b1e-4d0d-98bb-2a23d77b9778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 66,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "39c0fd38-fa59-4ccd-8689-c11651898939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 62,
                "y": 35
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f37ade7d-d318-4fd6-985c-79186e0a1e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 35
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "48169be0-2a5c-4a7e-a16d-c4c19864c50a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 51,
                "y": 35
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ccd7fd87-9037-4c15-be65-d8eb945f77b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 46,
                "y": 35
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7e426cd9-1810-4e68-8b1e-b5a0aed8879d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 39,
                "y": 35
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c47fc1c4-ba4e-45ce-a7a6-b7b3bfd6ffc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d69b71b8-4dd0-406b-b06b-980139058a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 22,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "81e7310e-d6d4-4ad4-aa3b-3741e5b1ee26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 16,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "df91eba0-363e-4eff-8200-f4d7a996bcc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 12,
                "y": 13
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a0d2d002-5c1f-451c-bd64-c23248de8445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d331d7ea-0eef-4ee5-98f9-73d2719a2f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8b7f4319-126b-4f20-9b99-c9b321461625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "59b59517-2538-4806-93e6-d2ba949f850c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8cc2c37e-e9dc-40bc-93a2-247ec0a56ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ce9aff9f-e792-4fc5-9c19-dd129727c999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4b3bc505-9163-4dfa-938d-d60301ed3f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "40f42649-9e96-4868-9213-752d8973c8a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2d239fc1-5426-4ef2-aeb7-5a5405a8e79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "028f519f-6763-46bc-99a5-dc0a273a7534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 1,
                "x": 9,
                "y": 13
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9d5bcc36-26d4-4f3b-8728-1983a6e5ef71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cbdaf953-63e8-4d11-a8e7-4e4e8f675341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d84b8e79-924b-4623-afeb-8ebfb657b235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c8f706c5-5f5b-45e2-b5b5-4aa22aaaa18c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "911dfc1a-0389-4365-b72b-9f4d38bca6b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "94fbdcdb-de27-4853-87a0-67fb294629ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6cd3e4b6-a906-44bf-a56b-d9b0f2483f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7955a858-2b2a-41b9-b8ec-f6068281032b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8fc00cc1-f4e4-4f0f-8563-ccc883925ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "effb0044-0cd1-4bfc-9d16-167faa5180b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b48263fd-0466-4de3-8f8b-8631cda0d677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3d025911-71e1-4122-aa1c-468440f35f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 13
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c8d655ca-3a33-4e96-a4a0-d1321b7168b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 79,
                "y": 13
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e09b6929-0bb0-4db9-9e2e-91c6c380f81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 26,
                "y": 13
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a9141eef-79c5-4cb3-ba7a-47e9d07a2480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 7,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "bed7b344-6e9a-4c7f-9ccd-0c9d2eca6406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1789cbf3-ab9a-4408-919e-3eba9982449d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 121,
                "y": 13
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6dc5c7d1-52b0-465f-99b4-81faac87001f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 114,
                "y": 13
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7da67e93-afe2-403a-846c-59d14aec829f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 109,
                "y": 13
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "727ae3c8-77cd-4250-88d1-330de58c4574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 103,
                "y": 13
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8f668663-fc60-45f5-a530-8498c8905192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 98,
                "y": 13
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "672a3f92-8f4a-447d-a189-05acedfa50f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 92,
                "y": 13
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bfde461c-7e68-4f31-ade6-b409f653e744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 88,
                "y": 13
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f108b4c0-810a-4d0e-9aab-5f086dcad6a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 10,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "81d02028-84cd-4dec-bacf-8c4e7dcdbb0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 84,
                "y": 13
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "59885dbc-11fa-4d07-844c-d9aabe2b6cce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 74,
                "y": 13
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9ee38cd0-bac9-4b8c-9346-7893f4e4e2bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 68,
                "y": 13
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ed6f444c-c16e-4749-bf6b-479efee7c004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 13
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4e2fe735-a815-4d13-9605-ef187f515224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 13
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1a2599d8-2286-4b7c-a308-8cc020666afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 48,
                "y": 13
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a65ce2b2-d8aa-48e2-8c38-6f4ebe4766ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 42,
                "y": 13
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3d2cea3f-c698-4112-8093-90917f5d6354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 38,
                "y": 13
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "51f77a4e-cd3d-4808-933d-5b604473a605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 34,
                "y": 13
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "fa5cf836-5aba-4ea5-a7e8-51fc71101193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 30,
                "y": 13
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0c2bccb0-108d-4801-8201-3c7d5d08371a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 46
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2453d4ea-716c-43c7-b949-a07834234951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 9,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 56,
                "y": 46
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}