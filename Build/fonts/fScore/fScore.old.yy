{
    "id": "f14c47b2-12d4-466a-850d-71831ed7f602",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fScore",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial Black",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3bfbcacf-bcbc-41ee-a9bc-3d4634d59fdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "87d9bfec-50b8-4228-9263-cf8777a4804e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 53,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "72fd73c2-52bb-4b13-87f1-758c8781630e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 46,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c7ed7cb4-e555-4a41-a50f-1f63cae9cfa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 37,
                "y": 70
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "17ed8b38-5f37-462c-a3b8-f4df83346f72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 28,
                "y": 70
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e97983f9-360b-4336-9b8a-28f6a4e58d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 17,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "57c5a693-f466-4ce5-aafc-3456e34266c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 7,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d5484733-2307-4d29-8b1a-4d10ac1b6a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "13959dcf-621a-4438-8492-4c866d68ee52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 120,
                "y": 53
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "272e5a7d-e199-4a78-9694-c8680748bee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 114,
                "y": 53
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9fdd10d0-e719-4a06-8367-f08bb55c25a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 57,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "287bf702-81f4-48a7-a19f-2317d8caac22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 106,
                "y": 53
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "01d228dd-9a61-4df1-be5d-5ef6b88ccee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 93,
                "y": 53
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1e2f0fe0-9ec7-476a-94e6-a9c4d0e5bf12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 88,
                "y": 53
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1b5d5260-a4f5-44ad-93db-e6a35aff49f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 84,
                "y": 53
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0a307c38-e75a-480e-a1b5-01d722bc4702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 79,
                "y": 53
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a168dacc-485a-4b95-bc9e-6f62b6c1dbee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 70,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "857d0865-1c99-46ff-8912-7271542a5e88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 64,
                "y": 53
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a4aa8ff7-ed0d-4f72-80b1-9d806a4311cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 53
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "10262c8d-baac-43cc-8b1a-d8715376e303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 53
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f5075ef3-ba60-4892-a95c-1318538d52ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 37,
                "y": 53
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "dadd343d-8e92-4730-a4f2-5a7562d908bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 97,
                "y": 53
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "468721e0-f43c-4d8c-8be8-6d9b72a9689f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 63,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1b01a94f-c384-422e-9e58-2308adc7caac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 72,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "75729d84-644e-489e-92cc-f52d571a7854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 81,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e1c4de95-4adc-4b78-80c4-1634cf7f4c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1cf76da0-9a54-4816-91db-fd83f3782031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 14,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6eff834c-a5f0-4f29-b5da-e2aa5cfb3d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 10,
                "y": 104
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "61f316d0-9251-4aae-aa1a-f294fb85d478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "143c34d3-6df2-4f59-aa8e-9cc3e9534246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 114,
                "y": 87
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8d040f3b-7a9e-48c2-aa67-6820bfa56cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 106,
                "y": 87
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9e6b775e-39e3-436c-ba35-c93ca991c22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 87
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ffc5bec4-ab4a-4f9b-9861-e060becd54c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 88,
                "y": 87
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "120e2891-13d1-41e3-bf0d-3cf9ba2f742a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 77,
                "y": 87
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6c0870f3-157f-43f8-ba65-b310bbbec1fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 87
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a192b0d5-26e4-4440-8d55-84c0bef7e831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 59,
                "y": 87
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c8659b29-3c28-4787-9052-3bcc7084aaf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 50,
                "y": 87
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3d7b2b0a-41af-40fa-83a5-69666757e60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 87
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6316e924-f63d-4561-8942-661fdf4f7c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 87
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6562b60f-befc-404a-bde1-62114e3d2f72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 24,
                "y": 87
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "aee92b0d-b163-47e1-a33f-bc205b54d563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 15,
                "y": 87
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "39019407-e06f-4c04-bb9e-c56876d54826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 11,
                "y": 87
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4301f801-66aa-4516-9c8b-4d3058783f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f68da7f5-471c-4a08-a16c-12e2599c87bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 109,
                "y": 70
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0211f5e2-f121-4957-931e-1d81bfce1e10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 70
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "98f8f26c-7bec-424d-9b61-69728bc26eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 90,
                "y": 70
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ecd5ed20-04b4-48e5-9a66-1657f0bac573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 28,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "42e6a11b-6614-4e1c-8a59-b21cb6a68ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 18,
                "y": 53
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "883f5124-a664-4b70-8fe4-7fbe4d309c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 53
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "354c33ae-2ff0-4461-a6d7-8a6586b9ba3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 70,
                "y": 19
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "22c18fd6-2a97-46ae-8cf6-12ece9e986d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 55,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e1d6460f-64f3-45be-904c-ed35db641b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 45,
                "y": 19
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5d74abd6-4b64-477c-a168-5fec4704dcf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 35,
                "y": 19
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "91575161-6010-4e08-8ad1-806877278f70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 26,
                "y": 19
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5bdf3d8f-fb9c-4078-9e78-f9fceb572379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 15,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c18e6dd3-3e9e-478a-aba5-45de538dd986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "00593daf-086b-44a8-841d-c28783bcfda4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "36abd0bb-8eed-457b-becc-3ea1399c2bd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "26edd1c1-6f6e-4381-a79f-888e2a33c05b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0588e6d5-f3d4-4857-a891-b5d4ada0bb05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 65,
                "y": 19
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e0ab4e84-16ab-4ed4-971e-702db53800b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3134d361-1a99-450c-8c20-30880de6b795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0b3c4ac6-6f47-4b02-95ad-099c6f8cdc07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2efad8bd-75c0-4b58-a511-cabd996db264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e64db4b2-71ac-42ad-81fc-460f47d11852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4d66f64f-c3c8-4515-ad42-7ee1be36ec63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "aa1b8d21-69dd-43f7-b1ea-d546f9ecb94e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d0925a5f-6138-489b-8875-271009d9f75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b02d6f56-f6f0-4303-aeef-d882f8440d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "09a794ca-a15f-4e4a-b573-8840d55cc233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3f380fc3-a277-4030-80b6-90505d1ad43d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "94cf5ce1-87c6-4593-9043-a57030887712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 80,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "cd0ce3d8-d018-491a-84fc-9d3f692d3c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 40,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dd86bfeb-89f5-4f5e-8e2e-006cb2a552f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 89,
                "y": 19
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8cc9b1bc-66ab-4b01-984d-093862ac37d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 118,
                "y": 36
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8c8cbff1-fd69-46b4-8014-ce88e5d4bd65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 110,
                "y": 36
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2bbb1137-86fe-4b2d-b76f-7c380ab4b979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 106,
                "y": 36
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "513bcf73-4eb8-4cb5-a245-14f37c3de232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 95,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b49c2734-8272-4ed4-abdc-1a1713e2deba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 87,
                "y": 36
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "052c6ac7-5d6a-4084-a03c-1f73939a154a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 78,
                "y": 36
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0af08f3c-b3ed-476d-b230-4305cbff1ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 70,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f2b19bb6-fb32-4770-a999-6c4c316ee763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 61,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "589f397a-7897-4209-b0f3-1ad427f39449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 55,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "607b499f-e764-4a12-b38c-5cb10880a110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "06f6cc7a-838b-4bf3-bfda-19e7dd808842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 48,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b326b804-86f6-449d-99bf-090f141fee9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "eefcd9c2-9058-4260-8553-741fa06ef8bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "58dbc08a-7bec-47b9-becb-67209c94a2e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7101ef14-1fd6-4422-823d-0666e5901e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "01425baa-a65a-44f6-805e-78017eb77017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 116,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "033677be-d13c-45b9-ab5d-728dc9140c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 108,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "94a7e80e-eabf-4ec3-865a-e9ee12131a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 102,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "10a024d6-d8bb-4014-be75-de41492814ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 99,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "054bc955-40b3-4843-8837-e8487210a8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 93,
                "y": 19
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "435a08e0-05be-4520-885e-a96577e598f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 27,
                "y": 104
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "57dcaa08-310c-4df2-a36a-8c18f6fd12aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 35,
                "y": 104
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "06a0d2dc-97cc-4baa-950e-351223478828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "69d19096-ba40-41eb-85ff-a81388da55e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "5934847b-e05f-43ad-a395-306e34d7235f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "07b5bd5c-81e8-4a82-a736-f751100d7515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "eba6a83f-d0cf-4d8b-8a68-624f6b11d650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "c8ad7f8d-0b3c-46cd-b020-e526e6a1c4c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "9b6ff9cd-680e-4690-9283-61be9f2d8045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "b395fd8d-41b5-4ffb-bfc9-45b72ebf1fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "d4c67ae3-b34c-48ea-b6d8-ad70c5c62c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "b1310e6a-5b18-4de4-a69a-863fcb31d5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Black",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}