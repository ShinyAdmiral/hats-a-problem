/// @desc Stadium(object,thickness)

//set the argument to an object variable
var object = argument0;
//set the argument to the Rectangle Thickness
var thickness = argument1/2;

//Find the Width and Hieght of the object
var Width = sprite_get_width(object.sprite_index);
var Height = sprite_get_height(object.sprite_index);


//find coordinates for box
var xMin = object.x - thickness;
var yMin = object.y - (Height/2);
var xMax = object.x + thickness;
//add 3 to compensate for player's collision
var yMax = (object.y + (Height/2)) - 3;

//possible if for vertical or horizontal argument

//clamp player's vertical movement
y = clamp(y, yMin, yMax);

//check semi Circle Collisions on ends
//passing end of rectangle of the x minimum
if (x < xMin)
{
	var xDist = x - xMin;
	var yDist = y - (object.y - 1.5);

	var maxDistance = Height/2;
	var currentAngle = point_direction(0,0,xDist,yDist);

	var maxDistX = lengthdir_x(maxDistance,currentAngle);
	var maxDistY = lengthdir_y(maxDistance,currentAngle);

	if abs(xDist) > abs(maxDistX) 
	{
		x += maxDistX - xDist;
	}
	
	if abs(yDist) > abs(maxDistY) 
	{
		y += maxDistY - yDist;
	} 
}

//passing end of rectangle of the x maximum
if (x > xMax)
{
	var xDist = x - xMax;
	var yDist = y - (object.y - 1.5);

	var maxDistance = Height/2;
	var currentAngle = point_direction(0,0,xDist,yDist);

	var maxDistX = lengthdir_x(maxDistance,currentAngle);
	var maxDistY = lengthdir_y(maxDistance,currentAngle);

	if abs(xDist) > abs(maxDistX) 
	{
		x += maxDistX - xDist;
	}
	
	if abs(yDist) > abs(maxDistY) 
	{
		y += maxDistY - yDist;
	} 
}