if (!one)
{
	a = instance_create_layer(60,188,"Bullets", obj_EvilHeart)
	a.image_angle = 90;
}
else
{
	a = instance_create_layer(60,188,"Bullets", obj_Heart)
	a.image_angle = 90;
}

if (!two)
{
	b = instance_create_layer(80,148,"Bullets", obj_EvilHeart45)
	b.image_xscale = -1;
}
else
{
	b = instance_create_layer(80,148,"Bullets", obj_Heart45)
	b.image_xscale = -1;	
}

if (!three)
{
	c = instance_create_layer(120,136,"Bullets", obj_EvilHeart)
}
else
{
	c = instance_create_layer(120,136,"Bullets", obj_Heart)
}

if (!four)
{
	d = instance_create_layer(160,148,"Bullets", obj_EvilHeart45)
}
else
{
	d = instance_create_layer(160,148,"Bullets", obj_Heart45)
}

if (!five)
{
	e = instance_create_layer(180,188,"Bullets", obj_EvilHeart)
	e.image_angle = -90;
}
else 
{
	e = instance_create_layer(180,188,"Bullets", obj_Heart)
	e.image_angle = -90;
}