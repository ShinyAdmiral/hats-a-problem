//pick random knife to not spawn
randomize();
ran = irandom_range(0,6);

//have we repeated this knife before?
if (missing[0]=ran || missing[1]=ran || missing[2]=ran)
{
	scr_RandomKnives();
}

//spawn all knives but one
else
{
	var i;
	for (i=0; i<7; i++)
	{
		if (ran != i)
		{
			instance_create_layer(84 + (12*i), 184, "Bullets", obj_knifeWarning);
		}
		else
		{
			//keep track of each knife that is left out
			missing[a] = ran;
			a++;
		}
	}
}