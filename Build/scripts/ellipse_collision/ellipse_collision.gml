//set the argument to an object variable
var object = argument0;
show_debug_message(string(object) + "'s Info --------------------------------");

// set the minor and major axis of the object
var majorAxis = sprite_get_width(object.sprite_index);
var minorAxis = sprite_get_height(object.sprite_index);
show_debug_message("Major Axis: " + string(majorAxis));
show_debug_message("Minor Axis: " + string(minorAxis));

//set the semi Major and Minor axis for calculations
var semiMajorAxis = majorAxis/2;
var semiMinorAxis = minorAxis/2;

//Solve for the Foci Offset which will be used to find the points of the foci
var fociOffset = sqrt(sqr(semiMajorAxis) - sqr(semiMinorAxis));
show_debug_message("Foci Offset: " + string(fociOffset));

//Starting Calculations for foci for ellipse collision. Fx, Gx, FGy, and FPGL are constants used for calcs
// grab the Foci's x's and y's
var Fx = object.x - fociOffset;
var Gx = object.x + fociOffset;
var FGy = object.y;

//After picking a known spot on the ellipse we use that to calculate the each distance between each foci and that point
var FPL = point_distance(Fx, FGy, object.x, object.y + semiMinorAxis);
var PGL = point_distance(Gx, FGy, object.x, object.y + semiMinorAxis);

//d1 plus d2 gets us a constance variable that won't change
var FPGL = abs(FPL) + abs(PGL);
show_debug_message("Max: " + string(FPGL));

//gather the PLayer's known spot and distance from both foci
var current_distanceF = point_distance(Fx, FGy, x, y);
var current_distanceG = point_distance(Gx, FGy, x, y);
var FPG = abs(current_distanceF) + abs(current_distanceG);
show_debug_message("FPG: " + string(FPG));

//start to find the point on the ellipse the player is heading to
//find the current angle from center to player. This will be the same for our point
var currentAngle = point_direction(object.x , object.y, x, y);
show_debug_message("Current Angle: " + string(currentAngle));
//show_debug_message("Anti Angle: " + string(-currentAngle));

//convert to radians
var currentRad = degtorad(currentAngle);
//var antiRad = degtorad(-currentAngle);


//find the point on the line based on the angle.
var Px = semiMajorAxis * cos(currentRad);
var Py = -semiMinorAxis * sin(currentRad);

var APx = semiMajorAxis * sin(currentRad);
var APy = -semiMinorAxis * cos(currentRad);

show_debug_message("Point: (" + string(Px)+ ", " + string(Py) + ")");
//show_debug_message("Anti-Point: (" + string(APx)+ ", " + string(APy) + ")");
show_debug_message("Player to Point: (" + string(x-object.x)+ ", " + string(y-object.y) + ")");

//define true cordinates of Point
var TPx = object.x + Px;
var TPy = object.y + Py;
//anti True coordinates
var ATPx = object.x - Px;
var ATPy = object.y - Py;

show_debug_message("True Point: (" + string(TPx)+ ", " + string(TPy) + ")");
show_debug_message("Anti True Point: (" + string(ATPx)+ ", " + string(ATPy) + ")");
show_debug_message("Player Position: (" + string(x)+ ", " + string(y) + ")");

if (FPG > FPGL)
{
	if((ver != 0) && (hor = 0))
	{
		y = ATPy;
		show_debug_message("Y Switch")
	}
	
	if((hor !=0 ) && (ver = 0))
	{
		x = ATPx
		show_debug_message("X Switch")
	}
	
	if ((hor != 0) && (ver != 0))
	{
		var ATPx = object.x - APx;
		var ATPy = object.y - APy;
		x = ATPx;
		y = ATPy;
	}
	
	//instance_create_layer(x,y,"Fire", obj_card)
//	x = ATPx;
//	y = ATPy;
	show_debug_message("!!!Bounds!!!");
}

//debugging purposes
if(point_direction(Fx, FGy, ATPx, ATPy) + (point_direction(Gx, FGy, ATPx, ATPy) = FPGL))
show_debug_message("ATP Working");
else
show_debug_message("ATP  Broke");

if(point_direction(Fx, FGy, TPx, TPy) + (point_direction(Gx, FGy, TPx, TPy) = FPGL))
show_debug_message("TP Working");
else
show_debug_message("TP Broke");


//if (abs(PlayerDistanceX) > abs(Px))
//{
//	instance_create_layer(x,y,"Fire", obj_card)
//	x = object.x//ATPx;
//	//y = ATPy;
//	//x -= Px + PlayerDistanceX;
//}
	
//if (abs(PlayerDistanceY) > abs(Py))
//{
//	//y = ATPy;
//	instance_create_layer(x,y,"Fire", obj_card)
//	y = object.y
//	//y -= Py + PlayerDistanceY;
//}