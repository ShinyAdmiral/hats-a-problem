randomize();

//number of cards in deck
#macro maxnum 5
#macro minimum 0

//determine the random spawns
spawn[0] = irandom_range(minimum, maxnum);
spawn[1] = irandom_range(minimum, maxnum);
spawn[2] = irandom_range(minimum, maxnum);
spawn[3] = irandom_range(minimum, maxnum);

#region check for duplicate numbers
if ((spawn[0] = spawn[1]) || (spawn[0] = spawn[2]) || (spawn[0] = spawn[3]) ||
	(spawn[1] = spawn[2]) || (spawn[1] = spawn[3]) || 
	(spawn[2] = spawn[3]))
{
	spawn[0] = -1;
	spawn[1] = -1;
	spawn[2] = -1;
	spawn[3] = -1;
	scr_SpawnCards();
}
#endregion

#region if there are no duplicate cards
else
{
	//array
	var i;
	//distance
	var d = -57;
	for(i = 0; i < 4; d += 38)
	{
		//spawn cards
		if (spawn[i] = 0)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardBang);
		}
		else if (spawn[i] = 1)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardKnife);
		}
		else if (spawn[i] = 2)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardFire);
		}
		else if (spawn[i] = 3)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardRose);
		}
		else if (spawn[i] = 4)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardSwing);
		}
		else if (spawn[i] = 5)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardHeart);
		}
		else if (spawn[i] = 6)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardFireBlast);
		}
		else if (spawn[i] = 7)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardRazorCard);
		}
		else if (spawn[i] = 8)
		{
			var a = instance_create_layer(obj_sumoCircle.x + d, room_height/2+ 12,"Cards", obj_CardFencingHat);
		}
		
		//alarm fro floating
		a.alarm[0] = 15*(i+1);
		i++;
	}
	instance_create_layer(x, y, "Instances", obj_AttackOrganizer)
}
#endregion