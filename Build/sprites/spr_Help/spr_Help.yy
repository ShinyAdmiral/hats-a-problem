{
    "id": "b7d40029-733d-4e96-9551-10fcdde5d3fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Help",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9108935-2b2c-43fc-87a6-0d81a0ed095d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7d40029-733d-4e96-9551-10fcdde5d3fe",
            "compositeImage": {
                "id": "4b542894-65bd-4d7a-b290-832e4f41314d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9108935-2b2c-43fc-87a6-0d81a0ed095d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac9ea89-b687-4437-998b-c68947fb8cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9108935-2b2c-43fc-87a6-0d81a0ed095d",
                    "LayerId": "6e062dbe-6df2-48ca-b3e0-fc5a066257c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6e062dbe-6df2-48ca-b3e0-fc5a066257c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7d40029-733d-4e96-9551-10fcdde5d3fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}