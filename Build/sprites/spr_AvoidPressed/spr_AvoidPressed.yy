{
    "id": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_AvoidPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef04617c-2d31-4c43-be0e-857c0a34877e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "7383f1a6-95b7-4975-bcd9-1f3e56c23c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef04617c-2d31-4c43-be0e-857c0a34877e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c24ef88-0078-487f-a7c8-a75d67c31eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef04617c-2d31-4c43-be0e-857c0a34877e",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "f540bcf5-19f6-4c43-a0ef-9a41e8b7c8e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "f7710800-faae-4d54-bf74-2801e82def65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f540bcf5-19f6-4c43-a0ef-9a41e8b7c8e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc11d142-9cee-4d48-baf5-1f7c0a7869f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f540bcf5-19f6-4c43-a0ef-9a41e8b7c8e6",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "b9921677-8b40-488a-8ccf-24a1b687ac6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "9cbe4e66-26de-49d6-9ba0-6751e9e50aca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9921677-8b40-488a-8ccf-24a1b687ac6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9fda98e-f541-4a1a-a1ef-b8b822ea4552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9921677-8b40-488a-8ccf-24a1b687ac6b",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "d26b0d7b-e718-4114-9c50-c07c48466a63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "5a2edb1d-9212-4c57-84dd-b05ad760a529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d26b0d7b-e718-4114-9c50-c07c48466a63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9292029d-c7a4-4ad1-bdc4-e057c80635f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d26b0d7b-e718-4114-9c50-c07c48466a63",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "07b19821-909d-42df-bcc5-ada09560b886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "403dcf5b-863f-4b29-a8af-2e9151bc6a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07b19821-909d-42df-bcc5-ada09560b886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aa55602-f69d-4dbc-b184-eaadaecb4bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07b19821-909d-42df-bcc5-ada09560b886",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "df542348-4b89-4b25-be47-6554b8978dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "77cd75e6-011f-4078-9ac7-ab1970230a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df542348-4b89-4b25-be47-6554b8978dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8844ae81-f938-47d9-a275-52ff987e5423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df542348-4b89-4b25-be47-6554b8978dc5",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "19a5cdb8-8c2f-40b0-8fb1-9c242d8d2a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "b7823656-216f-478b-8cd0-4ba3fb9afa36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a5cdb8-8c2f-40b0-8fb1-9c242d8d2a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d82f0445-7395-4bcc-b71d-89ffd2895eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a5cdb8-8c2f-40b0-8fb1-9c242d8d2a38",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "3b47b449-6fec-428b-976f-a62007053747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "fd6b9205-9ac3-4962-abf6-9f3c4507f633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b47b449-6fec-428b-976f-a62007053747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547827b4-28fd-4965-a33c-795ba68c52b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b47b449-6fec-428b-976f-a62007053747",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "08dad50c-5dce-4ef9-8116-957c5468e0b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "f02700bd-2c31-4dbe-b7cb-1138852a0de2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08dad50c-5dce-4ef9-8116-957c5468e0b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc956835-3827-4e54-841e-f971b65c2450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08dad50c-5dce-4ef9-8116-957c5468e0b8",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "3759d4f2-2b76-4441-8fe3-74d010ce066d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "2ebf347e-4e8e-4b46-8a68-5b5837fd7f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3759d4f2-2b76-4441-8fe3-74d010ce066d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c97539-76e6-49d3-af7c-2478290d72b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3759d4f2-2b76-4441-8fe3-74d010ce066d",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "d7755d7c-b096-4fb2-ba67-7ecf32aa8b49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "9481760d-60d5-49f3-af20-454fc5ed08a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7755d7c-b096-4fb2-ba67-7ecf32aa8b49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "451009df-55aa-45fa-8058-257e3f975185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7755d7c-b096-4fb2-ba67-7ecf32aa8b49",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "758984ab-2e66-4c49-8878-fff936c361c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "29c3e14b-95fc-468f-b32c-b91815426197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "758984ab-2e66-4c49-8878-fff936c361c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e2a026-9cad-421f-b81c-dcdedf002f8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "758984ab-2e66-4c49-8878-fff936c361c1",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "631f46c7-34f3-4620-8911-e263cd132877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "0cad40bc-ee29-46ba-8466-604d23f192fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "631f46c7-34f3-4620-8911-e263cd132877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f270b161-1fc7-416f-bca3-2ea554ff9687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "631f46c7-34f3-4620-8911-e263cd132877",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "80459194-5a5e-4f8f-84b6-a342961ebdce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "77b37400-9797-4c22-bded-d651b0996077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80459194-5a5e-4f8f-84b6-a342961ebdce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9963f6da-27e6-4771-bc45-f6b20c56b623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80459194-5a5e-4f8f-84b6-a342961ebdce",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "94089460-15a6-45c0-866f-33cfc7d2e0f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "2754a51f-5400-49bb-8f85-8afe26e1b928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94089460-15a6-45c0-866f-33cfc7d2e0f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2cff4a-2905-4fa1-96a6-910f7066a47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94089460-15a6-45c0-866f-33cfc7d2e0f5",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "81af8e11-f61b-4f33-b746-80a8f6ed5396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "8265db4a-289d-471c-9702-30ebbc268d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81af8e11-f61b-4f33-b746-80a8f6ed5396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c13ed6-6023-46eb-864c-f50211a68d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81af8e11-f61b-4f33-b746-80a8f6ed5396",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "3ff9dcdb-fbac-4a93-a5d3-13d8ed2a20d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "2c9f28e1-73ad-48c6-bfe1-9aafbf9a16c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ff9dcdb-fbac-4a93-a5d3-13d8ed2a20d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a9c69d5-e60c-4799-a219-2c24cdaba6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ff9dcdb-fbac-4a93-a5d3-13d8ed2a20d1",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "8031c0db-f973-47ba-a843-c0619ffcb7e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "8874d572-c03b-468c-b296-21aa47326b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8031c0db-f973-47ba-a843-c0619ffcb7e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a5608d8-783c-4c44-bdad-b9703ef2a06f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8031c0db-f973-47ba-a843-c0619ffcb7e3",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "2bee8dca-4f5c-4a52-8275-b8ba2e9cf6e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "b9449f34-0f2f-4fb8-9301-c875138dc165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bee8dca-4f5c-4a52-8275-b8ba2e9cf6e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffc15077-2871-464b-9fb1-fcefe6205063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bee8dca-4f5c-4a52-8275-b8ba2e9cf6e1",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "df88cd99-6705-481c-94a6-509e265ed16d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "a53365cc-fefc-4986-a6bf-52042c5c92ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df88cd99-6705-481c-94a6-509e265ed16d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f7f1f0-0850-4944-a23b-a7e60b0258d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df88cd99-6705-481c-94a6-509e265ed16d",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "171d01be-b052-4222-acb2-12cc0f47a5d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "e715f8e2-6c14-4204-b80d-4ddffa761954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171d01be-b052-4222-acb2-12cc0f47a5d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd41ce1-cf33-4791-9b6f-1a823ee1f1ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171d01be-b052-4222-acb2-12cc0f47a5d3",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "58c9756d-3a82-477c-bd48-967ffe8aea38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "b50c326b-7960-4058-9b36-049ee67f711d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c9756d-3a82-477c-bd48-967ffe8aea38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09923fd4-7754-4872-a0f0-c5fec948fa7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c9756d-3a82-477c-bd48-967ffe8aea38",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "0d40922f-5f1b-4066-99a2-ccc4122f86d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "25a339d2-e9a3-4818-b160-0e3f8c59afae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d40922f-5f1b-4066-99a2-ccc4122f86d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec47571a-1bda-480a-a35e-e1dbf511f6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d40922f-5f1b-4066-99a2-ccc4122f86d8",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "dedca72e-9b92-48eb-94b5-b390e79acabf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "fa9ec432-d721-4d06-97a0-beb3e684a838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dedca72e-9b92-48eb-94b5-b390e79acabf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a284ffa-fd9f-46ce-bcbd-393ccea3d893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dedca72e-9b92-48eb-94b5-b390e79acabf",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "0262eaa6-4f01-413d-aba2-2c3e0c2abf84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "c6e54a4b-6cef-40f4-a4cb-52d02bfd3c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0262eaa6-4f01-413d-aba2-2c3e0c2abf84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "069d5f6f-1ffe-4b84-a9da-f06486b03e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0262eaa6-4f01-413d-aba2-2c3e0c2abf84",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "f73e67d4-9a24-4fff-b165-0bd9cfb857ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "6f2c6a23-48e9-45af-af63-920bc1c87f38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f73e67d4-9a24-4fff-b165-0bd9cfb857ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70b9b0c-65dc-41d8-b4d8-dd2e0c500a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f73e67d4-9a24-4fff-b165-0bd9cfb857ad",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "9766ffd6-8a88-4f6c-9d2a-294d13a66755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "19c72142-0c3f-49c9-81dc-4d289f17223a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9766ffd6-8a88-4f6c-9d2a-294d13a66755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c780c6c4-e066-4fb7-8f6d-56b3d5f60afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9766ffd6-8a88-4f6c-9d2a-294d13a66755",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "2300f5b8-b304-4f8d-8491-15f138d3adee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "8893b2ac-22b2-4dc9-b637-c46e76fed2fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2300f5b8-b304-4f8d-8491-15f138d3adee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d0c4839-52cd-4c90-906c-49d67250fd89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2300f5b8-b304-4f8d-8491-15f138d3adee",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "3ebadf3f-79dd-4e63-86a0-54e261e3750b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "183a4508-45c1-48e7-966d-715e6d69bb95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ebadf3f-79dd-4e63-86a0-54e261e3750b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e04e77-f007-4814-ade6-cc0e47d25d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ebadf3f-79dd-4e63-86a0-54e261e3750b",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "035ba412-0385-4bed-a634-19a911a536e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "004ac02a-0702-47bf-8f36-0bcd73db39fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035ba412-0385-4bed-a634-19a911a536e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afb1ac90-fb87-4eaf-aedd-6eb991848aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035ba412-0385-4bed-a634-19a911a536e6",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "d6977e5c-90f1-4469-b970-bf2707300ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "98e5cfed-a621-49c4-adaf-bc6d1c84f188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6977e5c-90f1-4469-b970-bf2707300ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29255e21-8a7c-49d9-afc1-966d005f2957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6977e5c-90f1-4469-b970-bf2707300ab3",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "c9a349ca-bce1-4d30-8b41-3a69d92aa8bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "ed93ef88-0bb4-4812-abf7-f656bb170711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9a349ca-bce1-4d30-8b41-3a69d92aa8bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4136990d-b1ef-449d-97bd-026f4012f349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9a349ca-bce1-4d30-8b41-3a69d92aa8bc",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "7add30a6-a445-45f0-80dd-187d50f6cb9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "62f690ef-b6b8-4ffa-9134-d339077d7f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7add30a6-a445-45f0-80dd-187d50f6cb9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cca8f30-7817-4255-a973-c5a3825dd515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7add30a6-a445-45f0-80dd-187d50f6cb9a",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "6d95662e-5269-4555-9214-422a456fbbb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "603ab304-53c0-42c8-8f3c-bb947e9da7d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d95662e-5269-4555-9214-422a456fbbb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dd11a5d-995a-4a7d-a794-b25957c5dfdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d95662e-5269-4555-9214-422a456fbbb2",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "1daa936d-20f9-4f9a-83b1-69c6a93cb090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "542bfb9e-4dab-4d82-8e27-8633803f2512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1daa936d-20f9-4f9a-83b1-69c6a93cb090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "100c3ed6-9af6-44e3-9cd8-f74134b2086f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daa936d-20f9-4f9a-83b1-69c6a93cb090",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "50acccc0-b68a-4112-a461-3004aba45d82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "2d56c556-87e1-4270-b09a-f9f8bfd0e65d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50acccc0-b68a-4112-a461-3004aba45d82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bde41991-3759-41ea-8bc8-f739c9e99409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50acccc0-b68a-4112-a461-3004aba45d82",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "6cacc639-40d1-4bf4-8780-0710409fca79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "da9d938c-ba1b-449f-b37f-a4753affcbc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cacc639-40d1-4bf4-8780-0710409fca79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b657e35-9384-4fd5-aeae-2764fdd00f1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cacc639-40d1-4bf4-8780-0710409fca79",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "716980b4-bbc8-4872-8400-20553c07b9a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "a75ca074-236a-4b45-9c31-746a4790a771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "716980b4-bbc8-4872-8400-20553c07b9a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276450a7-2543-488a-abb6-fa40f29790bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "716980b4-bbc8-4872-8400-20553c07b9a4",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "2c3a904d-e507-4b14-aec7-77a7e0538ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "69c31faa-6919-4a53-98c7-10c0a777d211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3a904d-e507-4b14-aec7-77a7e0538ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b1f4016-c1b7-462d-99dc-b9707533e94f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3a904d-e507-4b14-aec7-77a7e0538ca1",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "52b98c79-81d4-41e8-a4f6-d2ac2d72e729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "e2a0c578-ac98-414e-a35c-b5aa1132ba21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b98c79-81d4-41e8-a4f6-d2ac2d72e729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed05dc1d-b18a-449d-bbad-8f7a8cb517e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b98c79-81d4-41e8-a4f6-d2ac2d72e729",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        },
        {
            "id": "264e71e6-ba3d-4efb-8f2c-43c3362880f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "compositeImage": {
                "id": "3ca39a86-ca34-4992-ad23-37acc994a209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "264e71e6-ba3d-4efb-8f2c-43c3362880f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcb21bd-45f2-4501-a7ee-ed160558d0fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "264e71e6-ba3d-4efb-8f2c-43c3362880f5",
                    "LayerId": "6b22d211-d180-4d37-991d-dda063e144ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6b22d211-d180-4d37-991d-dda063e144ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f39cbd80-159d-48b1-9dc0-bed1c1ef203e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}