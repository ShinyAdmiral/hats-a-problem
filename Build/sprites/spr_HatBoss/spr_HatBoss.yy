{
    "id": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_HatBoss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 17,
    "bbox_right": 76,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e143b696-f19d-4459-b995-5306fc93b162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "db640c7d-e0bb-46d4-8236-4ebe16b8f000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e143b696-f19d-4459-b995-5306fc93b162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935f0915-923d-4610-9d67-ed198b43c1c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e143b696-f19d-4459-b995-5306fc93b162",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "43e5a35e-2ddf-485b-988a-46b01d47e406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "942e4851-9ded-4776-8e9d-dd85205bc0c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43e5a35e-2ddf-485b-988a-46b01d47e406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357f3ba4-af49-4a93-928b-682b5fe13582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e5a35e-2ddf-485b-988a-46b01d47e406",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "9639114c-1861-479e-ab3c-c339ef834a5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "900bf010-dec6-4dfa-ba88-479bfba96491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9639114c-1861-479e-ab3c-c339ef834a5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34198570-8ecc-46d9-a5c4-19dfb16be98d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9639114c-1861-479e-ab3c-c339ef834a5c",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "54f57f97-9d32-4a1f-8f23-57a180d518f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "449a1869-b051-4bd4-9089-d2eba33c4bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54f57f97-9d32-4a1f-8f23-57a180d518f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7692424c-1339-4114-b4bb-4ae6fe2bc55b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54f57f97-9d32-4a1f-8f23-57a180d518f2",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "bf96bf96-5455-406d-9a26-340694fbfc88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "8d2b8adc-4c73-4f56-a173-625a11ecdec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf96bf96-5455-406d-9a26-340694fbfc88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b4caaa-c4b6-4c15-a679-cf571acbf2fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf96bf96-5455-406d-9a26-340694fbfc88",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "8a76873b-741e-43fb-acfb-ef002c13ebf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "d9202647-57fd-4f72-9786-620917b3eead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a76873b-741e-43fb-acfb-ef002c13ebf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb89725-9ccd-445d-8bf6-c0254b789e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a76873b-741e-43fb-acfb-ef002c13ebf7",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "0aa9aef9-b2e7-4f19-b2bb-c89864fd6d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "5f1b23df-7a9c-45cd-827d-25c957bef2d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aa9aef9-b2e7-4f19-b2bb-c89864fd6d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "007fcf60-9233-469c-9393-6fd7d8276f9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aa9aef9-b2e7-4f19-b2bb-c89864fd6d47",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "609d7bef-a2ea-4d75-b75e-b1aeccf37fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "5c1b848c-d4e0-4d81-9e5d-10d6a5943032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "609d7bef-a2ea-4d75-b75e-b1aeccf37fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01438ad7-c3d6-4a41-8750-ca6db9a131a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "609d7bef-a2ea-4d75-b75e-b1aeccf37fca",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "b4e47a20-43ba-4f90-8e81-ac782518b5fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "cc0b175a-a0d0-4113-99f0-af7999935c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4e47a20-43ba-4f90-8e81-ac782518b5fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e04e47-2e57-4950-815f-2f8abc751862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4e47a20-43ba-4f90-8e81-ac782518b5fb",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "dcb53ca8-f46a-478a-80e2-f9c37d02bd6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "f6b186d0-ce93-499d-8965-68b2b4682dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb53ca8-f46a-478a-80e2-f9c37d02bd6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da4a1e52-ddf9-40d8-b5c0-ad47d80f0c1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb53ca8-f46a-478a-80e2-f9c37d02bd6b",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "67a90e0a-82e1-46db-b8df-371e3ae14c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "25e57987-5c3f-42f6-93a0-ccce602c2183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a90e0a-82e1-46db-b8df-371e3ae14c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3245b08-3396-4593-becb-cbad16a2a230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a90e0a-82e1-46db-b8df-371e3ae14c02",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "5a3df457-8433-49fa-9095-7d1784b1097a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "ea8e2a1f-0569-4420-8531-4c1eb74fc3bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a3df457-8433-49fa-9095-7d1784b1097a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad34d2b-e8e6-4811-b718-b6231876ae56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3df457-8433-49fa-9095-7d1784b1097a",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "8bc1ad43-8614-4f97-9a91-465b2de2fff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "4abdcf19-5329-4679-bbbb-dfeeedcc3561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc1ad43-8614-4f97-9a91-465b2de2fff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8694970c-eab4-4ad2-a542-e671001a332e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc1ad43-8614-4f97-9a91-465b2de2fff3",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "7c3e605c-01a5-4775-9d65-ed30f888cc6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "3924a570-1f8b-4ef0-8e3a-1a1311169fd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c3e605c-01a5-4775-9d65-ed30f888cc6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a759802-638a-4484-ad6f-2f83f2c855e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c3e605c-01a5-4775-9d65-ed30f888cc6f",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "1c3ae648-7162-4d4e-823d-fb82fb5eccd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "35b0658b-a40e-4fdb-86f2-71bd751ee398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c3ae648-7162-4d4e-823d-fb82fb5eccd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3059f115-d0d6-45a5-a2e0-1835c45d77e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3ae648-7162-4d4e-823d-fb82fb5eccd1",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "9ee4060f-e277-4870-87af-3f9f6070d96c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "474c0895-cd10-41bb-b9ab-cfda4a5be146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ee4060f-e277-4870-87af-3f9f6070d96c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b908a37f-116a-4698-8390-bb9b9b807704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee4060f-e277-4870-87af-3f9f6070d96c",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "412e561e-759e-4572-b898-8ea92cbe0820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "ac0c86a3-4948-4ca9-ab0b-ce4c1c498919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "412e561e-759e-4572-b898-8ea92cbe0820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff633170-d600-4c47-a9f1-5c46989e5f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "412e561e-759e-4572-b898-8ea92cbe0820",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "666b39ac-e063-4987-a75d-728d48269d2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "64dbc4cd-cb73-4f64-8cd7-3e2cfcb34f05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666b39ac-e063-4987-a75d-728d48269d2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edbb7d05-89bd-422f-aa98-03441042c5e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666b39ac-e063-4987-a75d-728d48269d2a",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "659ae8e8-da6b-436f-bf3d-420d8d8a4a3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "6d59a124-9303-4da1-ab58-1ad9d95ccdad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "659ae8e8-da6b-436f-bf3d-420d8d8a4a3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "880acacb-ea61-4410-a3c5-9b54e9572793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "659ae8e8-da6b-436f-bf3d-420d8d8a4a3d",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "016b8c71-30e3-4e92-ab26-8bfc5aa8f947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "073d2395-07f1-4bd1-8e87-940ca8d8b60c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016b8c71-30e3-4e92-ab26-8bfc5aa8f947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01e3b072-6732-4b30-9142-d27d9150ab2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016b8c71-30e3-4e92-ab26-8bfc5aa8f947",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "6e930272-a17d-4261-931e-be27df38dbce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "5f0f21d3-7651-4ab0-a9ab-a3971582c45e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e930272-a17d-4261-931e-be27df38dbce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132d2aad-59e4-422f-99f1-a605df8de74d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e930272-a17d-4261-931e-be27df38dbce",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "da8be3af-8475-4231-b08b-8304bb7a8ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "1e1b7fa8-a666-4bde-9176-da7b0839b587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da8be3af-8475-4231-b08b-8304bb7a8ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68f29feb-1528-4137-87c4-1d641eadf68e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da8be3af-8475-4231-b08b-8304bb7a8ec2",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "c740b3b2-1871-4d2d-aa00-79aff322b621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "9d6b4473-1b7a-4653-85c3-fbdcc6e26874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c740b3b2-1871-4d2d-aa00-79aff322b621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1e9fbb-daa2-40e8-b920-83a56740b546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c740b3b2-1871-4d2d-aa00-79aff322b621",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "6b01cef9-0086-4706-8441-c3bf9f9508ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "e2b6a6fe-a3a3-45ed-a44b-efb38e4817ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b01cef9-0086-4706-8441-c3bf9f9508ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d19d92-3e0b-4b84-a39f-6fbe5a079e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b01cef9-0086-4706-8441-c3bf9f9508ed",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "fe9abd1f-3e44-4e1d-a5d5-f1fe64c5075a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "ec1f4e00-36c9-4335-a22e-302eb9a7eceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe9abd1f-3e44-4e1d-a5d5-f1fe64c5075a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bbc10ce-1f6e-4afe-8d0e-4dc39d3d9927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe9abd1f-3e44-4e1d-a5d5-f1fe64c5075a",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "921f1dbf-0721-4c25-8038-b85e3d7903c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "1f094275-dab3-4897-a3d2-970a1b305174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "921f1dbf-0721-4c25-8038-b85e3d7903c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7d09742-6555-4526-9d80-d159c8882178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "921f1dbf-0721-4c25-8038-b85e3d7903c3",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "763903e4-6a2a-4878-85a2-b0739915d24e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "15731f05-5451-46bb-88e2-8911b68edbef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "763903e4-6a2a-4878-85a2-b0739915d24e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3517e51-8e6c-426e-87a5-16affc4b6c41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "763903e4-6a2a-4878-85a2-b0739915d24e",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "5137916c-31bb-4837-bd01-9f8ebf10a42d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "67ef00a0-94a1-4c4b-908e-005f9fb9650f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5137916c-31bb-4837-bd01-9f8ebf10a42d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89a2b48-5aaa-4075-ae1e-a777e5710a6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5137916c-31bb-4837-bd01-9f8ebf10a42d",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "d72b1d38-756f-47c8-b385-678b4da8b6b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "a8a64cf5-4c5c-468b-b26d-3f6e6683418c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d72b1d38-756f-47c8-b385-678b4da8b6b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2301f42-eec0-4b61-b2d6-50431d14b77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d72b1d38-756f-47c8-b385-678b4da8b6b3",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "0cf10703-a1b4-4ff2-b0ec-6d17efdcd6b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "4bd8f463-2dc7-4904-b21c-cfd7beb29d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cf10703-a1b4-4ff2-b0ec-6d17efdcd6b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1265ad48-7af1-4415-bfd1-286e682b261c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cf10703-a1b4-4ff2-b0ec-6d17efdcd6b8",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "ae8cccd4-2018-44f9-baf0-b2c2d6220a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "e5708659-6859-4cf4-a7a0-65e268a91820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8cccd4-2018-44f9-baf0-b2c2d6220a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83f416a-8fdb-4b60-b323-598836109126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8cccd4-2018-44f9-baf0-b2c2d6220a3c",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "4292aa36-4d99-40cd-a000-f05b8ea2ce7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "924c3e60-1fe7-4862-aa21-14bc1866bcb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4292aa36-4d99-40cd-a000-f05b8ea2ce7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d52f260-5c73-49c8-8117-237ac103cd30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4292aa36-4d99-40cd-a000-f05b8ea2ce7d",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "b1e71335-5ca0-4f02-8f1c-c22a95b2ad0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "727d7d4e-e647-43e9-b2f2-4972153be96b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e71335-5ca0-4f02-8f1c-c22a95b2ad0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b8e986e-e178-446e-b857-8526e1bf91b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e71335-5ca0-4f02-8f1c-c22a95b2ad0c",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "7f0bd6ef-399a-4138-bbbe-236c03085e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "de0ee81c-fa30-4c22-9ade-afb72859472e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f0bd6ef-399a-4138-bbbe-236c03085e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d2fda5-3c7e-42fe-90f3-fc982d8b6ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f0bd6ef-399a-4138-bbbe-236c03085e1e",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "8c643961-a138-4558-9479-2311d2fae7bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "bab80f8b-798d-40ff-b66f-ec6adf7fd499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c643961-a138-4558-9479-2311d2fae7bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5c33693-2330-48b3-a677-f3bb6f7af62f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c643961-a138-4558-9479-2311d2fae7bf",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "f2299052-90cc-402c-b3b1-5fcf2bd6764a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "cec0cb39-8ce7-4870-a9bf-3a8b43f490a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2299052-90cc-402c-b3b1-5fcf2bd6764a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a586d0f-d1aa-494f-95ed-f6a664ef0a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2299052-90cc-402c-b3b1-5fcf2bd6764a",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "7a23e214-ab64-4a1e-80bf-f7d0001f959d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "8f427485-6bae-491b-af0e-489e66964c13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a23e214-ab64-4a1e-80bf-f7d0001f959d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc02cd58-b874-44c4-98c0-705632d69f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a23e214-ab64-4a1e-80bf-f7d0001f959d",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "ed0312a3-bc9b-4366-b1e0-af7c4e869f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "5ceb150b-94b1-44a8-b88e-8154598fa8e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed0312a3-bc9b-4366-b1e0-af7c4e869f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "841b7946-eb01-41b4-80ca-caeac19a53fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed0312a3-bc9b-4366-b1e0-af7c4e869f2f",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "710b4b47-f3bd-4af6-a82f-708df25d4ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "7f6fc870-29a1-4a7b-b828-bb28a1cacf8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710b4b47-f3bd-4af6-a82f-708df25d4ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bad15e3-813b-4721-b65e-b50c0783c715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710b4b47-f3bd-4af6-a82f-708df25d4ac5",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "78eafc5a-8441-4119-8e49-2ea7ba45ad85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "75d7a3bc-7fb7-44c9-8f51-8d43e1e8d1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78eafc5a-8441-4119-8e49-2ea7ba45ad85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc9998d-a6e6-4f4a-a550-7eededbfbb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78eafc5a-8441-4119-8e49-2ea7ba45ad85",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "8b06f6d0-bce5-444b-a0b8-7114e49ec057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "1d6ef9ba-fc2f-4fcf-aa78-5618b0bafc6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b06f6d0-bce5-444b-a0b8-7114e49ec057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcf41c2d-5055-49d5-bc0a-8cff49b4161c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b06f6d0-bce5-444b-a0b8-7114e49ec057",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "379178d8-59d1-4d28-ad00-5c6d89cf67a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "3270ca2d-e38e-4157-a74d-aab69337db13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "379178d8-59d1-4d28-ad00-5c6d89cf67a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c09dcdda-0a20-41ab-9268-9ed44d579113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379178d8-59d1-4d28-ad00-5c6d89cf67a5",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "f8011d90-051a-4c89-9bab-b4e7ebf0fc33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "04bf7d39-f7e6-424e-978a-fb0fb20deb7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8011d90-051a-4c89-9bab-b4e7ebf0fc33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41b4fafd-d6bf-47ec-98cd-ff63b3b6c377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8011d90-051a-4c89-9bab-b4e7ebf0fc33",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "cea482cb-28a3-465d-aaad-051c58a34f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "9589d14b-fdd5-47b5-b93a-de77d19e8bad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea482cb-28a3-465d-aaad-051c58a34f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73d0fb0-64c4-4475-a25a-bdfe89a906c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea482cb-28a3-465d-aaad-051c58a34f4b",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "e8d7d797-3201-4c0e-9d47-8e3b5a08dc9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "6580fef1-35ea-4844-9a08-d1d1db32fa22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d7d797-3201-4c0e-9d47-8e3b5a08dc9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7181604c-5492-44ff-8d59-982e566d2a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d7d797-3201-4c0e-9d47-8e3b5a08dc9d",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "f6b03efb-38ac-4eb6-8c54-c46f1a5e1675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "1c60ed30-5927-4374-876f-856cdbbc87be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6b03efb-38ac-4eb6-8c54-c46f1a5e1675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3a253d-6159-44c3-b3f4-841bec2d650c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6b03efb-38ac-4eb6-8c54-c46f1a5e1675",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "992a9bd1-28a4-477a-93b3-ea7b1ede25e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "40f57369-16b1-46e1-8e88-5f9f8e8fc1b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "992a9bd1-28a4-477a-93b3-ea7b1ede25e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c78b40-bf4a-4eb9-ac9a-554ad7cf30b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "992a9bd1-28a4-477a-93b3-ea7b1ede25e2",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "bc3d02c4-d7c1-4fbe-911c-cdaf44b65970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "dbe35dcc-03ec-47c6-91f2-3b76155a9287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc3d02c4-d7c1-4fbe-911c-cdaf44b65970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b2d377c-363f-4c72-9a81-bd869456d41c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc3d02c4-d7c1-4fbe-911c-cdaf44b65970",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "42434a18-a747-4711-96f5-65a206ece3d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "3554a0eb-8790-4abb-a2c4-01260af042b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42434a18-a747-4711-96f5-65a206ece3d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b8e46e4-94ac-4e30-9ea9-43c53cea4e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42434a18-a747-4711-96f5-65a206ece3d5",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "d4e9f26e-fd9d-404e-be3b-f5416093c366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "17c89f3c-f821-48e0-88b3-f5eab5885b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e9f26e-fd9d-404e-be3b-f5416093c366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68a9ffcd-7ca7-4a0e-804c-070041d9e476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e9f26e-fd9d-404e-be3b-f5416093c366",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "0a7ed199-d527-4c63-a07c-6cbdc6d29653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "8def0316-26e9-40ca-8eca-815cc8749e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a7ed199-d527-4c63-a07c-6cbdc6d29653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8f6413c-cf98-4b76-b4a5-a0515846d304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a7ed199-d527-4c63-a07c-6cbdc6d29653",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "9e2fee3b-d2aa-4a02-ae72-196f5c04ecee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "fe93df05-47cf-4e4d-a3a3-c3bf8f9988f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2fee3b-d2aa-4a02-ae72-196f5c04ecee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75414421-5ab8-43af-b6b7-e404de5e8179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2fee3b-d2aa-4a02-ae72-196f5c04ecee",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "da62b15f-cab0-4f89-ae31-9fd13084e1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "2ac4e451-dc5d-43a4-ac3d-3000541c093a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da62b15f-cab0-4f89-ae31-9fd13084e1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8615d93-929e-4065-b552-ebe2b297209f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da62b15f-cab0-4f89-ae31-9fd13084e1ea",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "bd3a2129-8d68-4e02-b5c0-e790fac31b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "5b0ae49d-8dec-4ea4-a26a-8ede7a9f0d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd3a2129-8d68-4e02-b5c0-e790fac31b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8bcec1d-77d9-4901-b024-a203e411eafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd3a2129-8d68-4e02-b5c0-e790fac31b22",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "5f635c20-bbe6-4549-9be3-74b8c6f25a85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "19970b82-ead4-4137-b611-1f3c0c552890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f635c20-bbe6-4549-9be3-74b8c6f25a85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "226620cf-7620-4ef3-b5a0-36d980fd4755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f635c20-bbe6-4549-9be3-74b8c6f25a85",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "3908fb24-2604-4ccb-ba4b-f8f4e6334a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "a2abe836-fb7e-4ec0-930e-2289992e1aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3908fb24-2604-4ccb-ba4b-f8f4e6334a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58faf2de-3b4c-4506-b6fc-149d6ab8412d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3908fb24-2604-4ccb-ba4b-f8f4e6334a12",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "b4c158b2-ec5d-4b25-8a36-0909657ca430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "9933c549-11dc-414e-a07b-8235d08c719a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c158b2-ec5d-4b25-8a36-0909657ca430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "223eb321-4c59-4d2d-9a26-7b25c642bfea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c158b2-ec5d-4b25-8a36-0909657ca430",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "e9484265-1fca-433d-9bbb-93d00451b688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "a458b62f-cd74-4be2-a4ea-51ba1f01f64c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9484265-1fca-433d-9bbb-93d00451b688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be257d83-cbb0-4b96-88ea-ece5a388071b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9484265-1fca-433d-9bbb-93d00451b688",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "3fa42921-cd40-46d0-81a9-01f1f4c42d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "a7d3e0fd-9c99-437a-9221-975d2659835c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa42921-cd40-46d0-81a9-01f1f4c42d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02640f1e-4401-4c6d-a285-20b81844491a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa42921-cd40-46d0-81a9-01f1f4c42d48",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "0292aedd-8d95-4daf-9c64-00d26bd0cce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "103eb9cc-8ace-4ba3-9300-40c7a51119f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0292aedd-8d95-4daf-9c64-00d26bd0cce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff14374a-9f45-4d56-bba2-8f97fd2338da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0292aedd-8d95-4daf-9c64-00d26bd0cce8",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "fbbc5019-84c9-48c9-8183-38019624d5b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "e9b6ef5c-9921-4e2a-bd56-c0f9a3ecff1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbbc5019-84c9-48c9-8183-38019624d5b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0392a63-a480-4d28-843a-532db777af6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbbc5019-84c9-48c9-8183-38019624d5b8",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "f64a4f3d-0101-4f51-b852-e8b642418d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "bc4df57d-f7a7-4107-9633-3175dddbef97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64a4f3d-0101-4f51-b852-e8b642418d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f438cd3a-ecdb-4342-b756-a6b3b83590c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64a4f3d-0101-4f51-b852-e8b642418d8d",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "06a13404-b53b-49f1-96f9-a18b9d4eb953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "39b63872-b34a-4eb7-9b04-420bcc75090b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06a13404-b53b-49f1-96f9-a18b9d4eb953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b1a264d-599d-4ece-893a-0167a8d19c6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06a13404-b53b-49f1-96f9-a18b9d4eb953",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        },
        {
            "id": "fc67e493-f457-4e3c-8a78-248812e71fd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "compositeImage": {
                "id": "982c0298-ca99-4a48-8b82-592d6b31b3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc67e493-f457-4e3c-8a78-248812e71fd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "544f3b85-18f8-4bb1-9d3c-7bf54a9aed2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc67e493-f457-4e3c-8a78-248812e71fd2",
                    "LayerId": "2fb74490-57a2-4806-8b63-ea31d051eff6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "2fb74490-57a2-4806-8b63-ea31d051eff6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": 47,
    "yorig": 47
}