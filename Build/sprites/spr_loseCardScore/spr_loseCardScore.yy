{
    "id": "c0f92f93-e387-44ac-8d3c-edc4f8b54d55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseCardScore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9410aff-8f42-4b58-8414-ea9fe5ae75ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f92f93-e387-44ac-8d3c-edc4f8b54d55",
            "compositeImage": {
                "id": "b74a1d5a-8b71-4c2b-85a2-1d27454661dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9410aff-8f42-4b58-8414-ea9fe5ae75ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35b7f44d-481a-432a-b95b-97798a74ae0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9410aff-8f42-4b58-8414-ea9fe5ae75ec",
                    "LayerId": "f3f7ccff-c81e-4ae8-835a-7699e8343c2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f3f7ccff-c81e-4ae8-835a-7699e8343c2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0f92f93-e387-44ac-8d3c-edc4f8b54d55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}