{
    "id": "d02ab7a5-8309-453b-a41f-350408944ccd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardClub1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f8d8ce8-a675-41ce-bb2b-cd747c938d1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d02ab7a5-8309-453b-a41f-350408944ccd",
            "compositeImage": {
                "id": "d464fd61-a068-4140-9911-3f17a59212a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8d8ce8-a675-41ce-bb2b-cd747c938d1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a61591ba-c8bf-45d6-9144-b97624326c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8d8ce8-a675-41ce-bb2b-cd747c938d1d",
                    "LayerId": "8468b526-4776-4af4-a503-2bc2395a08c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8468b526-4776-4af4-a503-2bc2395a08c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d02ab7a5-8309-453b-a41f-350408944ccd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}