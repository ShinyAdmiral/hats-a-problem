{
    "id": "9cff30ed-12e6-40a3-a920-f10ff7fb3445",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite43",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c42732d-0760-44e1-b5b2-90234061ddff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cff30ed-12e6-40a3-a920-f10ff7fb3445",
            "compositeImage": {
                "id": "b9cf4af4-c305-4c35-9dc0-f2c46f3c0b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c42732d-0760-44e1-b5b2-90234061ddff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9959855f-1659-4287-bcdb-19793c2be4f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c42732d-0760-44e1-b5b2-90234061ddff",
                    "LayerId": "c19ec156-39ce-4e6a-93b5-b56be4b88f25"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 6,
    "layers": [
        {
            "id": "c19ec156-39ce-4e6a-93b5-b56be4b88f25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cff30ed-12e6-40a3-a920-f10ff7fb3445",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 3
}