{
    "id": "457b1abe-4314-481e-9986-6d7530544cde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Snap1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 16,
    "bbox_right": 38,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "239a9048-2f63-4169-9fa8-a97b6b4ed5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "457b1abe-4314-481e-9986-6d7530544cde",
            "compositeImage": {
                "id": "78ea468d-6561-4744-b7de-02224a6a6a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "239a9048-2f63-4169-9fa8-a97b6b4ed5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e968cd94-4c63-43ad-a07d-b1f3e5f6af63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "239a9048-2f63-4169-9fa8-a97b6b4ed5fa",
                    "LayerId": "8cf5acbb-5812-4a03-8733-e9db219a0027"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8cf5acbb-5812-4a03-8733-e9db219a0027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "457b1abe-4314-481e-9986-6d7530544cde",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 24,
    "yorig": 24
}