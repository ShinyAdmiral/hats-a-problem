{
    "id": "57d375b0-9b27-452f-9fd1-8d851b1fd023",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardClub",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "745c0ca3-b5fc-4fbb-93dd-7f82a170faf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d375b0-9b27-452f-9fd1-8d851b1fd023",
            "compositeImage": {
                "id": "37914fd0-0c26-47f4-a651-dbe547105717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "745c0ca3-b5fc-4fbb-93dd-7f82a170faf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e518bf9c-e1db-454f-9bf7-22ba327c8fd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "745c0ca3-b5fc-4fbb-93dd-7f82a170faf3",
                    "LayerId": "32c96d39-8646-48c8-8926-09c336e737d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "32c96d39-8646-48c8-8926-09c336e737d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57d375b0-9b27-452f-9fd1-8d851b1fd023",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 6
}