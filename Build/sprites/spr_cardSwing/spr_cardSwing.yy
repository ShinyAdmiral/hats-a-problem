{
    "id": "62e2e793-9284-48ba-a5f2-7871a4af2ed6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardSwing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a10bfc83-4cf9-425e-916f-2707d5148afd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62e2e793-9284-48ba-a5f2-7871a4af2ed6",
            "compositeImage": {
                "id": "8d7d417f-f883-4a81-a27c-83d71e1ca5aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a10bfc83-4cf9-425e-916f-2707d5148afd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e15177e6-913d-4fd9-b4ec-6677c0d8083e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a10bfc83-4cf9-425e-916f-2707d5148afd",
                    "LayerId": "d919f4bf-99ed-4ee8-870e-c7ffdd06efb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d919f4bf-99ed-4ee8-870e-c7ffdd06efb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62e2e793-9284-48ba-a5f2-7871a4af2ed6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}