{
    "id": "e0114301-42a1-46fa-b083-b52d3d9e35d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardFireBlastPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "511cfe5c-58b7-45e0-98ac-7d26bafed272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0114301-42a1-46fa-b083-b52d3d9e35d7",
            "compositeImage": {
                "id": "a1a1a0bf-a276-44b7-8698-33f0945f25b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511cfe5c-58b7-45e0-98ac-7d26bafed272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5aba6f7-da8f-427f-a77b-e735fb37160d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511cfe5c-58b7-45e0-98ac-7d26bafed272",
                    "LayerId": "c91c564d-f4e6-400e-9864-7d9c08daec3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c91c564d-f4e6-400e-9864-7d9c08daec3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0114301-42a1-46fa-b083-b52d3d9e35d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}