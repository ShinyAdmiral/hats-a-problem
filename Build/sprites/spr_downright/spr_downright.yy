{
    "id": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_downright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f54139a0-5db2-4f0f-92a1-bcbd6dd14ba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "dbe9d41c-b451-42be-aa0a-87f8a32fb95a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54139a0-5db2-4f0f-92a1-bcbd6dd14ba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ecf54dc-c152-4194-9191-78a469c1644f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54139a0-5db2-4f0f-92a1-bcbd6dd14ba8",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        },
        {
            "id": "b477f773-883b-4079-8867-323f256b8c5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "d30abe0b-7cc9-4033-b22a-841b7f2cd80e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b477f773-883b-4079-8867-323f256b8c5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5546b8fe-6f30-4704-9940-c0d17df96ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b477f773-883b-4079-8867-323f256b8c5d",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        },
        {
            "id": "b829bca5-a663-47b5-83ef-f707422f5d3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "dbc0bc89-6b78-4253-a898-4cdcaffb79bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b829bca5-a663-47b5-83ef-f707422f5d3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9aa1942-da71-4f4b-95a2-d1d823fa7044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b829bca5-a663-47b5-83ef-f707422f5d3f",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        },
        {
            "id": "9e53bd0a-9b83-4b7b-99ea-420005ae7e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "5a6d476e-5219-43e8-8fa4-efb52f2fb2ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e53bd0a-9b83-4b7b-99ea-420005ae7e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0c2a443-16a0-41b0-b9af-2f486292c0c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e53bd0a-9b83-4b7b-99ea-420005ae7e77",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        },
        {
            "id": "c3f8b1fe-cfe7-4130-a902-7a273663d76a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "56a7f2e7-6f26-4ea4-a5d4-05d490d35957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f8b1fe-cfe7-4130-a902-7a273663d76a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289a2521-e04c-42f9-990b-056881976b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f8b1fe-cfe7-4130-a902-7a273663d76a",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        },
        {
            "id": "3eb7acaa-ca27-45f2-b1b5-109d4a6a91b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "50df05f7-58ca-4831-b2d6-1fc5088078fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eb7acaa-ca27-45f2-b1b5-109d4a6a91b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b3ff8f-3987-480a-83da-9ff38cb63e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eb7acaa-ca27-45f2-b1b5-109d4a6a91b0",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        },
        {
            "id": "14ddfb45-a442-48d3-877f-d480b712b006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "6c8e1626-fcb4-49a1-92ca-60bfc1120330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ddfb45-a442-48d3-877f-d480b712b006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c5ae430-66cb-45f1-a958-d1e49390823f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ddfb45-a442-48d3-877f-d480b712b006",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        },
        {
            "id": "fa59e73f-1aea-4834-80bd-357aa176911a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "compositeImage": {
                "id": "d3f2f38a-d425-49a9-b159-21e7fc717d85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa59e73f-1aea-4834-80bd-357aa176911a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a836687-15ca-4e01-bf47-eca51571b68c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa59e73f-1aea-4834-80bd-357aa176911a",
                    "LayerId": "29644046-0fc1-4fe8-92df-94530fd2ec82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "29644046-0fc1-4fe8-92df-94530fd2ec82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6108c297-3c19-40ac-8d2e-48b988b0aa41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}