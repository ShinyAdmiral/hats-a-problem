{
    "id": "6bfd22f7-a79e-4f34-9b0f-d4423ef846b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heartBad1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 4,
    "bbox_right": 16,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51f14ed7-245b-4e45-ae48-878ff7a83666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bfd22f7-a79e-4f34-9b0f-d4423ef846b6",
            "compositeImage": {
                "id": "e200fd9e-329d-45f7-9f0c-7e00664dab80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f14ed7-245b-4e45-ae48-878ff7a83666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a698b1dd-deba-4877-8db0-10fb0d28e20f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f14ed7-245b-4e45-ae48-878ff7a83666",
                    "LayerId": "a3f496ff-ed3e-44ab-9c42-ba57de03636a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "a3f496ff-ed3e-44ab-9c42-ba57de03636a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bfd22f7-a79e-4f34-9b0f-d4423ef846b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 10,
    "yorig": 10
}