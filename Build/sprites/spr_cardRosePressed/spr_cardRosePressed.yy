{
    "id": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardRosePressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d093c345-6f68-4145-82f9-0171305efbbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "015380ef-f18f-4813-99da-83f757b6a01d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d093c345-6f68-4145-82f9-0171305efbbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1cc130-9598-4f38-8f63-420bbb2347e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d093c345-6f68-4145-82f9-0171305efbbb",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "e1642948-59f2-4269-ba4b-2849f6c96e8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "cfd21c47-9a5f-4262-8c7e-59872358f930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1642948-59f2-4269-ba4b-2849f6c96e8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a807455-3bc2-4df7-b3b9-a2c930160cb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1642948-59f2-4269-ba4b-2849f6c96e8f",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "ca1069d8-5747-40ea-8750-be7a12cdbc14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "4dfc86ec-f57f-4375-a0f3-c50850120f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca1069d8-5747-40ea-8750-be7a12cdbc14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207a705e-e3b8-4c73-bc03-014727486401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca1069d8-5747-40ea-8750-be7a12cdbc14",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "d86ea136-96eb-4c5c-acc6-3ed815958180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "a6a4904c-dec1-42da-aedd-9a9645c825c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d86ea136-96eb-4c5c-acc6-3ed815958180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e3d91bd-b472-4890-b0f7-25a49ec69b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d86ea136-96eb-4c5c-acc6-3ed815958180",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "a62d3e32-03fa-44d1-8207-2d73210f6e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "d435dcc2-c9e2-472c-9582-ff6bddab9f54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a62d3e32-03fa-44d1-8207-2d73210f6e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b3ce65f-8015-49e8-bcf4-f02c7d8cfca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a62d3e32-03fa-44d1-8207-2d73210f6e86",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "49ad3dc2-1e2a-4845-ac04-4d5828b16af1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "907e1e18-fd25-40ed-be5a-95d7e17253f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ad3dc2-1e2a-4845-ac04-4d5828b16af1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5653e7a3-19fe-4566-aec2-3a19885e7c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ad3dc2-1e2a-4845-ac04-4d5828b16af1",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "a0be0b00-c39c-460f-ae18-910770f6fdb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "584ad776-30fc-4539-9523-342e8d2b98f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0be0b00-c39c-460f-ae18-910770f6fdb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95ff756d-48a3-4662-a40f-0056bdd4d561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0be0b00-c39c-460f-ae18-910770f6fdb0",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "91599dd2-d105-4b98-877e-e6a82c7b692b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "6a619490-53bf-4143-9422-4a6c27a9d01b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91599dd2-d105-4b98-877e-e6a82c7b692b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97105165-a168-431a-9a6f-58b9629eaedd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91599dd2-d105-4b98-877e-e6a82c7b692b",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "33300a5a-ee00-4a42-8397-d005a46abf0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "337ac22c-ce02-47c0-b2ed-75d86c93fbc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33300a5a-ee00-4a42-8397-d005a46abf0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af098f44-a45e-4ac1-a5b0-6f0d1949ba3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33300a5a-ee00-4a42-8397-d005a46abf0d",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "ef7c3875-b344-467e-8e88-c541a1010770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "8633d6af-ef0f-4271-9286-a8b70318a39c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef7c3875-b344-467e-8e88-c541a1010770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32144870-3a74-44d3-9eef-b3facb73b4f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef7c3875-b344-467e-8e88-c541a1010770",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "be0eff26-00da-45e7-ad64-368fd3d81fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "eb39c742-4452-45c8-8653-30bdb9670aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be0eff26-00da-45e7-ad64-368fd3d81fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7acfc93e-275d-40df-b280-a7211a929c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be0eff26-00da-45e7-ad64-368fd3d81fee",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "b9cb3c98-473b-4ab6-b61c-d3f4bde49775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "6285c1f0-e63d-44f8-a74e-cb20c16dadee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9cb3c98-473b-4ab6-b61c-d3f4bde49775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b49e2da-a6a8-4175-ab80-a99233fe831e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9cb3c98-473b-4ab6-b61c-d3f4bde49775",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "b815be88-316c-4e48-9796-ca8a6c2225d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "4ae92e07-b700-4bba-b5d7-3f42596e2776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b815be88-316c-4e48-9796-ca8a6c2225d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47546dca-58c9-4cb0-9ee9-36571c56484e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b815be88-316c-4e48-9796-ca8a6c2225d6",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "9ac3135a-ddb2-42d3-8465-8101dfa08e1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "a5245d1c-0411-42c8-bc0b-710cdc3cce25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac3135a-ddb2-42d3-8465-8101dfa08e1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97a09e12-bb45-40ee-817a-fab38487986c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac3135a-ddb2-42d3-8465-8101dfa08e1d",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "cfd80d53-e8df-4c33-8a4c-a505da0bcb5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "8792c65d-d165-4db4-8749-02dffc30bb66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfd80d53-e8df-4c33-8a4c-a505da0bcb5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08f5dc6-52b1-4f0a-977e-7b3047db1c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfd80d53-e8df-4c33-8a4c-a505da0bcb5e",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "c99da4ed-ecbc-4013-886d-15bbc7670408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "3a6568a2-6dee-4800-b9eb-3ac51c3f9b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c99da4ed-ecbc-4013-886d-15bbc7670408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dab27c6-7884-45b1-bf13-07a70c343c08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c99da4ed-ecbc-4013-886d-15bbc7670408",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "19a11a3f-97e8-4d78-9f99-27197b8b7364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "78f1deb6-e2ad-42a8-a5fe-bdaa5bb9c3ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a11a3f-97e8-4d78-9f99-27197b8b7364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd91bb3a-26d4-4e7b-bec3-f54c7f009132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a11a3f-97e8-4d78-9f99-27197b8b7364",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "9800f717-6444-4a85-82c4-02e0ca3166ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "64e922b6-e96e-4fa2-97d9-3a131acd5265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9800f717-6444-4a85-82c4-02e0ca3166ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d112cde5-5885-43d4-b2b7-18a3d17d450e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9800f717-6444-4a85-82c4-02e0ca3166ae",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "331cf135-5e02-44db-a908-fa7915bc3625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "0b6b80fb-9f1e-41e9-a6fb-fb76df6d506b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "331cf135-5e02-44db-a908-fa7915bc3625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4db8fb5-cb31-4d0a-a445-ed99a68f253a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "331cf135-5e02-44db-a908-fa7915bc3625",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "0933e278-1a29-409f-915a-d008ba7ede08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "b67cd4f1-989d-4367-af6a-2dc32e5c850d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0933e278-1a29-409f-915a-d008ba7ede08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db652a0-8164-420e-a6b3-35c60b7e628a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0933e278-1a29-409f-915a-d008ba7ede08",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "af38fa54-5411-4c88-bae6-607e320cec35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "33604277-357f-45bf-9d18-26800bb56afb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af38fa54-5411-4c88-bae6-607e320cec35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f36f655-b295-4359-9324-f0c8650028c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af38fa54-5411-4c88-bae6-607e320cec35",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "3f8174f2-2229-42fa-a481-33c3c5c935f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "880714f8-c660-48a6-9d6f-de620cdc1d87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f8174f2-2229-42fa-a481-33c3c5c935f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81dc6c9c-b210-46ed-bae3-b87919bdb2bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f8174f2-2229-42fa-a481-33c3c5c935f5",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "01fd83dc-8519-4be3-b1b4-e8a9bb8c9b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "f25e9f5e-be9a-4b46-96fe-619e832b974a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fd83dc-8519-4be3-b1b4-e8a9bb8c9b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9846a8f6-eff8-4ff2-86ed-d1975fc7a681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fd83dc-8519-4be3-b1b4-e8a9bb8c9b6d",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "23dde275-2ae4-4c36-aff7-ddaa7b2e2a62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "cc509d7c-777f-4a56-a35b-afc5f9c31e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23dde275-2ae4-4c36-aff7-ddaa7b2e2a62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52f4e70c-6eee-4581-9572-af4d2aacf30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23dde275-2ae4-4c36-aff7-ddaa7b2e2a62",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "b0b4a582-2c8d-435b-b77c-755fc60997c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "17e63676-5067-476d-8848-f7dab73ada89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b4a582-2c8d-435b-b77c-755fc60997c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b71eec49-247b-43a2-b99d-795076563962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b4a582-2c8d-435b-b77c-755fc60997c8",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "5f8e0640-e958-4b62-8360-dc0106cc671e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "2960779b-7ade-4067-8144-3b2f412fc814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f8e0640-e958-4b62-8360-dc0106cc671e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a16f491d-bd84-4f8c-9d32-d710105ad7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f8e0640-e958-4b62-8360-dc0106cc671e",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "56623172-b9a0-4804-8c92-df2ea48b002d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "2a9502b7-8e1c-4279-a08a-1e9c4a27a68f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56623172-b9a0-4804-8c92-df2ea48b002d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f7e6907-c223-421e-a48e-01e2afaa54fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56623172-b9a0-4804-8c92-df2ea48b002d",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "216a050a-7ed2-4b4b-9501-7a72f71b721d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "e0f57b80-2faf-4094-ab2a-22aa07fe3c48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "216a050a-7ed2-4b4b-9501-7a72f71b721d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838222b2-f4cd-46cc-8217-ad8ac1160b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "216a050a-7ed2-4b4b-9501-7a72f71b721d",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "935f0cea-d4ae-4115-a809-52a4de901c2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "402576bf-c4a0-4c77-83e4-57f2e1b9c89d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935f0cea-d4ae-4115-a809-52a4de901c2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8abbf9e3-5d0a-4e55-8664-1e7cf1b6db16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935f0cea-d4ae-4115-a809-52a4de901c2b",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "9edf42ef-0090-42a3-95ff-c938cf87991b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "797e59ca-2deb-485e-803a-f9b77d90c207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9edf42ef-0090-42a3-95ff-c938cf87991b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c68638c-9e99-4ee6-9e3d-4c21c215e113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9edf42ef-0090-42a3-95ff-c938cf87991b",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "b05959b9-4892-4322-94c0-d3f537716514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "8a5f228f-cf75-4539-8701-0d2d90ab5182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05959b9-4892-4322-94c0-d3f537716514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d761443a-ad3b-47db-ab94-8ac6d7d9b802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05959b9-4892-4322-94c0-d3f537716514",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "840dc76b-4bb3-401b-b24c-8920fc80d2b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "179fec52-fd92-4950-b020-a9f449414dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "840dc76b-4bb3-401b-b24c-8920fc80d2b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033e8f11-bdc5-4dff-b8e6-1c08ed949b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "840dc76b-4bb3-401b-b24c-8920fc80d2b4",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "6dd88cdc-272d-46e5-b78d-1582dc657a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "d10f4143-5af3-4f64-a3fe-401f552f65c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd88cdc-272d-46e5-b78d-1582dc657a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0baffbf5-c7ef-4309-b398-fb3371f81724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd88cdc-272d-46e5-b78d-1582dc657a1e",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "ac1ab432-2c54-46c6-8067-7fdfecb94ac4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "666f41ff-3e7b-4ac2-ab99-13a3d677112c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac1ab432-2c54-46c6-8067-7fdfecb94ac4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1ddf4f9-3315-4dd5-8926-4492eb0b5d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1ab432-2c54-46c6-8067-7fdfecb94ac4",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "cbccc780-f3a3-4482-b558-2b8ce7ab2b85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "f983ce9b-acd5-498b-806b-bef5f3d17aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbccc780-f3a3-4482-b558-2b8ce7ab2b85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2346952-60cd-428c-b4e8-0fc4fee6ae16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbccc780-f3a3-4482-b558-2b8ce7ab2b85",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "b26f4e96-0971-4585-8536-7a43f73ff879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "3c8606f7-6df3-4561-8986-eb545d417ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b26f4e96-0971-4585-8536-7a43f73ff879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d463d3e-eb96-4845-b8dd-25c9e7ebf05e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b26f4e96-0971-4585-8536-7a43f73ff879",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "f0d017b7-d68c-4f0d-9803-a50ca7da2ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "8cd6dade-c4e9-4aa8-8095-9b1ece7f81f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d017b7-d68c-4f0d-9803-a50ca7da2ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4a6d50b-ddb0-44e1-83ca-1287a6ae036a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d017b7-d68c-4f0d-9803-a50ca7da2ad3",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "ffb9be7f-8a53-43a7-bbf3-abed644dc871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "4c761ef4-b475-44c4-8225-5c332932b358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb9be7f-8a53-43a7-bbf3-abed644dc871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0cc104-98d0-4809-af90-8d684e682716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb9be7f-8a53-43a7-bbf3-abed644dc871",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "64a46dba-0b91-404d-8d6a-ccfcc1b8e348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "1a46600a-aa07-4333-a878-fc9b83982590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a46dba-0b91-404d-8d6a-ccfcc1b8e348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8f85328-9149-4b76-a0b4-a15c762e7686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a46dba-0b91-404d-8d6a-ccfcc1b8e348",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        },
        {
            "id": "31764f71-8dd3-4dfd-a3ec-95c6a18ce73b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "compositeImage": {
                "id": "81cc1534-ac98-4398-93d2-78ee118320f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31764f71-8dd3-4dfd-a3ec-95c6a18ce73b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "697742c6-7201-4414-87f3-9cea26276bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31764f71-8dd3-4dfd-a3ec-95c6a18ce73b",
                    "LayerId": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "bb4ab497-9670-4f3a-bf39-0b64fe2b92a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "764f99b7-9671-4e6d-956d-2e03084bf3a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}