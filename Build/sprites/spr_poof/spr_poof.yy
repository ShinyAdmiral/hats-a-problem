{
    "id": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_poof",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 2,
    "bbox_right": 77,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b5712d3-8a17-481b-a3bb-90094eb3e065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "a93be37a-e1e0-4f68-9343-75bc772ab51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5712d3-8a17-481b-a3bb-90094eb3e065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c940d25b-ee6e-43f6-a566-84bb6e967973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5712d3-8a17-481b-a3bb-90094eb3e065",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "2fb4f2dc-9b27-4215-a726-cd996fd2fb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "23df79c1-da17-4ff5-8966-c16306381a2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb4f2dc-9b27-4215-a726-cd996fd2fb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "972d2e62-e19b-41e3-9122-4890d6fa8c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb4f2dc-9b27-4215-a726-cd996fd2fb2c",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "567e6553-58d4-4c74-9867-aee524a627d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "991f8b47-6adf-4fec-9f47-a74370aaed74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567e6553-58d4-4c74-9867-aee524a627d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7daca2-8f7e-4c16-b2c1-b1f9b1d12274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567e6553-58d4-4c74-9867-aee524a627d0",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "323a14df-5881-495b-853f-2ff2e964896b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "985b21b0-95e0-4a94-8637-b1b62c6117fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "323a14df-5881-495b-853f-2ff2e964896b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab642abc-a8b9-4947-886d-a87ad91470fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "323a14df-5881-495b-853f-2ff2e964896b",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "40bf9b56-117d-46e3-98ad-e310faa69dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "725b3d36-08a7-44de-8303-b13cec863591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40bf9b56-117d-46e3-98ad-e310faa69dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09483123-766f-4570-9ebe-542502ecfab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40bf9b56-117d-46e3-98ad-e310faa69dbe",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "e8399249-97f6-4d2f-8265-f5f6eaec6583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "5182ebc2-1d75-4ec2-ae57-7f66854b0d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8399249-97f6-4d2f-8265-f5f6eaec6583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c11077c-24f1-4274-9053-b4e326b0fd56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8399249-97f6-4d2f-8265-f5f6eaec6583",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "d4300d78-9d31-41fa-bc0f-36f99e554833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "d31e7a3f-f116-43f5-9bcf-a4685cfc300b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4300d78-9d31-41fa-bc0f-36f99e554833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b94909ea-290f-4e6e-8a18-0fd328ccf5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4300d78-9d31-41fa-bc0f-36f99e554833",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "9fd883e1-b52e-41d2-a069-de190f1a6710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "982e64d2-b403-4a64-8c88-24447a1b4a2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd883e1-b52e-41d2-a069-de190f1a6710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a19caf-cfe6-44c3-896a-84c173143448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd883e1-b52e-41d2-a069-de190f1a6710",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "6eb94602-b0dc-4b63-86cd-e1cf8c35d20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "259e2d5c-ac8f-4fed-a434-b09282879091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb94602-b0dc-4b63-86cd-e1cf8c35d20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9efd773a-e486-42db-b334-a1d92fee4f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb94602-b0dc-4b63-86cd-e1cf8c35d20a",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        },
        {
            "id": "0bd28515-2822-4c3b-821f-60b5b740ebd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "compositeImage": {
                "id": "513390cf-e085-4e92-a890-55071af4b4ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd28515-2822-4c3b-821f-60b5b740ebd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb1a13f-25e4-4a74-94be-ceff90c250c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd28515-2822-4c3b-821f-60b5b740ebd4",
                    "LayerId": "e80003b1-8c63-4dc4-a15d-3de09553e7ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "e80003b1-8c63-4dc4-a15d-3de09553e7ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 57
}