{
    "id": "263f9083-ba1e-4f28-a703-a86eda089a32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ControlsPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c158f705-f334-4bde-83cc-34bfb6a9b514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "a70a9af6-c42c-4959-bf4d-e40d527fbf5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c158f705-f334-4bde-83cc-34bfb6a9b514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3088cbc8-1b96-4ce3-bf43-5ef7274be43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c158f705-f334-4bde-83cc-34bfb6a9b514",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "e9f92342-5474-4740-af6c-72c87af33f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "aaf2209c-f60f-404b-a7cb-d1b0d20ffba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f92342-5474-4740-af6c-72c87af33f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdcda465-e327-4c90-b3ee-a01b6765e53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f92342-5474-4740-af6c-72c87af33f11",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "fa0ee32f-f540-45e0-8087-bf74d6cc8ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "c12bb0db-9ced-47bf-a8d9-df1efc7830af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0ee32f-f540-45e0-8087-bf74d6cc8ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d8ee5d-dae5-4674-ae52-727f918a277f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0ee32f-f540-45e0-8087-bf74d6cc8ff1",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "c4c74bfe-9c74-4acf-9c55-a5b707d2c5d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "e7026d12-929c-4b50-8a7c-ac5579aab90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4c74bfe-9c74-4acf-9c55-a5b707d2c5d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e5175f-e0ae-436a-8926-eb648c499b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4c74bfe-9c74-4acf-9c55-a5b707d2c5d1",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "7a37632f-dce7-46a0-a07f-3b91edfc854a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "991e7c09-da4b-46bc-bfdf-f083b6811338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a37632f-dce7-46a0-a07f-3b91edfc854a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35636bd-20c7-4674-b439-62afff1e0cd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a37632f-dce7-46a0-a07f-3b91edfc854a",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "8ac83c7e-41e6-4405-b0f8-5b1cff2711d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "f3e0e0ae-0ff5-4a90-a3a1-8b2e65fdf02b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac83c7e-41e6-4405-b0f8-5b1cff2711d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ae85ce0-3184-4b64-b3d9-54ef3545952f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac83c7e-41e6-4405-b0f8-5b1cff2711d4",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "b1c1fbda-840b-45b3-85e4-5244b664f408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "04ac9151-9d95-4b5d-859d-24e5fdf53673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c1fbda-840b-45b3-85e4-5244b664f408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bdc0b09-f0fe-42b4-bdd3-f317189f1cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c1fbda-840b-45b3-85e4-5244b664f408",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "cb986666-5dd3-4e3f-ab2d-a83dabacdc5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "095b1b14-e290-4eb1-97af-a88030c19235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb986666-5dd3-4e3f-ab2d-a83dabacdc5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fae56698-a03a-478d-9034-d8d7a67f23df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb986666-5dd3-4e3f-ab2d-a83dabacdc5c",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "6d8285a9-ef2f-41aa-a658-e10bd02986b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "de3ab89d-b8f4-4c9f-b404-f08c907110fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d8285a9-ef2f-41aa-a658-e10bd02986b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129cd7bf-17d7-47d6-a9b7-6554a5ed06c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d8285a9-ef2f-41aa-a658-e10bd02986b5",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "5dfd8ecb-c0e8-48ff-9785-ca397736b023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "81690796-73b5-4236-b8d7-b9055159e7d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dfd8ecb-c0e8-48ff-9785-ca397736b023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17ecc4eb-9b1f-43bb-855b-3eec0fec7348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dfd8ecb-c0e8-48ff-9785-ca397736b023",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "a3f4b6c9-225c-460d-90aa-df9f7c0c4af0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "5f16c551-33b8-4041-8abd-161e9141e373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f4b6c9-225c-460d-90aa-df9f7c0c4af0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13acacb4-691b-4fc4-a9bd-5da6a5a784d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f4b6c9-225c-460d-90aa-df9f7c0c4af0",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "4baf3fdb-7a1b-45ee-bd00-cf3c7cad0d28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "f5cdaa01-25fa-448b-879c-a79114c015c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4baf3fdb-7a1b-45ee-bd00-cf3c7cad0d28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e7649c8-15d5-4d61-90f3-2e31d66a275b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4baf3fdb-7a1b-45ee-bd00-cf3c7cad0d28",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "a8e1b0de-d81f-45b1-82a3-c0f44184120a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "4bc16b7e-da61-4348-929d-d2a1f7dc7a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e1b0de-d81f-45b1-82a3-c0f44184120a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa7f80e-7fe6-4c00-a890-c9bd2292512e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e1b0de-d81f-45b1-82a3-c0f44184120a",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "edda273f-4b03-4f3e-8d4e-ecfa4857fc9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "1761737b-958b-43ed-bf93-977157eb4704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edda273f-4b03-4f3e-8d4e-ecfa4857fc9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7259f784-7078-4945-833c-f601f0d49664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edda273f-4b03-4f3e-8d4e-ecfa4857fc9c",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "c798a14f-b993-4047-ae88-fc879c3677f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "70c34e39-894f-405f-9a57-a8d188ec0568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c798a14f-b993-4047-ae88-fc879c3677f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5989a497-3f28-4717-a297-357eadd6e8dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c798a14f-b993-4047-ae88-fc879c3677f1",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "077c7649-fcbd-4c25-8e20-f5b282a6d8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "1b01fd7e-c85a-4d46-94a2-853bf3bfafcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "077c7649-fcbd-4c25-8e20-f5b282a6d8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "492ad9ef-9f33-45a4-a90e-9d4f5fddf38c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "077c7649-fcbd-4c25-8e20-f5b282a6d8d4",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "49f03036-3428-4bed-b6b4-2e6f0c867746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "1809282d-8478-4a0d-861e-c311c88dafc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49f03036-3428-4bed-b6b4-2e6f0c867746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "516a4d7e-fcd2-4694-bd66-d598eb1a3654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f03036-3428-4bed-b6b4-2e6f0c867746",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "035b2a54-b95a-46e2-821e-24bc285a63bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "a8e766c0-0689-4d21-9b27-e2c58da00a4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035b2a54-b95a-46e2-821e-24bc285a63bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "735de517-ed3e-4981-8d5e-45d174d67976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035b2a54-b95a-46e2-821e-24bc285a63bf",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "86b28fa5-b898-44ca-a20d-f0b785eced10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "bfa77fcd-c99e-4e70-a639-970f709c3ee6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b28fa5-b898-44ca-a20d-f0b785eced10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70209d45-d4e5-4c6e-a731-af2f57ec8ef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b28fa5-b898-44ca-a20d-f0b785eced10",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "48a92fe6-e76c-4965-9d4f-bd2573dfc631",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "2fdc8887-44f3-4232-9845-b79ff5f46340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a92fe6-e76c-4965-9d4f-bd2573dfc631",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ba1f79-67e1-4457-bd03-c5c243cbeb03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a92fe6-e76c-4965-9d4f-bd2573dfc631",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "786dff6a-3559-4279-b617-22ec395fe9be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "202f1413-a9b0-482f-a2b2-31008684d70c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "786dff6a-3559-4279-b617-22ec395fe9be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0bf2c8a-d0cd-4d01-bf9d-2153b6a1efb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "786dff6a-3559-4279-b617-22ec395fe9be",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "2a69ffb2-c9ea-4b21-b43b-1448f726a51a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "5c7a0e09-9e17-419a-a4a7-d39403be1c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a69ffb2-c9ea-4b21-b43b-1448f726a51a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4e9e068-5013-4440-8fcc-a27c2ca88667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a69ffb2-c9ea-4b21-b43b-1448f726a51a",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "42f20faa-d1d9-4908-b3e2-63d976ed9535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "676836bc-7cc1-4801-90cb-ad000a441ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f20faa-d1d9-4908-b3e2-63d976ed9535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a32177b-e391-489e-b1b5-0103847e6f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f20faa-d1d9-4908-b3e2-63d976ed9535",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "92644f10-b57d-441e-b44b-d1c15b471de2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "8abba663-4f34-46b9-a72d-209ee8393ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92644f10-b57d-441e-b44b-d1c15b471de2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c97b675-daf4-4e2e-995b-6192c50bc0f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92644f10-b57d-441e-b44b-d1c15b471de2",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "37605bd3-ce48-4b85-a693-9d0c45abb182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "cfd4db08-7dc9-47b7-a462-7feff6783713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37605bd3-ce48-4b85-a693-9d0c45abb182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee302e8-183a-4ebc-be4a-ecda25832af5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37605bd3-ce48-4b85-a693-9d0c45abb182",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "8562423d-b725-4b02-8f7d-1409d116fc4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "608d1cb8-1bff-442b-ac1d-f98efc06ace0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8562423d-b725-4b02-8f7d-1409d116fc4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25760c46-ee3e-473b-bc63-6dda0e6cfaaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8562423d-b725-4b02-8f7d-1409d116fc4a",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "5498d18d-4389-47b4-81f8-6d79aaf2ae9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "3027764c-afdb-4105-9172-591083f8ab49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5498d18d-4389-47b4-81f8-6d79aaf2ae9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3758c505-2e29-45f1-addc-70dd0c7e4749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5498d18d-4389-47b4-81f8-6d79aaf2ae9c",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "19d10df0-89dc-4feb-8e0a-d4275eda0fe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "e26e344b-4531-4df1-86f4-4dd22c1632ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19d10df0-89dc-4feb-8e0a-d4275eda0fe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8991186-87d1-406e-8c5b-315541014120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19d10df0-89dc-4feb-8e0a-d4275eda0fe0",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "bcb59ccc-bc60-4e9a-8f9b-4e65072e66ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "f17b9647-0b51-46ef-a258-5cd012d38a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcb59ccc-bc60-4e9a-8f9b-4e65072e66ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a00c63d-832a-4670-8d1b-4e716cdad66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcb59ccc-bc60-4e9a-8f9b-4e65072e66ee",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "98d461b2-e77c-4fa3-a4e2-c8ebcf7a1a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "c585c86b-6f0c-4288-9a62-9d0100f9ca62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d461b2-e77c-4fa3-a4e2-c8ebcf7a1a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f1f5b6e-f10d-4dd8-a5a3-6c4a70042575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d461b2-e77c-4fa3-a4e2-c8ebcf7a1a6c",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "33d6170b-e7ef-4a5b-8480-59948fa966d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "df36a2e3-3502-4385-894e-48223109bac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d6170b-e7ef-4a5b-8480-59948fa966d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38c5bc03-1e4d-4f97-a2fd-243853dee35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d6170b-e7ef-4a5b-8480-59948fa966d2",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "67f70754-0e67-4637-b58d-7df8f0d2be0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "d121c2aa-4109-401e-94e3-ab66684c07e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67f70754-0e67-4637-b58d-7df8f0d2be0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daee6f13-d55e-43df-9d03-d7c55fdf0eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67f70754-0e67-4637-b58d-7df8f0d2be0d",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "b6a4f30f-1fdb-4151-b7ec-a7538446c56e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "52521a42-27fc-41be-aeab-72904bf2e2fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6a4f30f-1fdb-4151-b7ec-a7538446c56e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b248aa-cc1b-4352-9138-41a25930d50f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a4f30f-1fdb-4151-b7ec-a7538446c56e",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "1b9c07af-c3a0-495e-94a4-407303f2d5c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "83a671ae-a1a5-43c7-854c-d3ebc2fe2dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b9c07af-c3a0-495e-94a4-407303f2d5c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bff3a5e-5002-42cb-b54b-3aba268d5a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b9c07af-c3a0-495e-94a4-407303f2d5c8",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "3bd0172b-2f23-44ce-a55c-017179b2eee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "3750becd-d720-42de-8f0f-d250bc90f64b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd0172b-2f23-44ce-a55c-017179b2eee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05881965-3c48-4144-9aa4-8b778ec638a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd0172b-2f23-44ce-a55c-017179b2eee6",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "182845e8-eb23-4016-87e6-3ceae77092cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "f770e624-831e-4d2d-acba-f94734d28ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "182845e8-eb23-4016-87e6-3ceae77092cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bd15ad9-1b57-4f91-b2fa-6a1627152059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "182845e8-eb23-4016-87e6-3ceae77092cb",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "980b3260-f078-4700-9299-0394971d3182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "4c88e9a4-de91-4ada-9540-21723335fa98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "980b3260-f078-4700-9299-0394971d3182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fcf646c-0062-456d-8e57-a056e1f747ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "980b3260-f078-4700-9299-0394971d3182",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "cfb8f567-9cd8-4fb2-a2e2-65c88fbfffbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "50b3310e-3443-4a95-bf71-5c61bf632cfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb8f567-9cd8-4fb2-a2e2-65c88fbfffbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f009406-5aea-4e5e-b076-e22b36e2cd18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb8f567-9cd8-4fb2-a2e2-65c88fbfffbc",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "667ea799-abfa-47bb-867c-89108890ebe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "7cdb2a31-b617-45e2-a28a-07f13ed4bf6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "667ea799-abfa-47bb-867c-89108890ebe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f209a7-18a3-4217-823f-4d9bed39dd5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "667ea799-abfa-47bb-867c-89108890ebe9",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "8340f9a6-7edf-49c4-9a2c-4387603b78ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "70d93aa9-3470-4c07-973d-c9ab8710ca24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8340f9a6-7edf-49c4-9a2c-4387603b78ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dd6b279-adb1-41b1-86c1-a135d1d51859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8340f9a6-7edf-49c4-9a2c-4387603b78ae",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "d780d55c-cf5f-4e06-8f78-5ac84123fc16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "b6f563b9-142f-4938-8ae2-e1bdecc2f166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d780d55c-cf5f-4e06-8f78-5ac84123fc16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a544c9-fff3-40cb-8c1a-b15c43dbdee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d780d55c-cf5f-4e06-8f78-5ac84123fc16",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        },
        {
            "id": "04ed95cb-8fdb-49ad-a67c-4900f025dd51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "compositeImage": {
                "id": "cc13c1b9-984f-4145-9cd1-e9c2f02ac920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04ed95cb-8fdb-49ad-a67c-4900f025dd51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5dee6df-ea8d-41fb-88e8-00435110fdf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04ed95cb-8fdb-49ad-a67c-4900f025dd51",
                    "LayerId": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6ab30b8d-5fa8-4418-aa41-e58a62e9ea1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "263f9083-ba1e-4f28-a703-a86eda089a32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}