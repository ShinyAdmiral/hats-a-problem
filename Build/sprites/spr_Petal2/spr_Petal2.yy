{
    "id": "a0d6833c-9789-4310-82b3-3d5d61557d1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Petal2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 2,
    "bbox_right": 4,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e86b524-6103-4a3d-9d33-1a05bf33586a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0d6833c-9789-4310-82b3-3d5d61557d1d",
            "compositeImage": {
                "id": "95ef75f6-ec2f-4ff1-806d-ddeabb6f1208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e86b524-6103-4a3d-9d33-1a05bf33586a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "617105d8-b527-46c2-9554-2eaccd1d236b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e86b524-6103-4a3d-9d33-1a05bf33586a",
                    "LayerId": "d2dca9ef-a330-41f2-975c-9a82c652126d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "d2dca9ef-a330-41f2-975c-9a82c652126d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0d6833c-9789-4310-82b3-3d5d61557d1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 5
}