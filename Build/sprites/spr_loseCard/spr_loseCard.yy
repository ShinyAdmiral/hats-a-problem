{
    "id": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseCard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "271faaef-e1f7-48a7-b8cf-7f490890bfd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "4006067e-f9cf-4ed6-b092-d2107b9711ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "271faaef-e1f7-48a7-b8cf-7f490890bfd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7203a08-c088-4ff7-a7d8-4880056cc269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "271faaef-e1f7-48a7-b8cf-7f490890bfd1",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "3adc596d-07bc-44ca-8ba3-f5731d91ed7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "a21b330d-dc07-49dd-ba83-c416dc5a8aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3adc596d-07bc-44ca-8ba3-f5731d91ed7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a888d52d-8bc2-45ac-ba69-b6b437bcf624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3adc596d-07bc-44ca-8ba3-f5731d91ed7e",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "b84f28b6-d85b-41aa-8f8b-e5d387713628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "a8bbe21c-d88e-4559-aaf5-3177df8301e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84f28b6-d85b-41aa-8f8b-e5d387713628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64423249-7335-41ab-b139-3ea1650118c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84f28b6-d85b-41aa-8f8b-e5d387713628",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "174cb9c6-9c78-4639-ba20-0b844d8a9f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "a0fb4b14-2dc4-4bdd-8e82-528941cb2a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "174cb9c6-9c78-4639-ba20-0b844d8a9f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14ece40-18bd-4e78-93b9-04360d9e5758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174cb9c6-9c78-4639-ba20-0b844d8a9f40",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "c2d69eae-0efb-47e9-8270-0f5356957e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "fc73bb6b-b0a8-4853-a9cb-54e9f19eea3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d69eae-0efb-47e9-8270-0f5356957e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b2876cf-ff53-4048-accc-d30daaee9e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d69eae-0efb-47e9-8270-0f5356957e61",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "a0bb2ac9-0f83-4733-a450-f32c67432e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "50f9d157-0f78-4676-9239-6955691fe817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0bb2ac9-0f83-4733-a450-f32c67432e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4e889c-316e-4071-a6df-c857bee05d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0bb2ac9-0f83-4733-a450-f32c67432e40",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "61d927ed-3ed1-4610-9456-a280a624118b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "af253128-d6ca-4da4-a240-cf12b6648644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d927ed-3ed1-4610-9456-a280a624118b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da7f823-4b55-4dbd-8a13-f710ae82640f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d927ed-3ed1-4610-9456-a280a624118b",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "b8e47cf0-ceb2-4daa-a3e7-2c9dbe30d379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "935a8251-192d-42f0-bee6-35d1d03613f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8e47cf0-ceb2-4daa-a3e7-2c9dbe30d379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4c6e66d-ef26-48cb-bd11-231e5125dfb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8e47cf0-ceb2-4daa-a3e7-2c9dbe30d379",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "803e1298-87b0-49e7-9052-7c1c568e1a48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "c117d0dd-23ef-4f4b-bb48-fbc8c17f7ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "803e1298-87b0-49e7-9052-7c1c568e1a48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58058054-fedd-4ac5-95af-062ffeafe448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "803e1298-87b0-49e7-9052-7c1c568e1a48",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "3b8db74b-4403-40ce-9505-6dda99cfbbac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "8603a090-2a45-4034-976b-35e5ec375013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b8db74b-4403-40ce-9505-6dda99cfbbac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "794b4148-c159-48e6-9192-baf0e068f14b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b8db74b-4403-40ce-9505-6dda99cfbbac",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "a5c19a97-8070-4dde-b543-5fbb766a0dd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "ccc4b82b-26c9-4a70-920a-3565e1394478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c19a97-8070-4dde-b543-5fbb766a0dd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b6cc88-a407-42de-9547-722c152a3512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c19a97-8070-4dde-b543-5fbb766a0dd7",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "0ec8aec8-2cd6-44f3-b296-26b9e1b7b44e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "927e4d8c-6cfc-49ab-84c2-2b9668a4cc04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec8aec8-2cd6-44f3-b296-26b9e1b7b44e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c677349-9670-491a-b441-1d9dbac18324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec8aec8-2cd6-44f3-b296-26b9e1b7b44e",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "accd56cb-357a-4c9e-9840-56b206791bf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "a1940a24-b199-4566-ae4f-c5aff33ccf0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "accd56cb-357a-4c9e-9840-56b206791bf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7160f60e-f89c-434c-8fe0-0cbe557df183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "accd56cb-357a-4c9e-9840-56b206791bf4",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "66fe117f-cba0-4afd-8c51-4ce8c7107565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "d6737a7e-4297-42ac-8a13-f0d251d540e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fe117f-cba0-4afd-8c51-4ce8c7107565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b173c49-ec55-489b-bd62-2523d8dd50ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fe117f-cba0-4afd-8c51-4ce8c7107565",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "d9937a05-f757-4ae0-b2da-ed23420e5f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "c8830aa1-a89a-4cca-9a48-38a2ead278db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9937a05-f757-4ae0-b2da-ed23420e5f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c1693a-d29c-4875-ab6d-de4617fbaed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9937a05-f757-4ae0-b2da-ed23420e5f2f",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "9242eec5-60d0-4d94-9d84-e878d3a54925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "ff79aa41-b306-4d21-b7b7-3a8222b29657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9242eec5-60d0-4d94-9d84-e878d3a54925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5601eb7c-775f-4b72-b7c7-ecf8c0ebf60c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9242eec5-60d0-4d94-9d84-e878d3a54925",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "c0026995-4cdf-4a07-87fd-3156a12e3476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "34b8afac-487b-4198-b6ce-9c82faf170f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0026995-4cdf-4a07-87fd-3156a12e3476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5f82dd-3560-4c4a-9aad-751d416b2872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0026995-4cdf-4a07-87fd-3156a12e3476",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "cc87afce-b784-4fe6-ab00-e90d0ae4f643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "0fa0f3cd-65b4-40aa-9b6d-cc4900ab9dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc87afce-b784-4fe6-ab00-e90d0ae4f643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a295b0a-6150-4a17-9a3a-9aab734c1b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc87afce-b784-4fe6-ab00-e90d0ae4f643",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "cc1b4243-b9e9-41e9-a8bb-a669928c86da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "750c5a0f-944a-4941-9a5e-59526225f5fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc1b4243-b9e9-41e9-a8bb-a669928c86da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "619d6089-cb1c-4d3f-9ac1-fbc034b7872a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc1b4243-b9e9-41e9-a8bb-a669928c86da",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "1dbc48ef-5a6b-45be-9857-b261b4144659",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "95e793b1-f2a4-4f8c-be7c-256cd8fa8a04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dbc48ef-5a6b-45be-9857-b261b4144659",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "747644ae-f129-42f8-8264-99acae4e0333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dbc48ef-5a6b-45be-9857-b261b4144659",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "8199fcac-73af-4da8-871d-36cf8cb38bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "c83a68c8-15cf-46a0-a9b0-b95be7d16af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8199fcac-73af-4da8-871d-36cf8cb38bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39bfe576-2a21-42ea-9875-17a09938c747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8199fcac-73af-4da8-871d-36cf8cb38bb7",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "2e244bcc-672a-497b-b64b-d5a04960c137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "ce4b5f1f-ed8c-420a-8928-7ade3bf9a75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e244bcc-672a-497b-b64b-d5a04960c137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5c2a1f-22a3-4eed-9aeb-5c4e179f9874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e244bcc-672a-497b-b64b-d5a04960c137",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "ccb0e5cd-3d7a-4c04-a23b-2909f0ee5766",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "cb9ced70-bb2b-468b-b308-50a8ff897512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccb0e5cd-3d7a-4c04-a23b-2909f0ee5766",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2732ea31-8b18-45db-989d-3baee3209575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccb0e5cd-3d7a-4c04-a23b-2909f0ee5766",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "6bba46b9-3634-4030-8c5c-4e3bbe29ba4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "a4c78ab7-d75f-481a-912c-7481cc8cb99b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bba46b9-3634-4030-8c5c-4e3bbe29ba4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edaf9167-856f-4da9-b84f-7e48c0577657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bba46b9-3634-4030-8c5c-4e3bbe29ba4b",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "c0a3b629-f95c-481e-9e7e-e95218ed0e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "9f4ac8e6-6af4-4751-8152-88b49ed3fff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a3b629-f95c-481e-9e7e-e95218ed0e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bd61714-7045-4fbd-afb8-cc525a237e38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a3b629-f95c-481e-9e7e-e95218ed0e8e",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "8d62c4e6-35b0-4385-b7a5-c38e4deb407d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "ad38589b-4432-4817-bb7f-929cdbbfbd7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d62c4e6-35b0-4385-b7a5-c38e4deb407d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08851f0d-55a1-463c-bc1f-5d458955e2b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d62c4e6-35b0-4385-b7a5-c38e4deb407d",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "29555ba8-4fa0-4c53-97bf-70d85d9e809e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "a3a3b464-4774-4d90-888b-6801e7c03279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29555ba8-4fa0-4c53-97bf-70d85d9e809e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81cf6099-1024-413f-be33-969c8c4ce36a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29555ba8-4fa0-4c53-97bf-70d85d9e809e",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "ec3ce444-8f94-4d97-90d6-a613f4ed0296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "dd48d3eb-fd43-42a8-a59e-a1ffd270f3ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3ce444-8f94-4d97-90d6-a613f4ed0296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4053f52-f69e-4855-8f4a-b7a4be693c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3ce444-8f94-4d97-90d6-a613f4ed0296",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "b31071a6-ffce-4d13-8a15-42f763af5e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "45849a77-46d7-47bb-909c-235d3ccd486d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b31071a6-ffce-4d13-8a15-42f763af5e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ab98552-af50-4fc7-9de6-3f829d41f799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31071a6-ffce-4d13-8a15-42f763af5e8d",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "7954ecac-c1d4-4c62-b877-c111eac7f6fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "5a01e15d-621f-4db1-93d5-3f22222fa5f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7954ecac-c1d4-4c62-b877-c111eac7f6fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a97490df-c477-4741-b905-8e729fcc05c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7954ecac-c1d4-4c62-b877-c111eac7f6fa",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "91a38d10-d181-450e-b6b3-fd578aeb0059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "4bb9d7e2-745e-466c-87f2-f317eedf1bf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91a38d10-d181-450e-b6b3-fd578aeb0059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4932af2-936c-4378-be60-dd86f8e25797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91a38d10-d181-450e-b6b3-fd578aeb0059",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "ca480cd5-dd3b-4128-88bf-9244bf446f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "54a3faa5-1ea7-4c98-9ece-2fa6f29168bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca480cd5-dd3b-4128-88bf-9244bf446f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d285c0-e843-49ec-96f9-68a43aef5054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca480cd5-dd3b-4128-88bf-9244bf446f11",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "fc5f7f48-318b-42ea-a092-b49a9a0a8d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "189f5795-69c7-4d7d-8cd4-541425e58a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc5f7f48-318b-42ea-a092-b49a9a0a8d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5da8732-5ba3-4db8-b11b-6887eff09136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc5f7f48-318b-42ea-a092-b49a9a0a8d2e",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "856c9b80-a2e4-4b44-b1b6-9868f17991ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "3dcddae5-15d4-43f0-8102-220443eba8f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "856c9b80-a2e4-4b44-b1b6-9868f17991ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2867d57f-90a5-4f16-9e67-02f2ec21a189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "856c9b80-a2e4-4b44-b1b6-9868f17991ae",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "9af47c81-58a6-4c2d-983e-1bb461d0d0ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "a26a8e2b-bb4e-4b3e-81cb-00dbe279a19d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af47c81-58a6-4c2d-983e-1bb461d0d0ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa7dd84-5f7e-4ed5-8729-36e0078254b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af47c81-58a6-4c2d-983e-1bb461d0d0ba",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "456c4191-6470-46d6-a7e7-c7173b360c38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "f28dec6f-531e-4536-9c2d-bc70425bd17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456c4191-6470-46d6-a7e7-c7173b360c38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd90312e-2afa-4027-b25d-9c39185d5741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456c4191-6470-46d6-a7e7-c7173b360c38",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "9b21f973-3391-4501-a731-707b499bca28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "1e74b534-e2ad-436b-8c9b-25d6ae32c136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b21f973-3391-4501-a731-707b499bca28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a66de2e-cc4f-4703-86ec-1f9900a1f597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b21f973-3391-4501-a731-707b499bca28",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "0dd76ac9-33ca-4bbe-8221-44a9b95a71b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "e8532345-67d2-4710-86b9-8a15fcc7b47c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd76ac9-33ca-4bbe-8221-44a9b95a71b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "901f4226-4426-4f44-b9de-8009795490d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd76ac9-33ca-4bbe-8221-44a9b95a71b4",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "d478dbdf-65fa-4e09-b41e-6078889677d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "6c571e16-819d-4242-a5fd-129c9579a6f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d478dbdf-65fa-4e09-b41e-6078889677d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "102ae654-9fd7-4f55-836a-f47acb00460e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d478dbdf-65fa-4e09-b41e-6078889677d8",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "a3e86d04-4029-44e3-b93b-b6cae7c75a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "35ee1f79-58a9-46e5-9e69-b00a8108e32b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e86d04-4029-44e3-b93b-b6cae7c75a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112a2a01-ebae-4ee3-8a56-a2551546a748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e86d04-4029-44e3-b93b-b6cae7c75a40",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "1ad49c0b-067e-47ae-bfdb-361af8973154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "618b19f7-ad4e-431e-b002-2dbd7e1687ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ad49c0b-067e-47ae-bfdb-361af8973154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cf782af-36c9-408f-82b0-0d5fc2649063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ad49c0b-067e-47ae-bfdb-361af8973154",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "529a6f33-0e35-4c51-88e5-2834d0a3b831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "ed57be55-26d1-4633-b5f2-e833526f3167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "529a6f33-0e35-4c51-88e5-2834d0a3b831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cb53280-1270-424b-a3eb-7fa2c2858c5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "529a6f33-0e35-4c51-88e5-2834d0a3b831",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        },
        {
            "id": "ae98244f-0782-4b22-beb3-e650a1420cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "compositeImage": {
                "id": "aeeca2ed-7d27-487a-a686-22b4294f472a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae98244f-0782-4b22-beb3-e650a1420cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83d405a-2aaf-4504-afdb-40a0cd926a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae98244f-0782-4b22-beb3-e650a1420cbd",
                    "LayerId": "3ce500b8-e922-421a-8d67-debb36b54172"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3ce500b8-e922-421a-8d67-debb36b54172",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}