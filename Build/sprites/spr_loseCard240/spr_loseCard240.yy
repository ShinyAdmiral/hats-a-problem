{
    "id": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseCard240",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fb31d59-551f-4047-85a4-c08dc7ef3725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "949d3ed4-01b3-4166-9fdd-267321aed585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb31d59-551f-4047-85a4-c08dc7ef3725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52d12c29-2bab-45e8-80cd-2ff46e39ce51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb31d59-551f-4047-85a4-c08dc7ef3725",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "89e46046-2f89-405b-87df-6daff43f43cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "b20d01ab-2a76-4242-a26d-59c274a2ef26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e46046-2f89-405b-87df-6daff43f43cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bc48ca4-d929-4e1b-aa66-adeed48a9359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e46046-2f89-405b-87df-6daff43f43cf",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "aff5da7c-a5a1-4ccb-96e2-a5d2ff0d106d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "d407dc7a-3270-4cb3-b4b3-2b7f58d9d284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aff5da7c-a5a1-4ccb-96e2-a5d2ff0d106d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15893023-91c2-4995-a9d6-174b815045ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aff5da7c-a5a1-4ccb-96e2-a5d2ff0d106d",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "fa9beb25-fc81-46cb-9a8c-d7040ec40bd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "feccd3cb-0105-4b9c-930c-0f0a4360c512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa9beb25-fc81-46cb-9a8c-d7040ec40bd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f13cfdb7-9bb8-489b-9261-ad3f673a8ec7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa9beb25-fc81-46cb-9a8c-d7040ec40bd2",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "d30c75a8-00df-4a0e-a805-14dab658f643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "dd70fc0e-3f16-493b-b775-75b154f470aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d30c75a8-00df-4a0e-a805-14dab658f643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc5ec914-f902-459b-9350-9bf2d5f149ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d30c75a8-00df-4a0e-a805-14dab658f643",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "4ec3e680-faca-475f-a8bb-7e962d013e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "194402a1-f392-4def-bf1d-9bfaf06a5249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec3e680-faca-475f-a8bb-7e962d013e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "231689a6-bbee-4c94-a20d-26c1b0697e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec3e680-faca-475f-a8bb-7e962d013e52",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "52d85c66-9b4d-49d8-802d-c261feaa0a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "1d983b13-00d9-4832-ad31-ba96c3ac2e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d85c66-9b4d-49d8-802d-c261feaa0a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c0c17ea-8d42-4068-85a8-a2e2e573ce6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d85c66-9b4d-49d8-802d-c261feaa0a89",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "65d51f7f-1437-4932-a363-ba5970d30ec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "cae8163e-3c07-41a7-b747-c724531da631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d51f7f-1437-4932-a363-ba5970d30ec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1982eb94-4770-4731-8e26-d1510cc225ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d51f7f-1437-4932-a363-ba5970d30ec8",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "5071be3f-9c67-4228-9138-4ed5689a5113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "532b938f-e8e4-4cc4-86f5-4c8b096e3b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5071be3f-9c67-4228-9138-4ed5689a5113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4692c1b4-a413-4f83-998b-05fd9f17be8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5071be3f-9c67-4228-9138-4ed5689a5113",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "2e67f30e-145c-4df3-85c7-2145ccab4617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "3ae7a9f7-9494-4d99-9d42-968ef33b3f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e67f30e-145c-4df3-85c7-2145ccab4617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7f87b1-f507-4d2a-92ea-2ae8bf1d2d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e67f30e-145c-4df3-85c7-2145ccab4617",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "2da85de1-af9c-4bc9-83bc-acb76b4c6afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "42d020a0-9abb-459e-80c5-64ae9235c30c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da85de1-af9c-4bc9-83bc-acb76b4c6afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4ee7b64-2209-4fe6-8917-cf35050d4fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da85de1-af9c-4bc9-83bc-acb76b4c6afb",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "27ebe9ff-ac64-4cf1-9a20-893a31845eb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "9c44d049-1ca6-4eb3-a8a1-8d3ab2cc88ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27ebe9ff-ac64-4cf1-9a20-893a31845eb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa019d66-603b-491a-b2c5-2f2e42deff8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27ebe9ff-ac64-4cf1-9a20-893a31845eb7",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "64c909e4-0707-4771-baf8-eab8d9939035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "1dc5fe27-5357-4aaf-9d13-b332bcab7063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c909e4-0707-4771-baf8-eab8d9939035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d94450-8149-4b23-8e75-c50e41226c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c909e4-0707-4771-baf8-eab8d9939035",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "7c2217a9-36ad-47b0-ab96-93954f8ad5b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "a7d87ce3-4a85-4d32-818d-0f05dcad81a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2217a9-36ad-47b0-ab96-93954f8ad5b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a15fa66-51ad-4530-b831-f35fcf312541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2217a9-36ad-47b0-ab96-93954f8ad5b7",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "5afaabe2-2e67-4477-99a5-66a0aa997190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "c2c94da1-5b6f-45dd-9562-2ba94265aaee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5afaabe2-2e67-4477-99a5-66a0aa997190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10386b51-00d6-4f8f-8989-b4a5f84a64fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5afaabe2-2e67-4477-99a5-66a0aa997190",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "074ff508-b5b3-4db1-95f1-ca52a9c6a30f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "9aadd48f-e632-4c13-b980-c0eae9ffefa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074ff508-b5b3-4db1-95f1-ca52a9c6a30f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5f943b-64b0-4f0f-a7cd-4431b6ed9ae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074ff508-b5b3-4db1-95f1-ca52a9c6a30f",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "28e25b0b-1353-4a17-ab59-3c6f50fcd225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "6dd84ccb-096b-4493-aeb2-e5d0ac578697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28e25b0b-1353-4a17-ab59-3c6f50fcd225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aab2b98-ba4b-4ada-872b-a8f5e5137d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28e25b0b-1353-4a17-ab59-3c6f50fcd225",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "75fbdf20-415b-4142-8814-c0954cefe76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "67398235-e5e5-4393-860f-05a247bede6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75fbdf20-415b-4142-8814-c0954cefe76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff50766-48a9-420e-8c53-ec0397d0ec03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75fbdf20-415b-4142-8814-c0954cefe76f",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "18d2f35a-bde5-4041-b7ab-7cb7973aad41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "bda74d94-6ee3-4b17-8bf5-f3a62337f112",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d2f35a-bde5-4041-b7ab-7cb7973aad41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab805312-12c2-40d6-b844-0ad047c51bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d2f35a-bde5-4041-b7ab-7cb7973aad41",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "3dbfbb24-92a7-44f3-bd44-51e139e06696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "929a9902-8d31-4a7f-aa00-ef6e6408216d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dbfbb24-92a7-44f3-bd44-51e139e06696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df763f37-7ad2-4f75-867a-fdaa2b5f8cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dbfbb24-92a7-44f3-bd44-51e139e06696",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "ced0e4cc-dcf9-4956-95b9-af9f62c3dee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "1921d27b-c750-4221-9d44-0a5d64bf0950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced0e4cc-dcf9-4956-95b9-af9f62c3dee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61235c77-176d-436a-82d1-5ad55c0a3428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced0e4cc-dcf9-4956-95b9-af9f62c3dee7",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "f6c000f4-0eaa-4963-8a09-78db755589db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "c6c937df-294b-4d5c-85dd-20a0f0e768cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6c000f4-0eaa-4963-8a09-78db755589db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbee5e39-b48d-4242-bf6b-93f2acc64598",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6c000f4-0eaa-4963-8a09-78db755589db",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "273ea19c-2584-4edf-b5eb-7bda4eeef510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "553fd33a-8e59-4757-a1ad-f6543a1cbce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273ea19c-2584-4edf-b5eb-7bda4eeef510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604a2464-3f3b-4b7c-bf84-2b7f0b227a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273ea19c-2584-4edf-b5eb-7bda4eeef510",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "feae31e5-7c4a-4244-870e-96f99ccee5ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "a2eda1a8-98a6-4cdb-91ca-3d37e8ef8cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feae31e5-7c4a-4244-870e-96f99ccee5ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94ba5e18-633f-4f53-9f7c-b5b45e79328f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feae31e5-7c4a-4244-870e-96f99ccee5ec",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "303b9f8b-b445-4e7b-ab2a-0c86d131e53e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "b347ecd9-ff02-48a8-8ab3-0b0867fa949c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "303b9f8b-b445-4e7b-ab2a-0c86d131e53e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52976438-2dd5-4eb8-b34e-f3090580ecce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "303b9f8b-b445-4e7b-ab2a-0c86d131e53e",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "c7740693-cc2a-4ca6-bd7f-9937ec02c2f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "53d232dd-10b9-4719-b602-0295fd97df79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7740693-cc2a-4ca6-bd7f-9937ec02c2f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb68396-5a9d-48bb-aa68-4d5d5d9123cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7740693-cc2a-4ca6-bd7f-9937ec02c2f3",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "8cdee462-1cfa-4577-8f03-35e4f8beb40d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "452bc812-16c5-4576-b8ea-97886d348083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdee462-1cfa-4577-8f03-35e4f8beb40d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55fa089c-c26d-42fc-8013-9d2c95c14438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdee462-1cfa-4577-8f03-35e4f8beb40d",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "16ba45d2-5ee2-44ba-a9e2-055c373b5151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "6ac8c067-c7e8-4c4e-9b20-a3a32cc03f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16ba45d2-5ee2-44ba-a9e2-055c373b5151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ec31e4-4d0f-4644-b647-c64ec22af25c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16ba45d2-5ee2-44ba-a9e2-055c373b5151",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "8d3df593-e6c1-4950-b4c5-1d4a6f1fb20c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "0228f003-78e7-4b19-9456-15b50cbe4019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3df593-e6c1-4950-b4c5-1d4a6f1fb20c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31dcc489-2b26-46e8-9ec9-9f0aad858e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3df593-e6c1-4950-b4c5-1d4a6f1fb20c",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "412cbc00-b475-41dd-9830-12a36feef9d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "363d7dd5-4a1d-4909-8128-b71501f8e5ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "412cbc00-b475-41dd-9830-12a36feef9d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6216743e-d773-4393-89b3-a56b95ba1528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "412cbc00-b475-41dd-9830-12a36feef9d8",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "a6dde30e-aab1-4945-ac3d-8b74003ed486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "69a898f6-cd32-4aab-bb75-3488885dff76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6dde30e-aab1-4945-ac3d-8b74003ed486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb9f98a1-770a-410c-bad4-0faf5186d662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6dde30e-aab1-4945-ac3d-8b74003ed486",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "7b1cc542-e988-4fb0-9076-a2a33598a848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "ff5ba307-851b-44b4-8c27-e98893a3b302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b1cc542-e988-4fb0-9076-a2a33598a848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61312aa-2dd7-4555-87f6-9e931e61b804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b1cc542-e988-4fb0-9076-a2a33598a848",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "c89dafcf-fc4e-472e-a7c8-b93d86d377f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "b3bd6b6f-915c-44b9-8d4a-6e345faeab48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c89dafcf-fc4e-472e-a7c8-b93d86d377f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5857b9ef-27c8-43bf-9fea-4ccaa205810a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c89dafcf-fc4e-472e-a7c8-b93d86d377f0",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "0c3a9a6b-43d5-46ee-9606-b97a34de976b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "9e5cef23-a6cb-40f8-809d-0bd0f1c89911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3a9a6b-43d5-46ee-9606-b97a34de976b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e04d192-8d02-4844-8803-a01b55e0aebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3a9a6b-43d5-46ee-9606-b97a34de976b",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "70935f06-f16d-486f-b4a0-463c0ef3a89d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "cfbfb415-bf55-4c69-bb8e-30e34b111126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70935f06-f16d-486f-b4a0-463c0ef3a89d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc5716ee-8d86-45a6-8f31-a5f7ffd4e960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70935f06-f16d-486f-b4a0-463c0ef3a89d",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "7e3e017b-b14a-4089-9e66-14cf2f76c653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "1ede01e3-572a-4620-8145-4f9f9cb1cb85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e3e017b-b14a-4089-9e66-14cf2f76c653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d708ed4-f647-45c3-8ddd-4c63f7ad3aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e3e017b-b14a-4089-9e66-14cf2f76c653",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "c649d414-eeba-4f27-b90f-7c0523024ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "9ee1105e-0aa8-4b0c-b3d5-668e77678b31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c649d414-eeba-4f27-b90f-7c0523024ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8c7f0c-8c07-4bcc-84f2-4698576ac679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c649d414-eeba-4f27-b90f-7c0523024ccf",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "57a0c51c-4804-468c-945a-26faca76b2e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "b509e255-f892-45ab-a82e-8301e879056c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a0c51c-4804-468c-945a-26faca76b2e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "242e8b17-5034-47b6-a0fb-30de71bfcf70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a0c51c-4804-468c-945a-26faca76b2e8",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "9280073a-6591-4dd6-82da-6f984006f96a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "4b427b60-1f58-4852-917b-11be06ef2962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9280073a-6591-4dd6-82da-6f984006f96a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2878b7ad-a090-4900-bcbd-ece37e85d57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9280073a-6591-4dd6-82da-6f984006f96a",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "88e6f617-092a-411f-8bd6-983359194d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "7ad1be27-4b48-4e1f-9990-49845b7d730c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88e6f617-092a-411f-8bd6-983359194d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e193689e-3aa2-437d-962d-f0dfe1afaed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88e6f617-092a-411f-8bd6-983359194d67",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "4f8f5545-9278-4956-8a59-fa58d4c67888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "ebda878a-0388-4781-8235-086479046f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8f5545-9278-4956-8a59-fa58d4c67888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d420a32e-66db-4444-a69e-ea1732c1b150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8f5545-9278-4956-8a59-fa58d4c67888",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "4d4dab80-998f-41ef-836e-990a2403f5a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "acff92c9-220a-4dfa-a709-0de3fd5c8eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d4dab80-998f-41ef-836e-990a2403f5a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4384b394-ac00-47a7-b2ce-98bb17ec5a7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d4dab80-998f-41ef-836e-990a2403f5a2",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        },
        {
            "id": "8531246c-86f1-40a0-8594-8275d65825be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "compositeImage": {
                "id": "9dfd68cb-8591-4430-ab64-7c05ad42e081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8531246c-86f1-40a0-8594-8275d65825be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b77168-b92c-423c-b64e-e0e054859824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8531246c-86f1-40a0-8594-8275d65825be",
                    "LayerId": "fbbf498b-1b4b-400d-878f-70e67fb3881b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "fbbf498b-1b4b-400d-878f-70e67fb3881b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7dc83f7-e7b6-4f89-9459-2b338b6920cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 240,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}