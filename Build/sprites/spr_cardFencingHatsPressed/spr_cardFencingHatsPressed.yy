{
    "id": "90e99d88-dfd8-4d87-99d6-77402963b9c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardFencingHatsPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb4fa3ce-80e1-4d20-bbee-4e9d2a93b7eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90e99d88-dfd8-4d87-99d6-77402963b9c9",
            "compositeImage": {
                "id": "768e6ebb-5028-47b8-bf68-cd48104789e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb4fa3ce-80e1-4d20-bbee-4e9d2a93b7eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64438f08-fe70-4f04-a4e0-f88d61296fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4fa3ce-80e1-4d20-bbee-4e9d2a93b7eb",
                    "LayerId": "c8479b93-f025-4897-895a-e290a44b2522"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c8479b93-f025-4897-895a-e290a44b2522",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90e99d88-dfd8-4d87-99d6-77402963b9c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}