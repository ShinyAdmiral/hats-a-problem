{
    "id": "0ecfb2a8-444d-4ed6-b209-818019f4d4ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCamera",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "499a3d72-849c-4f1e-b30a-c7188c5c3088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ecfb2a8-444d-4ed6-b209-818019f4d4ac",
            "compositeImage": {
                "id": "46a906fd-3882-4393-a8c7-0d3ea2f98119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "499a3d72-849c-4f1e-b30a-c7188c5c3088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbb7db8-f33c-4130-a122-46cbb421a0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "499a3d72-849c-4f1e-b30a-c7188c5c3088",
                    "LayerId": "2382dc81-9acb-44b2-b379-2d47fbacad5a"
                },
                {
                    "id": "9274011e-1813-4689-972b-6261693ea092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "499a3d72-849c-4f1e-b30a-c7188c5c3088",
                    "LayerId": "6b9e28b3-7d70-4b61-a337-901488d17219"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b9e28b3-7d70-4b61-a337-901488d17219",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ecfb2a8-444d-4ed6-b209-818019f4d4ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2382dc81-9acb-44b2-b379-2d47fbacad5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ecfb2a8-444d-4ed6-b209-818019f4d4ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}