{
    "id": "7bf71c0c-4aea-4a23-802c-330e0b24a0af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_glove1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 10,
    "bbox_right": 48,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0d743c2-d4b3-4731-856d-5fd61a636b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bf71c0c-4aea-4a23-802c-330e0b24a0af",
            "compositeImage": {
                "id": "19d8c289-a456-4f4b-873c-12f013e8b964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0d743c2-d4b3-4731-856d-5fd61a636b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68e210c7-014b-4727-8d88-353046826e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0d743c2-d4b3-4731-856d-5fd61a636b89",
                    "LayerId": "79351313-3be2-4cbb-b29a-03713f864a49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "79351313-3be2-4cbb-b29a-03713f864a49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bf71c0c-4aea-4a23-802c-330e0b24a0af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 15,
    "yorig": 31
}