{
    "id": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardKnifePressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62f931ea-fff4-450f-bdee-21cf9a90c948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "cf269a33-f5b0-46bb-a7d8-3edbc1151dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f931ea-fff4-450f-bdee-21cf9a90c948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08199daf-fd46-4a53-96e3-58adab040f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f931ea-fff4-450f-bdee-21cf9a90c948",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "5a6381ec-2a39-4b94-861d-56059370bfd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "4f952ba2-4dc4-4d76-b3b7-a5b5d69020b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a6381ec-2a39-4b94-861d-56059370bfd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30422707-050a-4f74-9712-226ec2a21521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a6381ec-2a39-4b94-861d-56059370bfd1",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "39c29ca2-7163-4f68-8939-0f0f939ba8f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "3b9f0841-9ff7-4fcf-bb50-fc9e41c63def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c29ca2-7163-4f68-8939-0f0f939ba8f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d5a6bff-d7be-41c9-a8d5-4de1421d65f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c29ca2-7163-4f68-8939-0f0f939ba8f8",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "a43a7171-9709-45fe-bef6-2bc99490c20f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "e624f47a-abd0-431d-aee0-a3b24a6560fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a43a7171-9709-45fe-bef6-2bc99490c20f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c689d75-f2f0-4a23-bbe7-5f7832853163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a43a7171-9709-45fe-bef6-2bc99490c20f",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "f87efe7a-de5c-4fbe-a4a0-b00e2fc0603f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "4b3dbe26-8f12-45b9-a647-894aeee437f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f87efe7a-de5c-4fbe-a4a0-b00e2fc0603f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804570fa-10f7-460e-91f0-b6c66d843099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f87efe7a-de5c-4fbe-a4a0-b00e2fc0603f",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "80c051ed-54c2-4af1-9a2c-6a6b317c2d16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "7a143609-22be-4f4c-82e0-0323902433a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80c051ed-54c2-4af1-9a2c-6a6b317c2d16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8731f5-7a3b-4071-ab1a-74aa79253a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80c051ed-54c2-4af1-9a2c-6a6b317c2d16",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "68eb2dca-34dd-49b1-8b8d-c8b4e716f4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "6c69f526-7df9-4b6e-b8d7-9723e29d4883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68eb2dca-34dd-49b1-8b8d-c8b4e716f4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be64ea2f-3c16-4aa8-97a1-6e73de027be1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68eb2dca-34dd-49b1-8b8d-c8b4e716f4b4",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "0fceacb4-4ee2-4bd3-8a6e-e1544009f4e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "e69a364c-2af2-4d1e-bc7a-685698c74b48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fceacb4-4ee2-4bd3-8a6e-e1544009f4e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca133e3d-f626-4872-b131-0071cabda183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fceacb4-4ee2-4bd3-8a6e-e1544009f4e7",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "b3b545d4-85c6-42ab-95f3-acb56edde3b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "7d781718-428d-45b5-96e9-1d824b7653ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b545d4-85c6-42ab-95f3-acb56edde3b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95bb475-11ff-4e1f-af34-24c79309470c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b545d4-85c6-42ab-95f3-acb56edde3b4",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "b7527e0b-0d5e-4c0c-b29e-071485317d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "1d886d2b-54c6-46b8-8920-1280a22af7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7527e0b-0d5e-4c0c-b29e-071485317d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbde53a5-cf41-4525-881a-b97f5ed80cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7527e0b-0d5e-4c0c-b29e-071485317d10",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "83d48284-5d9b-4648-9e15-407e99265c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "211cf92d-bfd4-4e17-8f30-11f71b20591a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d48284-5d9b-4648-9e15-407e99265c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d851ab-1477-4776-90ec-9993a512e9dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d48284-5d9b-4648-9e15-407e99265c27",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "94117438-072e-4144-adf9-87e054a50788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "b243e860-bb3e-49d5-ade3-7a94c0d88b97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94117438-072e-4144-adf9-87e054a50788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99dbcdd6-46f6-4849-899e-4e3e73de9d8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94117438-072e-4144-adf9-87e054a50788",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "02e337a5-13f8-4d4b-83a4-43a5c417c1da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "af17e7e8-ee66-41c8-b653-441652cc52b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02e337a5-13f8-4d4b-83a4-43a5c417c1da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a4d53b9-2b5f-4503-a26f-8991b7434079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02e337a5-13f8-4d4b-83a4-43a5c417c1da",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "d508f5fc-3b17-4cca-a92f-ab36c85e7cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "b103b61e-e1d3-456a-804a-4385e36650dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d508f5fc-3b17-4cca-a92f-ab36c85e7cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21d409bc-2cc4-4456-af37-355b06f6eaf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d508f5fc-3b17-4cca-a92f-ab36c85e7cd0",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "71a16b86-a7cb-4284-bcb0-4f627ee42d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "a7e61404-4e07-49f6-97a5-da54f4191d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a16b86-a7cb-4284-bcb0-4f627ee42d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27e7667d-0d7a-4a94-9180-907ef95576cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a16b86-a7cb-4284-bcb0-4f627ee42d4e",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "c9be3263-1e3a-42f4-845d-e605d6794681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "1f99c805-023b-43c1-b610-b1c989ad9f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9be3263-1e3a-42f4-845d-e605d6794681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb83dc73-22bc-4fa1-a14b-a1fe85506cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9be3263-1e3a-42f4-845d-e605d6794681",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "76b5d910-7049-4ffa-a123-35e1a6cfd629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "5c466741-a90d-4aa6-b9cd-bbc76909ca06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b5d910-7049-4ffa-a123-35e1a6cfd629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f860e530-a093-49be-9444-f1bfeb48b53f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b5d910-7049-4ffa-a123-35e1a6cfd629",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "b235e2a1-c721-4cb8-942d-e2410ad21c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "f5aed7a9-5183-4b41-8bd0-10da5fb9f669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b235e2a1-c721-4cb8-942d-e2410ad21c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3686b0c-065f-411b-b3be-0e4d8284785b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b235e2a1-c721-4cb8-942d-e2410ad21c34",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "56c8b6d1-0583-4764-8ce0-fb4d3cd23fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "762e6208-20c1-471d-b664-6c4457616b94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c8b6d1-0583-4764-8ce0-fb4d3cd23fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0e65b1-7e96-46ad-8867-8ddbbc2e262f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c8b6d1-0583-4764-8ce0-fb4d3cd23fa0",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "ea331381-69a1-413c-b8f8-d4ad6ebb6add",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "a2f8fac5-77a2-489f-b8ca-7e8b8c8e53e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea331381-69a1-413c-b8f8-d4ad6ebb6add",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b18ee68-48c4-4e1b-b51b-dacb578b4c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea331381-69a1-413c-b8f8-d4ad6ebb6add",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "f3ea4cb9-b2ce-41c4-beec-57167954af2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "0054fc26-f8c8-464e-90c7-f672cfefd2db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ea4cb9-b2ce-41c4-beec-57167954af2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21240502-de0a-40df-a07e-bb2d59119169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ea4cb9-b2ce-41c4-beec-57167954af2e",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "2d16a889-6772-4140-bbc0-777e28e1062b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "0b795038-9236-4ae1-897d-b985229cb72e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d16a889-6772-4140-bbc0-777e28e1062b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d571846-a9a9-4492-b048-9afe115c8bc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d16a889-6772-4140-bbc0-777e28e1062b",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "720b0474-7675-4711-8352-29267b93bc49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "74d582e1-908e-4519-930f-405b6101ef4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720b0474-7675-4711-8352-29267b93bc49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46f4bc5f-6ef9-4d45-b123-675f1d7ce12f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720b0474-7675-4711-8352-29267b93bc49",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "fb06ef93-2457-41bd-8719-54912fa0c8f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "6a679bdc-1651-4db7-9cc8-43c33f89ddd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb06ef93-2457-41bd-8719-54912fa0c8f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aeb240a-8f5f-463b-a737-f6fadd4b318f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb06ef93-2457-41bd-8719-54912fa0c8f9",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "c21d36bb-b1ad-4754-9bb2-0a65f6197335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "f29373c9-a54e-4bc5-9c5e-428a9e7cd891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c21d36bb-b1ad-4754-9bb2-0a65f6197335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ff15e2-69eb-4662-a565-5165918bea72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21d36bb-b1ad-4754-9bb2-0a65f6197335",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "e81933c6-96b1-4e3d-8cb1-c8add1bfa711",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "3aab3a96-1f57-437b-8b51-f2c0ef48b533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e81933c6-96b1-4e3d-8cb1-c8add1bfa711",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7501b646-c8ed-4425-a94a-f371fd108d0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e81933c6-96b1-4e3d-8cb1-c8add1bfa711",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "5b8af370-2966-45c0-b5f3-71fd1fd97fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "020c3879-dc77-4310-8a85-cd4cd0eb4771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8af370-2966-45c0-b5f3-71fd1fd97fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47032460-97ef-4b18-827d-4f5dcf897934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8af370-2966-45c0-b5f3-71fd1fd97fff",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "9026776f-332c-418f-8510-9db311f12a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "4f8fe9ca-87b2-4d24-909c-8847bfa3fa17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9026776f-332c-418f-8510-9db311f12a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bec25e0-014d-4236-a2fe-c0998ff9b1a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9026776f-332c-418f-8510-9db311f12a24",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "a728d913-2482-4b76-83c3-7a59c337452e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "e46472ae-6018-45f7-a9db-81e21b5ed7fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a728d913-2482-4b76-83c3-7a59c337452e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6145dc-19c0-4550-9801-baf227b1d0e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a728d913-2482-4b76-83c3-7a59c337452e",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "d7cb46db-88f6-4da5-b2c2-ea13e278ac56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "86bb843d-9820-4a4e-82ff-006669ee32ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7cb46db-88f6-4da5-b2c2-ea13e278ac56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "343287fb-17e6-4803-8f44-749a7131f68c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7cb46db-88f6-4da5-b2c2-ea13e278ac56",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "2375492b-30c8-4838-806a-9dcdf0b89c71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "d038ec61-13cb-4803-80f4-5f34f0adec1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2375492b-30c8-4838-806a-9dcdf0b89c71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96d7477-0472-488d-bc38-513435b62b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2375492b-30c8-4838-806a-9dcdf0b89c71",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "2697b8ac-cccc-48c2-be96-fcbac4ea847f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "a3703cd7-d0f0-4c03-93a5-bd4be513c324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2697b8ac-cccc-48c2-be96-fcbac4ea847f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb14d78-f096-4c26-aa91-ee321d4d0cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2697b8ac-cccc-48c2-be96-fcbac4ea847f",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "85aa5751-3ff1-4093-8622-f0c240a7f2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "91c5d5bd-a36e-478d-9089-0a946180cdeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85aa5751-3ff1-4093-8622-f0c240a7f2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e12d365-adc0-49ab-a372-ce431765de64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85aa5751-3ff1-4093-8622-f0c240a7f2e3",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "c7e4d5b9-429d-4fac-995e-0fc27716eb8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "d5f2de14-e4bf-4d48-b117-5ba02854699c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e4d5b9-429d-4fac-995e-0fc27716eb8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e5b2b27-4072-4abb-a44b-29cad6bbd391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e4d5b9-429d-4fac-995e-0fc27716eb8a",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "78fc9c73-f064-47c3-bf72-12e3bf9690f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "618649ec-c522-4510-991c-59704e8d0724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78fc9c73-f064-47c3-bf72-12e3bf9690f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37fe58e1-a58d-489a-9873-11d2d5000cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78fc9c73-f064-47c3-bf72-12e3bf9690f9",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "37e62d65-1209-4d57-ad24-68c6431ca49d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "5cc6af15-6fec-4583-b158-37fd481f371d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37e62d65-1209-4d57-ad24-68c6431ca49d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df5dfdae-079f-4fb9-8f33-42392e572a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37e62d65-1209-4d57-ad24-68c6431ca49d",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "84ea6025-f333-4abf-93a1-17b9f08e9525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "46ab9813-add5-47dd-94ff-e8571a416624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84ea6025-f333-4abf-93a1-17b9f08e9525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaefcb81-168e-4a21-9941-7da5bfcd8f3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84ea6025-f333-4abf-93a1-17b9f08e9525",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "eb458bd7-7f45-4ec0-9538-380101d1f449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "660bae64-0aee-40dd-8275-7c0b8b01c077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb458bd7-7f45-4ec0-9538-380101d1f449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc3e446-f8ad-43b5-a19b-ea77b32ff0b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb458bd7-7f45-4ec0-9538-380101d1f449",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "5e02398d-b019-4178-8f90-c7180d0aef8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "7efb4d66-e99a-4abe-94a7-7ef278ba591b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e02398d-b019-4178-8f90-c7180d0aef8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "394d57f6-1ac2-408f-956c-d328ddfeafd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e02398d-b019-4178-8f90-c7180d0aef8b",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        },
        {
            "id": "90f3d36b-cff1-40f3-8987-f7b1b7d37f1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "compositeImage": {
                "id": "118dafe5-cc1e-4f3a-9772-eb74bbe3557e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f3d36b-cff1-40f3-8987-f7b1b7d37f1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c62242-eb14-48ce-9ce6-6051176c43be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f3d36b-cff1-40f3-8987-f7b1b7d37f1e",
                    "LayerId": "89429f1b-0c06-4416-9957-dc010a7ae08c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "89429f1b-0c06-4416-9957-dc010a7ae08c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a5c8ef7-18cb-4cc0-bda1-4d4ffc70c709",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}