{
    "id": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CreditsPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1aa94800-6b18-4291-b474-178f2d4a2aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "5e77da0d-8250-4da9-a9f4-b01d82bc2181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aa94800-6b18-4291-b474-178f2d4a2aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ffa36b-e34b-4661-8a0a-2ecf850bf002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aa94800-6b18-4291-b474-178f2d4a2aa8",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "c09bf0ac-1230-4265-9f22-6669ae3792cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "93ebfe44-9ec0-4e73-b672-4975337f5da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c09bf0ac-1230-4265-9f22-6669ae3792cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9560e7-7d21-414c-ac9f-2d4ca8455f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c09bf0ac-1230-4265-9f22-6669ae3792cb",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "03ece38b-6618-47b8-a331-f4f6f952acad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "b5debbf6-72b7-4a77-bdc9-7266183b745f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ece38b-6618-47b8-a331-f4f6f952acad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac56aca5-dea3-463f-a4cb-f2140fc68ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ece38b-6618-47b8-a331-f4f6f952acad",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "73da848a-e6ca-4152-a45d-8e9ad293459e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "5430e4d7-7f55-4762-a84f-275870a02405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73da848a-e6ca-4152-a45d-8e9ad293459e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c260690f-cfae-4a92-ade5-3c2dff620252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73da848a-e6ca-4152-a45d-8e9ad293459e",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "5b36582c-ce57-4f36-b8d2-9b10e5f4700d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "dba37713-ecfa-418c-81cb-be0ce570a6bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b36582c-ce57-4f36-b8d2-9b10e5f4700d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0388234d-d226-409c-aef4-0ef827927ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b36582c-ce57-4f36-b8d2-9b10e5f4700d",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "ebdfd1d5-30eb-477b-80c4-1d9377221eec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "c74bfa22-1595-4481-92c4-d04ad03d32e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebdfd1d5-30eb-477b-80c4-1d9377221eec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8045ec41-3632-48ae-a9a0-063efe008523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebdfd1d5-30eb-477b-80c4-1d9377221eec",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "35720ab8-db5b-4510-b9fd-3b77da21e84a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "231ec970-c9af-4021-8eb9-95d22cb477c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35720ab8-db5b-4510-b9fd-3b77da21e84a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47c0da56-cc31-4443-b2ba-80fc45f80c28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35720ab8-db5b-4510-b9fd-3b77da21e84a",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "1e598dda-b663-41d9-91ce-7565e4b7edea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "b2d58c95-454a-4a1f-a05d-776685344578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e598dda-b663-41d9-91ce-7565e4b7edea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78b4fe0e-f7d9-4024-9412-e71c700d7836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e598dda-b663-41d9-91ce-7565e4b7edea",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "f5259afe-4d08-459f-a830-32cfd85a5574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "41720fea-c309-4e69-a097-73ff035844f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5259afe-4d08-459f-a830-32cfd85a5574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b551af-878e-4a5c-949f-07cb9276b1b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5259afe-4d08-459f-a830-32cfd85a5574",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "ecbbf105-f01d-4dab-9568-35b1f1866603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "303f122d-32e2-49df-8cc9-b6ad3970d783",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecbbf105-f01d-4dab-9568-35b1f1866603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0818ac9-b83f-4e5a-9533-9e90ffa56c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecbbf105-f01d-4dab-9568-35b1f1866603",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "53e3774a-fd36-4b9d-bbc6-c9575d2c5581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "cee02075-f7c4-49cc-87ea-ce55db8e12b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53e3774a-fd36-4b9d-bbc6-c9575d2c5581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a47446-a2d0-4fe3-965e-fdd23cf20752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53e3774a-fd36-4b9d-bbc6-c9575d2c5581",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "068f0396-6011-45cc-9959-465fdee19db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "d5852b44-59e7-44ab-b275-73d0ed996634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "068f0396-6011-45cc-9959-465fdee19db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49f75c8e-51e2-4df7-8a9a-64452aed6471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "068f0396-6011-45cc-9959-465fdee19db6",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "d4193581-24ae-443a-83bc-9c9b85dc4352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "814e000b-933e-4b86-b206-e79230c09850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4193581-24ae-443a-83bc-9c9b85dc4352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1322485b-c84f-4b47-a762-713f6fe47992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4193581-24ae-443a-83bc-9c9b85dc4352",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "1a7bf9dc-c867-4c26-b208-327dc489ba43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "c63e9f4a-703b-4bcf-ba4c-f1d5917d5114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a7bf9dc-c867-4c26-b208-327dc489ba43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea37187-db53-42fb-b835-109b5bff8d4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a7bf9dc-c867-4c26-b208-327dc489ba43",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "9994dd1f-4b92-4179-be4f-768be7f12a37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "def6dc1d-e2cd-4f88-aea5-7219db0eaff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9994dd1f-4b92-4179-be4f-768be7f12a37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f903d5c7-9677-45a9-92b4-3cdf923bff7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9994dd1f-4b92-4179-be4f-768be7f12a37",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "f55a6128-7b7c-41d6-b0ff-aed2b2bcd4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "7ac0a640-e867-4d77-9614-9f3029b8238d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55a6128-7b7c-41d6-b0ff-aed2b2bcd4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12cb84ed-491b-4bd9-8f72-847ebdc7d328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55a6128-7b7c-41d6-b0ff-aed2b2bcd4c9",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "a3b23c57-9ae7-42fe-942b-0a61d6f9768f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "ce2a8802-60a0-4149-be21-f61389473f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b23c57-9ae7-42fe-942b-0a61d6f9768f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a1a140-d130-4861-a471-1bab6b1957d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b23c57-9ae7-42fe-942b-0a61d6f9768f",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "15c68b36-9439-4ea9-9ea8-50f75fb394c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "fe09c723-c4f7-4d03-b897-0eb52c5c17e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c68b36-9439-4ea9-9ea8-50f75fb394c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea31e9b6-4f7e-4abb-a155-99c8ffc950ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c68b36-9439-4ea9-9ea8-50f75fb394c0",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "1aeccbb6-309d-4469-9aa3-33824b09705a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "bc54a671-ee24-435d-96e6-ffd65cb552fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aeccbb6-309d-4469-9aa3-33824b09705a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db0db7dc-85f9-4d78-914e-90a2f96cee64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aeccbb6-309d-4469-9aa3-33824b09705a",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "bbdf11cd-e3b6-4d2e-b1c8-7502c700edee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "918f80d5-656b-4217-a603-74895a46d6b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbdf11cd-e3b6-4d2e-b1c8-7502c700edee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662ef64f-a47a-4be7-961c-8c51d60fd0b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbdf11cd-e3b6-4d2e-b1c8-7502c700edee",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "124ade95-9e4e-462b-9612-91622af4c742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "e052c1b6-ae02-48a9-b680-b9ca72bdee4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "124ade95-9e4e-462b-9612-91622af4c742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f413987a-41ac-4139-9f80-6c78720dda92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124ade95-9e4e-462b-9612-91622af4c742",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "23192714-fdf4-444a-a868-b5ed76052bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "ab85f612-d403-44b5-96bc-83c0c2910584",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23192714-fdf4-444a-a868-b5ed76052bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "738b4e2a-b6b3-4327-a8fe-8f747483decd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23192714-fdf4-444a-a868-b5ed76052bc4",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "543491a8-801f-4c97-b8e8-39507edd7d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "3d74ec60-21ed-45d2-942e-f406789dcc18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "543491a8-801f-4c97-b8e8-39507edd7d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ea5de7-cce3-47ba-9aa3-85662f681210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "543491a8-801f-4c97-b8e8-39507edd7d9b",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "aea744e3-ddc4-44bc-8366-20f7e8c7a2c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "64758961-2c6c-4669-9d69-45fba6335abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea744e3-ddc4-44bc-8366-20f7e8c7a2c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "007e0963-642b-4073-8693-2e760b08828d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea744e3-ddc4-44bc-8366-20f7e8c7a2c9",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "4200a9b2-4af3-49f9-94ee-c710e01198d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "137d36d1-238a-4f29-aec1-a346582b766a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4200a9b2-4af3-49f9-94ee-c710e01198d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "411fa95a-d58e-4f8a-af1c-a67eef1f5963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4200a9b2-4af3-49f9-94ee-c710e01198d9",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "08911db2-45fc-47c0-9700-869e705edd36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "9ad4b1fe-637d-41fb-9d8a-ed1f29989ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08911db2-45fc-47c0-9700-869e705edd36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de7d3b74-3cd0-4d58-a1e4-a4449fa2a982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08911db2-45fc-47c0-9700-869e705edd36",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "51929698-14d9-48c3-a29f-9743dfd36f80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "810c1b45-8a22-49b7-99f9-c7fc23f71adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51929698-14d9-48c3-a29f-9743dfd36f80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8368b1-c2c8-4d47-92b9-a2d61ea4cbdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51929698-14d9-48c3-a29f-9743dfd36f80",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "78d09442-79d6-4d13-97ee-f79482885b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "f2f1324d-0914-4eec-b81e-c10368ad2120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d09442-79d6-4d13-97ee-f79482885b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b902b66-ce9e-4cc1-8402-b5856c343fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d09442-79d6-4d13-97ee-f79482885b12",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "8ebb8872-9c33-4938-9b1d-e6f5cf9017d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "835d0e96-a387-48af-afd7-a1d37b4e915c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ebb8872-9c33-4938-9b1d-e6f5cf9017d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53c42e1f-25a9-47b6-83d6-da5e46c73cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ebb8872-9c33-4938-9b1d-e6f5cf9017d2",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "f0695df3-1924-40ac-9b65-6a5092dd3fba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "1f2e56ab-2e56-4c83-b436-e814a7f8d8d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0695df3-1924-40ac-9b65-6a5092dd3fba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5690ba-7ce3-4c77-bfbe-ae0839a40dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0695df3-1924-40ac-9b65-6a5092dd3fba",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "2bf32513-14c9-40dd-9cfd-72c195a58312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "75863b9b-bc1b-44b6-b668-8f31d060d557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf32513-14c9-40dd-9cfd-72c195a58312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b599711-5039-4bd4-a0d5-7900035d2fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf32513-14c9-40dd-9cfd-72c195a58312",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "2d2e048f-5c6f-476a-8363-aa50149ea3c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "913099a7-d8a2-4578-9b04-f62f1112694f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d2e048f-5c6f-476a-8363-aa50149ea3c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be3d8903-ea7a-424f-99de-9ec15c44015e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d2e048f-5c6f-476a-8363-aa50149ea3c5",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "714c8bce-01c8-478a-ab0d-ef8aa76d0423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "bc2ac223-eb82-4d34-9b3c-633851b567df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "714c8bce-01c8-478a-ab0d-ef8aa76d0423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53450be3-2ad4-4e5b-bcc2-937b88702be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "714c8bce-01c8-478a-ab0d-ef8aa76d0423",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "85d9c8db-491a-41d1-990a-39d2f84779a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "ae40880d-ffe2-4f53-a86c-6a4f93c6d344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d9c8db-491a-41d1-990a-39d2f84779a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "599d99c8-ee70-4073-b7b8-c1a8568a545e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d9c8db-491a-41d1-990a-39d2f84779a1",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "c88b2168-138d-435c-9583-b63487744e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "9172a8de-494e-45d8-bcce-e3b4892e9cda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c88b2168-138d-435c-9583-b63487744e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72386d01-1933-40a0-b073-949a259280f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c88b2168-138d-435c-9583-b63487744e0f",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "5f628bf9-2d33-4a9d-b97e-398f627b2e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "86099706-8a59-40a4-aba6-ea5e8c84727e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f628bf9-2d33-4a9d-b97e-398f627b2e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241ad38f-e995-44ba-81a7-498d36c1fc6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f628bf9-2d33-4a9d-b97e-398f627b2e3f",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "a9a59e55-e36f-4d25-85e2-4876f665d8fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "0f47ce9b-6e40-46ef-b197-f101d8d8a3a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a59e55-e36f-4d25-85e2-4876f665d8fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cddf4897-ab7a-4d35-a7e5-8a6a52250373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a59e55-e36f-4d25-85e2-4876f665d8fa",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "7693e240-b898-493a-bef6-b36aab5670f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "77540f36-38c3-455f-ae55-250d99f204d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7693e240-b898-493a-bef6-b36aab5670f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef55c292-e2f4-421f-baef-0c4a48b8ef8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7693e240-b898-493a-bef6-b36aab5670f5",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        },
        {
            "id": "e96189cd-67ca-4c01-853d-7663cc30fa15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "compositeImage": {
                "id": "d1366d8e-d2ee-4348-8dc6-eba1f84e770a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96189cd-67ca-4c01-853d-7663cc30fa15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "987f1c07-7d40-4602-982a-294d001b656f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96189cd-67ca-4c01-853d-7663cc30fa15",
                    "LayerId": "3531b770-08f6-4739-8cef-8eb285e074fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3531b770-08f6-4739-8cef-8eb285e074fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce6234a2-88c0-4aba-94f6-79bc689ca9d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}