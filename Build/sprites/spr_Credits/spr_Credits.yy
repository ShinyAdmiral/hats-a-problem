{
    "id": "4787788b-18a5-4ce3-be37-ad1b77c54d95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Credits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52235e56-2c79-4cef-bdd7-651484eeb7d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787788b-18a5-4ce3-be37-ad1b77c54d95",
            "compositeImage": {
                "id": "a6c99b87-80dc-46ef-ab5b-87d03c2975e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52235e56-2c79-4cef-bdd7-651484eeb7d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2219cd65-ee58-43fb-b501-84e3ad0e2ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52235e56-2c79-4cef-bdd7-651484eeb7d0",
                    "LayerId": "d0018f43-f5b9-4fd1-99c5-c1d3d5abcf08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d0018f43-f5b9-4fd1-99c5-c1d3d5abcf08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4787788b-18a5-4ce3-be37-ad1b77c54d95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}