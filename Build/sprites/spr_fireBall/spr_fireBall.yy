{
    "id": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireBall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 8,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42eee6c9-4931-4487-a994-b4f59e4ce543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "compositeImage": {
                "id": "e280420f-e691-436e-96e5-cccbf8775de2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42eee6c9-4931-4487-a994-b4f59e4ce543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84e4c54-973c-4b2d-b464-a7151a50abe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42eee6c9-4931-4487-a994-b4f59e4ce543",
                    "LayerId": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d"
                },
                {
                    "id": "453d7106-1cb9-417a-be08-7dca22ad0374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42eee6c9-4931-4487-a994-b4f59e4ce543",
                    "LayerId": "1c8fed40-f4a9-4210-bb44-ed60068a53f9"
                }
            ]
        },
        {
            "id": "14385b2c-8c3c-4d6c-858d-695a1d6d38df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "compositeImage": {
                "id": "1edd8629-61e9-45ff-98f7-de8860653b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14385b2c-8c3c-4d6c-858d-695a1d6d38df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7fbec3-4996-49d8-930f-b219924fd7cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14385b2c-8c3c-4d6c-858d-695a1d6d38df",
                    "LayerId": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d"
                },
                {
                    "id": "c2500462-c906-49ee-b638-b3306af6a87c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14385b2c-8c3c-4d6c-858d-695a1d6d38df",
                    "LayerId": "1c8fed40-f4a9-4210-bb44-ed60068a53f9"
                }
            ]
        },
        {
            "id": "2b44ede6-32c9-4019-a65f-42b08fae2cec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "compositeImage": {
                "id": "5571075e-92e9-4130-9289-f481726f1ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b44ede6-32c9-4019-a65f-42b08fae2cec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ad456d0-eb08-494a-816e-a6fbfaed556b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b44ede6-32c9-4019-a65f-42b08fae2cec",
                    "LayerId": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d"
                },
                {
                    "id": "75e1a762-7dbe-4a54-ad96-aa5679ac45e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b44ede6-32c9-4019-a65f-42b08fae2cec",
                    "LayerId": "1c8fed40-f4a9-4210-bb44-ed60068a53f9"
                }
            ]
        },
        {
            "id": "b091a70a-95e2-4b5e-a9cf-0fe1e30a09bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "compositeImage": {
                "id": "c9eadb22-64ed-4573-b5e6-02fd8f14e6f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b091a70a-95e2-4b5e-a9cf-0fe1e30a09bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f358b73-9f12-4cd9-b084-083bbc8d1608",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b091a70a-95e2-4b5e-a9cf-0fe1e30a09bc",
                    "LayerId": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d"
                },
                {
                    "id": "68411c53-7be4-4d87-940d-8fcb87ba3c48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b091a70a-95e2-4b5e-a9cf-0fe1e30a09bc",
                    "LayerId": "1c8fed40-f4a9-4210-bb44-ed60068a53f9"
                }
            ]
        },
        {
            "id": "a4e2fbcf-7f51-4cb4-a03b-cbe85eb34b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "compositeImage": {
                "id": "20a9f868-8998-4a5d-88d6-7d73f9207f27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e2fbcf-7f51-4cb4-a03b-cbe85eb34b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c628cb11-5654-489c-8030-c536ea8bf9bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e2fbcf-7f51-4cb4-a03b-cbe85eb34b9a",
                    "LayerId": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d"
                },
                {
                    "id": "26af7a16-db8d-4b16-b477-e1bede1ea68b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e2fbcf-7f51-4cb4-a03b-cbe85eb34b9a",
                    "LayerId": "1c8fed40-f4a9-4210-bb44-ed60068a53f9"
                }
            ]
        },
        {
            "id": "c295536f-66d3-4905-b2b7-567cb28108ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "compositeImage": {
                "id": "48a7118f-d6a8-4c67-9a2e-da058a740551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c295536f-66d3-4905-b2b7-567cb28108ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d46b5a59-32a1-4c72-990a-ef74542d7100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c295536f-66d3-4905-b2b7-567cb28108ab",
                    "LayerId": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d"
                },
                {
                    "id": "097c414c-4fc3-4022-bc66-7a38acca9942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c295536f-66d3-4905-b2b7-567cb28108ab",
                    "LayerId": "1c8fed40-f4a9-4210-bb44-ed60068a53f9"
                }
            ]
        },
        {
            "id": "b1502ebc-c959-46f6-8e8f-f87ac228c522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "compositeImage": {
                "id": "f104edd3-800d-4ce6-9d6f-bb0b7f6db8cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1502ebc-c959-46f6-8e8f-f87ac228c522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "546cb3cb-6912-49d1-9fa0-a4877b4cd93b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1502ebc-c959-46f6-8e8f-f87ac228c522",
                    "LayerId": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d"
                },
                {
                    "id": "dd1e6314-1848-4f49-b96a-39c2cbd6a3f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1502ebc-c959-46f6-8e8f-f87ac228c522",
                    "LayerId": "1c8fed40-f4a9-4210-bb44-ed60068a53f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "1c8fed40-f4a9-4210-bb44-ed60068a53f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "46efeceb-e7bd-4bf3-b9f7-87c1ff205e5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}