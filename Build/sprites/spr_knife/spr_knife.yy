{
    "id": "8d21fbbf-2955-4f0b-ba04-186d33ddf890",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 2,
    "bbox_right": 7,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80f2cc79-2e7e-4ce1-bbc8-2e6157e48c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d21fbbf-2955-4f0b-ba04-186d33ddf890",
            "compositeImage": {
                "id": "dcf3634d-a19f-4e6a-a8ac-197110e95231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80f2cc79-2e7e-4ce1-bbc8-2e6157e48c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "130028c6-fe32-4eca-abac-6f25059f56de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80f2cc79-2e7e-4ce1-bbc8-2e6157e48c4e",
                    "LayerId": "804389a5-5285-4f7a-a23d-6d90495649c4"
                },
                {
                    "id": "e5259122-7af5-435e-a68e-1102e8009af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80f2cc79-2e7e-4ce1-bbc8-2e6157e48c4e",
                    "LayerId": "ddaa3a3d-e56c-41ae-b936-f973445bd303"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "ddaa3a3d-e56c-41ae-b936-f973445bd303",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d21fbbf-2955-4f0b-ba04-186d33ddf890",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "804389a5-5285-4f7a-a23d-6d90495649c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d21fbbf-2955-4f0b-ba04-186d33ddf890",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 11
}