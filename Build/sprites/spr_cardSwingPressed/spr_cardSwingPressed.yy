{
    "id": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardSwingPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c37b1b4e-a02c-4b6d-8b0c-195eb6e8a07c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "337c5dcc-85b0-4b9c-ab67-0c255e24914a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37b1b4e-a02c-4b6d-8b0c-195eb6e8a07c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db0efb38-3a4e-4e50-b685-1d7c9708b38b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37b1b4e-a02c-4b6d-8b0c-195eb6e8a07c",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "4e2a758e-e7ad-44e7-8157-a208cd5712e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "31e1c104-88cb-4de7-adc9-cd2765ac3a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2a758e-e7ad-44e7-8157-a208cd5712e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3333d244-d845-41fd-8d87-2b6a5763c0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2a758e-e7ad-44e7-8157-a208cd5712e6",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "4e32313e-052c-45db-b583-2f5da0c76f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "fa091d2b-e707-4f7e-b5db-7d698efade44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e32313e-052c-45db-b583-2f5da0c76f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff8016a3-393b-48af-811c-45a73032adec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e32313e-052c-45db-b583-2f5da0c76f4b",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "6d325435-1176-49d0-8475-2c1446ff1cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "fe428608-2e2c-48fa-b282-8f9a0475c59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d325435-1176-49d0-8475-2c1446ff1cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133f7e67-da69-4df2-ade2-afbd1963d1c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d325435-1176-49d0-8475-2c1446ff1cf0",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "8141fcae-d019-4da4-97a9-b84d39607720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "342174a7-98ae-4a5c-b26a-548781ca2f5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8141fcae-d019-4da4-97a9-b84d39607720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68f45bb0-40bf-4baf-8f91-051604d098c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8141fcae-d019-4da4-97a9-b84d39607720",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "c09413c7-e31b-4c25-8cd9-70dd53f4d096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "d563ea25-c94b-4b51-8215-f2d358145d6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c09413c7-e31b-4c25-8cd9-70dd53f4d096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b06a7c0-6153-41eb-874e-21b2784cb050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c09413c7-e31b-4c25-8cd9-70dd53f4d096",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "0c54ee1e-9c57-42d3-895d-4cebecec2edb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "4e48def8-db26-4482-98c7-1fe2f3c19622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c54ee1e-9c57-42d3-895d-4cebecec2edb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d266d005-14b1-4d68-9c67-a8dd79c303bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c54ee1e-9c57-42d3-895d-4cebecec2edb",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "5809f362-0cf2-4078-a631-aac8d9e9e4d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "94501dcd-734d-42cd-885c-5d9962a67b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5809f362-0cf2-4078-a631-aac8d9e9e4d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "326d62a1-1eb1-44de-9106-5822cd5fd37c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5809f362-0cf2-4078-a631-aac8d9e9e4d8",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "0a654a7d-cae5-4793-b955-b6025a306876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "7743b315-165d-4658-bd24-1526da5b14be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a654a7d-cae5-4793-b955-b6025a306876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482b2d90-3030-4ff8-9fa5-22c5ddb0d216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a654a7d-cae5-4793-b955-b6025a306876",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "4dabf3d0-fe6a-4dba-aa56-6ba615f0d947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "61e21918-0cec-4108-9342-46f3a333dad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dabf3d0-fe6a-4dba-aa56-6ba615f0d947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11e46a7-8e72-4d2a-bda3-1dac6db38ab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dabf3d0-fe6a-4dba-aa56-6ba615f0d947",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "4d3337ae-dadb-494d-b7b0-a56b200ecc7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "f02f9e90-3627-4b34-9588-9603b8471669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d3337ae-dadb-494d-b7b0-a56b200ecc7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c6031f-d6a2-44d5-9b35-888007f80e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d3337ae-dadb-494d-b7b0-a56b200ecc7d",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "f01da32c-ae29-43e6-b5b9-8a1acc3c07dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "dff6bb2b-db55-4784-b475-51c03aaa4b07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f01da32c-ae29-43e6-b5b9-8a1acc3c07dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b28db1-e6e6-40fb-a187-2e773f8c60aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f01da32c-ae29-43e6-b5b9-8a1acc3c07dc",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "6b733c1d-ff1b-4c63-a862-7dcc6fad2c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "5e1807e6-51b5-42ef-a81d-fa9809b83f5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b733c1d-ff1b-4c63-a862-7dcc6fad2c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b75c96da-7eea-4ab9-b055-3b21c637458e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b733c1d-ff1b-4c63-a862-7dcc6fad2c76",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "17d81256-4c49-4e81-b1ac-6844e558b653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "926e7550-c13d-479d-ac5e-817b6ffa972f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17d81256-4c49-4e81-b1ac-6844e558b653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c3a5a8f-5fd2-4085-a1c7-add494d964ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17d81256-4c49-4e81-b1ac-6844e558b653",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "6d30d0d2-e169-4865-a55f-1f35930a72cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "a1812e79-91c7-4d47-b69b-f0afb5a7582a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d30d0d2-e169-4865-a55f-1f35930a72cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfc79f10-39bb-4a69-b305-414e13fd8047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d30d0d2-e169-4865-a55f-1f35930a72cc",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "8fa4d530-e7bb-4a6d-ae0f-f58cacd04949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "97bbb492-8dbb-460c-9069-e1726d71a819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fa4d530-e7bb-4a6d-ae0f-f58cacd04949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7128448e-0944-4ae6-a892-00e3adbaf8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fa4d530-e7bb-4a6d-ae0f-f58cacd04949",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "c053702b-c2b5-47b6-ba43-005614480a30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "958005eb-415f-44d1-866c-a5c765697c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c053702b-c2b5-47b6-ba43-005614480a30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "399a542e-fee0-4280-b497-964273fcc1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c053702b-c2b5-47b6-ba43-005614480a30",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "415da82c-c810-4ddf-a094-a6712674eded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "a886334b-7c0a-476a-910f-feff99a9980d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "415da82c-c810-4ddf-a094-a6712674eded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3474f77-6248-48c7-af7a-3e86184fb7e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "415da82c-c810-4ddf-a094-a6712674eded",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "d326216e-69a3-4fce-8cd5-f0b577546f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "3220223a-56c6-4bc8-9026-5692ac88e091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d326216e-69a3-4fce-8cd5-f0b577546f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34a468fb-c276-4308-b3fe-4055894b3c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d326216e-69a3-4fce-8cd5-f0b577546f28",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "553bcec2-f5ec-4f20-a6aa-0567a290cfbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "37bd7f98-5c2a-4e00-b938-1aaf6067ab28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553bcec2-f5ec-4f20-a6aa-0567a290cfbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca79a7ad-deb8-4f03-a4eb-5c37835feee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553bcec2-f5ec-4f20-a6aa-0567a290cfbb",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "23af37a3-33b5-40a1-a041-3f0f3631d2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "7d27b338-caec-4cf9-b64d-3ab9fe0b567e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23af37a3-33b5-40a1-a041-3f0f3631d2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e911e6-99bd-4a4f-becd-1c6b4a6775f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23af37a3-33b5-40a1-a041-3f0f3631d2c3",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "78ae7e1c-04a6-407e-9bd0-7c1a438035a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "899d1ade-f07b-4f6e-ac50-7fc3b341f977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78ae7e1c-04a6-407e-9bd0-7c1a438035a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b4e157a-8e26-45ca-8ac2-c0c9921226cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78ae7e1c-04a6-407e-9bd0-7c1a438035a3",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "ee2f9bcd-682e-4fa0-ba5e-9d2224a2399e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "ad081e68-a5a1-42ec-bc50-dc498d0c657e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2f9bcd-682e-4fa0-ba5e-9d2224a2399e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8337d5fa-0d42-4261-8507-1485590836e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2f9bcd-682e-4fa0-ba5e-9d2224a2399e",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "6b542fa4-c34f-4f4a-ae3d-66055205b8c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "bad2d388-96da-47db-a118-b307823e700a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b542fa4-c34f-4f4a-ae3d-66055205b8c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a7bf1ff-65ed-4d00-973f-24529e84ce57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b542fa4-c34f-4f4a-ae3d-66055205b8c2",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "d0b2559e-6453-4d67-b0df-207d16f144bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "fefb2638-fe3a-4c90-af67-67fc33e811c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b2559e-6453-4d67-b0df-207d16f144bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6f280e-0bf9-48ee-963e-dee69ac7218c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b2559e-6453-4d67-b0df-207d16f144bf",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "7b551013-73c6-4c18-8e7e-8e7ffe82ca0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "899fead9-8e8b-4403-ab3b-4717f77a576b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b551013-73c6-4c18-8e7e-8e7ffe82ca0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a469980f-bf54-4513-a4a7-5667e7a78abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b551013-73c6-4c18-8e7e-8e7ffe82ca0a",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "d3c80ca6-0833-43f7-b871-7f5a106629c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "450b339f-e7b4-40dd-8162-01ed4610fad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3c80ca6-0833-43f7-b871-7f5a106629c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "853afb73-8355-4ef9-800d-2c111fabe6f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3c80ca6-0833-43f7-b871-7f5a106629c1",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "6eb9a0f7-681b-4251-831b-0b28548edc00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "45e0ddaa-7c93-44f0-a80b-03b18d279c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb9a0f7-681b-4251-831b-0b28548edc00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64fc4b1e-c165-4744-9b77-ee6830534b09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb9a0f7-681b-4251-831b-0b28548edc00",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "c9fdf614-4bf3-4f44-88c1-51b546e60b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "879ee424-41db-4e12-b24f-7bec469ee756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9fdf614-4bf3-4f44-88c1-51b546e60b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac68044b-f6f9-4cd3-affc-6d3133040e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9fdf614-4bf3-4f44-88c1-51b546e60b42",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "c7bfad50-5a76-4224-8f2f-f083ed981257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "afa62d88-408a-4926-90ba-116db279a281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7bfad50-5a76-4224-8f2f-f083ed981257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8430c4aa-3b0c-4a13-aac4-cbcc52c8a909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7bfad50-5a76-4224-8f2f-f083ed981257",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "13f2d5a9-6d04-48c4-9df0-db192bebdb78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "09103bb9-109f-4da7-9d78-592e70a3d10e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13f2d5a9-6d04-48c4-9df0-db192bebdb78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a0f835-275b-4b03-99c8-9f1b2066a8dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13f2d5a9-6d04-48c4-9df0-db192bebdb78",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "f6945f12-210e-48a2-8fd5-a4761dc2f3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "0a375080-cb87-4d47-913f-6cddf622cf38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6945f12-210e-48a2-8fd5-a4761dc2f3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5fe24d1-8a40-459f-8e9c-d24d787c112e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6945f12-210e-48a2-8fd5-a4761dc2f3db",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "18f838ca-bb1d-4e6e-aaa0-bf0256801a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "10fbefa3-74ed-4d68-9ff1-69634b6ae85a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f838ca-bb1d-4e6e-aaa0-bf0256801a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a3dbdc4-d9ec-4a09-af20-85169f298bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f838ca-bb1d-4e6e-aaa0-bf0256801a7a",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "61ef5d87-d5bf-4425-abff-abe5698a4563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "36fb21be-67c4-4f1e-a14e-88a1b5f4e6e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ef5d87-d5bf-4425-abff-abe5698a4563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6a4adf-bdff-4526-9054-d4119c5244c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ef5d87-d5bf-4425-abff-abe5698a4563",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "2cbebcfc-13e6-400e-8ce6-148b7faa757d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "8fab4bfd-c606-4ba6-b609-1e9af902f5a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cbebcfc-13e6-400e-8ce6-148b7faa757d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff235219-2905-45e8-9adf-d71f4534be5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cbebcfc-13e6-400e-8ce6-148b7faa757d",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "93d337ae-b0d3-42be-be9b-f4d9dd2efb90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "16c55ad0-07cb-4f0c-bc29-5621b95f2ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d337ae-b0d3-42be-be9b-f4d9dd2efb90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c77e6a-bc80-4e23-9dc1-63de28efef92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d337ae-b0d3-42be-be9b-f4d9dd2efb90",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "59997be6-d1b9-4290-8497-4fa0048a35da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "7347da0a-4d8a-42ee-a806-591b185867b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59997be6-d1b9-4290-8497-4fa0048a35da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7cb2078-4f9a-4ad8-ba29-aa424fb8ab02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59997be6-d1b9-4290-8497-4fa0048a35da",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "2b58f0eb-ba0b-432f-a800-570a53c912a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "cc85e7c8-5141-489f-aece-445a32a02678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b58f0eb-ba0b-432f-a800-570a53c912a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd26c31a-deda-4b79-a2e8-b6381ddb5a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b58f0eb-ba0b-432f-a800-570a53c912a6",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "cace8f13-3dad-42a1-a5b4-c80532685268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "b4fc0979-5752-418e-b470-f034e3ad631b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cace8f13-3dad-42a1-a5b4-c80532685268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76171337-cf97-46bb-b53f-e39337324cab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cace8f13-3dad-42a1-a5b4-c80532685268",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        },
        {
            "id": "73fd7ea7-256c-489a-80f2-68f4e38f98f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "compositeImage": {
                "id": "220d31cf-f089-4440-99c5-2155d50bebb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73fd7ea7-256c-489a-80f2-68f4e38f98f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "add1bf25-639b-471e-8f78-c656bb7d9c94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fd7ea7-256c-489a-80f2-68f4e38f98f7",
                    "LayerId": "bc57d28e-c33b-4f3a-9876-f89a8309327d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "bc57d28e-c33b-4f3a-9876-f89a8309327d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11f4c249-2fda-4c32-8bc7-052ce5f1a61c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}