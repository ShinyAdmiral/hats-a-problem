{
    "id": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flowerWarning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a50d2c7-182c-4070-b408-165d2a0f6434",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "e15922f3-3a7a-470c-980b-c9c689ed1e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a50d2c7-182c-4070-b408-165d2a0f6434",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c43e90-4670-4c65-8a10-c98155c08b36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a50d2c7-182c-4070-b408-165d2a0f6434",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "562e8843-2360-410b-94a8-fda5aeaa9099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "9cdc30ca-926d-466b-9469-a9503582dc8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "562e8843-2360-410b-94a8-fda5aeaa9099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a088dd3-427a-495d-9852-08406e31d582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "562e8843-2360-410b-94a8-fda5aeaa9099",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "4fd63e5a-8986-4ed0-aaa3-f05a70730b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "eedf264d-cf5d-44e6-9898-0e445a15ba35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd63e5a-8986-4ed0-aaa3-f05a70730b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b367c21-2164-4d55-820f-ff429be0f15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd63e5a-8986-4ed0-aaa3-f05a70730b0e",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "07db896a-6086-430c-8c3a-5355ad46d60f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "8be92d91-3586-436e-a1c0-e08710152f05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07db896a-6086-430c-8c3a-5355ad46d60f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56743b87-7cce-4cbe-90f9-76d899b9c199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07db896a-6086-430c-8c3a-5355ad46d60f",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "3a28285d-5b59-4852-aa22-866f32f721ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "722712ae-6461-4b0c-8013-98f5bc986c4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a28285d-5b59-4852-aa22-866f32f721ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "359152e3-4c67-489e-90e2-94936517fa18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a28285d-5b59-4852-aa22-866f32f721ac",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "0449fc5a-a61f-4e61-a93b-aa76c73f26d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "5117e77c-47b2-4376-9bd9-0478a60b6e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0449fc5a-a61f-4e61-a93b-aa76c73f26d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e20216-24ad-42cc-a4de-2fdefa9c6150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0449fc5a-a61f-4e61-a93b-aa76c73f26d2",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "bbbb7ffc-c936-4845-8f3b-7c422de887cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "2a9017e8-0a01-4556-9e6d-8e7f603b125a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbbb7ffc-c936-4845-8f3b-7c422de887cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a4c18b9-5118-4027-bcc7-086d143ef989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbbb7ffc-c936-4845-8f3b-7c422de887cf",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "dfa24ca7-81dc-48f8-b425-35ab99537f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "0267f977-5189-4c9f-aa2f-e0116eb4668a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa24ca7-81dc-48f8-b425-35ab99537f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9498900-ceb1-454c-b8de-4792fb8cfd5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa24ca7-81dc-48f8-b425-35ab99537f21",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "06ff59e8-255f-4445-946c-6a52faa0c082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "b45354af-a551-439e-b7f4-448e17c7c0db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ff59e8-255f-4445-946c-6a52faa0c082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e4c48dd-4782-4230-9545-4d68a9fe73f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ff59e8-255f-4445-946c-6a52faa0c082",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "5df21ad5-fca7-421f-8a48-00f90264e7dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "fc63c5e4-9618-483d-81a1-dfe7e057c576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df21ad5-fca7-421f-8a48-00f90264e7dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51b4d854-1457-4f03-9230-b17a48d2f012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df21ad5-fca7-421f-8a48-00f90264e7dc",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "6ab62e67-e570-4db8-93a2-32cdcbdb6f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "c6a3460f-82c4-4b83-bf90-b6bfe4f90d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ab62e67-e570-4db8-93a2-32cdcbdb6f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a22de3b-1542-4121-85fc-c411401b35e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ab62e67-e570-4db8-93a2-32cdcbdb6f7b",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "2f11cc96-9d13-4f19-8c7c-2a16b000b886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "f879b891-d3ca-45b0-9956-b934771aed8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f11cc96-9d13-4f19-8c7c-2a16b000b886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d97e1e-02af-4b5f-b913-6433a182d4c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f11cc96-9d13-4f19-8c7c-2a16b000b886",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "ce5a8bd7-4c5e-437d-a974-85acd9d2fbea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "b021d808-ac10-489b-a40c-8b3cad38bebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce5a8bd7-4c5e-437d-a974-85acd9d2fbea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f600adc-5e13-4f6e-8d91-53ef388aff14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce5a8bd7-4c5e-437d-a974-85acd9d2fbea",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "7ccef7fe-7777-43e5-b088-ffbd8ac21cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "c9394055-15c1-4fc4-8ffb-49b34bd45f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ccef7fe-7777-43e5-b088-ffbd8ac21cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "997c292d-365e-41aa-872a-e27694451110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ccef7fe-7777-43e5-b088-ffbd8ac21cd2",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "145b63bb-df70-4dee-ac99-c8487e6b783b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "a18f3e30-99c0-4422-b97a-2402cf4859f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "145b63bb-df70-4dee-ac99-c8487e6b783b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1137ef8-6e8e-466e-a912-1e547f613286",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "145b63bb-df70-4dee-ac99-c8487e6b783b",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "c7a4cf45-cd07-4ede-8408-62cff477c6d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "b092bc19-265e-4641-a93a-22b63d40ddfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a4cf45-cd07-4ede-8408-62cff477c6d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea34625-1595-409f-9d10-e6cde446e23e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a4cf45-cd07-4ede-8408-62cff477c6d6",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "6cd14ed6-eb25-4229-88ec-01290d1d3931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "e5398944-7ea7-4b99-aea0-2d9b83f6e251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd14ed6-eb25-4229-88ec-01290d1d3931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9329a2a6-6dbc-44ea-a0b5-f667731a4077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd14ed6-eb25-4229-88ec-01290d1d3931",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "f897c62d-6784-43d4-af14-a4cc4e6a8557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "6e656f90-96b9-476a-b67c-9637b4519a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f897c62d-6784-43d4-af14-a4cc4e6a8557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5d20df-f656-4b9d-9f56-b2a5379d0e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f897c62d-6784-43d4-af14-a4cc4e6a8557",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "1d66dd6d-2142-4353-8b02-25e23fc514f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "b8d6b409-6080-4d88-9285-39b82d7ac4f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d66dd6d-2142-4353-8b02-25e23fc514f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3464f0fd-0e40-4f5e-9fb1-b6c097e380c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d66dd6d-2142-4353-8b02-25e23fc514f2",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "a4f8afe7-4b03-498c-92c3-a04b350d2073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "38796621-f66e-47fc-8a79-9b01aed779c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4f8afe7-4b03-498c-92c3-a04b350d2073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a19a60b-11f6-4cc4-ad56-a9205c04b1c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4f8afe7-4b03-498c-92c3-a04b350d2073",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "f5667adc-a292-449a-93b5-bb0295a9833c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "40eac1a4-53fe-463d-9685-5cc91e26afa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5667adc-a292-449a-93b5-bb0295a9833c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea920723-c3a9-4631-bcdd-56eafb6de0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5667adc-a292-449a-93b5-bb0295a9833c",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "c4cd5a80-8f5b-4a5b-a075-aabf7ef92086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "eee3e6d8-1a33-46d3-ba2e-a2590191c444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4cd5a80-8f5b-4a5b-a075-aabf7ef92086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2abcfb4d-8b76-410c-83cb-f7b741a5284e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4cd5a80-8f5b-4a5b-a075-aabf7ef92086",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "aa2e7d39-ad36-404b-8dad-933182cb0693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "c53ad93c-b2e9-4a20-a35d-211e485cd780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa2e7d39-ad36-404b-8dad-933182cb0693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4973e8b-0c6b-4465-b044-5a20a184eb33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2e7d39-ad36-404b-8dad-933182cb0693",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "c4dadca9-4ae9-426a-896a-d5b4989a3186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "4a1b9243-1f7a-45c8-a2a2-252731cf2559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4dadca9-4ae9-426a-896a-d5b4989a3186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c552550-96da-4c9c-9ced-4ebb04090c64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4dadca9-4ae9-426a-896a-d5b4989a3186",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "429d4b8d-1103-4c00-af32-ae634c357470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "cbf3db47-49fe-4684-bb84-4d2457a01977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "429d4b8d-1103-4c00-af32-ae634c357470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c85d73-42f3-417c-a168-9085e4e8f0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "429d4b8d-1103-4c00-af32-ae634c357470",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "906cb032-5ac9-4929-87bd-6504f46fb422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "16c4f407-63ba-441d-a7dd-5288ce7067c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "906cb032-5ac9-4929-87bd-6504f46fb422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dfc7ee2-c08b-421a-a60b-8343ffdc82b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "906cb032-5ac9-4929-87bd-6504f46fb422",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        },
        {
            "id": "0eba1bb9-4303-4166-8a75-bf5563e378e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "compositeImage": {
                "id": "2b01b92f-54a5-4abe-b2da-e6965caaf02a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eba1bb9-4303-4166-8a75-bf5563e378e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "779b96b2-a315-4bf7-8cfd-f03d3df4962f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eba1bb9-4303-4166-8a75-bf5563e378e6",
                    "LayerId": "f89024b9-041b-4b7f-aa01-39b019177e4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f89024b9-041b-4b7f-aa01-39b019177e4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 32
}