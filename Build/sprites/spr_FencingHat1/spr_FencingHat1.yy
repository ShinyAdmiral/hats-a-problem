{
    "id": "f7f13ff0-457e-46a4-a60c-6c6f7d391901",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FencingHat1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9af89bcf-e6f5-4a67-b6c9-e4b6c0415b39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f13ff0-457e-46a4-a60c-6c6f7d391901",
            "compositeImage": {
                "id": "3072d88c-bbbe-4b24-b10d-f5a66241f282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af89bcf-e6f5-4a67-b6c9-e4b6c0415b39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1533f906-a385-4809-8f2e-6cf4c6de6d20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af89bcf-e6f5-4a67-b6c9-e4b6c0415b39",
                    "LayerId": "3f5670fb-8615-4036-bb13-5d13059c385e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3f5670fb-8615-4036-bb13-5d13059c385e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7f13ff0-457e-46a4-a60c-6c6f7d391901",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 0
}