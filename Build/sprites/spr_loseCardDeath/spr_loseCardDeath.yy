{
    "id": "ed2e2a55-ebb1-4555-9a43-1bc3b29a08fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseCardDeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96ee2888-8238-44a7-94a0-a2cf49a074f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e2a55-ebb1-4555-9a43-1bc3b29a08fa",
            "compositeImage": {
                "id": "09724c6c-8089-4ad5-8e25-49c4e51dbb10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ee2888-8238-44a7-94a0-a2cf49a074f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05adc8a9-1f1f-48c5-8920-4a8fbaa7bd71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ee2888-8238-44a7-94a0-a2cf49a074f7",
                    "LayerId": "d8416b0a-e52a-4a70-a161-787a48d49505"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d8416b0a-e52a-4a70-a161-787a48d49505",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed2e2a55-ebb1-4555-9a43-1bc3b29a08fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}