{
    "id": "7e96f988-2921-43bc-9421-4d2f6c438197",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardHearts21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ef0ed48-dd49-447c-8420-9247495c26c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e96f988-2921-43bc-9421-4d2f6c438197",
            "compositeImage": {
                "id": "ab21248a-7bec-4aa7-8060-23e85ff20e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef0ed48-dd49-447c-8420-9247495c26c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c10e00-9167-4f41-a754-6a068c49b02c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef0ed48-dd49-447c-8420-9247495c26c4",
                    "LayerId": "95d01b9b-7ccd-4eeb-9e65-c1825bb93a72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "95d01b9b-7ccd-4eeb-9e65-c1825bb93a72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e96f988-2921-43bc-9421-4d2f6c438197",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 12
}