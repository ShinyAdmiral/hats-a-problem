{
    "id": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laugh",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 10,
    "bbox_right": 69,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28c3eafe-6966-4064-8c2a-ad4f49d5138f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "34001bd6-cd4e-43fd-9a0e-db6e3ee08bc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28c3eafe-6966-4064-8c2a-ad4f49d5138f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e17174-574b-4d71-83d5-af61bb511f62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28c3eafe-6966-4064-8c2a-ad4f49d5138f",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "3d0530e7-438c-4e24-878d-0f3cbf31cd3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "a9ec0912-5618-4132-8631-d49715aa076e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0530e7-438c-4e24-878d-0f3cbf31cd3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf0ae577-abff-4237-90bc-94aa1821e707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0530e7-438c-4e24-878d-0f3cbf31cd3f",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "b8dfb545-9f35-4bf4-9af3-048aa1e232ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "ff840bef-427c-450f-8a57-fe973236f3bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8dfb545-9f35-4bf4-9af3-048aa1e232ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c158d93-827a-4909-9db9-61a8414f652c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8dfb545-9f35-4bf4-9af3-048aa1e232ad",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "0c4c2ec7-d27b-4848-955d-b29e2443c21a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "380429ce-582b-4ae3-9686-2c092b4f4bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c4c2ec7-d27b-4848-955d-b29e2443c21a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0855494-163a-40b5-bc42-7b34fe9cc8ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c4c2ec7-d27b-4848-955d-b29e2443c21a",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "82e24edb-9c17-453f-bc4d-e83d64370f63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "2fb409e8-d61a-4985-a87a-82da9b08d75c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e24edb-9c17-453f-bc4d-e83d64370f63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38e5293f-05dc-4b34-869e-2d245cd7fd59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e24edb-9c17-453f-bc4d-e83d64370f63",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "e799a460-7a6a-4c68-ba48-f09d0bccf02f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "f24bf0ed-070c-4fe3-8e6d-10ada477338b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e799a460-7a6a-4c68-ba48-f09d0bccf02f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1564348-504c-4d48-8841-055e669112c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e799a460-7a6a-4c68-ba48-f09d0bccf02f",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "67df380e-e7c9-4347-887d-4ef067faced4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "c0017086-6ca0-4182-8386-039e7148e3a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67df380e-e7c9-4347-887d-4ef067faced4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c72428-01c4-430f-8aef-7b84ee8e9973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67df380e-e7c9-4347-887d-4ef067faced4",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "93a2d46e-59d1-4e77-bfda-b19cee300304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "e9651842-6396-4198-89f6-eefc3f7b6ecb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a2d46e-59d1-4e77-bfda-b19cee300304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f840d2-496a-474b-bcd9-562309fef8f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a2d46e-59d1-4e77-bfda-b19cee300304",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "6cdef6dc-8fbb-4c88-99f8-ddbaee4061b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "0868acd1-20c8-4315-949f-bd5f698aa726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cdef6dc-8fbb-4c88-99f8-ddbaee4061b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf75e6eb-17c4-47f7-8550-4abfe7a51864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cdef6dc-8fbb-4c88-99f8-ddbaee4061b5",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "82e31731-ff0f-4d80-a8e5-6536c6618cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "e7ce5284-f6da-4d5c-ad0f-d35ccd09adc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e31731-ff0f-4d80-a8e5-6536c6618cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "235829f8-5283-40e3-bf0e-588cc1edffb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e31731-ff0f-4d80-a8e5-6536c6618cbe",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "e9d78cea-8ce3-48c6-8660-5a1fa3f79d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "41f0c237-468f-4ffa-a7e5-bb9c70f76fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d78cea-8ce3-48c6-8660-5a1fa3f79d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed38a228-b3cf-440e-be68-35612aeeca9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d78cea-8ce3-48c6-8660-5a1fa3f79d4e",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "d62683d9-8386-4e8e-9a30-80a4abfb881b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "481ccf08-6b17-4cd0-a038-74ca199ba2f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62683d9-8386-4e8e-9a30-80a4abfb881b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5575c7e-1181-48ef-8d1a-01caa3835d8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62683d9-8386-4e8e-9a30-80a4abfb881b",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "d5c72f24-3105-4a97-953b-66eb5ce5cc10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "955f175d-eca3-4a83-9f8a-16bac046cae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c72f24-3105-4a97-953b-66eb5ce5cc10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa79d8d-3f3e-4c6d-94f1-0b0ad0cdd3ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c72f24-3105-4a97-953b-66eb5ce5cc10",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "455a46f5-cb88-4b54-bdaa-c85ab4b0d2b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "568d7bb2-052a-4049-b901-843e97976279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "455a46f5-cb88-4b54-bdaa-c85ab4b0d2b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee364501-7034-40be-bb03-85d33ceeca1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "455a46f5-cb88-4b54-bdaa-c85ab4b0d2b8",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "7fb6371a-3a74-4c10-ab52-9773737c1a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "8bbff642-fcbc-4b9f-b766-9bbb600c7640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb6371a-3a74-4c10-ab52-9773737c1a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db2e0f44-2c12-4bf6-85db-7d598a473631",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb6371a-3a74-4c10-ab52-9773737c1a83",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "02fc0220-7b3d-426d-a85d-9842c558866f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "0857aac0-5e3c-4145-b412-98693bbf2239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02fc0220-7b3d-426d-a85d-9842c558866f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24280fcb-4ac4-47a7-9c19-c790a78c4abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02fc0220-7b3d-426d-a85d-9842c558866f",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "0b802df5-b2c5-4bdf-b6a6-0f74574e805a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "41bb60fe-6c15-40d0-9105-2988ebd12850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b802df5-b2c5-4bdf-b6a6-0f74574e805a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e7f899b-c78b-42bd-a313-7f1fbcc41fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b802df5-b2c5-4bdf-b6a6-0f74574e805a",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "55dbfb59-c6c0-43c4-be43-314da2c34f3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "4f9f0414-d416-49ab-9f24-7ef5d5147f38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55dbfb59-c6c0-43c4-be43-314da2c34f3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03cbf95a-7f2b-4bc0-8967-bc2b260eb25f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55dbfb59-c6c0-43c4-be43-314da2c34f3b",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "bb68938c-d0ce-48b9-9a3d-5ebb2fd98ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "35155d2a-6ca6-406b-b7ff-5df465abefb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb68938c-d0ce-48b9-9a3d-5ebb2fd98ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03d8b056-44f6-42c5-a86e-7affa3534d6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb68938c-d0ce-48b9-9a3d-5ebb2fd98ee6",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "58900b71-130f-43cc-b6bc-d69f82928e80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "d0e0c940-1429-4d7e-b285-c41908c77098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58900b71-130f-43cc-b6bc-d69f82928e80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c10f6e7f-2d02-42b8-9b4a-d04bca0a6c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58900b71-130f-43cc-b6bc-d69f82928e80",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "c2be7a1f-b261-43aa-a0ca-d9d628110b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "954ca969-3eec-430e-b169-d919f22d9cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2be7a1f-b261-43aa-a0ca-d9d628110b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1972f327-0b7f-4563-aa6a-d774c55ba540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2be7a1f-b261-43aa-a0ca-d9d628110b58",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "3851a81c-9a6d-4ae4-a219-9aeae958f995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "94986daf-fc57-4283-8a46-0fdc5bd19275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3851a81c-9a6d-4ae4-a219-9aeae958f995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17041448-4c0a-42e1-a76e-e4cebd9151e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3851a81c-9a6d-4ae4-a219-9aeae958f995",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "01d38342-029c-4995-9a2d-c8175d4a1865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "03f3cdde-c736-448c-8108-a31387272785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d38342-029c-4995-9a2d-c8175d4a1865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937891a3-b61d-43c4-bfb9-591f307c57f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d38342-029c-4995-9a2d-c8175d4a1865",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "c71ef0a0-0a82-4162-bbaf-61efed464525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "d7668b3b-3a50-498e-825a-0ef88d264955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c71ef0a0-0a82-4162-bbaf-61efed464525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0138b87f-5b40-4417-a999-78b070048d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c71ef0a0-0a82-4162-bbaf-61efed464525",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "c37350a8-ef83-4922-adb6-f9758062ce82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "0439f9cb-d318-4116-bddc-dac9564389aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37350a8-ef83-4922-adb6-f9758062ce82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aebd5b07-032d-411d-9d76-9772c4a28373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37350a8-ef83-4922-adb6-f9758062ce82",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "11fc7c82-15da-46b7-b6e2-424303786d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "d697bebd-6bd2-4fc6-b6dd-ac793c30ad50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11fc7c82-15da-46b7-b6e2-424303786d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0324b29-5ddc-47f5-822d-71d505eb68b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11fc7c82-15da-46b7-b6e2-424303786d56",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "5b144296-7b74-4342-9b71-bd9e52287c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "b9086a6a-2114-4762-bc22-f9dfb630d36d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b144296-7b74-4342-9b71-bd9e52287c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbbffced-b76a-4327-a32d-d54c2d21ea6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b144296-7b74-4342-9b71-bd9e52287c84",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "fd71ca34-9f86-4875-832d-a1c309250eaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "1c472773-4426-4eae-9be4-3922cb70b07b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd71ca34-9f86-4875-832d-a1c309250eaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31a7664b-d361-4fca-9f3c-c1c5cd91f037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd71ca34-9f86-4875-832d-a1c309250eaa",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "41285e5e-b784-4fa9-9eb0-aaff9e08c34c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "5e81b31f-48b3-4c6d-aff1-f803a4be12d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41285e5e-b784-4fa9-9eb0-aaff9e08c34c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1911c7d8-0073-4b2c-9120-108b1aeaf0cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41285e5e-b784-4fa9-9eb0-aaff9e08c34c",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "4646d228-ffdc-4c3a-9c2b-9765f8d39222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "243c0ecb-0c4b-48f8-8675-054cf2931c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4646d228-ffdc-4c3a-9c2b-9765f8d39222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2976d291-faef-4575-9694-8b29f2ab7a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4646d228-ffdc-4c3a-9c2b-9765f8d39222",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "bbd186b0-1e62-443c-84a3-85ddc796ac78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "d361ca07-e54e-46d6-bb0c-8191b4ae4a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd186b0-1e62-443c-84a3-85ddc796ac78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfce42d-0cb2-43cc-b658-6f62cd6544ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd186b0-1e62-443c-84a3-85ddc796ac78",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "7f4ef1d4-4c93-4420-9110-135428566255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "50afdfa9-bfa3-4522-b77e-261e71e218cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4ef1d4-4c93-4420-9110-135428566255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2e68744-5dde-4dc0-8748-8a6e29b7b19d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4ef1d4-4c93-4420-9110-135428566255",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "9d5b0bc1-1f13-4317-9d3c-e9ff1b53f3e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "8619b0b8-7c25-43fc-ae1f-7dfe5e8d5a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d5b0bc1-1f13-4317-9d3c-e9ff1b53f3e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ec6614d-ff46-4bad-b64f-ea1e8580b84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d5b0bc1-1f13-4317-9d3c-e9ff1b53f3e7",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "2fa81da5-772a-487a-a951-b6b61d179b87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "8bfbc7ae-1a47-4cb8-92d6-eaf01359f63b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fa81da5-772a-487a-a951-b6b61d179b87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f215c297-d9cd-4563-bd03-e057f582189b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fa81da5-772a-487a-a951-b6b61d179b87",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "06aa90a3-4230-484f-b049-28e6fbd2eade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "f6ad48c2-d74f-4dc0-a430-e9e96aae4f07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06aa90a3-4230-484f-b049-28e6fbd2eade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1371d1d-ce6a-49a5-841e-87275032c892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06aa90a3-4230-484f-b049-28e6fbd2eade",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "fe42dfd8-dfbd-4877-a468-98d901775b6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "f2422c17-ce9b-4b3e-be4a-1fda36e85a00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe42dfd8-dfbd-4877-a468-98d901775b6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffad1f3c-1e34-4325-996d-802d4de1c53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe42dfd8-dfbd-4877-a468-98d901775b6b",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "aab0d3c2-bc1e-4f52-a93a-7ee939a7a0f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "0605b487-b4eb-4ff6-b830-48ac2ae668dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab0d3c2-bc1e-4f52-a93a-7ee939a7a0f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dbfbdc5-e0d9-43d5-899b-5bbf41b1c8e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab0d3c2-bc1e-4f52-a93a-7ee939a7a0f6",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "393ec752-cf0d-40fd-be37-1f454cafb08f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "30518a25-8877-4786-bb53-66e2ab2414fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393ec752-cf0d-40fd-be37-1f454cafb08f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53cea4c0-a78d-4da5-96cb-069e94895ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393ec752-cf0d-40fd-be37-1f454cafb08f",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "222ea262-642d-4384-b95a-e4f20cf30215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "8aa6217b-0874-4053-98f5-3ba96640cfa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "222ea262-642d-4384-b95a-e4f20cf30215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "825d2abc-717b-4da5-9bc7-e10949dcb793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "222ea262-642d-4384-b95a-e4f20cf30215",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "54d8a99a-b877-4ed8-a265-1e62f243b877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "52f70334-5afc-413e-a651-ec20e8431040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d8a99a-b877-4ed8-a265-1e62f243b877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2170540-dea7-4336-8b97-f1a4a2e1973c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d8a99a-b877-4ed8-a265-1e62f243b877",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "6eba5b50-95fb-4c06-b8f3-1e9bdbbce312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "8d8789bf-2f95-414e-9774-1dfac2ffedf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eba5b50-95fb-4c06-b8f3-1e9bdbbce312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb515edd-e722-4166-a0c0-0eb5ec9a53a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eba5b50-95fb-4c06-b8f3-1e9bdbbce312",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "352a03bf-d198-4e50-b5df-0fd179e9b0fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "3b674e2e-6801-424f-951e-589d138f4419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "352a03bf-d198-4e50-b5df-0fd179e9b0fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b05ddd48-153d-4fad-b94f-3ab3bbe4296e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "352a03bf-d198-4e50-b5df-0fd179e9b0fa",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "ccef064c-af7f-4ff0-95d1-c5103af7ae50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "25f65283-0c3b-4b08-8b64-6e0863008081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccef064c-af7f-4ff0-95d1-c5103af7ae50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d264adca-e1fb-4514-b959-0cf542ec9bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccef064c-af7f-4ff0-95d1-c5103af7ae50",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "9ad51a28-6e0e-4bb8-9a5f-e92542c692aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "1fa34748-63c1-4516-9e40-acc4c9afbbeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad51a28-6e0e-4bb8-9a5f-e92542c692aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a22900-84e1-4a3b-8275-7ce6f11b78f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad51a28-6e0e-4bb8-9a5f-e92542c692aa",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        },
        {
            "id": "ce785ded-56c9-4700-8924-2d8b29ae42d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "compositeImage": {
                "id": "9f68f4df-c8ea-47a4-8e92-03838541b94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce785ded-56c9-4700-8924-2d8b29ae42d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919ec2c1-63d5-4462-ac0f-f6c9082310e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce785ded-56c9-4700-8924-2d8b29ae42d0",
                    "LayerId": "bfd416d6-6920-461f-bd1b-625a3abdad65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "bfd416d6-6920-461f-bd1b-625a3abdad65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5c25cd1-4d1a-4e80-aa9b-d7f6dc1751f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 35
}