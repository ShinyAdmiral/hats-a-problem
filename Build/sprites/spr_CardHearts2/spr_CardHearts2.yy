{
    "id": "cac94bc8-91e2-475b-918f-4b0501843b7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardHearts2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e2f2dab-5f1b-4a5f-ad55-898be78f09a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cac94bc8-91e2-475b-918f-4b0501843b7d",
            "compositeImage": {
                "id": "744bc497-1c4e-49d1-ab39-5373b7fd4357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2f2dab-5f1b-4a5f-ad55-898be78f09a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0f58569-2431-454f-af11-65dbbafbc524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2f2dab-5f1b-4a5f-ad55-898be78f09a0",
                    "LayerId": "244b146b-5270-4aa5-83df-6e988b5814fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "244b146b-5270-4aa5-83df-6e988b5814fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cac94bc8-91e2-475b-918f-4b0501843b7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 6
}