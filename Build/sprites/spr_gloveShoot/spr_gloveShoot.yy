{
    "id": "470cb260-e864-4634-893f-63d6d9da383c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gloveShoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "402a4bc5-4fe7-42e8-b066-b19616517e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "778512e9-3a30-4b7e-a1d4-b99a86463c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "402a4bc5-4fe7-42e8-b066-b19616517e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d547ba15-258a-4276-93b9-eafc67bfc616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "402a4bc5-4fe7-42e8-b066-b19616517e40",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "832818f9-1466-4772-847c-56b42f04b4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "257f63a6-b063-4d58-b93b-890ae6b392fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832818f9-1466-4772-847c-56b42f04b4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d746426-191d-4994-afaa-e0a06b14c1ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832818f9-1466-4772-847c-56b42f04b4b4",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "89a25a6c-c3e9-4d0f-92fa-ab396237822c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "d21e8406-7af3-4b48-a1e8-2e66fde7c9ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a25a6c-c3e9-4d0f-92fa-ab396237822c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1504d65-0b8d-4c7e-859d-abe87a5beefc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a25a6c-c3e9-4d0f-92fa-ab396237822c",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "992d5141-dfaa-4c8a-a639-334fa54c2d2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "e834f741-6d45-4454-8667-ebee1c147013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "992d5141-dfaa-4c8a-a639-334fa54c2d2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc529e3d-5b11-44d4-9730-8a0c298697f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "992d5141-dfaa-4c8a-a639-334fa54c2d2b",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "71113892-8700-4e5b-b06c-7dba22b46281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "8f681ecd-f92e-426e-ac64-01925a60d00b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71113892-8700-4e5b-b06c-7dba22b46281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "859ec896-e94d-4de9-8e07-8abeb83d7010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71113892-8700-4e5b-b06c-7dba22b46281",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "855cbbc7-cc1c-47c4-bbd9-038652059567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "386c816b-a8a9-472d-89f4-b734e8b42e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855cbbc7-cc1c-47c4-bbd9-038652059567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3bacac-d0ac-4f39-afe3-c9ccc27d1f03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855cbbc7-cc1c-47c4-bbd9-038652059567",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "50a1386b-5627-43cb-844c-3dd29b1c06d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "41442b36-65cc-4026-95f7-27fb6901da4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50a1386b-5627-43cb-844c-3dd29b1c06d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "678370c3-5f3e-4ff4-a487-b1f67c566959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50a1386b-5627-43cb-844c-3dd29b1c06d8",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "057ebda8-a2ff-4aa6-bd4e-0fdfee6accc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "c468486e-f576-47c0-beee-8cd498008df5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057ebda8-a2ff-4aa6-bd4e-0fdfee6accc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8d7c094-0ed1-4027-b693-7f0eb64d4d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057ebda8-a2ff-4aa6-bd4e-0fdfee6accc5",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        },
        {
            "id": "332aa759-1458-45ce-b33f-361228db1cdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "compositeImage": {
                "id": "d884484e-fdf3-4199-958e-0bb0c647b079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "332aa759-1458-45ce-b33f-361228db1cdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d536da4-44ed-48e8-af12-a1f0d5200690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "332aa759-1458-45ce-b33f-361228db1cdc",
                    "LayerId": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5c0ae8a2-5052-4c4b-91bc-86c9b8b6b994",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "470cb260-e864-4634-893f-63d6d9da383c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 25,
    "yorig": 31
}