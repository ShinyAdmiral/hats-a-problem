{
    "id": "3921d363-2017-4120-8903-b0827b9d4cb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rightHand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 4,
    "bbox_right": 40,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bb973d6-52eb-4660-a644-d8846a8dc736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "5c48e5d3-4668-4a37-aab2-292e707759e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb973d6-52eb-4660-a644-d8846a8dc736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a9e7dd-c31b-43ef-a11c-36f7679f97fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb973d6-52eb-4660-a644-d8846a8dc736",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "b175e506-4d00-4d26-a802-eb3687b90f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "c9caf313-7157-4294-838f-eefcc0d3dc3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b175e506-4d00-4d26-a802-eb3687b90f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150750ab-42aa-4c5c-8934-3ce6fc65a67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b175e506-4d00-4d26-a802-eb3687b90f29",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "0e5711f8-88fa-4748-9f4b-3e4c4cd89ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "b698d661-bb6d-49bb-9cbb-0b1f5b2c1362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e5711f8-88fa-4748-9f4b-3e4c4cd89ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2098182-7db5-432f-a479-5921c248539d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e5711f8-88fa-4748-9f4b-3e4c4cd89ead",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "b470c739-27a6-47ea-87dd-04f81d119f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "1fe46717-23b8-47c8-b84d-a4e6d3e4dfcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b470c739-27a6-47ea-87dd-04f81d119f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d159c45-f84b-4316-a39b-3d48caa063b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b470c739-27a6-47ea-87dd-04f81d119f6f",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "cf5d0eca-531c-4519-be93-833aa39f9f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "11007152-beaf-4f1d-9ad0-6c24396ee195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf5d0eca-531c-4519-be93-833aa39f9f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbff708f-2d24-4441-b173-0cf920d3d1fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf5d0eca-531c-4519-be93-833aa39f9f5b",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "33723d5e-a464-432b-b1e8-005aecb55d16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "8eb0830a-0314-4d5c-908f-b1bc0d02215e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33723d5e-a464-432b-b1e8-005aecb55d16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ad45ae8-9a96-49f0-b22b-8a7d3c9ef045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33723d5e-a464-432b-b1e8-005aecb55d16",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "3510db57-4812-443f-be08-3121ff33a740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "dd71acdf-6a71-45f7-82d9-a8022d5d73d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3510db57-4812-443f-be08-3121ff33a740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "665fd715-0643-4e8d-a9e4-319f197d84de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3510db57-4812-443f-be08-3121ff33a740",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "e2549cd3-f726-4574-9c4c-01fe1524e170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "43d19863-12ca-4a88-a3fa-67bc9b2ce904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2549cd3-f726-4574-9c4c-01fe1524e170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548a245b-2403-442c-9e38-12806b50f499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2549cd3-f726-4574-9c4c-01fe1524e170",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "fd4ac923-2536-4e6b-8384-dcf119e4a9de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "3e4f267a-6646-48f4-934b-52d6f30ee2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd4ac923-2536-4e6b-8384-dcf119e4a9de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "828611b9-fecc-4267-87b1-20784a4a13f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd4ac923-2536-4e6b-8384-dcf119e4a9de",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "8e032ce5-7233-4ce5-a780-c516b889f67d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "3f5e3588-7608-42cd-ab1f-1f4032dae64d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e032ce5-7233-4ce5-a780-c516b889f67d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2069ad9c-57f3-4c3c-ac6e-db7c5a38b023",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e032ce5-7233-4ce5-a780-c516b889f67d",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "1e78b31d-0f0c-47ef-b63d-611c73b56c7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "f0f39230-607a-4a26-b048-895a433ce230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e78b31d-0f0c-47ef-b63d-611c73b56c7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b92ea272-3dc6-4bc4-8d62-2a3072dfc4b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e78b31d-0f0c-47ef-b63d-611c73b56c7e",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "8ba69841-6b89-4178-98a0-0599c0ddc094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "20a4f02b-2cfc-4749-a337-f69fd7cd7acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba69841-6b89-4178-98a0-0599c0ddc094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6965cd5-29ad-45ee-a7c8-b01ab310e53c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba69841-6b89-4178-98a0-0599c0ddc094",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "43ec29c8-fe18-4019-8431-82be12908f80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "f2241edf-d3ff-4599-a8ca-2dc3aef384b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ec29c8-fe18-4019-8431-82be12908f80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071b5b31-607e-4910-aa04-4bf096f97f5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ec29c8-fe18-4019-8431-82be12908f80",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "973aa2f5-eae9-4a1f-8e1d-d2d4f29505a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "27f86cd3-7176-4c33-9fd2-8a6b283d1ed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "973aa2f5-eae9-4a1f-8e1d-d2d4f29505a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207b8f11-3fcd-4cda-bef4-9174fc8f0aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "973aa2f5-eae9-4a1f-8e1d-d2d4f29505a4",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "7fd58c98-456b-4481-88c3-ee45ca9e9149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "bfb6f34b-b6b4-48b1-8da8-fe59fcb28ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd58c98-456b-4481-88c3-ee45ca9e9149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31c26cd0-c2a7-40fa-861c-db4f37535b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd58c98-456b-4481-88c3-ee45ca9e9149",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "b675e6ac-4d22-464b-b56b-4aa76fcb1bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "f2a8dbbe-439b-4d94-8428-afd7f9b0cd37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b675e6ac-4d22-464b-b56b-4aa76fcb1bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47efd157-bb2b-4099-842a-fea09549bbaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b675e6ac-4d22-464b-b56b-4aa76fcb1bba",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "815ca969-f598-4baa-bb63-842386d2b9ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "8282c65a-6dd3-4ea4-951a-f7f11188c5ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815ca969-f598-4baa-bb63-842386d2b9ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e5dcb94-bc66-4dde-8706-35c066f14054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815ca969-f598-4baa-bb63-842386d2b9ec",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "8a8b860e-0dd0-4da0-b18a-d348925f2ffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "9d2a1eae-5162-415a-b68e-defbcd8d8b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8b860e-0dd0-4da0-b18a-d348925f2ffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8b46de-a9f8-41ba-8bd5-9525e4ad45d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8b860e-0dd0-4da0-b18a-d348925f2ffa",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "839da74b-a9c5-4ce5-9830-ceb3ed99221f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "fe64eb17-d308-420e-9c90-d4176bb7f34a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839da74b-a9c5-4ce5-9830-ceb3ed99221f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14819ca0-9010-4dc0-8bd5-73542ae9a42f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839da74b-a9c5-4ce5-9830-ceb3ed99221f",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "87eb034a-6671-487f-91c9-a11c15eb4d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "3de51e75-69ef-4153-b9c8-9a145c91cfaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87eb034a-6671-487f-91c9-a11c15eb4d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60939acb-55f8-4bb6-be48-ffceb8f947ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87eb034a-6671-487f-91c9-a11c15eb4d4f",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "1cd55b3b-26a1-459e-8b3e-f9815c6a3eff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "e205a64d-d7a2-4038-a99e-c4a9c9631fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd55b3b-26a1-459e-8b3e-f9815c6a3eff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae52abbd-e33c-4f58-b5ca-c66c35f15041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd55b3b-26a1-459e-8b3e-f9815c6a3eff",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "497389c8-4327-489b-859d-302daa05cc0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "e62aae0d-b407-41c9-a4d2-89af1bc6d754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "497389c8-4327-489b-859d-302daa05cc0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47a12c38-7d8e-46ef-8a82-ec61ca71bee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "497389c8-4327-489b-859d-302daa05cc0b",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "7840ea6f-01b3-440d-9900-19777faa4e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "f49538c5-89c9-4465-b64e-05cf457795af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7840ea6f-01b3-440d-9900-19777faa4e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48cbd95e-0024-41c3-803a-2351ff0c1376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7840ea6f-01b3-440d-9900-19777faa4e7d",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "140333a2-a9c2-4056-8711-80c82fdc3140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "3999b02b-f817-4851-9bdf-bb58a85062c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "140333a2-a9c2-4056-8711-80c82fdc3140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8606078-9e2b-4ab6-962a-4d4d309ac3cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "140333a2-a9c2-4056-8711-80c82fdc3140",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "609c3ea5-8f6a-4f43-85e7-48f5d718431c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "e587251d-5bb8-4234-a6a5-ef7d294c2737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "609c3ea5-8f6a-4f43-85e7-48f5d718431c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f48e50d-012f-401d-a8fb-e698c449d47d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "609c3ea5-8f6a-4f43-85e7-48f5d718431c",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "59014e34-802d-4fb6-aacd-59b324b1f2b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "c7de0ae1-7ea5-4177-b21f-c4adfaebbc49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59014e34-802d-4fb6-aacd-59b324b1f2b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7fee4db-9e7c-4572-9456-71bdb42c47cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59014e34-802d-4fb6-aacd-59b324b1f2b2",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "171185df-a76e-4959-8918-e966eaa8a7d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "c33d5060-7bd8-4cfc-b413-62409e9125bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171185df-a76e-4959-8918-e966eaa8a7d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b04908b-50dd-47bf-a22e-e8fcb5d56265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171185df-a76e-4959-8918-e966eaa8a7d3",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "56ca317c-db17-4205-85c5-f4e58e75e4c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "4b01cce0-cba9-496d-b3d4-cad2e44820ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ca317c-db17-4205-85c5-f4e58e75e4c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1480016-8f17-4682-917b-01d0d52c8b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ca317c-db17-4205-85c5-f4e58e75e4c7",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "0947efb0-db44-484c-9659-60b8e20c1a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "1669bb05-2509-4620-8c82-f14ce0cc638f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0947efb0-db44-484c-9659-60b8e20c1a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a5d60ae-8d4e-4091-817e-1c88d141c2fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0947efb0-db44-484c-9659-60b8e20c1a16",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "79894917-7483-42d7-b620-faabc0060ef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "c1af0146-e22e-4767-b229-48b0471eb129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79894917-7483-42d7-b620-faabc0060ef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65eb3bb5-eca5-4365-ad9e-3ab4bc17d54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79894917-7483-42d7-b620-faabc0060ef9",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "6a43d4f2-fc07-41c7-b69b-9138ac26af1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "a01b07fe-ca8f-4189-9b63-208017ab3100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a43d4f2-fc07-41c7-b69b-9138ac26af1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3e6e20-e6ab-4b4f-820c-1787271a8181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a43d4f2-fc07-41c7-b69b-9138ac26af1c",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "cdab68cf-e07f-42e8-9dbd-a747717d6521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "1cf1aa35-34d0-498c-9082-34c6db553055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdab68cf-e07f-42e8-9dbd-a747717d6521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72b37a5b-b8a4-440c-b6f2-b73b34099ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdab68cf-e07f-42e8-9dbd-a747717d6521",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "3052b012-3393-48b5-8dde-6059dccd399e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "a6d3f4b5-a387-4505-b9fd-2e4f9279437b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3052b012-3393-48b5-8dde-6059dccd399e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "effa8c80-dbd3-4099-820e-7b36f366940e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3052b012-3393-48b5-8dde-6059dccd399e",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "818b043f-e02f-45b2-bde4-6ce77142a4a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "edd2b099-a505-4ae9-8272-aa2d3637b73f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "818b043f-e02f-45b2-bde4-6ce77142a4a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7d9f66-78c8-4ceb-acb5-ebf2081a8a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "818b043f-e02f-45b2-bde4-6ce77142a4a4",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "8faec5a5-d656-4ed3-bd8b-55b252261a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "f118ff0e-8451-499c-ac25-ac34de12c590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8faec5a5-d656-4ed3-bd8b-55b252261a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0805687c-7a00-4ac6-a776-7b1e7f18302c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8faec5a5-d656-4ed3-bd8b-55b252261a89",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "3f86a87d-861f-4471-8336-7aaef719aadc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "049cdc3c-dd74-46b7-8373-2b753af2bbf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f86a87d-861f-4471-8336-7aaef719aadc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88b9c01b-b1b2-4709-9eb4-5448b57893f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f86a87d-861f-4471-8336-7aaef719aadc",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "1fd72c56-93e6-48f1-b90c-b08a43358cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "d8d9e676-32b2-4a08-a218-eb988588a545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd72c56-93e6-48f1-b90c-b08a43358cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c53ad62c-047f-40a7-93f2-1d0596f6a08a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd72c56-93e6-48f1-b90c-b08a43358cd2",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "8dea5438-b6c8-4f9b-8c2a-425989125ab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "f0b531b0-2c1d-4790-b74c-547af07dff0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dea5438-b6c8-4f9b-8c2a-425989125ab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b7fac23-e78c-44c2-b119-dc2373890f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dea5438-b6c8-4f9b-8c2a-425989125ab8",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "6244f655-c28b-461e-aa59-168299e16010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "e6f2abed-fc90-4af0-86ea-95e2b7ee052e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6244f655-c28b-461e-aa59-168299e16010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41184167-521d-4487-adbd-d153898111c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6244f655-c28b-461e-aa59-168299e16010",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "ee44f7aa-2991-454d-85a1-d9914b1e1408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "cd30c886-ad1f-460a-9957-96c4dc2b8750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee44f7aa-2991-454d-85a1-d9914b1e1408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e65bc3-89e1-4118-805a-285a194522d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee44f7aa-2991-454d-85a1-d9914b1e1408",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "5909e293-645f-43e6-b457-c66894333d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "3d9062d2-a7f9-4df2-9901-6083f36d423c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5909e293-645f-43e6-b457-c66894333d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811a5a55-983e-46c4-a899-b73d4f70e089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5909e293-645f-43e6-b457-c66894333d0d",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        },
        {
            "id": "453b4133-c7ae-437c-b0f6-08f07420ecee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "compositeImage": {
                "id": "7e996ec6-4258-42a8-b8f9-14a1bb8f926f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "453b4133-c7ae-437c-b0f6-08f07420ecee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef380ac-08ba-42f0-8915-7bbb88b00824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "453b4133-c7ae-437c-b0f6-08f07420ecee",
                    "LayerId": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "8769a91f-32a5-4ddf-bc87-113bfad2c7fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 25
}