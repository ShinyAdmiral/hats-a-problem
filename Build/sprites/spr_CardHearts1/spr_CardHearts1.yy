{
    "id": "35d3751c-3d2b-4c34-a63a-d72a5bd3d28e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardHearts1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f930ff3e-14c7-42e3-8808-ef538d9b8b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35d3751c-3d2b-4c34-a63a-d72a5bd3d28e",
            "compositeImage": {
                "id": "47ce43df-d22a-4ac3-b340-b91c917bb38a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f930ff3e-14c7-42e3-8808-ef538d9b8b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfc531e7-a5eb-4249-895b-524fefb9c24a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f930ff3e-14c7-42e3-8808-ef538d9b8b3e",
                    "LayerId": "ce5d61b4-219f-4f1a-831c-b8a60dbe3c4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ce5d61b4-219f-4f1a-831c-b8a60dbe3c4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35d3751c-3d2b-4c34-a63a-d72a5bd3d28e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 12
}