{
    "id": "857d7c4e-2e3a-485c-944e-9c427755bcda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fda4cf97-769d-438d-9cf9-8dc810065706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "857d7c4e-2e3a-485c-944e-9c427755bcda",
            "compositeImage": {
                "id": "8f701e07-5e37-47ab-bb1b-b17f907ed945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda4cf97-769d-438d-9cf9-8dc810065706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63bcd43a-c266-419d-bc6c-6311aeb89444",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda4cf97-769d-438d-9cf9-8dc810065706",
                    "LayerId": "ae2646a3-a7f2-428b-b2be-cf879a25aa90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ae2646a3-a7f2-428b-b2be-cf879a25aa90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "857d7c4e-2e3a-485c-944e-9c427755bcda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}