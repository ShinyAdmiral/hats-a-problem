{
    "id": "dd9e5dfb-2947-4b54-a673-b532f1f88d55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4067459-4ed9-4680-9348-a305ee3f1fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd9e5dfb-2947-4b54-a673-b532f1f88d55",
            "compositeImage": {
                "id": "1587f61f-084f-4b69-949c-6844dac375c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4067459-4ed9-4680-9348-a305ee3f1fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41280ec7-7fe1-42c4-b645-58a2106260a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4067459-4ed9-4680-9348-a305ee3f1fed",
                    "LayerId": "08d0b0ec-ad98-48dd-94d0-fb0ed26a063d"
                }
            ]
        },
        {
            "id": "c25ef63e-0750-4ae5-831b-c6e321ee8bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd9e5dfb-2947-4b54-a673-b532f1f88d55",
            "compositeImage": {
                "id": "2bf6e715-ed6a-4eb0-a56c-2e25d1e2706c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c25ef63e-0750-4ae5-831b-c6e321ee8bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "107be223-1845-465d-aa94-7894634d287d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c25ef63e-0750-4ae5-831b-c6e321ee8bdb",
                    "LayerId": "08d0b0ec-ad98-48dd-94d0-fb0ed26a063d"
                }
            ]
        },
        {
            "id": "0dfcc54a-ba86-4eb9-af4a-37110aba0f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd9e5dfb-2947-4b54-a673-b532f1f88d55",
            "compositeImage": {
                "id": "497b3b48-fccd-4a18-97da-eb48b58d44f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dfcc54a-ba86-4eb9-af4a-37110aba0f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e062e2d-4b09-42b4-bc70-77242f62184a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dfcc54a-ba86-4eb9-af4a-37110aba0f13",
                    "LayerId": "08d0b0ec-ad98-48dd-94d0-fb0ed26a063d"
                }
            ]
        },
        {
            "id": "ae79e8a8-12a8-4265-b6e7-6e0e83c1cb5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd9e5dfb-2947-4b54-a673-b532f1f88d55",
            "compositeImage": {
                "id": "9d5b9381-bdf2-481d-b100-5124149e5800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae79e8a8-12a8-4265-b6e7-6e0e83c1cb5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad89c166-2d16-485f-9796-6e2037fdaa35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae79e8a8-12a8-4265-b6e7-6e0e83c1cb5f",
                    "LayerId": "08d0b0ec-ad98-48dd-94d0-fb0ed26a063d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "08d0b0ec-ad98-48dd-94d0-fb0ed26a063d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd9e5dfb-2947-4b54-a673-b532f1f88d55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}