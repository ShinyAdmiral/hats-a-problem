{
    "id": "be0a939f-29e6-4c7e-82a7-ac3b927f19b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite221",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88b22cfd-f181-4299-8f8a-a7548757d346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be0a939f-29e6-4c7e-82a7-ac3b927f19b8",
            "compositeImage": {
                "id": "497c0772-7e7c-4faa-923e-ce0fef73f9af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88b22cfd-f181-4299-8f8a-a7548757d346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a65d21-b67e-4d57-ba35-3df4a4981bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88b22cfd-f181-4299-8f8a-a7548757d346",
                    "LayerId": "eb99a0d7-0829-4dd5-a82c-edf531e660c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "eb99a0d7-0829-4dd5-a82c-edf531e660c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be0a939f-29e6-4c7e-82a7-ac3b927f19b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 0
}