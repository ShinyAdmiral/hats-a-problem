{
    "id": "1f23a773-310e-4a8c-8560-4df0b9d0aad0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heart1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b57dab6-0a9b-4f64-91dc-14e0a177043c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f23a773-310e-4a8c-8560-4df0b9d0aad0",
            "compositeImage": {
                "id": "493ec4d2-9762-4bc2-9973-47de2fc62b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b57dab6-0a9b-4f64-91dc-14e0a177043c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eeb42bc-8111-47bb-a04f-e8fb54ba0e03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b57dab6-0a9b-4f64-91dc-14e0a177043c",
                    "LayerId": "23852ea8-69a7-4cd9-8656-c6ab33782d78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "23852ea8-69a7-4cd9-8656-c6ab33782d78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f23a773-310e-4a8c-8560-4df0b9d0aad0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}