{
    "id": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05c48983-9173-4a14-815d-dc8130b0d559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ee41d6e2-65b3-4eea-8b06-f9901a67abfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c48983-9173-4a14-815d-dc8130b0d559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b5ae25-14d3-4821-8918-d2d27b158c43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c48983-9173-4a14-815d-dc8130b0d559",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a8a4dcf3-78b3-4e57-81a7-a2a3d1d70dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a8c2d00c-3798-451b-aeb3-6c251a259d39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8a4dcf3-78b3-4e57-81a7-a2a3d1d70dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7622eb67-d910-4875-99db-4d0e879368e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a4dcf3-78b3-4e57-81a7-a2a3d1d70dc8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a68c65e7-96b2-4ca9-aa53-f5768aab10bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "510bf93e-3fd3-4172-a2b3-45aa21c0b2a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a68c65e7-96b2-4ca9-aa53-f5768aab10bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55dba5fe-0cdb-4b6d-aa8a-4e54d392ec99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a68c65e7-96b2-4ca9-aa53-f5768aab10bc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3d48c616-5002-4bae-b1cd-472a6bdeeba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "352fafdd-ef48-4af1-a42b-77f47569721c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d48c616-5002-4bae-b1cd-472a6bdeeba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb872de-2643-48c8-ae06-700f1b15f2f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d48c616-5002-4bae-b1cd-472a6bdeeba3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9a0eca07-de7a-4736-97fd-f1887d4acdb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "dda3cb97-72a6-4f95-905c-d2021b5318cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a0eca07-de7a-4736-97fd-f1887d4acdb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36b9d04c-80bb-44b9-b313-986a894a708f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a0eca07-de7a-4736-97fd-f1887d4acdb4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "afa60ad4-e950-4b7f-a5a3-05026f921dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0de68a21-322a-4f98-9867-0b2ea611047c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa60ad4-e950-4b7f-a5a3-05026f921dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b1c936-754d-4e93-bd09-fdac770bb5ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa60ad4-e950-4b7f-a5a3-05026f921dfc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d76d30fd-08ef-40b2-9b9e-ca91c270fdec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7d8a6f31-f043-48b4-91f8-84c3e78d6916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d76d30fd-08ef-40b2-9b9e-ca91c270fdec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "725a2542-ecec-4e40-a22a-0f84fa97d06c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d76d30fd-08ef-40b2-9b9e-ca91c270fdec",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "92b8d2d6-e287-4e6b-90fd-b4fe1d0fde1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "eb13ae86-8aa5-4f87-8942-1f86823f3901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b8d2d6-e287-4e6b-90fd-b4fe1d0fde1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54478d7a-5da8-42d3-ab04-13ddf1d82a02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b8d2d6-e287-4e6b-90fd-b4fe1d0fde1f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2540cb3a-8fac-4298-8f9a-83ad674ec671",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "58e25d62-122a-42b0-9d32-4a6473fe6ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2540cb3a-8fac-4298-8f9a-83ad674ec671",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "392624c3-1f7c-4cef-90bd-4ce79afb408c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2540cb3a-8fac-4298-8f9a-83ad674ec671",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d6a09eb9-d293-426a-bdd3-e49c4781cb5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "014aebee-ab81-4ae0-9b3b-e455ec0b74cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a09eb9-d293-426a-bdd3-e49c4781cb5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6153567-ecf4-4bbc-91fe-c89edff21ff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a09eb9-d293-426a-bdd3-e49c4781cb5c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "21a2c311-ef3d-4d3d-b23e-3315db948562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "527445f1-3fc6-4cb7-809b-30862dd7210f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21a2c311-ef3d-4d3d-b23e-3315db948562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9e0c851-13ba-4317-a1c4-7733cc6c3167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21a2c311-ef3d-4d3d-b23e-3315db948562",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "84471135-9375-472d-b2b6-eece7d688801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d99429fd-3e50-4cb1-a890-dd4e18ab5c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84471135-9375-472d-b2b6-eece7d688801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f446806c-6eff-476f-8315-30a7e4ac8c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84471135-9375-472d-b2b6-eece7d688801",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "55714b7c-7172-4daf-a305-fac654c4ce16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "4b17e362-a1e8-47a3-a5c9-d561698ae0af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55714b7c-7172-4daf-a305-fac654c4ce16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf6e3693-3dd0-4b6b-87a0-26a80e1f56dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55714b7c-7172-4daf-a305-fac654c4ce16",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9563712e-156e-4b77-901b-61a218340b25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "64c0193b-a1ee-4096-8723-54808e20b0a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9563712e-156e-4b77-901b-61a218340b25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40ef3c8-f815-47b9-8a7a-88fb37935496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9563712e-156e-4b77-901b-61a218340b25",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2effbdaa-67b5-461a-8ebb-6746b2d17f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f4fc4cc9-b196-4593-8dc3-bc3660663a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2effbdaa-67b5-461a-8ebb-6746b2d17f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc1b403-34d4-4ea5-8aa7-b0e7eac9b103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2effbdaa-67b5-461a-8ebb-6746b2d17f10",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b1708b2c-f1da-4224-a5ce-63847b3b86c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f903a1f3-35ad-483f-a05d-0d999867a357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1708b2c-f1da-4224-a5ce-63847b3b86c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b36dc7-b447-47c2-a7a4-f8415f11f60f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1708b2c-f1da-4224-a5ce-63847b3b86c4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5fa6751a-cf18-46e3-8539-b7ab54d4ae46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b9742a56-9147-4a14-9766-800378a4981e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa6751a-cf18-46e3-8539-b7ab54d4ae46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6868e271-8c2d-474a-84e4-3cc791cbaa5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa6751a-cf18-46e3-8539-b7ab54d4ae46",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "08136990-8591-48ec-b35e-4baca080d482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8cb0fd48-7dd2-4483-b145-33c1d5f02063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08136990-8591-48ec-b35e-4baca080d482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d428f08-c7a9-4595-be4e-48ff63f99d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08136990-8591-48ec-b35e-4baca080d482",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "19f77517-ab4d-44a6-9114-c6c2580c0b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2428eec4-6b23-4e7f-915a-87e2053d1c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f77517-ab4d-44a6-9114-c6c2580c0b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc4eaaf8-1675-4019-97ca-8c467fbfb383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f77517-ab4d-44a6-9114-c6c2580c0b72",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8f660ee8-fd11-469c-841e-5421ff9e66cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "eb80b3ad-9f87-45a2-bf60-675e8e981492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f660ee8-fd11-469c-841e-5421ff9e66cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f6a1a6-de4c-401a-a892-9896af3b1654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f660ee8-fd11-469c-841e-5421ff9e66cf",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "607c8589-546f-45b8-a219-1d565a55c14a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "100f916a-db4f-409f-b7c5-de0e5c2c19e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "607c8589-546f-45b8-a219-1d565a55c14a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae1f4df1-8057-4548-ae68-6b918cc287c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "607c8589-546f-45b8-a219-1d565a55c14a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b85057fb-288d-454f-8a44-f41264b08f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0f6f4d7c-c316-4748-a762-0b1c9901e008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b85057fb-288d-454f-8a44-f41264b08f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3db6426-34b7-4e91-bd2b-e3eb91e141ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b85057fb-288d-454f-8a44-f41264b08f71",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0808bf9c-80f4-4153-9d39-52233bc225e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3733ee9e-0f91-431b-9315-0805f8d9230b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0808bf9c-80f4-4153-9d39-52233bc225e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57685b1a-d236-4adc-ad4e-250b936c9e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0808bf9c-80f4-4153-9d39-52233bc225e4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "64a8f2c6-ba2e-409f-aa40-94a01c4482a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cf382a57-c884-43f2-814c-66e8182c0f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a8f2c6-ba2e-409f-aa40-94a01c4482a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4c4eec-5d67-49ac-95cd-2aa6c98ad0b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a8f2c6-ba2e-409f-aa40-94a01c4482a7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b091e9f7-c00e-4f27-abef-418dcc9b66f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b2eb544a-9124-432f-8a3d-16488599a316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b091e9f7-c00e-4f27-abef-418dcc9b66f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2af0c89-9221-472e-a171-4de4a9dd2715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b091e9f7-c00e-4f27-abef-418dcc9b66f3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0e4bf3f8-dc2a-4174-9a35-486f84284aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3c6219fb-f3a6-423a-80ce-251484287d51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4bf3f8-dc2a-4174-9a35-486f84284aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38883bcf-85b3-4dae-96cd-a5d21f27cf41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4bf3f8-dc2a-4174-9a35-486f84284aea",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4edb0d9e-10e3-44d9-bd9a-dde6a2004240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "18954fdd-dcd0-4764-b8fa-3fb60f9d456f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4edb0d9e-10e3-44d9-bd9a-dde6a2004240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e25bb033-900d-45f4-a443-3fe62970c79b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4edb0d9e-10e3-44d9-bd9a-dde6a2004240",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5341e3de-da69-4c22-b2f8-4bb25f169205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "81bcc97b-da3e-4a27-8f3f-18138c28a117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5341e3de-da69-4c22-b2f8-4bb25f169205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1956072-8db2-4a2e-baad-4d204e3f8cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5341e3de-da69-4c22-b2f8-4bb25f169205",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b5a0bd0e-5a3f-4bf0-b54d-c9ae38f95462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "dcb58677-7e1b-4810-953e-84b475321deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5a0bd0e-5a3f-4bf0-b54d-c9ae38f95462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb751edc-9e41-4519-8d6f-daefbd07e938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5a0bd0e-5a3f-4bf0-b54d-c9ae38f95462",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2a1acb5c-6f96-478e-8a61-15c2ea5a0c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "668763d5-ff09-470b-986a-c998f5daad63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1acb5c-6f96-478e-8a61-15c2ea5a0c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc22b7b-6926-4df6-974d-bedaff9cead1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1acb5c-6f96-478e-8a61-15c2ea5a0c4b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "136680cc-3720-4e58-a84e-65300b5369b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b6217a1d-685f-4fdc-b9ef-7ec655c912d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136680cc-3720-4e58-a84e-65300b5369b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad33f38-adcd-45f8-9e03-9533127cabf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136680cc-3720-4e58-a84e-65300b5369b0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fcf72cfc-abff-46b2-8167-7e3b606f3e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b704d40b-61b2-42c6-97e9-e7760977bd82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf72cfc-abff-46b2-8167-7e3b606f3e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692dc27b-8c21-433b-8c7a-92b0316ce82a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf72cfc-abff-46b2-8167-7e3b606f3e59",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "12f76fa4-3309-4458-a570-00835bbce4b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "68e7d100-bae7-4616-afe0-dafc3410a180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f76fa4-3309-4458-a570-00835bbce4b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "effdd05e-e547-449e-8f81-6e57f58a760b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f76fa4-3309-4458-a570-00835bbce4b3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "955422b6-f4ef-4816-af82-138b35a4239e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ade822c3-23e6-4d3b-80fa-c3f38e368571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955422b6-f4ef-4816-af82-138b35a4239e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afc4815a-4320-4a03-96f6-94878afad2e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955422b6-f4ef-4816-af82-138b35a4239e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ca666c59-a9fe-47a9-b5fb-d21945581729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bb445aab-7dd8-4531-b2de-422d5ec8edf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca666c59-a9fe-47a9-b5fb-d21945581729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7946bd4-14fb-4ae1-8d88-ad186cd51c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca666c59-a9fe-47a9-b5fb-d21945581729",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a721ac0c-2dce-48cc-95de-9d18796d7f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f7fef6e7-8618-461e-aaff-c11e316f718a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a721ac0c-2dce-48cc-95de-9d18796d7f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85086564-32dc-4c36-adc7-d8b911e0599b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a721ac0c-2dce-48cc-95de-9d18796d7f40",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "1c219c89-2b64-48b3-bf7d-d01f6d6c5d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "70b0b8b1-d42d-404e-97c1-a152ab18c884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c219c89-2b64-48b3-bf7d-d01f6d6c5d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f0a27e-aab1-4d20-abef-db75c9627540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c219c89-2b64-48b3-bf7d-d01f6d6c5d3e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4b90a297-3597-4a1a-b214-21c688d20ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0d59e5da-c1f0-4d78-b3e4-79ff02f73998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b90a297-3597-4a1a-b214-21c688d20ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8079f6-cee4-461f-9bfd-e96badc64cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b90a297-3597-4a1a-b214-21c688d20ca7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "06329dc5-dc20-45b8-80ff-37e72de8eba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f21b2f03-1d2a-451b-9561-974d28d388c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06329dc5-dc20-45b8-80ff-37e72de8eba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25bd3eec-2a9d-404e-964c-f9759690d19a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06329dc5-dc20-45b8-80ff-37e72de8eba3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b203f086-98b4-4852-87b2-40a06faf8b3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1b6e2f69-2ea5-4c6e-9317-706bcfd5a52f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b203f086-98b4-4852-87b2-40a06faf8b3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0e643b-69e0-4299-a24a-e71a3b04ce3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b203f086-98b4-4852-87b2-40a06faf8b3d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "bc3ef504-973c-4934-aadb-981db4a0fcd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "67197e30-3c51-4b51-8f35-0be90624e97f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc3ef504-973c-4934-aadb-981db4a0fcd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c548d7e8-bdb7-4eb5-85df-016ae4b93a68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc3ef504-973c-4934-aadb-981db4a0fcd9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "87cd56cf-c2c4-4921-badd-c417ebdc9be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ee8bb2ef-170e-4018-9fcb-dd09cb7f1819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87cd56cf-c2c4-4921-badd-c417ebdc9be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44deaff0-7716-4fdc-a674-8f0aa57963d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87cd56cf-c2c4-4921-badd-c417ebdc9be2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a30ed8ac-5697-4c21-90c3-b18fe0ee4504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3f47222f-61ed-4245-b73b-1a7f638d32f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30ed8ac-5697-4c21-90c3-b18fe0ee4504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a5d088b-bfac-469d-a539-af04bbf7790a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30ed8ac-5697-4c21-90c3-b18fe0ee4504",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "1d2e5b5c-b2bb-4235-8e52-e0ba52e77ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ffd86834-79dc-4cf6-8c48-a60d538ae4db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2e5b5c-b2bb-4235-8e52-e0ba52e77ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b6f5ba-a9ab-4a91-81f1-c1e4a4c168fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2e5b5c-b2bb-4235-8e52-e0ba52e77ebd",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "28ff4300-a8d7-4064-ae5f-9d8a1e2871bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cb8a15dd-7d99-4a0d-812f-c31fa0bc64df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ff4300-a8d7-4064-ae5f-9d8a1e2871bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21199f61-97c0-4b62-8433-371789984bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ff4300-a8d7-4064-ae5f-9d8a1e2871bd",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "849ca044-2cbe-4a3c-bbd5-3805bbcbcea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b414c0df-aa78-4374-82bb-483c123a3bd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "849ca044-2cbe-4a3c-bbd5-3805bbcbcea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "715616df-4f26-4b91-8173-16a16ed8091c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "849ca044-2cbe-4a3c-bbd5-3805bbcbcea7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "af35571e-d7ab-4e79-b66c-cd70ad40da0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3789e96e-8d20-4a96-a127-7454880c5906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af35571e-d7ab-4e79-b66c-cd70ad40da0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b775cee-5cb1-402c-abbd-b55a5c01971f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af35571e-d7ab-4e79-b66c-cd70ad40da0b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4f992c6c-6466-4a3f-9ecd-b78921e5cab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "53a733b3-5879-4bc7-8ce7-71ebf61bc427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f992c6c-6466-4a3f-9ecd-b78921e5cab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "499dbfc9-c996-481a-81eb-c4ac39647f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f992c6c-6466-4a3f-9ecd-b78921e5cab4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b58b25ca-29f1-4401-a5ff-9cb73cbe6d37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "79021254-cad4-46f3-ae0b-e292e380f1bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b58b25ca-29f1-4401-a5ff-9cb73cbe6d37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48f8bde3-3441-4ede-ac94-2fd8973ee1e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b58b25ca-29f1-4401-a5ff-9cb73cbe6d37",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "41aa235e-0595-4f59-96ed-5eee715234ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "441da3e1-687f-4598-973d-6394098dcecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41aa235e-0595-4f59-96ed-5eee715234ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3479ff2d-24e3-45e0-91cc-9e4408c3f0de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41aa235e-0595-4f59-96ed-5eee715234ad",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ce467245-730c-4914-a48b-a230908c5629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fd1acd2d-02ac-44b7-a9d3-922bd66bd49a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce467245-730c-4914-a48b-a230908c5629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eff4816-cdd7-47e8-bef0-0981ad12f1c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce467245-730c-4914-a48b-a230908c5629",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "acf959a1-17a8-4d61-90ce-f4c75968bcdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "de5f4f2c-659a-4b7e-a352-3eee6001af4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acf959a1-17a8-4d61-90ce-f4c75968bcdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa1ea70-5c98-4871-809b-89dca4b5af2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acf959a1-17a8-4d61-90ce-f4c75968bcdb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ab759910-f760-4672-88de-78669de51d33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "642e5cac-4e0b-4e76-a32f-1be537ffa821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab759910-f760-4672-88de-78669de51d33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dda9239-3265-4710-a334-98f6752cb17b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab759910-f760-4672-88de-78669de51d33",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "97eb2eec-cd95-49f7-9e36-c928ba7ddc5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bc8ea63a-504d-4edf-8cd0-b97d94d2b094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97eb2eec-cd95-49f7-9e36-c928ba7ddc5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34443a16-70df-42d0-bad4-4ef362ad6d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97eb2eec-cd95-49f7-9e36-c928ba7ddc5c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "65d5d6b1-20bc-4678-947f-ef7ce7944b53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1ffbce85-4674-4c36-8088-aca628e468d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d5d6b1-20bc-4678-947f-ef7ce7944b53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84be9534-c9a9-4b1a-a6d8-e18aac7cbbb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d5d6b1-20bc-4678-947f-ef7ce7944b53",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "317b825a-7d2f-4e69-a483-2f02234674f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0b4c2265-0ef0-4698-b47a-0bdcbd2ea25c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317b825a-7d2f-4e69-a483-2f02234674f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a776b25-5111-439a-aacc-0710d620a6b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317b825a-7d2f-4e69-a483-2f02234674f2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "cd90f35e-044b-43fc-a608-a343bbe32c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "33aa4b53-0171-4170-ac0a-bd14af8712a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd90f35e-044b-43fc-a608-a343bbe32c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bccc77c4-93df-4be2-8bcf-e4a9fdb131dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd90f35e-044b-43fc-a608-a343bbe32c53",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2f8c9ce7-0593-48df-b34c-175115e69eb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "93546645-8714-47a6-978b-5c26f92dc5d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f8c9ce7-0593-48df-b34c-175115e69eb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d312b7-647e-4871-8d09-55ebb38a5fba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f8c9ce7-0593-48df-b34c-175115e69eb2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f90d4cd2-5afe-4d71-9e1e-2505d2472c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "97996842-d106-4294-abfc-df51951775e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f90d4cd2-5afe-4d71-9e1e-2505d2472c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d6ce44f-1de9-4129-b1cf-ba9a95ca1cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f90d4cd2-5afe-4d71-9e1e-2505d2472c3d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3716a121-fbb2-4d0a-8a58-8e6a02075eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "47a1c96f-a7d2-42cd-9d7e-c4219c15bd67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3716a121-fbb2-4d0a-8a58-8e6a02075eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46420a85-a974-4d77-ab40-8462549bd1ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3716a121-fbb2-4d0a-8a58-8e6a02075eeb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fc25741b-180f-4a74-9ce6-77cad97ff7a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "77170946-84f0-4e7b-be37-9bbfdad5b785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc25741b-180f-4a74-9ce6-77cad97ff7a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a964436-af49-4dcd-a15e-bd9466086d0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc25741b-180f-4a74-9ce6-77cad97ff7a5",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "33afa094-5151-476d-afb6-be9bc74666c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0c36a5fd-9c7c-4226-9e32-ce5fd2f5ae94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33afa094-5151-476d-afb6-be9bc74666c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2c01e1-1971-4a00-9f3f-58f157561462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33afa094-5151-476d-afb6-be9bc74666c9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f07a4dc7-1619-4cae-a53a-36aa5f25d41b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "29fadda7-c391-4b15-b1d9-2b5a39c1d6ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f07a4dc7-1619-4cae-a53a-36aa5f25d41b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73b1f08-d1f6-4ea6-aafe-25b29795e0dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f07a4dc7-1619-4cae-a53a-36aa5f25d41b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "37f8e6ff-7c4f-4d90-ac28-d030f059e067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "52f9e31e-27d8-454f-abbe-e40f39db31ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f8e6ff-7c4f-4d90-ac28-d030f059e067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c8a46f2-982d-4189-a87b-c2ad8a9e452d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f8e6ff-7c4f-4d90-ac28-d030f059e067",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c5d458f1-fbed-4987-88f3-71ac2117ee88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6d4f45ab-64ec-4685-a3e5-20121492ad00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d458f1-fbed-4987-88f3-71ac2117ee88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d248ab-9fab-4eff-8bc4-f0785b4f3c6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d458f1-fbed-4987-88f3-71ac2117ee88",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "741b5c3f-58e2-4a3c-9add-22195601a9b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d2fb6c80-da2a-495f-9808-4cc4c0257008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "741b5c3f-58e2-4a3c-9add-22195601a9b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598f369a-2677-4ecd-86cf-40b578410cc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "741b5c3f-58e2-4a3c-9add-22195601a9b1",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7caca33d-071e-42f7-acbe-811b95f5f0e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "15f1e943-17ec-4fb3-8ca8-f83a2dee076c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7caca33d-071e-42f7-acbe-811b95f5f0e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95de4faa-ea4e-472b-952c-6810853a9ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7caca33d-071e-42f7-acbe-811b95f5f0e5",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "523b0c83-0377-4332-9217-38a5581a9e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "19ae429b-4c59-49f3-873a-c0c2b0fb066e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523b0c83-0377-4332-9217-38a5581a9e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8af04ac7-9fa6-46b1-be56-dc9e88715e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523b0c83-0377-4332-9217-38a5581a9e8e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "19951013-f6a9-4f1c-9213-2c1a84d09158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8c766f56-1b83-45ad-ae0d-c7265559bd51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19951013-f6a9-4f1c-9213-2c1a84d09158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8770db95-52c3-4ee6-9fbe-ef49fd4d753c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19951013-f6a9-4f1c-9213-2c1a84d09158",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e54ffb13-ac9a-4f3a-a555-ff511a2a8340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "aeca33f0-a527-4d8b-8664-5d077d2303ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e54ffb13-ac9a-4f3a-a555-ff511a2a8340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bec3c6e-4061-4254-b725-d0f3af9ba58e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e54ffb13-ac9a-4f3a-a555-ff511a2a8340",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d4df8669-cfac-4bf9-9feb-1365f810a1fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "088f2988-ef31-4610-b5ec-516dae537f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4df8669-cfac-4bf9-9feb-1365f810a1fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52681fda-a67d-4bf1-b83d-bb50e9250130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4df8669-cfac-4bf9-9feb-1365f810a1fa",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "782a90ec-ac36-4803-a55d-81d942b7a32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8d747b09-cd3b-434c-b484-513b1395168d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "782a90ec-ac36-4803-a55d-81d942b7a32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "597c9621-01a7-466c-8960-4a314d3926da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "782a90ec-ac36-4803-a55d-81d942b7a32c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c848aee6-3b0c-4606-8085-202693b10759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "30d86393-7b2f-4637-b1a4-eb907f793877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c848aee6-3b0c-4606-8085-202693b10759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b752357-8b5e-433c-91d3-54a89600714a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c848aee6-3b0c-4606-8085-202693b10759",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "19ce1e2f-5868-457c-8dbe-a4dfddd7af50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a288e9e4-4d5f-4470-9511-a468371b1b56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ce1e2f-5868-457c-8dbe-a4dfddd7af50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0156c9-b2d3-4368-b981-dc1b4f3099b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ce1e2f-5868-457c-8dbe-a4dfddd7af50",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "cc0fb1dd-e8d5-443e-a976-d45734fe026d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "532ca9d3-28dd-4a04-af15-5a5306849c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc0fb1dd-e8d5-443e-a976-d45734fe026d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095a493f-e2f1-4eea-ba50-4d9ea952fc73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc0fb1dd-e8d5-443e-a976-d45734fe026d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "1fd1bef8-e1dd-43db-9993-fe72b73d8fdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c17bb9a0-56c3-4c8f-b390-968f3d2b80dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd1bef8-e1dd-43db-9993-fe72b73d8fdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "332002f8-ef82-4e82-a3ed-6f99908dfa00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd1bef8-e1dd-43db-9993-fe72b73d8fdf",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2ffca0ae-7bdc-4eff-b030-7bcb0b123463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a20bc7f4-b6a8-46ed-a9c9-3e3e2e46565d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ffca0ae-7bdc-4eff-b030-7bcb0b123463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "434da549-0225-4c96-8a9c-60fe1b63c99d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ffca0ae-7bdc-4eff-b030-7bcb0b123463",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6623169d-477c-43b4-b246-ead656a7553e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "46395f21-366b-45c5-87b7-d69642b985c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6623169d-477c-43b4-b246-ead656a7553e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8813f0-5a77-498d-906f-34e77e85dd27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6623169d-477c-43b4-b246-ead656a7553e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fd325352-d010-4651-8862-aaaa12fdac6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9a8d3dd8-6374-44fb-89c1-a4957c3a589b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd325352-d010-4651-8862-aaaa12fdac6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "786569bb-8cec-4629-a59a-113a4ddc7d7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd325352-d010-4651-8862-aaaa12fdac6f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3f5d6d7c-e923-4fca-8c14-88de4890c19a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "efe3d8df-cfc0-4e59-b3b5-89cdad71cd50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f5d6d7c-e923-4fca-8c14-88de4890c19a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1059a91-7352-4edc-a081-a5ffed342e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f5d6d7c-e923-4fca-8c14-88de4890c19a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8726d950-85b4-4507-ac30-2de12f473054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2e5cc85f-ce1a-4b18-afdc-b9a574700a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8726d950-85b4-4507-ac30-2de12f473054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf36e850-f36e-465c-964b-fe3df4476f0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8726d950-85b4-4507-ac30-2de12f473054",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "97227b55-5ac9-466c-8d1a-de93caa00ee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9577ff8e-c1a7-49ca-97a6-86126b00435d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97227b55-5ac9-466c-8d1a-de93caa00ee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1604a915-5758-4be2-9077-fbf9f902ac6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97227b55-5ac9-466c-8d1a-de93caa00ee0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "22bba194-01d1-490d-9d5e-ccc6161c933c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3acee70f-c4a3-44e3-8e8a-c2b76c733cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22bba194-01d1-490d-9d5e-ccc6161c933c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f975ff5c-9311-421e-83aa-5ddb8c043171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22bba194-01d1-490d-9d5e-ccc6161c933c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "69ce741d-0553-45e7-bc88-900a9ec76b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "093e8c27-2688-4e6b-8a3a-25c2019f799c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69ce741d-0553-45e7-bc88-900a9ec76b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f63f0cf-72d2-46f5-aff7-a64be692070a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69ce741d-0553-45e7-bc88-900a9ec76b08",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d6509842-34b3-4d72-8f27-7291482d5ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7938138a-610a-4e3f-87f8-87431bdc3764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6509842-34b3-4d72-8f27-7291482d5ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8660d1a2-82ba-4889-a336-222f3452223b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6509842-34b3-4d72-8f27-7291482d5ae4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "49c11673-3de4-4f91-97c8-647980668da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ace9e9ca-d12f-430c-9276-3660abfac930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c11673-3de4-4f91-97c8-647980668da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a37ccc-af31-4318-bd22-1c86c73c4b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c11673-3de4-4f91-97c8-647980668da7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "788a3b85-4cd0-4b96-ac5f-2568cafc5a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3f010f10-7bbc-4dcf-93ea-6c5c7060cea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "788a3b85-4cd0-4b96-ac5f-2568cafc5a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b19b4b-b72f-48b9-b58d-a95293ce0710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "788a3b85-4cd0-4b96-ac5f-2568cafc5a78",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c592499f-a56e-4b7b-b76b-540c12067fc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "78243e8c-5e2a-47aa-9b4d-46ff5bf52d92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c592499f-a56e-4b7b-b76b-540c12067fc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8382d01f-1a92-43b4-8cc9-1927547c4506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c592499f-a56e-4b7b-b76b-540c12067fc0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e9999c91-d6a5-4d2d-8c4d-49ee6e7d7d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "03826b78-6af4-499b-8421-0afb11b12a9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9999c91-d6a5-4d2d-8c4d-49ee6e7d7d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ee30637-b771-4a34-9b56-c95797bf431e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9999c91-d6a5-4d2d-8c4d-49ee6e7d7d3e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "53db559c-7e9a-4959-a150-d64e06a353c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5cadd767-9f10-4211-8852-b8f2a22a4ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53db559c-7e9a-4959-a150-d64e06a353c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce5f31d-c6d2-4ed5-9c1f-12d9b40da3d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53db559c-7e9a-4959-a150-d64e06a353c7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "76f3f758-4ff4-42c7-9358-ab525afe5b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "32844f53-ec7f-4c70-b28a-f80e6449197f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76f3f758-4ff4-42c7-9358-ab525afe5b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf4083f8-6818-4471-adb8-932b2f1c3a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76f3f758-4ff4-42c7-9358-ab525afe5b38",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0e924e46-df89-4187-8ee2-1ca4fe10f0fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "466c00c5-db88-4189-b1d6-a83b44aece72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e924e46-df89-4187-8ee2-1ca4fe10f0fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aceff56-ad5f-4371-80fc-b1911f786c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e924e46-df89-4187-8ee2-1ca4fe10f0fa",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "344a42ad-1b44-40d0-8933-2d06614af633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "acd79743-12f3-47c1-8810-190ad29545d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "344a42ad-1b44-40d0-8933-2d06614af633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceba93a5-9050-4dc7-aba1-e74fd7631146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "344a42ad-1b44-40d0-8933-2d06614af633",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "27cd7a3c-b6f2-4eb7-b6fc-c994bef487cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f345df97-0a82-48d1-a60d-85436ec01a05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27cd7a3c-b6f2-4eb7-b6fc-c994bef487cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe71d1e-077f-4fc7-82b9-cdc6584a5525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27cd7a3c-b6f2-4eb7-b6fc-c994bef487cc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2d99df66-17ff-402f-8e7f-21b2775c5cfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a117f973-129a-42a2-9e4d-a34ae0c72d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d99df66-17ff-402f-8e7f-21b2775c5cfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66ef42c8-923f-4c70-8a18-bb4352a4892b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d99df66-17ff-402f-8e7f-21b2775c5cfb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "41232ec8-d4fd-40e7-8383-ea07083fc50b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c5b8bc01-4836-4bca-9f6b-74a16aa18af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41232ec8-d4fd-40e7-8383-ea07083fc50b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66885b2a-0edf-4f62-a108-efb193cdf2ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41232ec8-d4fd-40e7-8383-ea07083fc50b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e9f2a18e-f0d5-4904-bad2-abf38abaff87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f6b48fcf-ebf9-43b4-a7c2-c2caa4fb3569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f2a18e-f0d5-4904-bad2-abf38abaff87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cfc3b46-df0d-4c1f-970a-90888977c0aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f2a18e-f0d5-4904-bad2-abf38abaff87",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "1b415420-1ed6-4746-9636-6cf1c697613b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cd02f055-0ad0-462d-b845-4de2eded39b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b415420-1ed6-4746-9636-6cf1c697613b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da09a5a-2d2d-4460-8654-a568ca6974dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b415420-1ed6-4746-9636-6cf1c697613b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6e73efdf-595d-4b20-8081-7d6c60d4ee44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8873c915-5e0f-4346-b731-cede2ee6bb80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e73efdf-595d-4b20-8081-7d6c60d4ee44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "954eda88-909d-4137-9e88-116b123e8f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e73efdf-595d-4b20-8081-7d6c60d4ee44",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "57c2d5a3-d3e5-414e-b0ea-1d80b989dc5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f2516175-0704-40d9-9c8e-aea102fe27e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57c2d5a3-d3e5-414e-b0ea-1d80b989dc5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b735e22-1ff7-42d1-92aa-d2497144e269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57c2d5a3-d3e5-414e-b0ea-1d80b989dc5f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "114ad46b-036a-497f-8d56-5fb892ed4fd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "41d4867b-b723-499b-b42a-e37485e9e120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114ad46b-036a-497f-8d56-5fb892ed4fd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d52ccf8-f25f-4017-8ba9-7a89b447c01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114ad46b-036a-497f-8d56-5fb892ed4fd0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8c1bb0ea-7d06-4f79-9b5b-b89e4f0577a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fe14592d-44f6-45eb-a39d-27717ad6980e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c1bb0ea-7d06-4f79-9b5b-b89e4f0577a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0a858a-c224-411d-8e21-5930d0c18acc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c1bb0ea-7d06-4f79-9b5b-b89e4f0577a1",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "194d8bc3-e593-45d5-a866-dbcce6d499d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f48aa7e9-da4f-42fa-9e67-33f555986b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "194d8bc3-e593-45d5-a866-dbcce6d499d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b691d3a-36c9-4d76-b31b-70911b37e5ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "194d8bc3-e593-45d5-a866-dbcce6d499d3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0cc31fbf-e0d1-49eb-835b-2bbb3527cb10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7a270b63-6565-4bb0-af5d-aed2e79246c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cc31fbf-e0d1-49eb-835b-2bbb3527cb10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dbdf80e-a8d1-4e0c-a47f-8b7e2ac5a241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cc31fbf-e0d1-49eb-835b-2bbb3527cb10",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "515aa03c-fad7-4f09-b70c-c89594ca37c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "91264be7-3363-410d-8b08-414e830efe00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515aa03c-fad7-4f09-b70c-c89594ca37c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f301b38-debc-4c97-9797-358cabf5c038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515aa03c-fad7-4f09-b70c-c89594ca37c7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fbc77210-1bdc-4484-99cf-a929c4954d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fc89bf43-4590-42f7-bc1a-414682645081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbc77210-1bdc-4484-99cf-a929c4954d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e30d77fe-74c8-4bb1-b015-ee5583ab396a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbc77210-1bdc-4484-99cf-a929c4954d83",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e12d2ff4-4dab-450a-95d3-dc7b8871905d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cff317e0-ab81-4c48-8e30-8aa3cda36e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12d2ff4-4dab-450a-95d3-dc7b8871905d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688e5299-0265-47f4-8b23-cd411ede2a9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12d2ff4-4dab-450a-95d3-dc7b8871905d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a9aa3ef0-ef03-4319-ac04-991489cfe234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "90bbcb2f-5fb4-4ecb-92f7-931849d4c7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9aa3ef0-ef03-4319-ac04-991489cfe234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d793195-115a-4f8e-b3d5-0d49985e9665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9aa3ef0-ef03-4319-ac04-991489cfe234",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6bb220cd-31c0-41f5-90b2-ccfbb1125a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0750f105-515f-4682-be15-7b615343f6b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bb220cd-31c0-41f5-90b2-ccfbb1125a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888b2119-1f5b-4140-b1a4-2c1fba0a34d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bb220cd-31c0-41f5-90b2-ccfbb1125a10",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c77b1273-43e7-440e-af5e-0986a0336f93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c20eab40-7e4a-4893-b7e3-5542731ecd7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c77b1273-43e7-440e-af5e-0986a0336f93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "466f9330-a576-4eef-b093-5a9a7039c6a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c77b1273-43e7-440e-af5e-0986a0336f93",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e647f8b0-d8dd-4792-be13-a635e6b76501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d4ef604c-436d-4665-b447-37024acd86f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e647f8b0-d8dd-4792-be13-a635e6b76501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8246491-28e2-4d16-bcbc-8d94d4e19a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e647f8b0-d8dd-4792-be13-a635e6b76501",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b71d9c1a-b827-4d8f-993c-3ea8395c9491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2eef28b0-ce56-4c79-9c00-882a8a533b36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71d9c1a-b827-4d8f-993c-3ea8395c9491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8e68491-ee4f-4f7c-b1ae-9bb97cd361fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71d9c1a-b827-4d8f-993c-3ea8395c9491",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7512ba19-8e0e-4339-b043-6f7c4c8844c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cec6fd56-79bb-4345-8aa8-2c6e05c7b44b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7512ba19-8e0e-4339-b043-6f7c4c8844c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca05a84-2f1c-4b2f-95e3-b94e82e2e8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7512ba19-8e0e-4339-b043-6f7c4c8844c0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9c97603b-6e00-41aa-9021-20805ceec05b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6d4bff14-5894-4c54-8359-a150bb18b4b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c97603b-6e00-41aa-9021-20805ceec05b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "189a33bd-6038-4b45-af5b-f2d251b38c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c97603b-6e00-41aa-9021-20805ceec05b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4493d587-9969-4e9c-8bc4-b18b7dbd080b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "491ed368-cd4d-4d31-a611-0c05df3fd549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4493d587-9969-4e9c-8bc4-b18b7dbd080b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31cfface-a73b-4445-b030-c4c063a9f682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4493d587-9969-4e9c-8bc4-b18b7dbd080b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9415f3e9-096d-4229-91a9-72a1463a03b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0ea790ec-ba57-4161-98d7-4c56a980b9f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9415f3e9-096d-4229-91a9-72a1463a03b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9afee7fa-c304-4c35-99db-a83634b1a184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9415f3e9-096d-4229-91a9-72a1463a03b2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "46b063e7-2c3b-496e-a029-14c6aca1429a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "30904347-dbc3-4b60-83fe-56067b33c704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b063e7-2c3b-496e-a029-14c6aca1429a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6cf9ad-565c-4455-abb0-2999b2c11c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b063e7-2c3b-496e-a029-14c6aca1429a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "43aec2ae-550f-452f-9559-80d58d6764f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a19eb12e-08f8-4be0-af5f-e0ad598be4ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43aec2ae-550f-452f-9559-80d58d6764f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce67752-a61e-465b-8c84-343fc4f70709",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43aec2ae-550f-452f-9559-80d58d6764f5",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "709f70c8-568b-4996-826e-be8b2c8aa697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "dd5b3a9c-05e1-4082-9a50-f6af710e191a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "709f70c8-568b-4996-826e-be8b2c8aa697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da474497-0e2e-4490-b134-fa83f8ca8bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "709f70c8-568b-4996-826e-be8b2c8aa697",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "47bf7f65-9a72-43a7-bc5d-751d2ba52166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6626ed33-6b86-4cb5-8900-4f4e93e6b8c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47bf7f65-9a72-43a7-bc5d-751d2ba52166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c690d56-f97e-4244-bf14-1292ccf3ee10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47bf7f65-9a72-43a7-bc5d-751d2ba52166",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0bfb072c-0735-40cd-b56b-5797b6e35be4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f1e03b31-482f-4d5b-afb5-14e4d06c5ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bfb072c-0735-40cd-b56b-5797b6e35be4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa8fc9a-2832-4f0d-8907-9e956705a954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bfb072c-0735-40cd-b56b-5797b6e35be4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c08069a5-0ab1-49da-ad48-6f52662e2a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2e11146e-718e-489b-a8ca-3555b689e393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08069a5-0ab1-49da-ad48-6f52662e2a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbe4e39f-e751-4a0c-ab3d-1c650f07832a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08069a5-0ab1-49da-ad48-6f52662e2a24",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9a276374-37c1-4d8d-9425-0a1a5a5c624f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fb3d6292-362e-4416-8394-fe54696436fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a276374-37c1-4d8d-9425-0a1a5a5c624f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508c58cd-9ca1-47e7-aacf-1247696d6514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a276374-37c1-4d8d-9425-0a1a5a5c624f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "33d8489e-1649-46e3-b8bb-43acc8f1f982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1d0cd4ce-6b90-486c-afd6-8c7154b2ed38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d8489e-1649-46e3-b8bb-43acc8f1f982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9d993ee-4236-4e2e-908d-8bd89f0476b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d8489e-1649-46e3-b8bb-43acc8f1f982",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "55e4b491-ade8-4baf-9018-cd421d34f394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b2dd8a74-65a7-4b50-9047-644a77bce942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e4b491-ade8-4baf-9018-cd421d34f394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f187f421-70c0-4ee6-8a3b-18495545f936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e4b491-ade8-4baf-9018-cd421d34f394",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4274c24b-cced-4a1d-8e61-6d93f9e82846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2874975a-4997-4836-b778-7e3c5f56d681",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4274c24b-cced-4a1d-8e61-6d93f9e82846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99467b1f-e491-44a8-ae04-0fb1ec5ef714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4274c24b-cced-4a1d-8e61-6d93f9e82846",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b1243bf2-f8a0-406b-b12f-4289f1a29dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a208b734-a20e-46e2-8a7a-5b5b9a968b6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1243bf2-f8a0-406b-b12f-4289f1a29dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ffc0515-b504-41ce-b1e7-1ffb0b78ccb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1243bf2-f8a0-406b-b12f-4289f1a29dd6",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0a8bd485-f178-440f-941c-01e5e2aae16e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "de06a046-59f4-4cd6-b42f-fed0b06eafd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a8bd485-f178-440f-941c-01e5e2aae16e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0260476-5c2d-4564-9afa-ba4f0e2e1333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a8bd485-f178-440f-941c-01e5e2aae16e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ebf86a0e-8adc-479e-b561-65322a5a93f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3667d41d-ab80-44e2-90cd-79d581e8ed3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf86a0e-8adc-479e-b561-65322a5a93f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5553a33b-38e6-4566-a6c7-36a834d1bfc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf86a0e-8adc-479e-b561-65322a5a93f7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "20fb96f0-1111-4760-8527-a3e09d61ad86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "4c03af2d-c3d6-4196-b4f0-5a9f96e85a54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20fb96f0-1111-4760-8527-a3e09d61ad86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2283cb9f-a8fa-49f9-9bea-ed26bd380d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20fb96f0-1111-4760-8527-a3e09d61ad86",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e8033190-7ec0-4fce-9756-90c325fc9596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fff06fb2-6460-4934-bc9f-dcd77fddc8e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8033190-7ec0-4fce-9756-90c325fc9596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f46a2c11-1e63-4fbf-9e21-85a277f7274e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8033190-7ec0-4fce-9756-90c325fc9596",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2ecdbd14-2eb9-4848-a380-3cf2e13bd6fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b879f01a-0226-41c2-80f0-9daf7e22e05d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ecdbd14-2eb9-4848-a380-3cf2e13bd6fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe453929-63d6-4c8c-a1fd-d93e387ce93e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ecdbd14-2eb9-4848-a380-3cf2e13bd6fc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9140a6e5-e363-4349-b5fd-afd5050030e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c3112e20-7ef3-4048-9924-4fa9ea06ebf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9140a6e5-e363-4349-b5fd-afd5050030e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0322f47c-ea6b-4ad8-b03c-b89193e72afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9140a6e5-e363-4349-b5fd-afd5050030e4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "17e20bcd-be85-41c2-8010-97715671656e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "dbe6ced3-1be5-4106-a89b-8065c7176859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e20bcd-be85-41c2-8010-97715671656e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d044e9-795b-4421-96d3-42a664865f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e20bcd-be85-41c2-8010-97715671656e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "1d319531-6824-4d1c-92d9-36a7cb756f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fd905a4b-9121-4a55-9f87-aebd6289e1e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d319531-6824-4d1c-92d9-36a7cb756f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b96c5948-0cb4-4ef8-a13f-9e9e4c4cb186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d319531-6824-4d1c-92d9-36a7cb756f91",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "33bda13b-a082-4be9-a5c8-1e60eb993276",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c6aa8f12-f9c7-4342-8bf1-aa1c0aa866b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bda13b-a082-4be9-a5c8-1e60eb993276",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4ee418-8c7f-4b01-90f3-994722eb2651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bda13b-a082-4be9-a5c8-1e60eb993276",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "52e8e2b7-b26f-4ab4-8fb6-6d598fcb30b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9ead289c-7d60-430d-9d83-45d2b37d397a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e8e2b7-b26f-4ab4-8fb6-6d598fcb30b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "886a411c-264a-4490-be56-b810c55447f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e8e2b7-b26f-4ab4-8fb6-6d598fcb30b8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f5e81bf0-d93f-4dec-ae85-5d1b489cc271",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "95f23658-111a-4771-9349-04a9d69b1343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e81bf0-d93f-4dec-ae85-5d1b489cc271",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d727720-a06f-4e81-bfb0-4886469dea86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e81bf0-d93f-4dec-ae85-5d1b489cc271",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "29bd1da9-079a-439b-8b25-40cda66a2fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "dfd9a9d3-fedb-478d-9818-4c3451da8297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29bd1da9-079a-439b-8b25-40cda66a2fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02647c8e-880c-4156-aa77-985ae245dc3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29bd1da9-079a-439b-8b25-40cda66a2fb4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "20250f22-7dd9-4278-ab6e-93faa7090f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1b1a7a18-709e-4e7f-bf81-e9a3483a09cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20250f22-7dd9-4278-ab6e-93faa7090f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32282880-dbee-48a9-bae7-0c2630d25c88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20250f22-7dd9-4278-ab6e-93faa7090f46",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "daf25486-b9cf-48ff-9d7e-e15cd2bc094b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f88dc9e0-a146-4798-849e-11c8917f4a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf25486-b9cf-48ff-9d7e-e15cd2bc094b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fe99cde-b30b-4869-b3aa-5ad407df8100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf25486-b9cf-48ff-9d7e-e15cd2bc094b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b66fbb61-df2e-4d21-8035-fe87c87c3625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "119cf004-5a57-4aa7-a5e4-47d1a9a52599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b66fbb61-df2e-4d21-8035-fe87c87c3625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bed3b8f-285d-4f4a-8ba1-c6e1f458c99b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b66fbb61-df2e-4d21-8035-fe87c87c3625",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "865ed43c-54d2-4731-b6dc-85d40e5a399f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c0a384e0-e7de-4add-8be2-f1761f11a62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "865ed43c-54d2-4731-b6dc-85d40e5a399f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ecd9aa5-6768-4665-8499-78bcb4995812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "865ed43c-54d2-4731-b6dc-85d40e5a399f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "1a02201d-83e9-4ff3-9405-be45b04b33d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "db13d886-d26a-47ca-8e47-81dcae9c29c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a02201d-83e9-4ff3-9405-be45b04b33d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be140b90-bb53-4439-a2ae-babb3c29e470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a02201d-83e9-4ff3-9405-be45b04b33d1",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f57e4ba5-88d9-461c-acc0-9b9b11e60718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b7d962ad-7d93-4fce-8f67-b7209b8f15ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f57e4ba5-88d9-461c-acc0-9b9b11e60718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4177c0-0dd8-4c7e-9bdd-df50cad1a5a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f57e4ba5-88d9-461c-acc0-9b9b11e60718",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9d5fc7c0-9b54-45e1-91f8-2196f2f30f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "86392a3c-834a-4810-a730-b1c78cd3d105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d5fc7c0-9b54-45e1-91f8-2196f2f30f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6e64a07-d9bd-4789-9fe8-0da1fe5a825b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d5fc7c0-9b54-45e1-91f8-2196f2f30f25",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ddcdca35-fbd6-43c2-8cd3-ada58c7fa027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cead6120-ba76-4a36-b299-ae06fe184a76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddcdca35-fbd6-43c2-8cd3-ada58c7fa027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4888462-839a-46d6-a9bd-57c0a5490bf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddcdca35-fbd6-43c2-8cd3-ada58c7fa027",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ceb3f4c2-182a-46a6-a835-01a038818e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fd5d39a2-868a-42c8-bd08-93a10053c355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceb3f4c2-182a-46a6-a835-01a038818e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c3e07b-62b3-4fcc-9f52-0b66b9c0dec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceb3f4c2-182a-46a6-a835-01a038818e3e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "444329b9-c155-4e43-8f25-69585f74c341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "29842aa0-97c9-431a-b087-ab50eb9b1e69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444329b9-c155-4e43-8f25-69585f74c341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee1a86a1-6b03-4c69-a150-bdc0d76f392d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444329b9-c155-4e43-8f25-69585f74c341",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "40fe51e1-225d-4a25-bef0-486d979c2119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "628a2687-9e76-4f81-a5d1-154ee6ed3c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40fe51e1-225d-4a25-bef0-486d979c2119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df8ab76-ac75-4885-bb6e-33f9aadd0c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40fe51e1-225d-4a25-bef0-486d979c2119",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "793437f8-50cf-48cb-b35c-ecbb5fbb7cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "58a1078a-206d-46ec-9971-5a9f626353ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "793437f8-50cf-48cb-b35c-ecbb5fbb7cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adb4af82-9fa0-4259-93ab-b76853f6a658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "793437f8-50cf-48cb-b35c-ecbb5fbb7cc7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "bdf3f274-010f-4bc5-8945-35364babcd02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b4522d96-01ab-4d9c-8cd0-ba74a4060574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf3f274-010f-4bc5-8945-35364babcd02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef0b99d-2e16-4123-85c1-0bb45051e22c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf3f274-010f-4bc5-8945-35364babcd02",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5b8cd102-4e69-4737-9647-36e91e6f690f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "42fdea06-c841-492a-80c8-78d813eeb20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8cd102-4e69-4737-9647-36e91e6f690f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d434c398-55cd-4039-b611-e6d560042a03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8cd102-4e69-4737-9647-36e91e6f690f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5963feb3-d948-4e76-ab22-41ff3e8c37be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b822c772-04dc-4c6e-8b97-59fec9573762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5963feb3-d948-4e76-ab22-41ff3e8c37be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d24e81c-8e46-4d0f-8a31-e1577d8fbc34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5963feb3-d948-4e76-ab22-41ff3e8c37be",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9d6451c6-0e47-46d8-9222-c8ca40b74bc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8b4b755b-bb42-450e-93cf-0970056fee3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d6451c6-0e47-46d8-9222-c8ca40b74bc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae72a3c5-a8da-4bdf-9fe1-8a0763dd7d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d6451c6-0e47-46d8-9222-c8ca40b74bc6",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a4ff222b-fd8b-404b-8bb2-495465490c61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "54ca4d6a-0df3-42e0-b291-4dee33d6e50b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ff222b-fd8b-404b-8bb2-495465490c61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "174a1e86-6c89-404f-bf59-241ba1469d99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ff222b-fd8b-404b-8bb2-495465490c61",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "244302cb-13cd-490f-9154-693a3d8c9e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fca8445a-3c5b-4359-a45c-acb9b4373a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244302cb-13cd-490f-9154-693a3d8c9e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "087309ef-881a-4137-8db6-cf7366127d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244302cb-13cd-490f-9154-693a3d8c9e44",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d463bc5d-2f0e-4c2b-af4f-77665f9d0915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "89facc34-407f-4c54-8751-19311afe900f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d463bc5d-2f0e-4c2b-af4f-77665f9d0915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39463f26-8a20-4c86-99c0-2d68fb625458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d463bc5d-2f0e-4c2b-af4f-77665f9d0915",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b4246235-4eff-4bed-a822-80ef01d92ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d2d4702d-404c-4b7a-93b7-12fd478ff49a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4246235-4eff-4bed-a822-80ef01d92ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5141fcac-1568-4928-8a40-443132357ad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4246235-4eff-4bed-a822-80ef01d92ce8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "14083e3f-4840-4d77-bfcd-302342fc9c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7593b06d-77c3-4a72-b66d-c3bee1e2232c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14083e3f-4840-4d77-bfcd-302342fc9c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87af158b-0481-44db-8beb-f59a48f40a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14083e3f-4840-4d77-bfcd-302342fc9c2f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4c64d764-1603-4251-8a5e-3f0ab7fde024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "440683a9-be15-493f-a443-0fa64c85ac27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c64d764-1603-4251-8a5e-3f0ab7fde024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ea5224-c690-4522-9959-80c04945b0b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c64d764-1603-4251-8a5e-3f0ab7fde024",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0ab41e4e-06ae-4e3b-a493-5d8e3f44b9e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1260a2a4-fc2c-48aa-8738-c67f97acf54e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ab41e4e-06ae-4e3b-a493-5d8e3f44b9e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f6d199-dc1d-4eea-ae41-076b490bf071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ab41e4e-06ae-4e3b-a493-5d8e3f44b9e9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fb4516db-fdbf-4865-b96f-0248f2c25eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "36112bf5-e957-407e-9d92-95d70bee9525",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb4516db-fdbf-4865-b96f-0248f2c25eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c79d5b-18b5-4bf6-8ed5-a229d02a9f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb4516db-fdbf-4865-b96f-0248f2c25eed",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "04e465ef-d36a-45ef-a7ba-32e3a9b448cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "af124ba8-5be2-41a0-837d-62810ddfe684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04e465ef-d36a-45ef-a7ba-32e3a9b448cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5633b27-dd96-4109-85be-35ce533167f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04e465ef-d36a-45ef-a7ba-32e3a9b448cb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "69e08518-6747-4f2c-94b4-0ae910dd8a48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c675ed4b-ccbf-44fc-90bc-b9ac87777055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69e08518-6747-4f2c-94b4-0ae910dd8a48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5fffd4-0495-40a7-9f15-14d73ea773e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69e08518-6747-4f2c-94b4-0ae910dd8a48",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9418dde7-96c6-4981-8721-b89106a873d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "623ef89a-1cf2-40b4-b176-5e11c7ac10b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9418dde7-96c6-4981-8721-b89106a873d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e53cdc4e-155a-42c4-8369-773052e70189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9418dde7-96c6-4981-8721-b89106a873d9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ddf76f4e-cd27-4fae-83eb-ab61fc01939d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f6fe9f86-b4e6-487e-b04d-6d8767939414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf76f4e-cd27-4fae-83eb-ab61fc01939d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc734979-c418-46c8-97f9-5d827b377fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf76f4e-cd27-4fae-83eb-ab61fc01939d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3a84b31d-922d-49f0-ac31-e3035618afe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fd5d459b-02c2-47f7-9dda-85c24999b7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a84b31d-922d-49f0-ac31-e3035618afe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a93a2e25-99e2-4f2f-b421-c9cbf072aae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a84b31d-922d-49f0-ac31-e3035618afe9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "01aca0f0-16f7-4f07-a5a6-160daadabc7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "50690405-dffb-4fdb-8aa1-63b89298b991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01aca0f0-16f7-4f07-a5a6-160daadabc7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b38d20-bc07-4fb5-9186-303e540fb1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01aca0f0-16f7-4f07-a5a6-160daadabc7a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e8f2ff9e-96f7-4174-a5da-f216fcb7c36d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "59af0dec-7e5b-43bb-b189-eb1027d8c3b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8f2ff9e-96f7-4174-a5da-f216fcb7c36d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a546559-504c-450a-8cac-de8f8b250b2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8f2ff9e-96f7-4174-a5da-f216fcb7c36d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6c41231c-01d9-407e-b00c-37c16f53945d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8556ce57-3cac-410c-9f25-5a11dc7f51e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c41231c-01d9-407e-b00c-37c16f53945d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94fcfb1b-9799-4cb1-afe1-9cc6c64224c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c41231c-01d9-407e-b00c-37c16f53945d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "243a10f2-a7fd-47cd-80e4-f1e38a86be10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "95a94c76-8ea7-4bc2-ab47-5c090022aade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "243a10f2-a7fd-47cd-80e4-f1e38a86be10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dd6a8f8-2231-4049-b85f-424ca39aa47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "243a10f2-a7fd-47cd-80e4-f1e38a86be10",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3810e1dc-5ad8-4520-895f-259314b7f4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b4d50f91-7e4b-4b4a-98b7-c3286158b5ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3810e1dc-5ad8-4520-895f-259314b7f4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a5c58ad-ad31-498a-86e7-2b6ed571a977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3810e1dc-5ad8-4520-895f-259314b7f4c9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "330d3131-f20e-4191-ae08-5634258f5e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7b30efc3-0939-4d14-8392-71d8f0eb8f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330d3131-f20e-4191-ae08-5634258f5e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae7042f2-a53e-423e-8808-ee650ca737a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330d3131-f20e-4191-ae08-5634258f5e28",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ad7350ff-eeee-444b-9108-b69db56e7b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c47dd648-9437-4859-9b9f-e9f703a16c01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad7350ff-eeee-444b-9108-b69db56e7b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5452d1-f506-4314-a648-c23354afa36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad7350ff-eeee-444b-9108-b69db56e7b21",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2f1ea274-0a6a-48f7-9d23-869b53caecf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ef3310b2-2759-4cec-bc19-0c8c438dabb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f1ea274-0a6a-48f7-9d23-869b53caecf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed780b8-ffb7-44ab-b119-b6590c8ee51f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f1ea274-0a6a-48f7-9d23-869b53caecf8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "16bbd246-73cc-447b-9941-239431796f25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "19b67e9d-e5a3-40ee-94a2-5011138c0f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16bbd246-73cc-447b-9941-239431796f25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "262b17e0-4d04-49bf-8b37-80c1d7cbdf20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16bbd246-73cc-447b-9941-239431796f25",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "865921cf-b0e5-4625-a308-08114df27a8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c1dfffc4-3157-4e33-87a1-86c49155b1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "865921cf-b0e5-4625-a308-08114df27a8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1236c8e-228f-42cc-9fe0-2b303d0de67a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "865921cf-b0e5-4625-a308-08114df27a8c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8007c011-5787-46c5-8480-747729d8f81b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3aca84c5-1263-48b3-8746-9d54a68c3097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8007c011-5787-46c5-8480-747729d8f81b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56481e75-c7b7-47c9-8805-ca1d39281ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8007c011-5787-46c5-8480-747729d8f81b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "931b54d4-ff35-42ef-991c-22b8c7eec5af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "43ddce82-a7fa-4a84-b9e6-0060f3917c92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "931b54d4-ff35-42ef-991c-22b8c7eec5af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a008abbb-65c8-469b-aa99-baea01dcd22a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "931b54d4-ff35-42ef-991c-22b8c7eec5af",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "42f5ccb7-0071-4343-898c-8e6bfe8d591b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "aec29873-a710-4219-84cb-c84620d4af5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f5ccb7-0071-4343-898c-8e6bfe8d591b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c711a2e-673e-4140-be8b-51b3a9ee9387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f5ccb7-0071-4343-898c-8e6bfe8d591b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "51c24c20-176b-42c9-983c-ce03026fd53f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cde3e2ea-1be6-4233-b97a-81183eebd852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51c24c20-176b-42c9-983c-ce03026fd53f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbce14b8-9164-4ce7-ace0-95f21ea2c1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51c24c20-176b-42c9-983c-ce03026fd53f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5c56e9d3-9894-4496-9fa8-0608db3fcb64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "79992ab0-61e3-4adc-958d-5f07a73631e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c56e9d3-9894-4496-9fa8-0608db3fcb64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa0647e-3668-4f33-aa3d-5c19d60eecd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c56e9d3-9894-4496-9fa8-0608db3fcb64",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c649b101-dfc2-401e-aa04-12ac912ad7e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8416d407-cc9f-4f9b-8de8-5dc1efa3a287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c649b101-dfc2-401e-aa04-12ac912ad7e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c42b7afd-90e4-41a5-aacb-ec0875db4575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c649b101-dfc2-401e-aa04-12ac912ad7e0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9eee542f-99dc-4af2-be4c-d57e821ce704",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b1790732-a0e1-4dc6-8eb6-86f1cf98bcdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eee542f-99dc-4af2-be4c-d57e821ce704",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a3a9d7-06a9-495d-bad4-697afddefddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eee542f-99dc-4af2-be4c-d57e821ce704",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a377e52e-de80-459c-821a-f32a9b692913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bd72b3b1-c24c-4911-9858-5e1f83317c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a377e52e-de80-459c-821a-f32a9b692913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60802482-d9b6-4ac3-a8df-d6529642c336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a377e52e-de80-459c-821a-f32a9b692913",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d0ea2d1d-e46b-47b4-8494-0815abba8b56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "acc4922a-e0c7-4bee-bc4a-1b214e242a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ea2d1d-e46b-47b4-8494-0815abba8b56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec347bb3-7d8f-4e5a-9648-1c3f3271fb15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ea2d1d-e46b-47b4-8494-0815abba8b56",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0b6cd8db-95c6-4dd9-88e8-7a8c6af34423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c055b4d6-c2a4-4a41-9186-c2fc24a3de73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6cd8db-95c6-4dd9-88e8-7a8c6af34423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0835e28b-ef9c-4f8d-97e3-e3bc5fefdad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6cd8db-95c6-4dd9-88e8-7a8c6af34423",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c1220119-0705-4b91-9f1e-86204dc17d6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a1c80416-8cf5-4403-b0ce-73f72e0c02b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1220119-0705-4b91-9f1e-86204dc17d6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116dff9f-2438-4efd-bfb2-c333bb5ff494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1220119-0705-4b91-9f1e-86204dc17d6a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d8a10404-e2ba-4c82-879e-033b0ce1fef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "aaf0de69-a5f3-4f03-a76c-d6171d069efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8a10404-e2ba-4c82-879e-033b0ce1fef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee4e614-217f-4efa-9f4b-f8b88971a2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a10404-e2ba-4c82-879e-033b0ce1fef5",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c25cea9e-dac2-4348-a647-7ea2719bd21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "dc8d5f0d-643c-4e62-8741-18cddbb24caa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c25cea9e-dac2-4348-a647-7ea2719bd21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1af668fb-dc41-414c-9008-2c28f0bf9939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c25cea9e-dac2-4348-a647-7ea2719bd21e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f63ab4fd-224a-47d3-92bc-dfdf34d25fba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1a967e44-cb2f-443b-9d06-c8e8f1b4d0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63ab4fd-224a-47d3-92bc-dfdf34d25fba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a084ce0e-f1c6-418c-87b7-25957d60cac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63ab4fd-224a-47d3-92bc-dfdf34d25fba",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d0bc937c-78a6-495c-8d82-a2df3000e7d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "903b5d14-3218-4b10-8355-80a1da518015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0bc937c-78a6-495c-8d82-a2df3000e7d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cff4e53c-8ed2-4609-9457-8e7ff4cc7350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0bc937c-78a6-495c-8d82-a2df3000e7d3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "33788c77-8d33-48fc-b9bf-c422254d3be1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "007139e4-4fdb-4bb2-b96d-0ccab80add19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33788c77-8d33-48fc-b9bf-c422254d3be1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2becec44-6663-45c6-81a5-db5403df5d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33788c77-8d33-48fc-b9bf-c422254d3be1",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4d8b5375-987e-4b36-8bc0-db86fe9b26dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a4e7e114-006a-454a-a8e2-915edc183dbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d8b5375-987e-4b36-8bc0-db86fe9b26dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3355e553-2cae-4216-95a2-3e93ff3729cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d8b5375-987e-4b36-8bc0-db86fe9b26dc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3ccd1538-1ae2-4652-a9e6-52724995abe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c0bcc6cc-0fc4-475f-9aab-064486831869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ccd1538-1ae2-4652-a9e6-52724995abe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b8ca29-9a34-4825-b07b-fafaf92866a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ccd1538-1ae2-4652-a9e6-52724995abe9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c8bee720-5950-4edf-85e2-9e3dd7fcd8f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a4fc41c1-5747-47a5-bccb-86bfdae44c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8bee720-5950-4edf-85e2-9e3dd7fcd8f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31eff55d-b28f-48e6-bdca-4238d5c75346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8bee720-5950-4edf-85e2-9e3dd7fcd8f2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e114ab8f-89a5-417c-8b89-4c31106ce3bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6d8a8821-357e-441b-bd55-41ac72f36abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e114ab8f-89a5-417c-8b89-4c31106ce3bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26547803-b940-4e49-a9e8-750eda535c2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e114ab8f-89a5-417c-8b89-4c31106ce3bb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d8777897-5451-4115-a3ba-b2978687b17c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "20770862-6214-48e3-a880-6e2b9928c8d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8777897-5451-4115-a3ba-b2978687b17c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15da21de-d528-4e4f-b8f7-9957c8b4d098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8777897-5451-4115-a3ba-b2978687b17c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8797fcd0-ff96-4012-8f59-353ec25c6cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "997cc66e-c1fd-4e77-bb8c-ae6c1e6ca93e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8797fcd0-ff96-4012-8f59-353ec25c6cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac5b64ca-9332-48bd-b01e-898e2e14e1f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8797fcd0-ff96-4012-8f59-353ec25c6cf0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c6aff7f5-3f9f-4d2e-b173-9ddb1806ca74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "be3406a0-9fac-4b96-8901-82d7108d3d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6aff7f5-3f9f-4d2e-b173-9ddb1806ca74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21fe7f40-1bb7-47c8-aea1-929e1fa57f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6aff7f5-3f9f-4d2e-b173-9ddb1806ca74",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "18ac32c4-ad85-4f22-8a9a-9ec6cf9f962e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a727b11a-1a1f-4566-8f84-4d8c00f9cd49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18ac32c4-ad85-4f22-8a9a-9ec6cf9f962e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d1f97a4-7326-4b93-bfb6-336c3765f8bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18ac32c4-ad85-4f22-8a9a-9ec6cf9f962e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "bb7c079a-c0d8-4012-ae80-6e9713bb7497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "16b1d202-1015-4011-8621-63573bb9e97e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb7c079a-c0d8-4012-ae80-6e9713bb7497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0406178-c507-4d10-830c-be2ff8d75c48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7c079a-c0d8-4012-ae80-6e9713bb7497",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "1a0a22d3-2aef-4320-9b23-01ba96dce64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1c5a4286-0884-4275-9384-b995262cb30b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a0a22d3-2aef-4320-9b23-01ba96dce64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded3c93a-a15b-46c8-ad78-4c19156c1dfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a0a22d3-2aef-4320-9b23-01ba96dce64b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7b704067-77fc-44ed-b11b-7652d7f74e00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cb92766c-8db8-46ff-8fd8-27f0e3de5f24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b704067-77fc-44ed-b11b-7652d7f74e00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "434fb3d2-33c3-4227-a632-550665bdbf61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b704067-77fc-44ed-b11b-7652d7f74e00",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "68769521-2961-496e-957a-91218c1386a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "efb46294-3908-4906-a480-0df2f7255997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68769521-2961-496e-957a-91218c1386a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22bea21-a7ad-4a29-9479-bc3a05694559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68769521-2961-496e-957a-91218c1386a2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6e8ea1ea-d5fd-49b2-ba30-d27084700853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d6b6bd34-f454-48a3-84d3-1122ccf66d40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e8ea1ea-d5fd-49b2-ba30-d27084700853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b22d0a-f384-478f-8a72-ea643ceb88af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e8ea1ea-d5fd-49b2-ba30-d27084700853",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d7814663-2e9e-4fc2-a9da-79a53173c281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "71ccb3d5-5c7e-4047-8bf0-33bd34c4e5b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7814663-2e9e-4fc2-a9da-79a53173c281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "947a2b08-9ecf-4464-8dbc-7422b46692fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7814663-2e9e-4fc2-a9da-79a53173c281",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ecdcb081-e59c-4640-b178-3c4a5f901b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "191389ae-3e1d-45de-ab73-0f1417f4aa09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecdcb081-e59c-4640-b178-3c4a5f901b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b518b40f-61cd-4461-b138-5ece79048691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecdcb081-e59c-4640-b178-3c4a5f901b4d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2a541378-efc1-49d7-a31f-14de13de0e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3b4b31fa-c995-4773-8098-80a409f644e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a541378-efc1-49d7-a31f-14de13de0e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dd0145d-efa4-423f-9e66-168da09126fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a541378-efc1-49d7-a31f-14de13de0e2b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "77fcd106-567f-46b3-bfb0-d87610f8d7a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3d64dc46-67e2-4b91-9246-1a4cfdbb2145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77fcd106-567f-46b3-bfb0-d87610f8d7a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bee97d55-5d98-431f-abf9-9156a12acd97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77fcd106-567f-46b3-bfb0-d87610f8d7a6",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "50c153de-9063-48d6-8e32-f308be8fecaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "4eb35fbd-eb13-49c5-b24c-409d69903c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c153de-9063-48d6-8e32-f308be8fecaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6015dc44-dcc7-40bd-9007-da1422b1c696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c153de-9063-48d6-8e32-f308be8fecaa",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0c939878-a2c2-44a2-bb3d-d390f8a4edf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f529cdea-6611-4553-8df5-3dce7851d31b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c939878-a2c2-44a2-bb3d-d390f8a4edf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d8c03e3-1aa8-46bb-94b7-ae8c9dad522d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c939878-a2c2-44a2-bb3d-d390f8a4edf4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fc07a614-c27b-4280-b996-56771c98e01b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9cde2c3d-d36d-4633-b162-08cbf15416a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc07a614-c27b-4280-b996-56771c98e01b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed538426-4edf-424b-9852-66f6e33b9647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc07a614-c27b-4280-b996-56771c98e01b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2d8323c9-8efb-46fd-b079-bd8f9926fde4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0eb27963-b6f4-41f6-a16e-248be02e2561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d8323c9-8efb-46fd-b079-bd8f9926fde4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d12d860f-37db-47b7-a33a-3f60e1cc0cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d8323c9-8efb-46fd-b079-bd8f9926fde4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "88c309ea-bf1e-4be9-ae80-9fc12741bbe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6e349dd1-89aa-42b7-9dcf-60ab1d64ef6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88c309ea-bf1e-4be9-ae80-9fc12741bbe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0fdb623-8e60-4de9-8e9c-256291f53302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88c309ea-bf1e-4be9-ae80-9fc12741bbe3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f39ff739-bed7-4b90-8a34-177230730aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c0775362-bba3-45d0-9c83-ef3070c3b05d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f39ff739-bed7-4b90-8a34-177230730aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1983f4ae-4d8b-466e-97c5-f21aa87e4ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f39ff739-bed7-4b90-8a34-177230730aa2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c94af1a9-53f6-419c-ac00-2f9bb0a97077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "4eb5cfcf-f87d-482c-94e9-a77d48cbee48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c94af1a9-53f6-419c-ac00-2f9bb0a97077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98126880-4ab4-4e6c-8912-279e553406e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c94af1a9-53f6-419c-ac00-2f9bb0a97077",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ccc3ec03-37cb-4c7c-80b8-413f1e9b72a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "76a07d24-c25c-4995-82e7-4eee565720cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc3ec03-37cb-4c7c-80b8-413f1e9b72a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1abfd6-e936-43d3-8776-521e21537b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc3ec03-37cb-4c7c-80b8-413f1e9b72a7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3d7fe516-185d-4b2c-aeb4-bd463b5cf112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2d9c7282-df34-444d-aae5-00dfebf1a770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d7fe516-185d-4b2c-aeb4-bd463b5cf112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28376d65-5962-43b0-9103-804e7f59e2b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d7fe516-185d-4b2c-aeb4-bd463b5cf112",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7337df64-ea83-49ea-88f7-240a3add0284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "e5984256-c81b-4010-9521-1eae53eecdd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7337df64-ea83-49ea-88f7-240a3add0284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f7f1f1-58f4-4d49-a776-40dee82b0c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7337df64-ea83-49ea-88f7-240a3add0284",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "394ad906-a657-44bc-b7e6-3078df38c25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "e776074b-1057-4c11-93e1-4e7e478eb2e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "394ad906-a657-44bc-b7e6-3078df38c25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ba4838-3a54-47b6-94f7-44ed2cb9f257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "394ad906-a657-44bc-b7e6-3078df38c25b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0d63f35f-c787-4700-9217-55de041c4856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "77b3a88f-a51e-4292-adf3-6c45065e70c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d63f35f-c787-4700-9217-55de041c4856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c481ce-b698-4f37-9c2d-b7d75cfd5cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d63f35f-c787-4700-9217-55de041c4856",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b691fa79-37e4-4df0-9a32-12e022a375ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f8690cb2-f445-42c8-b3b1-9862f4402bce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b691fa79-37e4-4df0-9a32-12e022a375ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34bc4435-d10f-4859-9559-af75fbed49f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b691fa79-37e4-4df0-9a32-12e022a375ca",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3a6254b4-006f-448b-982d-a517601ff96b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "e08c776b-c8ed-4ed6-9a35-4fba288f1336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a6254b4-006f-448b-982d-a517601ff96b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "118f3cef-7f12-4306-8e2a-232c72a4853f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a6254b4-006f-448b-982d-a517601ff96b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a56aba63-bfd4-41af-9fe1-230c6b5517bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ae5b812f-4d3c-4c05-9d16-db623d615874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a56aba63-bfd4-41af-9fe1-230c6b5517bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c10c285-7eed-44fc-a4ee-7bc72607731a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a56aba63-bfd4-41af-9fe1-230c6b5517bc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0f85e58a-9a4e-488f-a14e-3304a92c5b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c64bbbef-58fe-414d-8c13-cc196ff26a55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f85e58a-9a4e-488f-a14e-3304a92c5b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c64bbd29-6c9c-4af4-bf30-24d8d38ee2e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f85e58a-9a4e-488f-a14e-3304a92c5b42",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "79e4fadd-8d5b-49ba-8cbe-ec7890aa81c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "167ffdbc-2fd5-464b-b604-e026629138a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79e4fadd-8d5b-49ba-8cbe-ec7890aa81c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d97f7b79-8bf4-4818-855c-3d52a25051c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79e4fadd-8d5b-49ba-8cbe-ec7890aa81c9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "32875400-f786-4060-8ae9-3ad124c932f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3a488e54-4b9d-4fe3-936e-bafbd0a738f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32875400-f786-4060-8ae9-3ad124c932f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c702320-3859-4923-9731-f9d9e0416b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32875400-f786-4060-8ae9-3ad124c932f8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4ffb1c12-90c2-4e43-9e55-ba6d9c522bec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "851942fb-9b9e-46e4-82d1-c6ec06e2c3a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ffb1c12-90c2-4e43-9e55-ba6d9c522bec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43a89605-a6c6-4839-b570-bdcef750a1e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ffb1c12-90c2-4e43-9e55-ba6d9c522bec",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2b5e16a8-65d1-4b45-ab00-547151b08ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "738c0cda-6dfc-4c6d-8d9a-e78e0eec5885",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5e16a8-65d1-4b45-ab00-547151b08ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08dc75e-2c40-4316-b09d-472fb78a1968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5e16a8-65d1-4b45-ab00-547151b08ff1",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "65d27807-69f9-4fc8-9fba-c4f509aa4559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8f3546dc-fd59-4966-b7da-492b0baa045e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d27807-69f9-4fc8-9fba-c4f509aa4559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "842c416d-be57-430e-b699-d5aae1f5d2c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d27807-69f9-4fc8-9fba-c4f509aa4559",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "27341e10-c832-416a-b5f1-8b28be80f252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2adf392d-26ee-4c90-860f-259cf47522ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27341e10-c832-416a-b5f1-8b28be80f252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9420b9b6-350a-47fe-8eea-5c503105e2fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27341e10-c832-416a-b5f1-8b28be80f252",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "66d1a330-2ee7-4234-b34a-b1cca38ce8bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1a7dbb6a-af6a-4014-82c1-030679fa1c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d1a330-2ee7-4234-b34a-b1cca38ce8bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5b67d02-1697-46c2-b2fe-50377c5d81b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d1a330-2ee7-4234-b34a-b1cca38ce8bd",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5dbd081a-c4c7-44db-ac6c-cb0282a66884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "61c8a606-2ede-4fc9-8f17-d63ec7bf4a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dbd081a-c4c7-44db-ac6c-cb0282a66884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4552bf65-d748-47af-9730-70def4f11eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dbd081a-c4c7-44db-ac6c-cb0282a66884",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ef3f833e-3f1f-4ae2-8ffa-82b5d0a0897f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "73350997-3f97-49df-b57c-847c5615a578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3f833e-3f1f-4ae2-8ffa-82b5d0a0897f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f896afba-3922-46c4-90ee-9535d4c1afab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3f833e-3f1f-4ae2-8ffa-82b5d0a0897f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0d9e654d-710c-4830-93ed-ea4418b3ebb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "92990aa8-78a8-4dd9-b914-1070e33eae83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d9e654d-710c-4830-93ed-ea4418b3ebb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0833254-939f-483f-accb-af8d7eb93381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d9e654d-710c-4830-93ed-ea4418b3ebb7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5f739399-28c3-453b-ae70-358e870f3957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f34d82e7-2033-496f-b955-6c73daf33f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f739399-28c3-453b-ae70-358e870f3957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f0781b-5907-4fbf-8b2e-de845b3a802e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f739399-28c3-453b-ae70-358e870f3957",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8b3fb438-303b-4346-bd38-73d452ca23b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "998f475d-5214-4ca1-8783-2dfe10dedbc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3fb438-303b-4346-bd38-73d452ca23b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "427a1c22-38fa-4968-aa80-55344ddc7dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3fb438-303b-4346-bd38-73d452ca23b7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9cc5ad65-2fca-4a6c-ae95-bc6ea9f654b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1784fedb-1f23-4de7-a3e4-c214666602d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cc5ad65-2fca-4a6c-ae95-bc6ea9f654b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c2b7182-58ba-46e7-a88c-4ad3966fd407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cc5ad65-2fca-4a6c-ae95-bc6ea9f654b0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9c5426af-cce7-4497-9732-9b44a2681c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8211006e-44b1-4c72-8ced-3ac4ab58f81c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c5426af-cce7-4497-9732-9b44a2681c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f5af55-46df-4888-bdeb-912d4e9f5f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c5426af-cce7-4497-9732-9b44a2681c1f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ce4d176a-070e-4e20-8ac3-89c2d9581a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a1a4957d-543d-4ab4-90ae-5aacb3182b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce4d176a-070e-4e20-8ac3-89c2d9581a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c003f240-806b-46cb-9881-07313a2ae019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce4d176a-070e-4e20-8ac3-89c2d9581a12",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "01a9aa63-c945-4e0c-9011-1e31e3132dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cd61eb80-db5a-4061-9f28-9f81c9f185f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01a9aa63-c945-4e0c-9011-1e31e3132dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb925a6-6ab5-4240-a956-76cf19c6791c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01a9aa63-c945-4e0c-9011-1e31e3132dea",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6388325d-1773-445c-a16e-408b05837cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5640b103-6f53-4245-9ae2-4b3604590482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6388325d-1773-445c-a16e-408b05837cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01fa0ba2-b372-4486-b6f4-e2c521066604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6388325d-1773-445c-a16e-408b05837cdf",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f789da47-255c-403f-8125-cb956c605129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b67edea2-f6ff-4971-bac9-3316b1528639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f789da47-255c-403f-8125-cb956c605129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f389f4e5-c1ed-4b3e-8431-38ec2d59ed6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f789da47-255c-403f-8125-cb956c605129",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "cef19fab-7670-49d8-94d5-8e34428e30db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d75cfe89-7eed-4419-9f7a-f259eff1815d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cef19fab-7670-49d8-94d5-8e34428e30db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d3bd28-25e3-4744-97db-530b7a739446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cef19fab-7670-49d8-94d5-8e34428e30db",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "29345f7c-2b33-482e-9c03-f6b1ba4a441a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "de0884b6-7674-4dac-be5e-32ef1099fc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29345f7c-2b33-482e-9c03-f6b1ba4a441a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ccbf507-a31e-42c7-8b6c-137b7387a9e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29345f7c-2b33-482e-9c03-f6b1ba4a441a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "89fc9f27-a164-4690-8f82-bf857ef25752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "733c6187-6f27-4661-9adb-aa8923cece93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89fc9f27-a164-4690-8f82-bf857ef25752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa8bd344-8442-4680-ab09-dfd118b7f411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89fc9f27-a164-4690-8f82-bf857ef25752",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e0596404-2dd2-46da-a6c4-22d95da9283c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "50c63bbb-7f26-4ebf-a70c-dc59b883f266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0596404-2dd2-46da-a6c4-22d95da9283c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9baefb-c4e0-4821-9a58-9166aee8482e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0596404-2dd2-46da-a6c4-22d95da9283c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f34d3270-ee6b-4bf1-9b7a-6ec138a5cee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a2c40753-5002-4b12-be49-2115794ef56a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f34d3270-ee6b-4bf1-9b7a-6ec138a5cee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3f40ad-5657-4c36-bf11-53357e910714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f34d3270-ee6b-4bf1-9b7a-6ec138a5cee5",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f843d65c-c66e-47e1-ac00-c154803adbf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5cc0a3a3-8980-49e8-b270-7485051d1e16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f843d65c-c66e-47e1-ac00-c154803adbf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c944c524-a3b8-4d9a-845b-4c05f0999b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f843d65c-c66e-47e1-ac00-c154803adbf6",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "eb7b1721-1d48-4946-ac23-950204c1f95b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "40def5a2-1b3e-427e-b425-81c2fa089898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7b1721-1d48-4946-ac23-950204c1f95b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89a21a6-18ee-460b-85b8-45c57ecaa374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7b1721-1d48-4946-ac23-950204c1f95b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "06b4bd2a-0480-4ae1-847a-0078b6fc812e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "05399a70-c6fc-40c0-803f-4aeaa8a9b5f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06b4bd2a-0480-4ae1-847a-0078b6fc812e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f4c07b0-2671-4920-b7b5-80d5c21c3e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06b4bd2a-0480-4ae1-847a-0078b6fc812e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "002db157-bf37-4ed9-8fd5-099f4a4ce722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b13284cb-62fb-4fb1-b7a0-2fd762f80f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "002db157-bf37-4ed9-8fd5-099f4a4ce722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35c8086-6bae-4566-a55c-aca8665d8b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "002db157-bf37-4ed9-8fd5-099f4a4ce722",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "23347f42-4d33-4a4d-888f-aaf7be737316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8c223262-2024-45cd-b1b7-ee9add8343c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23347f42-4d33-4a4d-888f-aaf7be737316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec6e172c-3a9b-49e0-b1a9-21ff64c73ead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23347f42-4d33-4a4d-888f-aaf7be737316",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "534c69ec-a2b9-45a5-a5df-df0a067bfb6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "12a3813c-6b26-4d25-b71c-94517dc8e1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "534c69ec-a2b9-45a5-a5df-df0a067bfb6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f6ba97-1e97-4841-8c58-07bd9ade1088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "534c69ec-a2b9-45a5-a5df-df0a067bfb6c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e43529d2-848b-46f0-bf85-b5c6b6fec59e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "96895519-1bf5-42ec-b70c-4fd8be9bcf6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e43529d2-848b-46f0-bf85-b5c6b6fec59e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3804a95-6786-415f-a48d-78f18aa644ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e43529d2-848b-46f0-bf85-b5c6b6fec59e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b021c007-a880-4322-abcb-54a56a2fb753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "e59217af-9acd-462d-838e-a923bba70c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b021c007-a880-4322-abcb-54a56a2fb753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3e8880-4af7-41e1-a263-a2d7340514be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b021c007-a880-4322-abcb-54a56a2fb753",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "48122c43-e69e-4fd4-843d-265624a97b4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0bb0defa-4d6e-46ab-9809-4627a14fca45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48122c43-e69e-4fd4-843d-265624a97b4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ccdc85-59b4-404e-bdc4-3291d4d75289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48122c43-e69e-4fd4-843d-265624a97b4c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7576fa88-0290-4238-815c-779dfec65a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1bed9a97-2763-46b6-b859-578d31b5ef16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7576fa88-0290-4238-815c-779dfec65a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a46f56-2437-48f6-a4ac-de2b07d33c54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7576fa88-0290-4238-815c-779dfec65a1e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d662b13f-561e-4ab5-912b-a7819903b153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f217d059-dec3-4ceb-a8b3-d524750af193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d662b13f-561e-4ab5-912b-a7819903b153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a7f6806-fa75-46c4-8905-645aee9a4712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d662b13f-561e-4ab5-912b-a7819903b153",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a557eff9-4a4e-4b57-8783-dc5ec961c701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fb74e21e-7761-4bf1-9061-3038f8d201ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a557eff9-4a4e-4b57-8783-dc5ec961c701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "681b2d71-fe28-4a0d-9342-7f799a44af07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a557eff9-4a4e-4b57-8783-dc5ec961c701",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4c04682c-ca65-4a9f-be04-ea379cba23f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "562cbfcc-db73-4f6b-8fd4-27763e8654ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c04682c-ca65-4a9f-be04-ea379cba23f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d5a0f50-14ff-456b-b814-3db3cee92bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c04682c-ca65-4a9f-be04-ea379cba23f9",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fe18a1b7-f089-49d6-948c-7d3d39ca6e7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f6bc7432-2020-4055-ab16-386e697ef639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe18a1b7-f089-49d6-948c-7d3d39ca6e7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc1eb6c6-e471-4060-96ab-cf56c8e96654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe18a1b7-f089-49d6-948c-7d3d39ca6e7c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8bf5ecee-0139-4f08-b147-ff951e028a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "96bd807d-5928-4cdb-b493-13d0306d6e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bf5ecee-0139-4f08-b147-ff951e028a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7268a11-7ee8-4d6c-86dd-299c9fe08d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bf5ecee-0139-4f08-b147-ff951e028a83",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "09ca7ef0-01c4-419a-9873-0d2390a0e9aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ca662538-4f93-40f3-bfd6-c456d4d36eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ca7ef0-01c4-419a-9873-0d2390a0e9aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac3ad5a-0fc7-402a-ae14-8fc4a1478c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ca7ef0-01c4-419a-9873-0d2390a0e9aa",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4d8e6670-7f8c-4277-9239-ca387c80531a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "996eb14a-a58a-4ee9-ac2a-13db90f92e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d8e6670-7f8c-4277-9239-ca387c80531a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22b2a8d2-f308-4fe6-87ff-95c7406a0e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d8e6670-7f8c-4277-9239-ca387c80531a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6088218a-a756-499d-809d-3abe3e7b45cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8f19f0f3-300d-451e-82f0-305bc002efd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6088218a-a756-499d-809d-3abe3e7b45cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f563870-599e-48ce-bd2c-c0bcf81d97f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6088218a-a756-499d-809d-3abe3e7b45cd",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8540b2f3-a0e3-472d-9a47-6e81796d8eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "03738921-22e5-47da-8231-bc7801764064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8540b2f3-a0e3-472d-9a47-6e81796d8eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f8e6fa4-6209-4567-8d64-52939bcf314a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8540b2f3-a0e3-472d-9a47-6e81796d8eed",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5b74be8c-2be5-40d0-b67e-18389f2de9fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cc8a5e4c-e849-44aa-91f2-4757b3aab147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b74be8c-2be5-40d0-b67e-18389f2de9fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d12c0bc-0a34-48da-b27a-59268ad2797f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b74be8c-2be5-40d0-b67e-18389f2de9fa",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2cb99b12-b97d-4c19-81d3-3a6886ea19f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6cb6dd9d-cf90-4f48-9596-362a8b87cfb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cb99b12-b97d-4c19-81d3-3a6886ea19f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdafdb37-bfc0-4c09-a8a7-758873d334ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cb99b12-b97d-4c19-81d3-3a6886ea19f4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "bfbbdb52-ee23-4a89-8fae-8d80083852fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3aeff8d1-a6b8-463a-8fac-75f73d2596cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfbbdb52-ee23-4a89-8fae-8d80083852fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0ee7af-d79e-4d7c-81b0-3e42650eb0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfbbdb52-ee23-4a89-8fae-8d80083852fc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "75391ee4-a66d-4784-8562-b54a8d82ae62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7dee8512-31f5-4fa3-a36f-cf9a0b5e3567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75391ee4-a66d-4784-8562-b54a8d82ae62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c9add7-2ca4-4cac-bb26-4649385a067d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75391ee4-a66d-4784-8562-b54a8d82ae62",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a02d4594-a467-4717-b33c-5ad7d6467fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d9b6d50a-bac0-4c86-8a43-a1ca7d0bf0b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a02d4594-a467-4717-b33c-5ad7d6467fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06fc5f52-0ee2-4cbb-8ef5-3abe70c5959c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a02d4594-a467-4717-b33c-5ad7d6467fdb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e694f35f-4cdf-4718-a696-22874ce13857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "e1687709-5c88-4f83-a7d2-ddc96b214076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e694f35f-4cdf-4718-a696-22874ce13857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dfa5845-3f8e-4a3b-a475-16619b2c1787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e694f35f-4cdf-4718-a696-22874ce13857",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "14d536b1-eb3e-4a64-8257-8718052292f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "127fefbf-f638-4b78-9e33-6ee82df681f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d536b1-eb3e-4a64-8257-8718052292f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20213a0d-be9e-4464-a9c1-352663e266cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d536b1-eb3e-4a64-8257-8718052292f7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "bc6fab02-6d1c-40d4-9988-4c22aa706540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c473ada7-2dff-4b5f-b504-29544a73d227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc6fab02-6d1c-40d4-9988-4c22aa706540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd8e8521-e213-4c75-88b1-af8569b66818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc6fab02-6d1c-40d4-9988-4c22aa706540",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d5dff4be-5e6e-45f8-a68e-94754185a779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b90c8808-5041-4b5d-92aa-fa10f3ccd5d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5dff4be-5e6e-45f8-a68e-94754185a779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4131105c-9efd-4684-a3d7-e48d478da8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5dff4be-5e6e-45f8-a68e-94754185a779",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8c9d25b6-21c7-4d9a-adf6-99e402d178a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7571349e-12b7-40a8-9f85-dbbb02cefb9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c9d25b6-21c7-4d9a-adf6-99e402d178a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbdfde41-9a0a-4123-bdd4-099ac83ba0c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c9d25b6-21c7-4d9a-adf6-99e402d178a0",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b649035e-c41a-4de8-a706-3f868bc2a86d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9c12f9fc-d09b-45c7-ac49-372dc8971f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b649035e-c41a-4de8-a706-3f868bc2a86d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bcd80df-57ae-4000-9032-cad599090c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b649035e-c41a-4de8-a706-3f868bc2a86d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9a88484c-ca33-418d-a3e4-d05a94161765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6c5707fd-ebab-4264-9b28-4f25562c4cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a88484c-ca33-418d-a3e4-d05a94161765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad5aa46-3dd5-46d4-aefc-4dc059c967fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a88484c-ca33-418d-a3e4-d05a94161765",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9ac40c8c-1212-4aa3-be27-043720a96f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d98629b8-78d7-494c-a4b3-38537147d1bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac40c8c-1212-4aa3-be27-043720a96f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93ca0e89-5da8-4aec-928c-43ae81452163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac40c8c-1212-4aa3-be27-043720a96f40",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "21bf489e-6df7-4fd7-8b32-9e3d202fd110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2aa5917c-d9cd-41e2-ada1-728e0c84b539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21bf489e-6df7-4fd7-8b32-9e3d202fd110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2a87d6-be5c-4eab-956c-e12bfaa0189e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21bf489e-6df7-4fd7-8b32-9e3d202fd110",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "52e08bdd-4306-41ef-b56c-0d394e6684bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ea45380f-ff30-470a-b17e-03fd13ceca97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e08bdd-4306-41ef-b56c-0d394e6684bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddca268a-5776-460a-8e26-7a1faaaae7c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e08bdd-4306-41ef-b56c-0d394e6684bd",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "37361075-5fef-4c4c-8fb7-74a2c56d49b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "31727afb-317a-419b-8f74-180a6fe31e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37361075-5fef-4c4c-8fb7-74a2c56d49b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30132913-b2ca-4c04-bf39-610dbb015a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37361075-5fef-4c4c-8fb7-74a2c56d49b8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "eae7abb3-180a-4ed3-91d3-422e66313475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bb2e6cfc-2565-4790-8893-133ab1604b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eae7abb3-180a-4ed3-91d3-422e66313475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6858c539-d536-4010-85ba-ca1ced887116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eae7abb3-180a-4ed3-91d3-422e66313475",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "94ff2f63-1dd8-4495-ad61-c8795eddfdde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c8f60307-fa01-404d-8dad-3739c63ce275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94ff2f63-1dd8-4495-ad61-c8795eddfdde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1119d8bc-d5f3-459d-bba9-0b6b6f6e53d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94ff2f63-1dd8-4495-ad61-c8795eddfdde",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d21ab0c2-59dc-4d41-bf9b-73e430cff060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "de85b1b5-1fc5-4849-82b1-50955c676f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d21ab0c2-59dc-4d41-bf9b-73e430cff060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20c5b7a-3554-44a6-9151-52bfc3964c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d21ab0c2-59dc-4d41-bf9b-73e430cff060",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3f394170-dbc9-4d2f-8ee3-42713574e901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0bb38e39-29ba-407d-9420-3693f5f88f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f394170-dbc9-4d2f-8ee3-42713574e901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c849ec7-6300-4dcb-82f2-3245074e5973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f394170-dbc9-4d2f-8ee3-42713574e901",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e6bb2fe2-75d9-4490-93ea-c36cb362ef2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b4bfa442-2572-4ef1-b85f-ee41b94f66a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bb2fe2-75d9-4490-93ea-c36cb362ef2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b51d500-8c0c-43bc-98a1-57a41c6de837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bb2fe2-75d9-4490-93ea-c36cb362ef2c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2679c8dd-5895-4da7-b357-fcca3b393645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5ecd0266-47c6-4127-a124-a83d09b0be08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2679c8dd-5895-4da7-b357-fcca3b393645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ef3404-537c-4fd2-b656-4c24b4e0c390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2679c8dd-5895-4da7-b357-fcca3b393645",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0c0ecb7e-5164-46d6-92db-cfc5f88ac38b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ac3f7a67-99fc-4cc3-8912-04681f168651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c0ecb7e-5164-46d6-92db-cfc5f88ac38b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b63ee4-a26e-4ef9-b4df-499fe96367d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c0ecb7e-5164-46d6-92db-cfc5f88ac38b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "08594113-254d-4a7d-a926-f70dc110b225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9e34c6de-950e-4412-a9fa-b76aaa395016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08594113-254d-4a7d-a926-f70dc110b225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687be80c-56e5-496e-b79c-24b5487ebc33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08594113-254d-4a7d-a926-f70dc110b225",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b50536d1-b9c5-4157-a144-10af77f77441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "738c4dc4-f7b3-42da-9a4b-f7afbe870fee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50536d1-b9c5-4157-a144-10af77f77441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f67456e2-a3c7-41a0-b608-dd1be04ab251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50536d1-b9c5-4157-a144-10af77f77441",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "52b791be-9890-4610-8954-76bb80a1fc09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "d8a130e6-9f3a-42f4-9199-8a9797b1596c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b791be-9890-4610-8954-76bb80a1fc09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "428e4c6d-4b53-4fd7-9d52-b7e20de0d1c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b791be-9890-4610-8954-76bb80a1fc09",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6b7a75c5-5f1c-4b87-8852-833c4004ce55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0c582fcc-9b48-4e3d-a571-1c299fbf9b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b7a75c5-5f1c-4b87-8852-833c4004ce55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6416fef2-782a-4cef-9430-472a8c9d2ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b7a75c5-5f1c-4b87-8852-833c4004ce55",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "013a5cdb-9d62-47d6-8c66-5a12782a5e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5ae4fe0e-3e33-4c69-8118-fa338b28f0d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "013a5cdb-9d62-47d6-8c66-5a12782a5e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1301f7af-444b-4ad3-9c69-ee7cda825085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "013a5cdb-9d62-47d6-8c66-5a12782a5e0f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4b1056af-b56f-49ef-b3a3-5b17b95d6567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3caa265b-2c03-49bc-bf6c-6189e61d9272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b1056af-b56f-49ef-b3a3-5b17b95d6567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cb813d9-2cda-4e02-a268-8c42877561ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b1056af-b56f-49ef-b3a3-5b17b95d6567",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0b0cc786-13a4-4564-952b-86d2f7ea1b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0f2c8911-277d-473c-8c21-f745f6b1b64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b0cc786-13a4-4564-952b-86d2f7ea1b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ea2766-5c0c-4198-93a8-8394c336795b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b0cc786-13a4-4564-952b-86d2f7ea1b98",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4bc74488-e233-418b-b615-57549b56596c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a7590ad8-24fe-45f2-a2f8-944062ba9d0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc74488-e233-418b-b615-57549b56596c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b4994a5-cf8c-4123-8ed0-b6c1160fcc05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc74488-e233-418b-b615-57549b56596c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d8298183-885f-4ffd-b7cf-a5ece830cedb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ea4f8a7a-a836-4a30-b400-c753ef4a308a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8298183-885f-4ffd-b7cf-a5ece830cedb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53420aaa-2b1f-4f49-a121-ae4fb421294e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8298183-885f-4ffd-b7cf-a5ece830cedb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "96788a11-439e-4324-a542-40d58fb83cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "723c12aa-1e66-45b8-bc4c-7b9e150596cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96788a11-439e-4324-a542-40d58fb83cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5bbffae-78b8-4bf4-9d6b-1c2decb55daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96788a11-439e-4324-a542-40d58fb83cf2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7396d29f-14dc-46c5-b7ed-3572943ca67b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "0f2de3dc-bbfb-4640-b1d4-c3b4d453d97e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7396d29f-14dc-46c5-b7ed-3572943ca67b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f2292c-604f-4c26-b1ac-98a47e980cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7396d29f-14dc-46c5-b7ed-3572943ca67b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d22d4997-1dc3-4905-bf7a-5abc5d6a832d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bcae6ebb-966f-4e4a-b002-db3a395c496f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d22d4997-1dc3-4905-bf7a-5abc5d6a832d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34889873-b5ad-41e7-ae44-2b255ea55383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d22d4997-1dc3-4905-bf7a-5abc5d6a832d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "547d080d-66b9-4db8-a9e6-48446bfb6ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "25c4944d-37d3-48ea-80ab-631451e6475a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "547d080d-66b9-4db8-a9e6-48446bfb6ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f67ca9e4-63a8-4242-8953-126d873da61c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "547d080d-66b9-4db8-a9e6-48446bfb6ad4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "64a9e945-3899-430e-a10b-a37cbd740392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "211ca33c-26dc-4cfa-9b33-990cd778495e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a9e945-3899-430e-a10b-a37cbd740392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76bf3341-06e0-4fc7-958c-3f01b45a538e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a9e945-3899-430e-a10b-a37cbd740392",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "935e39eb-034b-451a-954b-bb0994867481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "390a5bdf-d379-4236-82f9-bc62a481ba6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935e39eb-034b-451a-954b-bb0994867481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea1aa5e-4870-45d7-94b4-17120ead8715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935e39eb-034b-451a-954b-bb0994867481",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e3a3f63e-cc49-41bf-93c5-5952a7f7bf6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bc0f4892-a9ae-47f1-99f5-0929ad9b94e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3a3f63e-cc49-41bf-93c5-5952a7f7bf6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "765ca3c3-53b2-49c9-b788-c89ed83778e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3a3f63e-cc49-41bf-93c5-5952a7f7bf6c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ccfc9989-ba2f-48fb-a072-f488d68f4470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9386ea30-6bbc-4fec-88f9-1566cd823d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccfc9989-ba2f-48fb-a072-f488d68f4470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b303bead-67a3-4eae-9591-3aacb97a01b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccfc9989-ba2f-48fb-a072-f488d68f4470",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7089a41a-3fc6-4e28-85e6-c46e3a98b8fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "6e138310-8660-459d-957c-8c0755eea4d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7089a41a-3fc6-4e28-85e6-c46e3a98b8fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67cb204-190b-48a4-883b-71ae8694aaf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7089a41a-3fc6-4e28-85e6-c46e3a98b8fa",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "82daef65-0644-415e-bec8-a0df1d97be81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "51601eb7-3834-4fa1-b73b-0977376fb06b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82daef65-0644-415e-bec8-a0df1d97be81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0e073b-19ee-4882-bd46-11f8c76bcdd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82daef65-0644-415e-bec8-a0df1d97be81",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "bdf46442-4982-46dd-92da-0507b1fc517c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "4590bd64-1685-430d-9557-efef44c17ae2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf46442-4982-46dd-92da-0507b1fc517c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1846557-498f-4c68-9530-1ac08bb24ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf46442-4982-46dd-92da-0507b1fc517c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0b158c23-9c0c-477c-9868-a9eecabb20c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f045fca4-414d-45da-b333-e083869de74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b158c23-9c0c-477c-9868-a9eecabb20c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a1d5383-961a-41b1-a367-1aeb60c7b87c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b158c23-9c0c-477c-9868-a9eecabb20c8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "725b6f9d-d0a5-4ed9-8003-943b17f2e718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ca09dc36-cca9-4633-84b3-a227836eb703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "725b6f9d-d0a5-4ed9-8003-943b17f2e718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d088329-81d6-4456-a00a-2b57369126ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "725b6f9d-d0a5-4ed9-8003-943b17f2e718",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2c436b19-1247-4ca3-b1f0-91d3f36b9d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9b1f564c-b15e-4e90-ba97-5d1d3ddaeea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c436b19-1247-4ca3-b1f0-91d3f36b9d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0466f22f-df1b-4050-abcf-b37243f51b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c436b19-1247-4ca3-b1f0-91d3f36b9d53",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "37ba4a42-cb91-4b1b-836b-45e48ed2c53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f6b97b6f-3a14-47c9-8855-200626ba2364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37ba4a42-cb91-4b1b-836b-45e48ed2c53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "364ca8fe-38d6-4309-aa24-b6ffd13727d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37ba4a42-cb91-4b1b-836b-45e48ed2c53d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d6dce83b-b65b-4474-a35a-ab540b08bec4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "79bae655-9d24-445b-bdbb-1af546fd4ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6dce83b-b65b-4474-a35a-ab540b08bec4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1945d6-e914-4f61-9d93-754954cc15f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6dce83b-b65b-4474-a35a-ab540b08bec4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "91b7d8aa-8f46-4de4-ae4d-5569f24f2b16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f6b2d34a-de98-4972-b015-8dac8f0b4482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91b7d8aa-8f46-4de4-ae4d-5569f24f2b16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "589d0a16-66ae-4d44-83f7-163711c61291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91b7d8aa-8f46-4de4-ae4d-5569f24f2b16",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "22f6859c-01ca-45fc-b784-7c7cacfb3a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "060762d1-d662-49dc-884f-52f36d06045a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f6859c-01ca-45fc-b784-7c7cacfb3a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4550b12-0ffd-4e59-ae2d-0fc92458d93c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f6859c-01ca-45fc-b784-7c7cacfb3a51",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "de322de0-c2df-41b1-859a-e114cfe7fc95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "511f5075-a5c6-435d-8061-9b366c40b3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de322de0-c2df-41b1-859a-e114cfe7fc95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "020fba78-78c8-439f-acaf-c5c049f40e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de322de0-c2df-41b1-859a-e114cfe7fc95",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "77f13091-51cb-47ca-b154-8df832423856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f6926bf7-0e55-42bf-ae37-edcf4117320f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f13091-51cb-47ca-b154-8df832423856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e55dae-6d3c-4b9d-afb3-99c9b6f8c144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f13091-51cb-47ca-b154-8df832423856",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b5e6fd96-53d0-4ef8-9358-148e895c7c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "16590398-1ae4-415a-8993-a933eae2c0c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e6fd96-53d0-4ef8-9358-148e895c7c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff7d33d-af00-4d0e-82e1-0f2af07598c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e6fd96-53d0-4ef8-9358-148e895c7c53",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e590214c-dc10-4c46-9aa3-d8b8e947148f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7a223c9d-dcea-4b8d-8902-fd869aca2c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e590214c-dc10-4c46-9aa3-d8b8e947148f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb1932e-124d-4fbe-a50c-910bad1c716f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e590214c-dc10-4c46-9aa3-d8b8e947148f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6f2556ad-c4e3-4781-9cf9-fa65fbae687d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bdddd558-d33c-4410-81df-2abb227a5cac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f2556ad-c4e3-4781-9cf9-fa65fbae687d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad1d0263-310b-4129-bea6-a9c0d9dd3829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f2556ad-c4e3-4781-9cf9-fa65fbae687d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "19d645ee-4907-4845-a8c6-de842e0251f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7779f00f-6504-4268-958a-f4d020afa9c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19d645ee-4907-4845-a8c6-de842e0251f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d85abb80-d3c0-45d1-a0c3-ab4fb90dd185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19d645ee-4907-4845-a8c6-de842e0251f7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "256450ca-41b5-4b55-9e79-b47f65a51275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c971b2e0-ab48-41b3-84ad-2e68b61fab8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256450ca-41b5-4b55-9e79-b47f65a51275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "699b7712-7965-49e3-828e-2cb97d906027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256450ca-41b5-4b55-9e79-b47f65a51275",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "e764e80f-071f-4b2f-8303-d9e78f5b332f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b0def84c-83c9-447f-80dd-563db6931c15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e764e80f-071f-4b2f-8303-d9e78f5b332f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32148b9d-240d-442d-9cc5-e3191bf97ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e764e80f-071f-4b2f-8303-d9e78f5b332f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "480bcd55-8437-41e8-ad22-5e82d17c0f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c17ef4a3-4669-4a6c-9f15-f6210a1ea711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "480bcd55-8437-41e8-ad22-5e82d17c0f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc32fe4-1c5b-44d1-b6f2-ce64b7cdf25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "480bcd55-8437-41e8-ad22-5e82d17c0f47",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9de8033c-b52f-4478-a820-60a7f266f86d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "c4c7ceef-b4c7-46ba-86e2-6b75abeb552e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de8033c-b52f-4478-a820-60a7f266f86d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658d15a5-2dd5-4c1d-abee-1cdb03926bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de8033c-b52f-4478-a820-60a7f266f86d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "b3427c23-f358-47d0-bf96-3cb331677c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f65e3135-76c0-4e06-bfb5-1a6461e3b901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3427c23-f358-47d0-bf96-3cb331677c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a08c716-543e-4384-9275-8e98fccf4123",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3427c23-f358-47d0-bf96-3cb331677c4b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ca988bd0-f8e3-4006-8414-9135dc5de449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7247dcb8-77b2-4acc-8df2-cd33b787a2b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca988bd0-f8e3-4006-8414-9135dc5de449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b147a696-e896-43eb-bffd-4c6a37379a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca988bd0-f8e3-4006-8414-9135dc5de449",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "aafaa702-6227-4704-a0ec-ee5f4790255a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "03e8c004-03b0-4779-ba7e-9e22152f73f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aafaa702-6227-4704-a0ec-ee5f4790255a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a249e281-9b43-4601-bbb9-a31500a18e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aafaa702-6227-4704-a0ec-ee5f4790255a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d87e922d-5d72-43ac-a45d-6b8743e649f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2bc614b0-5077-471e-b653-fc5bdd04de75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d87e922d-5d72-43ac-a45d-6b8743e649f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90321e2f-c666-43b6-b599-f6e479219c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87e922d-5d72-43ac-a45d-6b8743e649f4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8327c7fe-18a1-41d4-ba27-39e85e352eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "502fcca5-1cbf-4e6a-9b8d-8373b2786095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8327c7fe-18a1-41d4-ba27-39e85e352eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa4e6957-9b10-4c4f-97ed-97d9dee025f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8327c7fe-18a1-41d4-ba27-39e85e352eaf",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "fd58c575-cec1-4b21-8283-6005c0a1f254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8a4b0ec3-681c-4aa1-80c1-546c2595eb91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd58c575-cec1-4b21-8283-6005c0a1f254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e98eba9-a08d-4961-bb53-3c86eeaa907e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd58c575-cec1-4b21-8283-6005c0a1f254",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c7c95acb-7a5c-4e93-81de-9b8e82428dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "af1942fc-671e-4937-9346-98d0633b268b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7c95acb-7a5c-4e93-81de-9b8e82428dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20cfb722-8ded-4b58-891e-6f17991baadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7c95acb-7a5c-4e93-81de-9b8e82428dee",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d2f6da19-4e2a-443e-89ec-102ca43326b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5605a4d7-5b39-4a3c-a58c-e9a949331feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f6da19-4e2a-443e-89ec-102ca43326b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843cac5a-3b71-4ee9-a6c4-e7c3cb640ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f6da19-4e2a-443e-89ec-102ca43326b7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "55f2cbb0-2957-4182-bdb4-419eb39800a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "30cbe6ad-ceba-4692-8f0c-cd63352b58c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55f2cbb0-2957-4182-bdb4-419eb39800a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d116861-9b59-417b-a169-638661f73705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55f2cbb0-2957-4182-bdb4-419eb39800a8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7363f8e2-2842-4ce5-b68e-0aa6ca53ae14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "dda10cc9-f66b-4724-a520-6366b8aa1e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7363f8e2-2842-4ce5-b68e-0aa6ca53ae14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "894b4236-48c3-452f-83cb-c9cba3088f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7363f8e2-2842-4ce5-b68e-0aa6ca53ae14",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7410ea7c-9694-4a20-b761-46ed989b9fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cd95fac5-c14a-4cde-bed0-ae4d5c2c151c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7410ea7c-9694-4a20-b761-46ed989b9fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f92db02-f5ea-4558-bfaf-052768fc4e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7410ea7c-9694-4a20-b761-46ed989b9fcc",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "0086936b-3fdf-4527-96ce-09199e8c338f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9dcd622a-6bdc-4f36-bbe9-f8019583c36b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0086936b-3fdf-4527-96ce-09199e8c338f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bbe5371-ca23-4185-a187-10a126d568a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0086936b-3fdf-4527-96ce-09199e8c338f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4b1d7d4f-f936-453d-81f7-13c75b39ebb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ab3072f7-21ac-4ea9-94d8-35b2e3cca6b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b1d7d4f-f936-453d-81f7-13c75b39ebb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb80ea1f-a02b-47d3-bec7-7399350dae41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b1d7d4f-f936-453d-81f7-13c75b39ebb7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7923c493-cfa0-4b17-bb07-a859749b9400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "f4ec4091-b9bb-4fb5-a038-bda689733c3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7923c493-cfa0-4b17-bb07-a859749b9400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2ac15f-39e1-4467-861c-58cddb2363cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7923c493-cfa0-4b17-bb07-a859749b9400",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c8786cb2-90e6-462e-811d-b9cd665b84ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "49054db4-5bae-42ff-8f17-a8c0b6d79886",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8786cb2-90e6-462e-811d-b9cd665b84ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f54260e9-4926-4453-b939-c4cb8e2bfc3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8786cb2-90e6-462e-811d-b9cd665b84ed",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d211b7fa-f850-4e14-a6b8-42306b5a4241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1800ed0e-bbf1-4a27-8b7f-4fb2a932546c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d211b7fa-f850-4e14-a6b8-42306b5a4241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d76e3648-7d0f-4c37-b77a-1d739639f8cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d211b7fa-f850-4e14-a6b8-42306b5a4241",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "db4d5104-3a66-4932-99cc-d4b5c00072d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "03d6a660-d7e9-4457-892a-ae41da410e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db4d5104-3a66-4932-99cc-d4b5c00072d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da97cb12-5b50-4f8b-8b57-cf06d1dd43be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db4d5104-3a66-4932-99cc-d4b5c00072d7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c08ad999-b8b0-46c0-90c7-15610c13463f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "263e63c4-128c-4c8a-ab9b-b9a1e8cc9d49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08ad999-b8b0-46c0-90c7-15610c13463f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd9e6e1-0452-4007-9d21-a1fa0dc350b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08ad999-b8b0-46c0-90c7-15610c13463f",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "425e40a2-5177-46e0-bc88-9b84b3b11bcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "12082271-6519-44a9-b7af-c746e119b75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "425e40a2-5177-46e0-bc88-9b84b3b11bcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c68329f-063e-467e-9a93-5df7e814b4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "425e40a2-5177-46e0-bc88-9b84b3b11bcb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2f74c8fa-3d05-474a-8877-47ac34c728bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "83e80f81-0689-42c5-aeb8-d70833f307ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f74c8fa-3d05-474a-8877-47ac34c728bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d5d2db2-bbbd-455e-a3e1-92e710289f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f74c8fa-3d05-474a-8877-47ac34c728bf",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c4fd1f42-ba41-47be-b09d-d9049d155a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "742e5bff-6e66-4672-b211-864d1bd4f95e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4fd1f42-ba41-47be-b09d-d9049d155a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d0e380-47ad-415b-846c-afd5304658d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4fd1f42-ba41-47be-b09d-d9049d155a6b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "54f25d39-9936-4202-95b5-ea5e702cabbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "e1faa0fe-9e01-4c85-b538-f6ffe56ba384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54f25d39-9936-4202-95b5-ea5e702cabbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35e54f7-f731-4744-b330-f51df4d32273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54f25d39-9936-4202-95b5-ea5e702cabbd",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "591158c1-7d23-43ee-a114-947af32cd759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7b724268-a2f1-4c51-ba94-319ada3862ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591158c1-7d23-43ee-a114-947af32cd759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e69d7b00-c1d7-4892-acc4-b6cb7cab36de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591158c1-7d23-43ee-a114-947af32cd759",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "5bb25e2c-01e9-460c-9b60-bbe0af09500c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "941b4a93-78da-47bb-8898-1fd6586064e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bb25e2c-01e9-460c-9b60-bbe0af09500c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c9440f1-d869-4bcb-9e11-565b940199b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bb25e2c-01e9-460c-9b60-bbe0af09500c",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "855acf02-d12a-468d-bded-afdb5d529d91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b3a93b19-33f1-42cd-bea0-c3cf60d597ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855acf02-d12a-468d-bded-afdb5d529d91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b65dbbd9-89a3-42c2-ba3e-91424ff466af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855acf02-d12a-468d-bded-afdb5d529d91",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c13b5af2-8ee2-42fc-887c-4e13b8ecc371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "e54d198c-a30a-4de6-8c29-2cc8224a8510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13b5af2-8ee2-42fc-887c-4e13b8ecc371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27bea58c-bd5e-40f3-b8e6-46de090d3c17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13b5af2-8ee2-42fc-887c-4e13b8ecc371",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "61ea4904-1f2c-4b40-9e85-ca6255524631",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a8c4a246-3f5f-4a70-9702-dc9c0907a387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ea4904-1f2c-4b40-9e85-ca6255524631",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b0d4e7-d46a-4c67-9ddf-e4e8c52979a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ea4904-1f2c-4b40-9e85-ca6255524631",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "bb4afd21-e5dd-4dad-956c-cb779ff02c06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "3e1ade2d-f546-42da-8492-df804a8b66ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb4afd21-e5dd-4dad-956c-cb779ff02c06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a503ca93-c3dc-4c7f-9243-47c0c6de7a93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb4afd21-e5dd-4dad-956c-cb779ff02c06",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8accdaa3-f018-46d2-9158-ca968615304b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "8fe48efa-1529-4428-83b0-1e04cbcf3f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8accdaa3-f018-46d2-9158-ca968615304b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea69a06a-844e-4c14-a922-5e5265524411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8accdaa3-f018-46d2-9158-ca968615304b",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a7f59f35-b4a2-48fc-8e7c-7c75c4fe0870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2811ecd0-aeee-4afe-a970-231844ac7d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f59f35-b4a2-48fc-8e7c-7c75c4fe0870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c2d6ea-672b-44d7-881c-1b4dc33577b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f59f35-b4a2-48fc-8e7c-7c75c4fe0870",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "c5e19426-5640-4279-9a40-c58faa8796c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "4ab4c9b6-06f2-48a1-b591-18c8e14cc89c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5e19426-5640-4279-9a40-c58faa8796c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90776396-b207-4b7e-ac77-b19bb8b13d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5e19426-5640-4279-9a40-c58faa8796c1",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a4272994-e312-45ce-b42e-c17264186fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cda6e426-0da7-437e-b7e2-76ed1544737e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4272994-e312-45ce-b42e-c17264186fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f46ea024-ecef-4bd2-a375-6fa60d2006c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4272994-e312-45ce-b42e-c17264186fdb",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6de906de-d5f6-4ba3-809a-0681de223427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b04e207d-1951-4246-be99-6b013e268e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de906de-d5f6-4ba3-809a-0681de223427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a954ac1-f403-426c-acb7-a94861985996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de906de-d5f6-4ba3-809a-0681de223427",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "d63357bc-3254-4830-a07e-f2d2ab61ecfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "9ea9fce1-e927-4111-b923-3c67760e3c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63357bc-3254-4830-a07e-f2d2ab61ecfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b579335-8677-4fa5-a3c8-5ddf624a8784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63357bc-3254-4830-a07e-f2d2ab61ecfa",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "20992c1f-cb06-4696-b8d8-392d6eade094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fe928b9e-7f58-478d-81e1-979bc8f5e1b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20992c1f-cb06-4696-b8d8-392d6eade094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b1908c-5946-4df9-b91f-94c9594404ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20992c1f-cb06-4696-b8d8-392d6eade094",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "31ea2740-adba-4143-bf60-573cede9b262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "766b3c21-8ad9-469d-8880-df3b0f85f3a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ea2740-adba-4143-bf60-573cede9b262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8be0f6-3b0e-4ffa-a695-a884063aafc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ea2740-adba-4143-bf60-573cede9b262",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "a28b816d-c5d8-4fa5-acf6-0cf308157b91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "59b0c375-d74a-4446-8a08-70caf714cf24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28b816d-c5d8-4fa5-acf6-0cf308157b91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd858107-0e41-43cd-8083-e36c0cecf6a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28b816d-c5d8-4fa5-acf6-0cf308157b91",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "185ea464-d7ee-4903-a9c0-973b9a537481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7c553c97-b48d-4894-95e0-bd2c137b8b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "185ea464-d7ee-4903-a9c0-973b9a537481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5c85d0-d1b6-43ec-98dd-f498a67bbbe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "185ea464-d7ee-4903-a9c0-973b9a537481",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "351e62a6-fd5e-455d-a406-243722db1c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "a320aa45-f524-414a-8d8b-7e88755daad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351e62a6-fd5e-455d-a406-243722db1c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b10971-afea-469f-8f5c-b836ef303f10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351e62a6-fd5e-455d-a406-243722db1c5e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "23a0c492-32ff-496a-ad1b-03f93a1f62b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "24c37692-ebe3-4731-a795-30e85cf5d249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a0c492-32ff-496a-ad1b-03f93a1f62b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f2122fe-686a-4510-990c-ffe3a5d67767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a0c492-32ff-496a-ad1b-03f93a1f62b4",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "8e24b2c7-5f60-4251-97a2-adf514eddcc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "7ee68843-08ec-4631-8cd3-aaf86061619a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e24b2c7-5f60-4251-97a2-adf514eddcc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2f29bca-8b2c-44d2-bddd-2a100b4b280f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e24b2c7-5f60-4251-97a2-adf514eddcc8",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "284c7885-debe-4b6c-b7c6-93ed34e1bd24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "80f892ac-6844-4ea0-b80d-1a8294367fba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "284c7885-debe-4b6c-b7c6-93ed34e1bd24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3cfeba2-0a29-48d2-8caa-a9a44d1c56d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "284c7885-debe-4b6c-b7c6-93ed34e1bd24",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "cc19d1ae-24c3-40fc-97ad-090a90b90a46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "aee2a5b7-4d11-4f6b-961f-138c49bd9c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc19d1ae-24c3-40fc-97ad-090a90b90a46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a402de3e-80c5-4338-a84e-f0ecdfd1f6c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc19d1ae-24c3-40fc-97ad-090a90b90a46",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "2d22b77b-0ba2-4741-a39c-be7e3baabbae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "aa88a131-fa3f-4c7c-aa17-5b316ace24b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d22b77b-0ba2-4741-a39c-be7e3baabbae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164e1f16-b76b-4853-b907-5d69edbb4035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d22b77b-0ba2-4741-a39c-be7e3baabbae",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "13b5a585-52af-46dc-bec7-d23c64648ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5eb0d639-21af-4d62-a66b-d36d14a54a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13b5a585-52af-46dc-bec7-d23c64648ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec6d5c00-2a90-4c82-be17-770fee5abd4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13b5a585-52af-46dc-bec7-d23c64648ef3",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "3cf6e667-afe7-40ef-9c68-2508e02fc2b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "fdeeaafa-4381-450f-a434-e67ea1af3837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf6e667-afe7-40ef-9c68-2508e02fc2b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb44ed9f-359b-4174-b353-58a193251905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf6e667-afe7-40ef-9c68-2508e02fc2b2",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "4a5c3064-144a-4c88-bd11-2c7813504430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "52da501f-bd5d-4ba6-b681-881d3b91f9ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a5c3064-144a-4c88-bd11-2c7813504430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f29963-16a0-4137-b917-1c8e02f63fd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a5c3064-144a-4c88-bd11-2c7813504430",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "9205f1a1-5810-472e-9fd6-58da5d8059cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "36101eac-ed06-4ca7-af92-d3a0121a437d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9205f1a1-5810-472e-9fd6-58da5d8059cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3829b87d-3216-4a06-af93-6e338a56cd57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9205f1a1-5810-472e-9fd6-58da5d8059cf",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "65426f3d-8555-4bf4-9987-4d26d68eddd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1684fdad-b942-4376-8a1f-f60555b0ca25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65426f3d-8555-4bf4-9987-4d26d68eddd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf1471a-6252-4395-9976-38c0925c4fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65426f3d-8555-4bf4-9987-4d26d68eddd7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "76b9b08d-de07-48df-ae9b-072eb9d0285a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "b3d3debf-6645-4058-9a04-962da6a15f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b9b08d-de07-48df-ae9b-072eb9d0285a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "103f3d38-edb7-4c7b-80d5-4d49aef90bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b9b08d-de07-48df-ae9b-072eb9d0285a",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "7b8da453-f270-4c31-8f1b-c6d43492bf95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "5bdaf00b-7409-4573-af17-cdab2fcf9bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b8da453-f270-4c31-8f1b-c6d43492bf95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1663e389-cfef-4a81-9336-794deb607a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b8da453-f270-4c31-8f1b-c6d43492bf95",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "31827354-4da1-454c-9f3e-e3fa2950b7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "532ba1a3-49ea-4e2b-b8c2-7265c8e63279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31827354-4da1-454c-9f3e-e3fa2950b7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2017b44-e72a-43ea-8060-2aa1c8426633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31827354-4da1-454c-9f3e-e3fa2950b7a7",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "046082f4-288a-49be-b518-b822de01a54d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "bbb29718-0ab2-4034-ac77-a29078aebe56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046082f4-288a-49be-b518-b822de01a54d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26750238-23f4-48ed-af3b-d0ba06005e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046082f4-288a-49be-b518-b822de01a54d",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "70fc452e-4de8-4080-b509-571551bed70e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "1f5e882e-0797-4083-9764-7016597ed69b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70fc452e-4de8-4080-b509-571551bed70e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7560fc41-9db3-4a64-b14d-5955d7c44e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70fc452e-4de8-4080-b509-571551bed70e",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "f39d8467-ff70-49c3-abf9-02685ac27fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "cc5d297d-3be8-47f6-a8d2-daf7b74ea029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f39d8467-ff70-49c3-abf9-02685ac27fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65161d73-b9c8-4218-9de8-63c2e5712174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f39d8467-ff70-49c3-abf9-02685ac27fa5",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "6613194f-ea23-4750-a153-afe9da2e06bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "2eed495e-1f74-4e2b-b4d8-0cca2d8b43cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6613194f-ea23-4750-a153-afe9da2e06bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28afc99-7ab4-420d-9ff9-00c99d0f7f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6613194f-ea23-4750-a153-afe9da2e06bf",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "39cfc3dd-cadc-4ba6-b087-b45b655e4f70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "ca89f031-3de2-43a6-ae93-2ab00f8ceb51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39cfc3dd-cadc-4ba6-b087-b45b655e4f70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257d9a82-8c52-4804-8dd3-e0f224b42592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39cfc3dd-cadc-4ba6-b087-b45b655e4f70",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        },
        {
            "id": "ed8fdd30-45f1-49df-bb33-343d9b0c4472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "compositeImage": {
                "id": "527b7a16-7474-4bc7-b5b5-76da2f187523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed8fdd30-45f1-49df-bb33-343d9b0c4472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a84baa3-a3d1-4117-b018-9fc72dc9a13e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed8fdd30-45f1-49df-bb33-343d9b0c4472",
                    "LayerId": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "99fde1ea-1cf6-4535-a222-e92e0afaa6f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffefbbf2-ecf2-470c-a9bd-003fdd8494af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}