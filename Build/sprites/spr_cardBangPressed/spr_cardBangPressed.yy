{
    "id": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardBangPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed21f2aa-c7a3-4718-86ef-45b66670dc53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "2e506669-acfb-435d-bcd2-bb0116569c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed21f2aa-c7a3-4718-86ef-45b66670dc53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a3995f-8b36-4efe-8846-61310e4a9461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed21f2aa-c7a3-4718-86ef-45b66670dc53",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "5f2fbc07-1062-4309-8b57-58cffe67966f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "f89d89a5-af69-4122-8874-fcd1325a2555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f2fbc07-1062-4309-8b57-58cffe67966f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded2f587-0e64-459e-b464-a79203c3dc3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f2fbc07-1062-4309-8b57-58cffe67966f",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "f2850cf0-87bb-4aab-88e9-b817a6d02bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "d202be4d-c211-4d2d-9a53-17c1cc032eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2850cf0-87bb-4aab-88e9-b817a6d02bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e0804f-6506-4115-8bd5-944f33e8f5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2850cf0-87bb-4aab-88e9-b817a6d02bc8",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "f80f7a5b-1cf1-4124-929c-e36c9d32f477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "2f2b5d05-82fe-4369-8a0b-d33d900baf8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f80f7a5b-1cf1-4124-929c-e36c9d32f477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e196d6d-e576-45e2-ab36-b4e92ddb2f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f80f7a5b-1cf1-4124-929c-e36c9d32f477",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "f2bae54a-2837-4897-9930-ae9bf4478b36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "3ce08033-1029-4bcb-a58f-a690d594b800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2bae54a-2837-4897-9930-ae9bf4478b36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "690e62a3-9726-4588-a2ef-e5378bbece75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2bae54a-2837-4897-9930-ae9bf4478b36",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "c989f23a-5835-4681-af48-226a51963d6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "201ea49d-0737-4402-9c2d-97084621ec90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c989f23a-5835-4681-af48-226a51963d6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de658d7-5f6e-4832-9f81-bdea89d156d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c989f23a-5835-4681-af48-226a51963d6a",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "20a74fff-ea88-4738-898c-9fd25dd760d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "7b31d368-1c9f-48db-8671-90660832e913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20a74fff-ea88-4738-898c-9fd25dd760d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd2c402f-1e07-4d16-8b52-d8dd4aefc2fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20a74fff-ea88-4738-898c-9fd25dd760d1",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "8ea2d5df-d206-405f-be44-9addc66318b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "d6d9d5db-ac6d-43f7-adc9-1cd72eda8c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea2d5df-d206-405f-be44-9addc66318b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ecbad4-3b3f-4bfd-bb1e-115e09cc2fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea2d5df-d206-405f-be44-9addc66318b7",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "25278a29-eaef-48ba-aa4f-3608041be184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "e7e9de3f-8e4c-43fc-8a99-3a3c836d9486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25278a29-eaef-48ba-aa4f-3608041be184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adff0622-c3e0-4109-8f0b-3484749ef2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25278a29-eaef-48ba-aa4f-3608041be184",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "d0597493-39b0-4167-808f-147ed05f324a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "8ba9b6c3-616b-4f46-9835-ae23d87b4567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0597493-39b0-4167-808f-147ed05f324a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0dd23ab-00c9-4b57-9f9a-12cb36c38c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0597493-39b0-4167-808f-147ed05f324a",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "02917e9e-4a50-4edb-8b63-2319649f6541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "fa1fb7ee-5145-44ef-a626-59bc99847b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02917e9e-4a50-4edb-8b63-2319649f6541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a02fec5-e6c8-4d24-b563-77a23cb68e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02917e9e-4a50-4edb-8b63-2319649f6541",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "d25aea50-9f24-4e26-95f0-3c5e2904bfb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "03ebf194-5821-4a97-a9e0-b50a77211da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d25aea50-9f24-4e26-95f0-3c5e2904bfb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e23d20cc-02d7-4d9f-ac43-f20922e6f409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d25aea50-9f24-4e26-95f0-3c5e2904bfb2",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "fb86220b-aff4-49af-be25-369cec402dff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "4aea991e-23ea-4e5c-9647-a8905a3945a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb86220b-aff4-49af-be25-369cec402dff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a38d9f-3383-48c6-b84d-18d5d2838ca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb86220b-aff4-49af-be25-369cec402dff",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "dfd53292-368d-44d7-8c28-b6ca1e3691bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "2a829b8a-5155-403b-98c2-6e3e410c1f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd53292-368d-44d7-8c28-b6ca1e3691bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb05cd1-2ed7-410d-b1ea-bfaf07db374f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd53292-368d-44d7-8c28-b6ca1e3691bd",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "54521c0d-73d0-434a-b285-39cdf1f16465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "de86ad85-34c6-4244-b8ae-7af209e0fc20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54521c0d-73d0-434a-b285-39cdf1f16465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9570450e-5928-4903-91e8-b151b579a1a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54521c0d-73d0-434a-b285-39cdf1f16465",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "74895420-eb4b-4c26-b3f5-3c9a28a5be97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "7a62fe73-733f-4bcf-8e6c-d1c7c37cbcbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74895420-eb4b-4c26-b3f5-3c9a28a5be97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff14a5e3-6797-4f2e-825d-f6cdff67c08c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74895420-eb4b-4c26-b3f5-3c9a28a5be97",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "62d454bf-c68f-4295-9d3e-b917009a449a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "9dd591af-c412-4091-b654-0bd045dfb090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d454bf-c68f-4295-9d3e-b917009a449a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "813bfb2a-ea72-4ec6-acac-b4d5d8d401d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d454bf-c68f-4295-9d3e-b917009a449a",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "dd26cbe2-e0b1-4933-93f6-b59eef806e11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "09e91bab-5e30-48f3-8047-bbb8c303b894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd26cbe2-e0b1-4933-93f6-b59eef806e11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c26f4575-a0cc-4e6f-8d12-f178d1cde67e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd26cbe2-e0b1-4933-93f6-b59eef806e11",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "dc7fe8e5-77b2-4d3c-90fc-7a1725d6e8a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "102e6807-1d04-453c-9158-fdf5e1ae2758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc7fe8e5-77b2-4d3c-90fc-7a1725d6e8a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39e372b0-a5f2-40d3-bd31-74991f3d1173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc7fe8e5-77b2-4d3c-90fc-7a1725d6e8a8",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "15232ae6-1477-4e8f-966d-ba45a46b58c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "125611c7-0320-4a41-8f4f-04e61219504b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15232ae6-1477-4e8f-966d-ba45a46b58c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d77ea8c4-54f9-425d-b83c-089beb631c94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15232ae6-1477-4e8f-966d-ba45a46b58c3",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "4b2f03de-86aa-45d0-80be-2484cd481fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "a8625bbe-9670-420a-918d-f2e04bee425d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b2f03de-86aa-45d0-80be-2484cd481fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4479da20-f6cb-420a-b873-b34803683d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2f03de-86aa-45d0-80be-2484cd481fac",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "711b630d-52d5-48d7-ba38-ed37d2520286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "45fe1234-552a-4563-a79b-cfd1ee64bc91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711b630d-52d5-48d7-ba38-ed37d2520286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc48362-4738-468f-9757-686a399c9de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711b630d-52d5-48d7-ba38-ed37d2520286",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "c8b14f4f-88dc-43b3-a337-59e493e3ecf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "610c878b-5ee1-4e9b-9214-60fe2ae905f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b14f4f-88dc-43b3-a337-59e493e3ecf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16f7141b-9cc2-4bc8-ba3d-a9383b5a71e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b14f4f-88dc-43b3-a337-59e493e3ecf0",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "c1b3106f-4a66-47bc-9405-0576d9adfa28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "aac95e6d-1fb8-4409-8b84-e4ecb56430e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1b3106f-4a66-47bc-9405-0576d9adfa28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676a3fdd-b174-4e72-b6ef-08a1fd50b7e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1b3106f-4a66-47bc-9405-0576d9adfa28",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "151bcabc-6654-4c87-8c6a-7a0fb3eec682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "560160c4-3cac-405a-bc90-f4801ef65f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "151bcabc-6654-4c87-8c6a-7a0fb3eec682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f532d6-6cce-4dea-8b16-8964fe612f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "151bcabc-6654-4c87-8c6a-7a0fb3eec682",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "b5ce31ec-5534-40cf-8f67-f0d045c7073a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "fc152239-fa10-45c6-a730-31d6c4b0a948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5ce31ec-5534-40cf-8f67-f0d045c7073a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfceb8a8-fe86-4a03-9640-7a0579b973ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5ce31ec-5534-40cf-8f67-f0d045c7073a",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "4b032ee5-f050-4d33-8da4-8ce267fcc2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "01fcbca6-567d-45d1-83a3-d32a13fd90af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b032ee5-f050-4d33-8da4-8ce267fcc2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86e18987-1158-4ec4-b170-b34ef7107e11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b032ee5-f050-4d33-8da4-8ce267fcc2d0",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "3dca4711-bab7-4091-b38e-23f504c5c93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "8f070888-79f8-4bc2-9efd-758e4c9d44f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dca4711-bab7-4091-b38e-23f504c5c93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b45a8d9-43df-4edb-b06c-c8051f179103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dca4711-bab7-4091-b38e-23f504c5c93c",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "c61e0ae9-4fde-484b-b414-18c8ee10bf19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "a1750760-51c0-48a8-a1cc-e628b0306a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c61e0ae9-4fde-484b-b414-18c8ee10bf19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda724d9-abf0-482f-a22a-5660bed8109b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c61e0ae9-4fde-484b-b414-18c8ee10bf19",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "8f2d6581-0ffa-4470-818c-cfd8c15d4ebe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "d741f337-5aae-4df6-9dc6-73c818a83c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f2d6581-0ffa-4470-818c-cfd8c15d4ebe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfed65b-950f-4f9a-b8ff-59a25d68dfc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f2d6581-0ffa-4470-818c-cfd8c15d4ebe",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "7e90d493-c86f-4ece-87c8-1642c3973d5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "5bc32788-0948-4d1b-91e4-b7a816ba23d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e90d493-c86f-4ece-87c8-1642c3973d5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cbfa48f-0cb0-439e-a27b-b782a3fee31d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e90d493-c86f-4ece-87c8-1642c3973d5c",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "59044c2a-fc15-4a1e-a445-caf84a463a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "9b514346-6d43-44fa-a285-3aa92d4deb64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59044c2a-fc15-4a1e-a445-caf84a463a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8d21778-bfc7-44b8-9951-801f96c1f0ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59044c2a-fc15-4a1e-a445-caf84a463a24",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "42396982-89c0-4e95-8541-efeae3034631",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "99c65298-a977-405b-b871-5cc8cd0f11aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42396982-89c0-4e95-8541-efeae3034631",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63a808cb-f337-4a0d-bed1-a1968dca09f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42396982-89c0-4e95-8541-efeae3034631",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "b24d77a7-e007-423f-a80a-7f0bd4cd2e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "e81a6209-bc1e-4646-9d02-08564d69d7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b24d77a7-e007-423f-a80a-7f0bd4cd2e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f107f8f-3cb6-44a8-9e7e-dc8822bca7f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b24d77a7-e007-423f-a80a-7f0bd4cd2e7e",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "aecddcbb-02f3-4f56-9abb-6ad406b238b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "a575367d-028c-4fc1-bb07-bf28ecdf00e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aecddcbb-02f3-4f56-9abb-6ad406b238b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cde4cd9-5dae-4770-934b-fe82c6e96798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aecddcbb-02f3-4f56-9abb-6ad406b238b5",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "b2a73d21-9d22-44c1-96db-97578d1a539c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "2b7536e1-4797-41ff-929d-731b3e897374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a73d21-9d22-44c1-96db-97578d1a539c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cacc7a1-b4d7-4026-9900-498bb6b0f77f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a73d21-9d22-44c1-96db-97578d1a539c",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "796b3054-4379-4275-b100-7b82220d48ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "4499302e-932b-4009-ac28-ece4fb4346c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "796b3054-4379-4275-b100-7b82220d48ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda482e5-4d14-45a4-aed1-4774ca01e937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "796b3054-4379-4275-b100-7b82220d48ea",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "18f8e8ff-22ae-44d1-8dae-dd79c8331853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "45b63466-61be-429c-9b28-1041092a8e6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f8e8ff-22ae-44d1-8dae-dd79c8331853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf09ddc-5f4f-496c-83e7-7c53306a9263",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f8e8ff-22ae-44d1-8dae-dd79c8331853",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "73c53c76-c585-477a-908d-dce210d3d88a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "36ca4765-a77c-4900-b7dd-c2d65e26ff7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c53c76-c585-477a-908d-dce210d3d88a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90068d5a-691f-4bfc-ac6b-fb70eb2f40ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c53c76-c585-477a-908d-dce210d3d88a",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        },
        {
            "id": "ba3e8007-2615-4127-9613-ded03e8fc706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "compositeImage": {
                "id": "1b38d952-7d73-466d-91e8-8c19b736890d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba3e8007-2615-4127-9613-ded03e8fc706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8afb3868-e6a9-432f-9b38-8070bba0834c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba3e8007-2615-4127-9613-ded03e8fc706",
                    "LayerId": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b15e59a9-7f51-4f67-8d2c-294e8cfaeade",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}