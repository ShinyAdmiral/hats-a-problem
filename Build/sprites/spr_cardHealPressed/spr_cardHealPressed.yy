{
    "id": "ec712919-200f-483f-b6ae-6480465fcc1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardHealPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "865c6679-1fc2-403c-8264-904db98c2cee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "9708ea7c-d5aa-4280-ad78-9d4539e759f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "865c6679-1fc2-403c-8264-904db98c2cee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c6dfc1-9f79-4f42-9441-967258e19b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "865c6679-1fc2-403c-8264-904db98c2cee",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "62e6b2bb-e743-435e-8ab3-d2817ba76707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "0e727c51-055e-4f76-83dd-130f9c8f6ce8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e6b2bb-e743-435e-8ab3-d2817ba76707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a84e6d1-fe6a-426a-8bf1-450743460a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e6b2bb-e743-435e-8ab3-d2817ba76707",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "38436f9e-7e4e-4aad-9ac8-c211ed4a5e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "49f1249a-205b-4539-bf40-993afadd9349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38436f9e-7e4e-4aad-9ac8-c211ed4a5e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "510beb39-28b5-4159-9920-e1a241fd66b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38436f9e-7e4e-4aad-9ac8-c211ed4a5e0a",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "d3bb6cda-ff15-4bd2-af3d-c29164a1d110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "cb27c54a-fddc-4fb2-b374-3d8559a83a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3bb6cda-ff15-4bd2-af3d-c29164a1d110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc4f5d9-e5ca-43e2-ae93-d23312bf8fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3bb6cda-ff15-4bd2-af3d-c29164a1d110",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "abfa23c5-0a15-475a-9bea-6d46fda91804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "b960bff9-b153-4524-bb16-8e1d50de5616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abfa23c5-0a15-475a-9bea-6d46fda91804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f50d7c0-a5e7-45bf-80e3-7c9831901a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abfa23c5-0a15-475a-9bea-6d46fda91804",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "843708ec-c5fd-4168-8c25-8c0726fc660b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "9644f8d1-ce8d-4682-9165-a35387ebd2ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843708ec-c5fd-4168-8c25-8c0726fc660b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd8a4d5-06ae-4cd0-afcd-c2b2bc67efa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843708ec-c5fd-4168-8c25-8c0726fc660b",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "a0b47268-5cb1-4f2e-82f9-8a948bb2162d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "be5eb2a9-4acc-4a9b-a078-03644ce76ca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b47268-5cb1-4f2e-82f9-8a948bb2162d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2efe4271-f3c9-4799-b566-50a7ab01aea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b47268-5cb1-4f2e-82f9-8a948bb2162d",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "d3a31432-05cb-43ff-baae-8be23f1ebc77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "38bbb6d4-ce40-47b5-9a3b-975c7a1afd1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a31432-05cb-43ff-baae-8be23f1ebc77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fa022ff-fc8c-4d8f-9e02-e6eca580713b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a31432-05cb-43ff-baae-8be23f1ebc77",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "452b184b-e637-477b-8eb9-0ce2baf3c90f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "494877f6-e1d7-429a-930e-f93c346c6072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "452b184b-e637-477b-8eb9-0ce2baf3c90f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e6fe557-cf64-4e8f-9994-8f8472370e25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "452b184b-e637-477b-8eb9-0ce2baf3c90f",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "8d72d334-6b6d-43e1-8b6f-ddbae498c05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "8ecd40b2-5d38-4bb3-a505-c3a30d6ec2be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d72d334-6b6d-43e1-8b6f-ddbae498c05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "022a5e83-dab0-4e36-b234-c882328761b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d72d334-6b6d-43e1-8b6f-ddbae498c05d",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "41a8fcd8-3484-4525-b732-0a283c5e937f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "52954027-adcb-4265-a993-b3403cd197cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a8fcd8-3484-4525-b732-0a283c5e937f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d38fa22-1de2-4632-bb84-dfdd17323ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a8fcd8-3484-4525-b732-0a283c5e937f",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "ac36e8d7-b94e-4d0b-a629-5e97d7dc5297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "34de425a-57db-426b-9da6-53b5b3058bf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac36e8d7-b94e-4d0b-a629-5e97d7dc5297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2582f9c5-891c-4cc1-a1c3-15ce6af076d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac36e8d7-b94e-4d0b-a629-5e97d7dc5297",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "33815006-02b9-4679-b20f-e4fa07b9ee7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "8c69ca9b-7a15-4e3b-91b8-3a5f74f16ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33815006-02b9-4679-b20f-e4fa07b9ee7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "637e4066-aa19-40b4-a7aa-5a028f12df9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33815006-02b9-4679-b20f-e4fa07b9ee7f",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "ea5a0a96-305f-4a49-be6b-df4fc99daaf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "38d22368-a3ce-4d4d-a272-f6cf841a5e8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5a0a96-305f-4a49-be6b-df4fc99daaf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96dfbae-ed39-4fe1-bce2-ee2022d6812b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5a0a96-305f-4a49-be6b-df4fc99daaf3",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "c6aeed30-32bb-469e-911d-26ede52ba61b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "1cefa670-3aa7-43cc-b05a-b80bda1411be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6aeed30-32bb-469e-911d-26ede52ba61b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71242339-a468-4d94-9c29-8ee923b41a1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6aeed30-32bb-469e-911d-26ede52ba61b",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "667db9c0-31ab-411b-959b-991a5ab26381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "963a6859-6c01-48a6-abcc-86f8e32be794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "667db9c0-31ab-411b-959b-991a5ab26381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecba54fc-412c-4cec-a81c-9c4e0d3b8cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "667db9c0-31ab-411b-959b-991a5ab26381",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "30028f9a-3928-4308-8890-3d0d18e0f159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "512a5d1e-b0a1-4461-a2b6-6564350a61a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30028f9a-3928-4308-8890-3d0d18e0f159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "659a7691-9f90-43a3-bdb7-1bc0f2ec88e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30028f9a-3928-4308-8890-3d0d18e0f159",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "549d8dfa-3785-403a-aec4-e46b83f82297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "f447eeb0-e131-4a49-a7fc-9871e37dd2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "549d8dfa-3785-403a-aec4-e46b83f82297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "818b82df-ecf7-41cb-8157-f0cfbb446939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "549d8dfa-3785-403a-aec4-e46b83f82297",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "1c90962b-2f81-4be6-9e56-f55a521b782b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "4392958c-5258-4475-80d7-3c6c0ee5ce89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c90962b-2f81-4be6-9e56-f55a521b782b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f72a00de-5e61-4ffd-92e6-9a7254cf68a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c90962b-2f81-4be6-9e56-f55a521b782b",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "89b2dfc6-981d-4a73-90cf-c5ca0b928473",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "f80ea104-f649-4b1f-b328-53648562130d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b2dfc6-981d-4a73-90cf-c5ca0b928473",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4611841e-7162-42fe-8079-31f9bb9cf0bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b2dfc6-981d-4a73-90cf-c5ca0b928473",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "cc009217-bc7d-4f6d-b2e7-a0c2f9635c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "fd6e36e9-c165-413f-83ef-670c1b2475b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc009217-bc7d-4f6d-b2e7-a0c2f9635c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "197f24c4-e895-457b-86e4-4e40a4da9a7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc009217-bc7d-4f6d-b2e7-a0c2f9635c9e",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "58084709-b909-4bb7-be36-ec0770780c6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "89bfbe09-f01d-4c7a-9a0b-95ca3cdc82b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58084709-b909-4bb7-be36-ec0770780c6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a3ca17-a6be-4655-b9ad-ad0ae5bc9fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58084709-b909-4bb7-be36-ec0770780c6c",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "a32433eb-af2c-4c90-9549-9f18b3eaa660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "7e9bb6c6-86c4-42d1-8d1a-6e7e68f592ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a32433eb-af2c-4c90-9549-9f18b3eaa660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "037f7a49-efa1-4f7d-95b3-4ab6831d7650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a32433eb-af2c-4c90-9549-9f18b3eaa660",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "915b91b0-9f03-435f-9197-09af5d29cc45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "81b93b0d-1650-4b40-90c1-3806209f159d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "915b91b0-9f03-435f-9197-09af5d29cc45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af5da673-aa3d-4782-9f39-95f82f6c8a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "915b91b0-9f03-435f-9197-09af5d29cc45",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "41cf192f-8ea3-4264-8390-ba33a1730c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "e2fdcd2f-9eb1-488a-afbe-f289d03028b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41cf192f-8ea3-4264-8390-ba33a1730c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016c0cba-2821-4f1c-9c33-fcee22f01831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41cf192f-8ea3-4264-8390-ba33a1730c04",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "0fd40587-e7d3-4560-8c8a-af6b160bd96c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "3c09dd05-3bb7-4c09-911f-5505f03632e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd40587-e7d3-4560-8c8a-af6b160bd96c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a21eaf4-e957-438c-af22-01c17a25ba0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd40587-e7d3-4560-8c8a-af6b160bd96c",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "97c0b5a6-efaa-4382-80bd-51f5634a1165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "513b45e5-304a-44c2-9c58-387da2bec55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c0b5a6-efaa-4382-80bd-51f5634a1165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07bd0bce-ef45-4acc-9fb1-5e5e37beeedf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c0b5a6-efaa-4382-80bd-51f5634a1165",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "1c23bf35-6a4b-4efe-99db-57f072e63560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "67c5969b-f1e5-490c-ad76-1e4894fe7726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c23bf35-6a4b-4efe-99db-57f072e63560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "225b4419-d701-4347-95db-759300996ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c23bf35-6a4b-4efe-99db-57f072e63560",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "a36322c5-32ff-496f-9c75-a4591def6a5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "673fa41e-1c45-4b24-8bd5-6436e21807c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36322c5-32ff-496f-9c75-a4591def6a5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "528fa727-8a6e-4e8f-b329-4c614c879f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36322c5-32ff-496f-9c75-a4591def6a5d",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "e2365580-8d22-4bfb-bfa5-7a9d70b34914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "d2b8893c-c404-4699-bc61-073e60767283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2365580-8d22-4bfb-bfa5-7a9d70b34914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d3f0b30-918a-4986-a601-d40d90bb4b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2365580-8d22-4bfb-bfa5-7a9d70b34914",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "0fe44b73-540b-4326-8e00-9b6e4252dd85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "b8dbea71-8560-4b0f-ad6f-af9e0736b725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe44b73-540b-4326-8e00-9b6e4252dd85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1679df5b-33f8-45fb-b36b-c51818ff9b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe44b73-540b-4326-8e00-9b6e4252dd85",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "c6f86617-c273-4841-9a09-f3c79200cb18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "c621083d-3354-409f-b541-9c3165169fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f86617-c273-4841-9a09-f3c79200cb18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8ac068-32f6-44f8-8ec0-869a6a8bfceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f86617-c273-4841-9a09-f3c79200cb18",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "3677628c-450a-45d9-955a-1420df0efea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "da66ccc7-b3b9-41bd-b3ea-1b1016035888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3677628c-450a-45d9-955a-1420df0efea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc9bdfc5-b403-4ded-8ca4-7cadd31a50c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3677628c-450a-45d9-955a-1420df0efea4",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "d15fb63c-7164-4e85-9207-0787f964f77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "48c693ec-fed6-44af-a274-b89c21b73973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d15fb63c-7164-4e85-9207-0787f964f77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243ae97a-7d1e-40b4-9bac-814310f8894f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d15fb63c-7164-4e85-9207-0787f964f77d",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "bf9256f6-0b3a-4179-9b88-9d83746d26fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "a1c6f574-201d-47ad-9e68-bd8c13f5b741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf9256f6-0b3a-4179-9b88-9d83746d26fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9cd02b9-5c39-4a04-8f2e-e09bb2582ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf9256f6-0b3a-4179-9b88-9d83746d26fc",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "553d0ab4-a33a-4363-b81e-f4cb77d74d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "014abeb9-d240-4912-9df3-80fce010699c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553d0ab4-a33a-4363-b81e-f4cb77d74d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095aeb86-2c56-4f6a-a605-737973a2a192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553d0ab4-a33a-4363-b81e-f4cb77d74d3a",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "12e11f30-803b-4f6a-b49b-c9b3d51cd623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "4dc2e99b-3e73-47a7-808d-aa53cd6e0df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e11f30-803b-4f6a-b49b-c9b3d51cd623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92affd87-c03f-4da8-8469-2a2e229cf705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e11f30-803b-4f6a-b49b-c9b3d51cd623",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "5d872557-cc4e-4d0e-ba91-f04d91a73df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "ecb1e56d-adef-4230-8bca-1e87fb1ab142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d872557-cc4e-4d0e-ba91-f04d91a73df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25fa607c-6497-403c-b134-4160a3612ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d872557-cc4e-4d0e-ba91-f04d91a73df1",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "e798a0cb-1ae9-42be-87e1-4215d6ccf6b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "ae708c9a-8caf-4507-b924-24b23d43b575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e798a0cb-1ae9-42be-87e1-4215d6ccf6b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d7f1bb0-cf6b-4053-b932-dd1308230ff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e798a0cb-1ae9-42be-87e1-4215d6ccf6b3",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        },
        {
            "id": "ada004e6-fac8-4570-83a7-cbd2de9643c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "compositeImage": {
                "id": "fb6641ba-b17f-43cc-878b-871194a4fbc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ada004e6-fac8-4570-83a7-cbd2de9643c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ccb86b1-ca8b-480d-8dd6-d7e9e926ee4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ada004e6-fac8-4570-83a7-cbd2de9643c5",
                    "LayerId": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "8f9d8635-5a6b-4094-a9f2-51e0a919e42b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}