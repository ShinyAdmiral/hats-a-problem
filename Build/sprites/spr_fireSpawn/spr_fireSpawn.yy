{
    "id": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireSpawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cf0cc49-d634-4df2-9a55-66ae9a062ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
            "compositeImage": {
                "id": "4bed04a5-9c5d-4f99-b052-1e5812a47898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf0cc49-d634-4df2-9a55-66ae9a062ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15920c63-c547-408d-9fa2-9bbb04dfe82a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf0cc49-d634-4df2-9a55-66ae9a062ee2",
                    "LayerId": "a183909c-ebc1-4cac-a6e0-e4cc26b965af"
                }
            ]
        },
        {
            "id": "c30f89f6-a6a4-4d37-9dc3-5f356030f2ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
            "compositeImage": {
                "id": "b2ea7bf4-fd8c-4c54-8e5e-78743124780b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c30f89f6-a6a4-4d37-9dc3-5f356030f2ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ec87f3-9d7e-4036-8e72-46267a5faa29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c30f89f6-a6a4-4d37-9dc3-5f356030f2ee",
                    "LayerId": "a183909c-ebc1-4cac-a6e0-e4cc26b965af"
                }
            ]
        },
        {
            "id": "3de09f28-dafa-4c54-ad07-251a47be51a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
            "compositeImage": {
                "id": "9a71f3a0-9cf3-4909-86a4-997b516fdb53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de09f28-dafa-4c54-ad07-251a47be51a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b14ad07f-6c6b-4172-9736-99fb68166850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de09f28-dafa-4c54-ad07-251a47be51a8",
                    "LayerId": "a183909c-ebc1-4cac-a6e0-e4cc26b965af"
                }
            ]
        },
        {
            "id": "7291aecc-819a-484d-822d-cfccfcb8f970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
            "compositeImage": {
                "id": "3368bce9-6df3-41d4-9211-8b60ce4f6da6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7291aecc-819a-484d-822d-cfccfcb8f970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48beb437-ae61-4919-891d-6b7c28c46f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7291aecc-819a-484d-822d-cfccfcb8f970",
                    "LayerId": "a183909c-ebc1-4cac-a6e0-e4cc26b965af"
                }
            ]
        },
        {
            "id": "dd5d256a-8557-4632-8b1b-e44dd69e3b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
            "compositeImage": {
                "id": "ed0b699b-6186-43a1-b1c9-876b9f2f5070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd5d256a-8557-4632-8b1b-e44dd69e3b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1090f6e-ce85-4758-a63b-fa2e30efdd36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5d256a-8557-4632-8b1b-e44dd69e3b2b",
                    "LayerId": "a183909c-ebc1-4cac-a6e0-e4cc26b965af"
                }
            ]
        },
        {
            "id": "e8edd0eb-50f2-4627-a0c5-ef7baff2b871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
            "compositeImage": {
                "id": "fb59b88e-ccf3-4bd4-a79e-9084918e374b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8edd0eb-50f2-4627-a0c5-ef7baff2b871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5631c20f-853f-4f60-b7dc-05b93c1bdc7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8edd0eb-50f2-4627-a0c5-ef7baff2b871",
                    "LayerId": "a183909c-ebc1-4cac-a6e0-e4cc26b965af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "a183909c-ebc1-4cac-a6e0-e4cc26b965af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04c3dba2-829c-48f5-b5e1-5fbebcdd0438",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 23,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}