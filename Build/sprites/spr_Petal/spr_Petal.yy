{
    "id": "c4018a9f-b4b1-4d2f-94ea-f8f6468acd17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Petal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06e54325-acec-44e1-a66b-210364b1af2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4018a9f-b4b1-4d2f-94ea-f8f6468acd17",
            "compositeImage": {
                "id": "1b701ac2-a5a9-4249-8d1e-5159e9d0ba77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e54325-acec-44e1-a66b-210364b1af2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d705c21e-d489-4e52-b618-ea88243a479d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e54325-acec-44e1-a66b-210364b1af2a",
                    "LayerId": "1edf403d-6323-49dc-975e-874bb55f3449"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1edf403d-6323-49dc-975e-874bb55f3449",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4018a9f-b4b1-4d2f-94ea-f8f6468acd17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}