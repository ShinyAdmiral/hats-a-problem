{
    "id": "847e7a94-c323-4e00-bd95-16f4f5500694",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardFirePressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0858edd-50b1-4a48-b6a6-640d52b46de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "9f664b9a-36af-4fda-90c7-158652f84f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0858edd-50b1-4a48-b6a6-640d52b46de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49477193-27bc-419c-9b03-45a26e50d266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0858edd-50b1-4a48-b6a6-640d52b46de5",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "21abab97-85a7-4584-9411-1b8cb9244c8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "92250109-c8bd-4869-bd8e-9c435c7e6cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21abab97-85a7-4584-9411-1b8cb9244c8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8eabc7-95b6-4094-8282-d1c7e771f1f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21abab97-85a7-4584-9411-1b8cb9244c8d",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "4ce53760-c748-4733-b75a-cccf6fb6e3b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "7da509de-665c-4e3b-8f11-4738c173f7e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ce53760-c748-4733-b75a-cccf6fb6e3b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35c61760-fa41-465a-a36c-96fbc342d39b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ce53760-c748-4733-b75a-cccf6fb6e3b7",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "9f11ddc7-6c28-4e1c-9654-207d234446c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "94387dde-66e3-4269-8963-4689e14d3e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f11ddc7-6c28-4e1c-9654-207d234446c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53b35105-d584-472a-8aa2-7cafc31ef2b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f11ddc7-6c28-4e1c-9654-207d234446c7",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "af2c5af7-b06d-42ec-b088-93ae996b596e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "dccdcac1-1e5a-46dc-83b1-cd57947249d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af2c5af7-b06d-42ec-b088-93ae996b596e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782fcb33-4c34-4ad5-8b92-5929139e1b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af2c5af7-b06d-42ec-b088-93ae996b596e",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "aef4b51f-36b6-4813-b8e5-fe71ecb8277e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "b65c9a95-2681-4408-b42a-385bffd35ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef4b51f-36b6-4813-b8e5-fe71ecb8277e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb44e556-ff0a-41f6-b50b-47c7e2cf34e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef4b51f-36b6-4813-b8e5-fe71ecb8277e",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "fae6b395-8c46-411b-bacc-112c532359aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "744d2537-b2d9-4cc0-80b0-fba98d8f37ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fae6b395-8c46-411b-bacc-112c532359aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc570d5-0efe-4e81-850a-3770c13c75c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fae6b395-8c46-411b-bacc-112c532359aa",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "b0ff6cc1-7824-49bf-9833-65811c5c311d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "4375c2ab-5269-439f-9411-8b5ea634c0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ff6cc1-7824-49bf-9833-65811c5c311d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b7068f-7c10-4e58-b03d-9c9d87021fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ff6cc1-7824-49bf-9833-65811c5c311d",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "d09bdcbd-d283-4892-a850-3bc011f8ddbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "3e9f8621-4302-406a-bc0d-52d3c4ced2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09bdcbd-d283-4892-a850-3bc011f8ddbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1b2a33-8154-4146-9f5a-7f45f9d7975f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09bdcbd-d283-4892-a850-3bc011f8ddbd",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "b0edb367-53bb-4eca-b48a-38c97b518524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "5bf55808-9d1b-4f9f-9484-ae7843779aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0edb367-53bb-4eca-b48a-38c97b518524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2518370-e6ad-43f3-bd5f-bf021214524b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0edb367-53bb-4eca-b48a-38c97b518524",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "703bbb5d-f37a-48e1-9c43-29a1ff3c09f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "2a634cad-9271-4da7-b369-551f617bebd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703bbb5d-f37a-48e1-9c43-29a1ff3c09f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95919ad1-9d5a-4c07-989a-e65037a74584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703bbb5d-f37a-48e1-9c43-29a1ff3c09f8",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "f090fb51-01b9-40f2-846b-c09aec4a0b6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "96664f55-50a4-4e0a-976b-b0d616903a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f090fb51-01b9-40f2-846b-c09aec4a0b6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca496c48-cbaa-437b-9bbc-2a7ee73ee39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f090fb51-01b9-40f2-846b-c09aec4a0b6f",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "158ccf3c-6a14-4fe0-bb21-a33b58eef237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "b02f291c-ac80-4a5e-9da3-3a326f0680dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158ccf3c-6a14-4fe0-bb21-a33b58eef237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93121fb-bb5c-4dd1-9407-22d59564172f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158ccf3c-6a14-4fe0-bb21-a33b58eef237",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "8a128286-c004-4b92-8baa-ae4ed0dc599c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "3cbf5e5b-1433-44b0-9a17-b91f68164d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a128286-c004-4b92-8baa-ae4ed0dc599c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58c49655-e9f8-42d5-b612-1ee378866392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a128286-c004-4b92-8baa-ae4ed0dc599c",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "4e0d2eca-457b-4a0a-82ff-4b81561ecd60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "f3e47340-f077-4c87-94f8-de7a6dee5480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0d2eca-457b-4a0a-82ff-4b81561ecd60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b09896b3-7fb4-4498-a131-3e3017981147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0d2eca-457b-4a0a-82ff-4b81561ecd60",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "7b862260-33a9-45e2-b16c-efe51e012970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "43b5bee4-de95-4cc6-bc6c-2e565fa406ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b862260-33a9-45e2-b16c-efe51e012970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4750d03-3e45-4fed-a46d-eb9f6ce03906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b862260-33a9-45e2-b16c-efe51e012970",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "3ad339f6-12a0-4f97-a9ea-b107e7e056ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "fd114471-5b01-49c0-9b93-4839363d51e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ad339f6-12a0-4f97-a9ea-b107e7e056ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86717319-e761-4c81-8342-d69e7d94d86e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ad339f6-12a0-4f97-a9ea-b107e7e056ac",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "b7eaa15e-8b2f-4817-8538-f271ab6ea020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "1110bd8d-9a69-4f90-876b-fe4485e19cd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7eaa15e-8b2f-4817-8538-f271ab6ea020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad499ceb-11d9-4d32-9ac7-27fe74776f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7eaa15e-8b2f-4817-8538-f271ab6ea020",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "833e63cb-6322-465c-b9b6-d7163bd64d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "b15e1820-7606-490d-bff6-18ed46c1a231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "833e63cb-6322-465c-b9b6-d7163bd64d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3564b1c-dbd7-4a38-bbfc-bd66cf24dddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833e63cb-6322-465c-b9b6-d7163bd64d85",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "f543eec8-fcee-43b2-bac1-1dc7482a4840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "9ea42ed4-9c05-491f-b0e6-b7fbf56852bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f543eec8-fcee-43b2-bac1-1dc7482a4840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a155d39-22e4-4866-85fd-466df02d7933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f543eec8-fcee-43b2-bac1-1dc7482a4840",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "8264f81b-1117-4198-822d-ad039f63c428",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "02a4a169-64ce-40ac-86a2-047f39409663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8264f81b-1117-4198-822d-ad039f63c428",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536ef8d2-c676-4099-8d8f-30c41774788f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8264f81b-1117-4198-822d-ad039f63c428",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "8b0a1976-8299-47d0-a116-2281d8441e6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "76abca08-b7d0-4b6d-86ba-b47877a9e329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0a1976-8299-47d0-a116-2281d8441e6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610faa42-f615-4ce1-817d-796860170d24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0a1976-8299-47d0-a116-2281d8441e6e",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "ba7dd7c8-dc8c-4bb3-82ac-a95f1d748e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "d209709b-d79d-4d08-b1ad-cefc5b14a32d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba7dd7c8-dc8c-4bb3-82ac-a95f1d748e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e747875c-58e7-4646-a863-87ffd94a278e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba7dd7c8-dc8c-4bb3-82ac-a95f1d748e9e",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "02d4aff2-7388-4998-9592-8d639084c440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "26e3413b-e52c-441d-8967-a000c4b4cb6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d4aff2-7388-4998-9592-8d639084c440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8711d479-0ff4-4339-8596-5d3454da5d26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d4aff2-7388-4998-9592-8d639084c440",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "3c54b9a7-9c8b-4126-8fee-b65bbad68738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "3c77bfa4-c3ea-4b51-8bbd-0d1fa0f9ce85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c54b9a7-9c8b-4126-8fee-b65bbad68738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1495b3-d977-4562-8237-5dea67be202d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c54b9a7-9c8b-4126-8fee-b65bbad68738",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "3e75555d-0d22-4088-b8c5-b112967bec40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "d965a783-7da3-4c72-9641-2856fad1784f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e75555d-0d22-4088-b8c5-b112967bec40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9548fb03-8672-4ef3-b843-cb312924068b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e75555d-0d22-4088-b8c5-b112967bec40",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "a07ddfa5-66b7-41d9-92ab-93863b0a5fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "1050aa24-2a9a-4fb4-8a8c-38275c92f5d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07ddfa5-66b7-41d9-92ab-93863b0a5fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eeeeffd-ac62-47b9-b9e7-5ced0eaabe78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07ddfa5-66b7-41d9-92ab-93863b0a5fb0",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "9fc8156f-7924-41eb-bd73-b6dc89cf8ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "8c89fad2-34e3-495b-a377-88e548ebd3a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc8156f-7924-41eb-bd73-b6dc89cf8ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce17ed23-5529-4f0e-8120-d3571ef6e94b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc8156f-7924-41eb-bd73-b6dc89cf8ffb",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "a9a01f31-0b90-44fb-9e28-da5debbdad38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "b64a3c24-3692-45ab-8480-06683137cd70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a01f31-0b90-44fb-9e28-da5debbdad38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01923e6e-28e4-477f-abf3-97c3758076d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a01f31-0b90-44fb-9e28-da5debbdad38",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "9a623f2f-9f29-42ed-be9b-7ea37b3ce926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "8fa69430-8722-46c6-b2fc-3a91d1ce3ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a623f2f-9f29-42ed-be9b-7ea37b3ce926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6e8fabe-a880-4506-9822-1d9acca2ed11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a623f2f-9f29-42ed-be9b-7ea37b3ce926",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "b4d12d32-6a92-4376-a424-dc4cf3af3655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "2075e23f-5e09-41a7-adb8-4d12ca2cf093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d12d32-6a92-4376-a424-dc4cf3af3655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ba7edc-a6da-4faa-a6e7-5e9a0a404053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d12d32-6a92-4376-a424-dc4cf3af3655",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "5d123dea-e6b5-4efa-94a8-ff736563605c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "efa47e59-556c-4c2d-8ca0-da45e61eea13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d123dea-e6b5-4efa-94a8-ff736563605c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b633a136-dc91-40d5-90c6-b831634fd6be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d123dea-e6b5-4efa-94a8-ff736563605c",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "5ff22734-feac-471e-8cba-f417360bc855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "55c356f6-241d-4c65-8bef-a68566935a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff22734-feac-471e-8cba-f417360bc855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6601ca-972b-46d5-b513-f566bae29a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff22734-feac-471e-8cba-f417360bc855",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "0073dfa2-30eb-4541-b83b-4f6aa19f3cfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "38be147c-496e-4510-86bf-58d904eea46e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0073dfa2-30eb-4541-b83b-4f6aa19f3cfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726a7606-5121-4179-a244-3f9d185e7128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0073dfa2-30eb-4541-b83b-4f6aa19f3cfc",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "9a0aca76-f100-417c-8bbb-97ff07661527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "984d875f-dc3f-49c2-8fb3-97568fa538e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a0aca76-f100-417c-8bbb-97ff07661527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797d496d-8dd0-4627-9f93-a73b29a9ce17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a0aca76-f100-417c-8bbb-97ff07661527",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "7bd0f1cd-7e76-4dd4-a2d8-1bf59e8275f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "7e41fc55-0589-4b1f-a2e9-bf4f51c0114e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bd0f1cd-7e76-4dd4-a2d8-1bf59e8275f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3009330a-f01b-45bb-8bfb-3234abdff6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bd0f1cd-7e76-4dd4-a2d8-1bf59e8275f8",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "f7ffdca2-253d-4916-a6fb-70bcf03341ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "e402c109-c61a-4d07-81d7-76c746726d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ffdca2-253d-4916-a6fb-70bcf03341ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ade5fc1-eefe-45ec-aef9-1df910699eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ffdca2-253d-4916-a6fb-70bcf03341ea",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "8f45b50d-dd56-4b5d-ae7e-4051866d6599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "71c33304-d443-46b0-9828-8a5cd0d46126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f45b50d-dd56-4b5d-ae7e-4051866d6599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2eaa9d4-438a-4459-ae03-8d017d5454df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f45b50d-dd56-4b5d-ae7e-4051866d6599",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "d6c2727f-5fe8-4cd3-a064-455f96919de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "f7a09daa-3ff0-42b3-8414-c2b8bed21b06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c2727f-5fe8-4cd3-a064-455f96919de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ef933c1-78b7-44ec-b302-e87788ac32df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c2727f-5fe8-4cd3-a064-455f96919de9",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        },
        {
            "id": "6886d05d-93e5-48dc-92af-041d2c3476da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "compositeImage": {
                "id": "46c6f48d-0d3c-40fc-9a01-8515deafd86e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6886d05d-93e5-48dc-92af-041d2c3476da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "506d463e-3290-432c-82a9-9414df2e2ef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6886d05d-93e5-48dc-92af-041d2c3476da",
                    "LayerId": "64e04f49-f689-4b1d-9784-5573f9413e1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "64e04f49-f689-4b1d-9784-5573f9413e1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}