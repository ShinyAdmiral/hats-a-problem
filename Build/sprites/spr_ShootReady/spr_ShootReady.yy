{
    "id": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ShootReady",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 9,
    "bbox_right": 51,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92096224-12e3-4cc1-95f9-cc3ad9335444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "17eb2a4d-39c1-4a45-999a-221275d4af99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92096224-12e3-4cc1-95f9-cc3ad9335444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe74349-000b-40be-abe0-ffa8b19948b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92096224-12e3-4cc1-95f9-cc3ad9335444",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "6567298a-6838-43d9-85d6-0f2369403367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "4f1d12f6-08e6-47e9-8938-7f5b9e89e12a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6567298a-6838-43d9-85d6-0f2369403367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b6573a8-e633-49e3-a36d-b2b23fd7ce63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6567298a-6838-43d9-85d6-0f2369403367",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "96d29c11-0b30-4d23-a3d0-4099485124b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "42b02a94-15a7-4f65-88b6-37a0001a8de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d29c11-0b30-4d23-a3d0-4099485124b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a816380-739f-4d8a-847a-90d118f8cd52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d29c11-0b30-4d23-a3d0-4099485124b4",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "c34febe1-1d72-4af2-9fd2-80f8f694d12e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "6f2c018e-70fa-45b5-a435-297a58489b36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c34febe1-1d72-4af2-9fd2-80f8f694d12e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff8fde3d-a82e-458d-8b0e-1c0ad132ca00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c34febe1-1d72-4af2-9fd2-80f8f694d12e",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "bcffd9e3-c75b-4db2-a606-b4b076cd356b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "d9ef1971-f9a8-4979-b32c-260501cb376d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcffd9e3-c75b-4db2-a606-b4b076cd356b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d53acb0a-c6b8-4246-ba4a-0861cea47138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcffd9e3-c75b-4db2-a606-b4b076cd356b",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "d4fb800b-ece8-45db-8484-0f2a88d5bf13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "89e07cd1-7abf-48a9-96a5-588a5b16afda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4fb800b-ece8-45db-8484-0f2a88d5bf13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff7b3bf-4f8d-4083-af3b-1d3278c3f12d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4fb800b-ece8-45db-8484-0f2a88d5bf13",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "c8a66060-c9d4-497f-982f-09f486a4c2dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "9bc50c84-408a-48ae-8739-d4bdb2a2fe03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a66060-c9d4-497f-982f-09f486a4c2dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81909794-ab70-4ed9-91ef-6502a0b7e9c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a66060-c9d4-497f-982f-09f486a4c2dc",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "46419a5a-8008-43bf-8ef8-fa92d2582896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "1896a7a8-0c89-443e-9531-f497445f1357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46419a5a-8008-43bf-8ef8-fa92d2582896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce76534-20f7-43eb-b3b4-875663eb6c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46419a5a-8008-43bf-8ef8-fa92d2582896",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "e694706d-e866-45be-9831-34966fce701b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "53e61ebb-8c69-4a49-b682-67b027a1f9dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e694706d-e866-45be-9831-34966fce701b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a02fdc5-c081-4e4d-bbb3-c9a812f5f7c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e694706d-e866-45be-9831-34966fce701b",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "cbd04672-4298-430d-b357-de71d4a5c506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "50310c30-2196-4aa9-8546-288ff9693870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd04672-4298-430d-b357-de71d4a5c506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4f39aa-b549-4c30-9659-0d1e3f0defd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd04672-4298-430d-b357-de71d4a5c506",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "251c073c-ca17-41a3-8c17-03b7cb3a07e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "6f609886-56d1-4ed0-955b-9a1609f85666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "251c073c-ca17-41a3-8c17-03b7cb3a07e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28a16f9f-bb3d-42c3-a0b6-6fa26d540b2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "251c073c-ca17-41a3-8c17-03b7cb3a07e3",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "085966a3-83a6-448a-87ae-c313b5c560c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "21f45693-8657-4053-89e9-506d49df2434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085966a3-83a6-448a-87ae-c313b5c560c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218787a8-512d-4c04-9614-039a4e7a6fc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085966a3-83a6-448a-87ae-c313b5c560c0",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "6b5fdacd-a53e-452b-ae94-afbfecb59acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "dfd15595-fe07-4127-8971-08c7e53183a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b5fdacd-a53e-452b-ae94-afbfecb59acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e749b839-00b5-475c-809b-43864c58b913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b5fdacd-a53e-452b-ae94-afbfecb59acd",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "1a2234e4-010d-4f21-a09b-68922b21898a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "f93f1e6e-0d95-45a7-99b5-e555cfb8ab41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2234e4-010d-4f21-a09b-68922b21898a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4f10f6c-5a70-4a02-9cee-024d0095cf0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2234e4-010d-4f21-a09b-68922b21898a",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "255ec5c3-cafd-41c0-ae9a-1834c6cbe143",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "d145569e-9891-4815-8249-99d683946bbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "255ec5c3-cafd-41c0-ae9a-1834c6cbe143",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f724b7b-ba6e-466e-9ea5-3dd54c9cd3d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "255ec5c3-cafd-41c0-ae9a-1834c6cbe143",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "e5c67610-b194-47f2-b7af-a7ee0ea697ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "ec23a85b-82f8-4631-8401-d19abead8fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c67610-b194-47f2-b7af-a7ee0ea697ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6569b82b-110e-4e1c-ace9-0f9fc9104303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c67610-b194-47f2-b7af-a7ee0ea697ab",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        },
        {
            "id": "007a485a-5f58-4877-b3d9-d59a5985f702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "compositeImage": {
                "id": "574ba436-461d-4179-9f85-8d7bdde7f9eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007a485a-5f58-4877-b3d9-d59a5985f702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0e8adf-ec08-4d6e-852b-55b580d490f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007a485a-5f58-4877-b3d9-d59a5985f702",
                    "LayerId": "1a52a621-c59b-4060-bbde-d06c47d57a30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "1a52a621-c59b-4060-bbde-d06c47d57a30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "465872c1-4e7e-4a60-b2f5-68a1dff1d4be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 15,
    "yorig": 31
}