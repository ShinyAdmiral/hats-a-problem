{
    "id": "fcd084f2-6b83-41c0-9295-a04544d545ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BorderCards",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b4e72b7-9263-4e7f-a432-79017c23060d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcd084f2-6b83-41c0-9295-a04544d545ea",
            "compositeImage": {
                "id": "9deec9f2-e75b-4903-9893-f4f82e242f7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4e72b7-9263-4e7f-a432-79017c23060d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e51b3cfd-bc60-416c-86b1-da0ca36f02d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4e72b7-9263-4e7f-a432-79017c23060d",
                    "LayerId": "20499e75-c766-43ae-8339-e363c74275bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "20499e75-c766-43ae-8339-e363c74275bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcd084f2-6b83-41c0-9295-a04544d545ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}