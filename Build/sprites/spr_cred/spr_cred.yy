{
    "id": "c15663a7-747e-4989-b90f-efc00a88c2c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cred",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 279,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98c5a4c8-b329-4b10-8a0b-4aa5f972c7b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c15663a7-747e-4989-b90f-efc00a88c2c2",
            "compositeImage": {
                "id": "6f18405f-b243-426b-a9c5-156da4257a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c5a4c8-b329-4b10-8a0b-4aa5f972c7b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e5d8e9-b476-478f-9f8b-c3589843400b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c5a4c8-b329-4b10-8a0b-4aa5f972c7b0",
                    "LayerId": "8d6d3a23-3408-45da-b429-39d4bb3f36f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 280,
    "layers": [
        {
            "id": "8d6d3a23-3408-45da-b429-39d4bb3f36f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c15663a7-747e-4989-b90f-efc00a88c2c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 72,
    "yorig": 0
}