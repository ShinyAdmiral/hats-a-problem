{
    "id": "50fd291a-06e8-488a-91d6-0864554a4eab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Controls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81206739-7666-4262-b023-36f02634a6ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50fd291a-06e8-488a-91d6-0864554a4eab",
            "compositeImage": {
                "id": "7d2e6a02-aafd-4610-b57f-07a789873364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81206739-7666-4262-b023-36f02634a6ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfafb4f2-b2ea-4a44-8c90-8df81f6d708d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81206739-7666-4262-b023-36f02634a6ef",
                    "LayerId": "5b352361-d57b-45b0-bf05-de4f9ef9b411"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5b352361-d57b-45b0-bf05-de4f9ef9b411",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50fd291a-06e8-488a-91d6-0864554a4eab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}