{
    "id": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_HelpPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a014855-5b55-4505-a80c-431e084c502a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "a338d109-4188-42d8-b54e-38f313b50b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a014855-5b55-4505-a80c-431e084c502a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d059cf-3ca6-4267-89b1-f0c40bf7a2a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a014855-5b55-4505-a80c-431e084c502a",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "e1769d14-089c-4d49-8116-0d7b3eb0a3c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "e46be543-415b-4802-8375-4e75b68efa3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1769d14-089c-4d49-8116-0d7b3eb0a3c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e355855-2562-435c-9cd2-fbf8b771de3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1769d14-089c-4d49-8116-0d7b3eb0a3c3",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "3374ff16-ff07-4392-80e8-b182d6f9170a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "8e3b63ab-a995-4ce0-8dab-11ae19edb071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3374ff16-ff07-4392-80e8-b182d6f9170a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1ae413-2602-4a1c-bc20-a09ef45b44cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3374ff16-ff07-4392-80e8-b182d6f9170a",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "111ac11d-e45b-47fb-bf3b-c08c80793365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "afef68c1-e9e9-44e8-9a21-b88e05690713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "111ac11d-e45b-47fb-bf3b-c08c80793365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c36fd388-d841-4954-b614-44f3b2676159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "111ac11d-e45b-47fb-bf3b-c08c80793365",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "082a92e0-0bd1-4b5a-b7e6-941b8c59995f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "e38beacb-3146-4941-be80-4a40c4f16a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "082a92e0-0bd1-4b5a-b7e6-941b8c59995f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d8f8e7-2202-4e7e-8988-b5f36c1959d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082a92e0-0bd1-4b5a-b7e6-941b8c59995f",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "f1b676c7-cb06-4bfc-b128-c6e0713a455a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "cc0b578f-85e7-46d9-bf9b-8a9f2bb52a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b676c7-cb06-4bfc-b128-c6e0713a455a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beff63f9-f2da-4135-af77-69849f534553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b676c7-cb06-4bfc-b128-c6e0713a455a",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "35a87bdc-102c-4a57-93ec-39ed2f88c182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "1d51d1c6-5fc0-471d-b379-3e1bd1786d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a87bdc-102c-4a57-93ec-39ed2f88c182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b17e03-3187-4df9-a424-a87b413080b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a87bdc-102c-4a57-93ec-39ed2f88c182",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "ee66f8de-ce44-4ebe-a813-dd7614eb64a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "ec4b08b4-40d3-4ee0-b1f5-d4e4fba63947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee66f8de-ce44-4ebe-a813-dd7614eb64a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3de9cb80-0efc-4f8a-8358-75a80e23b4a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee66f8de-ce44-4ebe-a813-dd7614eb64a3",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "1ec18f10-d3d8-4985-9a09-1fed3dabc56e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "78f80d2c-206e-4148-9817-33f3b7eaf5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec18f10-d3d8-4985-9a09-1fed3dabc56e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e99cfe3-f5df-4cbb-bf6f-15c4fd8d4c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec18f10-d3d8-4985-9a09-1fed3dabc56e",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "c3f67771-5e0f-45e4-83c1-9cf04bdf64d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "04eada4b-9b46-4cb9-a17f-3b1c91a3f918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f67771-5e0f-45e4-83c1-9cf04bdf64d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "661271d5-559b-4e08-89bb-5da329a24dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f67771-5e0f-45e4-83c1-9cf04bdf64d2",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "996e9c8d-16c9-4a96-8892-c1dba91fd4db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "28c357f0-ac1d-4500-865f-c511dc2a5b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "996e9c8d-16c9-4a96-8892-c1dba91fd4db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69fe23d-7787-4c71-90b3-765368b439f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996e9c8d-16c9-4a96-8892-c1dba91fd4db",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "1ca8886e-5eab-4f94-bf45-ad1e1c4826de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "fc134724-c3af-4511-8a05-dfdbba0093c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca8886e-5eab-4f94-bf45-ad1e1c4826de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "189b6d92-202e-4cf1-84bb-99862fc413fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca8886e-5eab-4f94-bf45-ad1e1c4826de",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "d0f82890-560a-4475-b5bf-30f1f44b9ce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "f224f78c-65f8-4a39-be05-fc2bf856a10c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0f82890-560a-4475-b5bf-30f1f44b9ce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71ec2220-7e4c-4474-bd12-b4bd9f7b6ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0f82890-560a-4475-b5bf-30f1f44b9ce0",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "9cb86bf9-9116-4f9a-9baa-e5f649a4fc5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "adb9be23-2a9b-4a02-8478-8585b87e6988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cb86bf9-9116-4f9a-9baa-e5f649a4fc5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ebf7717-1196-451c-b2fc-a9c7f638f91d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cb86bf9-9116-4f9a-9baa-e5f649a4fc5d",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "ff70b5e9-e3b0-44d3-80d8-32132fbd5b4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "02b20b13-50a0-42d6-819f-bfbdabead7e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff70b5e9-e3b0-44d3-80d8-32132fbd5b4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6863116-6764-4379-a0e2-21bd72d6ab64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff70b5e9-e3b0-44d3-80d8-32132fbd5b4b",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "1316d678-ae41-469a-9218-74591976eb4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "2e766a1e-5d60-44c9-8a0f-60d11b8382f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1316d678-ae41-469a-9218-74591976eb4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62167ad0-35ac-4147-ab3d-c0583cd1af10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1316d678-ae41-469a-9218-74591976eb4a",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "2b43c9f9-dd23-4a2f-a1d0-00c479e19607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "3e134926-0973-4430-b78f-ea0814d01d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b43c9f9-dd23-4a2f-a1d0-00c479e19607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1383d0ec-dff6-4df4-84bd-c925c996a9a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b43c9f9-dd23-4a2f-a1d0-00c479e19607",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "7a2e7507-6008-4b59-ac3c-232f7a524b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "879d71ee-43f7-44a4-a635-2cd6b4ec106b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a2e7507-6008-4b59-ac3c-232f7a524b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74e3c956-c4fc-45c5-b824-1becf1583e20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2e7507-6008-4b59-ac3c-232f7a524b61",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "536a30c8-5675-43b1-b533-14524255b0f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "22df96ca-d8af-4655-9025-d3c1bf5c76ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "536a30c8-5675-43b1-b533-14524255b0f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c0ce82f-e2e0-411e-84db-4ebf2960b883",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "536a30c8-5675-43b1-b533-14524255b0f4",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "0881e9c0-bcd5-4d8a-95e1-95e5e5f71e6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "b72311f0-6f78-48e6-b633-f15e60c04f2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0881e9c0-bcd5-4d8a-95e1-95e5e5f71e6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c044a3-cfef-4ed0-abee-4a631bbd019a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0881e9c0-bcd5-4d8a-95e1-95e5e5f71e6e",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "a4034134-f167-4748-aaa1-51d64696a72a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "99361634-1224-425f-a276-6805b95fb94b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4034134-f167-4748-aaa1-51d64696a72a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc95de5-0401-4d0e-ac36-e3f69252d8a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4034134-f167-4748-aaa1-51d64696a72a",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "3265dc83-7063-4052-99bf-e85264a94e2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "30ca09d8-6546-46b8-80c6-9bcc53cf7876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3265dc83-7063-4052-99bf-e85264a94e2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b10b2d6e-5485-45ce-8692-7bb43fcbf63f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3265dc83-7063-4052-99bf-e85264a94e2e",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "6c60f020-bbd9-4c09-a4d5-d7039ddb7147",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "1eafae0c-504f-46e0-99fe-a3ef7bb52145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c60f020-bbd9-4c09-a4d5-d7039ddb7147",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b83ac7-b5a4-463a-86f2-d1651f4a0130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c60f020-bbd9-4c09-a4d5-d7039ddb7147",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "996d3a9c-4651-4faf-89ad-fb868d431dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "73b1e560-f134-4b92-be57-735136025f2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "996d3a9c-4651-4faf-89ad-fb868d431dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b47fcb-1eed-4496-963d-eef7d4821340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996d3a9c-4651-4faf-89ad-fb868d431dc8",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "595e6855-ac28-4eb8-a579-b849e2357ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "0b69a7b2-70d2-4994-a197-f44ce0599729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "595e6855-ac28-4eb8-a579-b849e2357ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ffbab81-5821-45dc-9f8b-3643313b0d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "595e6855-ac28-4eb8-a579-b849e2357ecd",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "18182d37-a4b2-4866-a2a8-c51108cb3379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "8ab7f633-e3de-4402-9dee-184f97a592a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18182d37-a4b2-4866-a2a8-c51108cb3379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5daf954-e83f-4dd0-8383-85d1864b6f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18182d37-a4b2-4866-a2a8-c51108cb3379",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "c5a53d13-5acd-4fd6-b3ca-f4303ce9f31e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "c610d89f-8ea8-4c48-907b-f956549245b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a53d13-5acd-4fd6-b3ca-f4303ce9f31e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2728d5d6-4bb1-4394-84fd-c95eab865f02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a53d13-5acd-4fd6-b3ca-f4303ce9f31e",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "448f80de-a8cf-44ba-bf0a-aaebd2deb1d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "1ec50791-effe-46d9-8729-f8c84f393869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448f80de-a8cf-44ba-bf0a-aaebd2deb1d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f364050-f4c3-41c8-ad89-c5c117dfbf40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448f80de-a8cf-44ba-bf0a-aaebd2deb1d8",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "4d7459f3-6ae0-4558-af4b-f7d3a845a5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "1de0f9e6-58d7-42e8-a969-f39b6cfa3458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7459f3-6ae0-4558-af4b-f7d3a845a5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b569da-fe54-4bde-94eb-29627fe1d20c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7459f3-6ae0-4558-af4b-f7d3a845a5fa",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "3e9bffd8-f534-407d-83c7-291641b4af79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "37a2a853-c74d-4d7e-b2ec-b2ec624e3e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e9bffd8-f534-407d-83c7-291641b4af79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673a3f78-cf76-41f4-8e8e-53ee01ebcf32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e9bffd8-f534-407d-83c7-291641b4af79",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "8210eb8e-f1a9-4105-8360-2cf2cdcc4234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "a8181fe5-ece1-4a90-acb2-2456844c7211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8210eb8e-f1a9-4105-8360-2cf2cdcc4234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77aa864a-38d9-4990-976f-de39448d4b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8210eb8e-f1a9-4105-8360-2cf2cdcc4234",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "816ed178-094a-4897-833f-14cfc9ca2b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "97de66db-1faf-4394-b7b4-363b876fb1c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "816ed178-094a-4897-833f-14cfc9ca2b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf5ef1d5-4251-4f56-9c3a-8564e5401849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "816ed178-094a-4897-833f-14cfc9ca2b4f",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "b30edebe-3138-4ec2-813a-ee83e4f4eb84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "9a3bffd3-e36e-4f47-9065-16efb216c159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30edebe-3138-4ec2-813a-ee83e4f4eb84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec386ba2-eafd-481e-b45a-02670800c5c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30edebe-3138-4ec2-813a-ee83e4f4eb84",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "e7c8ea88-d7b6-441b-b375-7b0a4d907f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "171deaa9-3f9e-48c6-8e97-f5ad6ab85724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c8ea88-d7b6-441b-b375-7b0a4d907f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "537105b8-3542-4e2a-ac3a-6efe40196a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c8ea88-d7b6-441b-b375-7b0a4d907f2f",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "26d5bec2-5ce9-486c-9aaf-34878d6f5842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "c2777d0d-e305-4d88-b828-478a156f902f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26d5bec2-5ce9-486c-9aaf-34878d6f5842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1776c60-824d-47c5-82da-b41c685b0556",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26d5bec2-5ce9-486c-9aaf-34878d6f5842",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "9a911e17-0a13-4633-b606-d6e135d97fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "418f54a4-0073-4d15-80c8-43c8ece63ea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a911e17-0a13-4633-b606-d6e135d97fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1db788b9-bca9-4e9a-b7e9-5ee45239d70a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a911e17-0a13-4633-b606-d6e135d97fd6",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "ee0ef129-1a41-474c-bbb9-cb22e5ea8690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "6056b1ef-c6c7-49d4-b04b-3332dc4c7beb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0ef129-1a41-474c-bbb9-cb22e5ea8690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1e0887-ea9a-4fd1-9a6b-50ea63ff74b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0ef129-1a41-474c-bbb9-cb22e5ea8690",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "f7ec31df-87be-49c6-a56f-58ac2065d493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "5fd72fc9-639e-40e8-a8ba-c5cf9b14d0b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ec31df-87be-49c6-a56f-58ac2065d493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2762d60a-7a2c-45e4-b094-cff2e48b4488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ec31df-87be-49c6-a56f-58ac2065d493",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        },
        {
            "id": "267ff757-8aed-4fbb-8ede-f5840a166a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "compositeImage": {
                "id": "ae7b58d0-d322-45e2-a674-d28add1252bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "267ff757-8aed-4fbb-8ede-f5840a166a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd08ab86-cab1-4d70-aa8c-f6e68317df1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "267ff757-8aed-4fbb-8ede-f5840a166a86",
                    "LayerId": "cd179396-881d-4fef-b28d-55b895a15148"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "cd179396-881d-4fef-b28d-55b895a15148",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccebd63e-81b5-40dd-84dd-35679e8a0701",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}