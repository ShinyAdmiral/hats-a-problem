{
    "id": "cc42f23d-d0dc-4b09-b151-6bc8eb7b0c78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7521fd25-6886-466a-ab31-1d4553a231c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc42f23d-d0dc-4b09-b151-6bc8eb7b0c78",
            "compositeImage": {
                "id": "db0ad1b0-06e9-4b9b-bb88-f03248dee79d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7521fd25-6886-466a-ab31-1d4553a231c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7262dd7-1550-455a-9ee1-e07fa099b691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7521fd25-6886-466a-ab31-1d4553a231c5",
                    "LayerId": "557a87e0-fb18-4316-a73e-a91da90d8abd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "557a87e0-fb18-4316-a73e-a91da90d8abd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc42f23d-d0dc-4b09-b151-6bc8eb7b0c78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}