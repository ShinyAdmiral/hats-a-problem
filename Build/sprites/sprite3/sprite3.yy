{
    "id": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c3e8058-7242-4eed-8ae9-57f9ec8c24eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "0c7e8432-0fa6-46e4-bc81-f6d429ce0c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3e8058-7242-4eed-8ae9-57f9ec8c24eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c3a5204-2a99-484f-85f0-ccc91a1ba743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3e8058-7242-4eed-8ae9-57f9ec8c24eb",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "78734df2-ba57-4dcb-a533-16dce2e48c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "0d8f6742-c0cd-44fc-a0b8-e110e3a3db6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78734df2-ba57-4dcb-a533-16dce2e48c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0865aef-32a2-4498-9138-40f6b0cf0aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78734df2-ba57-4dcb-a533-16dce2e48c20",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "ecd25446-692a-49e7-b319-94c7932f0b5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "51e83b66-615e-4bc5-8cb1-817e38d5f4ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecd25446-692a-49e7-b319-94c7932f0b5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d08750c5-6cad-4e5f-8ef5-3da55826102a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd25446-692a-49e7-b319-94c7932f0b5d",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "220cb78f-ab20-4e99-893c-d667f541c44d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "885b73c8-2044-4585-8078-218ef8ea26a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220cb78f-ab20-4e99-893c-d667f541c44d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b0c0845-d866-4405-a229-58620f1a306c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220cb78f-ab20-4e99-893c-d667f541c44d",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "58a5e0d8-7d21-40b1-84ce-ccce8ccd9d5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "78469c06-0211-4cae-9b48-2e89e6fa8c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58a5e0d8-7d21-40b1-84ce-ccce8ccd9d5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9daf04ce-f59a-4378-a00d-d7f2837b9b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58a5e0d8-7d21-40b1-84ce-ccce8ccd9d5c",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "38ac0bd6-01bf-4e6d-a1d6-7564a496fe86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "00d57938-1ea5-40a9-a254-0fd1f937a92f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ac0bd6-01bf-4e6d-a1d6-7564a496fe86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "568750cc-209b-4116-ad34-a5cd30286823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ac0bd6-01bf-4e6d-a1d6-7564a496fe86",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "3566f5a0-07b8-4552-b8d6-8f52799b57d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "8e57ea97-cd1a-4c6f-b9ef-2c6388a56ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3566f5a0-07b8-4552-b8d6-8f52799b57d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21fd080-0757-4a56-8cfa-a2e18c861ca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3566f5a0-07b8-4552-b8d6-8f52799b57d5",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "22262418-8c82-4637-b83f-b7ad408a71cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "0bbef3d1-a3b9-4f41-bfef-75f1c45a6c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22262418-8c82-4637-b83f-b7ad408a71cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d490b7-edc5-4960-a28a-9692ddb63ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22262418-8c82-4637-b83f-b7ad408a71cf",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        },
        {
            "id": "622a9a1c-6bd2-4731-99f0-98ccd09d9532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "compositeImage": {
                "id": "d854ba31-4423-45f4-b3a5-e84e21cca50e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622a9a1c-6bd2-4731-99f0-98ccd09d9532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf4f4073-ef3b-4fee-b3c5-32c68d8446b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622a9a1c-6bd2-4731-99f0-98ccd09d9532",
                    "LayerId": "09476516-cc70-4d18-8fe4-39fd8f5cad6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "09476516-cc70-4d18-8fe4-39fd8f5cad6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}