{
    "id": "9f905b9e-20b0-4cc6-bd6b-48a7cc4ea866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardSpades1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59738b93-19db-4015-85f9-7af7a36e5d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f905b9e-20b0-4cc6-bd6b-48a7cc4ea866",
            "compositeImage": {
                "id": "250269db-55fd-4d2c-8a43-a0251f3be514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59738b93-19db-4015-85f9-7af7a36e5d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abf79d2-4dc2-4a1e-8c88-2370d33a60dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59738b93-19db-4015-85f9-7af7a36e5d72",
                    "LayerId": "a34de652-79cc-44e2-ab02-65d9623017f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a34de652-79cc-44e2-ab02-65d9623017f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f905b9e-20b0-4cc6-bd6b-48a7cc4ea866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}