{
    "id": "6bb67bf6-b663-4093-9a76-a3c1bb49a60e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe5f93f2-5a4a-45cd-bee2-9496237167af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bb67bf6-b663-4093-9a76-a3c1bb49a60e",
            "compositeImage": {
                "id": "efe7eba7-1064-4d84-aaa1-3cb6d3aa0a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe5f93f2-5a4a-45cd-bee2-9496237167af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bedb8d74-450b-428c-841a-42e81e74e7a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5f93f2-5a4a-45cd-bee2-9496237167af",
                    "LayerId": "0e7f0398-57e1-47b8-8afa-9c3b95d1d846"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0e7f0398-57e1-47b8-8afa-9c3b95d1d846",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bb67bf6-b663-4093-9a76-a3c1bb49a60e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}