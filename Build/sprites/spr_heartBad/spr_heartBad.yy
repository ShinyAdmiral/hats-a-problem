{
    "id": "75fe7679-d8cf-4229-8f9f-e0c389a8204e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heartBad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 4,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "978be70b-05a7-4196-895b-c53336f52ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75fe7679-d8cf-4229-8f9f-e0c389a8204e",
            "compositeImage": {
                "id": "f2a3d40c-32ce-4da3-89f3-1d724e384f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978be70b-05a7-4196-895b-c53336f52ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d53cbef3-ef68-4485-b9b4-29a71b19630e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978be70b-05a7-4196-895b-c53336f52ad6",
                    "LayerId": "38a27251-4f01-495d-8238-2fdecf7df1a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "38a27251-4f01-495d-8238-2fdecf7df1a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75fe7679-d8cf-4229-8f9f-e0c389a8204e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}