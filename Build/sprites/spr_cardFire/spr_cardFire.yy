{
    "id": "9294cc2f-5a27-4446-b468-20eed2e7bda9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardFire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "862cfd29-fe4d-40f6-89c6-eedf8a4a0f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9294cc2f-5a27-4446-b468-20eed2e7bda9",
            "compositeImage": {
                "id": "8634cecd-0610-4c0f-b706-12b89220045b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "862cfd29-fe4d-40f6-89c6-eedf8a4a0f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71d6d45a-798f-4996-ab7d-8ec3007fcd6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "862cfd29-fe4d-40f6-89c6-eedf8a4a0f42",
                    "LayerId": "0b7ce283-b881-4464-83cf-a1ed88d937e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0b7ce283-b881-4464-83cf-a1ed88d937e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9294cc2f-5a27-4446-b468-20eed2e7bda9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}