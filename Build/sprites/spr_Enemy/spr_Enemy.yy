{
    "id": "de61c415-e8f0-415d-969b-7e9a94dccf2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03ac875c-0efa-45aa-aca9-a5e6cb8b1c15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de61c415-e8f0-415d-969b-7e9a94dccf2c",
            "compositeImage": {
                "id": "8257d71e-d130-4548-b530-eff3ed4d63d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ac875c-0efa-45aa-aca9-a5e6cb8b1c15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86586744-ae4c-49e2-aa50-4d31a0a90e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ac875c-0efa-45aa-aca9-a5e6cb8b1c15",
                    "LayerId": "2ed2b074-ba4d-4b6e-a958-f742f43150bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2ed2b074-ba4d-4b6e-a958-f742f43150bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de61c415-e8f0-415d-969b-7e9a94dccf2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}