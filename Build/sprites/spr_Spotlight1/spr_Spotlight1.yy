{
    "id": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Spotlight1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a48520e2-2fd9-41b4-81ec-9b4e1cf59a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
            "compositeImage": {
                "id": "6d7a54e8-4843-4ecc-8172-e5b68815ae37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48520e2-2fd9-41b4-81ec-9b4e1cf59a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0effe616-42df-40ca-adeb-8421b6ff8496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48520e2-2fd9-41b4-81ec-9b4e1cf59a6b",
                    "LayerId": "bf3f0c3d-5d2e-4d2e-90c8-a719b9abc53f"
                },
                {
                    "id": "429b37ff-f08e-465c-a017-6cd2eedfdee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48520e2-2fd9-41b4-81ec-9b4e1cf59a6b",
                    "LayerId": "8ab6c7c4-5247-48de-ae6c-e7bd2c7845a0"
                },
                {
                    "id": "e8d14ee4-620a-4178-8b4e-fd6b6d2b867a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48520e2-2fd9-41b4-81ec-9b4e1cf59a6b",
                    "LayerId": "e7f8df17-3982-46c9-9160-506cd2fb2745"
                },
                {
                    "id": "939662ff-55fd-4275-ae3b-09e5ece599ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48520e2-2fd9-41b4-81ec-9b4e1cf59a6b",
                    "LayerId": "a96df49a-c4ba-458a-86d0-d5e79a065aa6"
                }
            ]
        },
        {
            "id": "2157e06b-d54b-4c77-a103-e4c7ab9df347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
            "compositeImage": {
                "id": "64620ad5-a0a0-4d42-bc92-001f7a77a503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2157e06b-d54b-4c77-a103-e4c7ab9df347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5fdbcbf-013a-4f5b-9ca9-60d545d00fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2157e06b-d54b-4c77-a103-e4c7ab9df347",
                    "LayerId": "bf3f0c3d-5d2e-4d2e-90c8-a719b9abc53f"
                },
                {
                    "id": "a1c8735d-cd63-462b-aa82-36b6b2c4abcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2157e06b-d54b-4c77-a103-e4c7ab9df347",
                    "LayerId": "8ab6c7c4-5247-48de-ae6c-e7bd2c7845a0"
                },
                {
                    "id": "b38b335f-6768-4f20-8f86-04bce61698cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2157e06b-d54b-4c77-a103-e4c7ab9df347",
                    "LayerId": "e7f8df17-3982-46c9-9160-506cd2fb2745"
                },
                {
                    "id": "d91109d7-941b-4237-a649-f28d2011d995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2157e06b-d54b-4c77-a103-e4c7ab9df347",
                    "LayerId": "a96df49a-c4ba-458a-86d0-d5e79a065aa6"
                }
            ]
        },
        {
            "id": "eb1913fa-f4e9-4992-bf88-5797b0321463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
            "compositeImage": {
                "id": "b1ae38b0-0287-4e86-b621-ab65512ddfcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1913fa-f4e9-4992-bf88-5797b0321463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "933d5361-a212-43a8-aef1-ca2a4369a18f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1913fa-f4e9-4992-bf88-5797b0321463",
                    "LayerId": "bf3f0c3d-5d2e-4d2e-90c8-a719b9abc53f"
                },
                {
                    "id": "22134552-8cb7-41ab-aace-fc27cc0e607d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1913fa-f4e9-4992-bf88-5797b0321463",
                    "LayerId": "8ab6c7c4-5247-48de-ae6c-e7bd2c7845a0"
                },
                {
                    "id": "cfd54875-7e78-49bc-8c1d-e92189f800a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1913fa-f4e9-4992-bf88-5797b0321463",
                    "LayerId": "e7f8df17-3982-46c9-9160-506cd2fb2745"
                },
                {
                    "id": "59169c69-817d-4dcd-9c1e-ad85d80c4e3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1913fa-f4e9-4992-bf88-5797b0321463",
                    "LayerId": "a96df49a-c4ba-458a-86d0-d5e79a065aa6"
                }
            ]
        }
    ],
    "gridX": 48,
    "gridY": 48,
    "height": 48,
    "layers": [
        {
            "id": "a96df49a-c4ba-458a-86d0-d5e79a065aa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e7f8df17-3982-46c9-9160-506cd2fb2745",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "8ab6c7c4-5247-48de-ae6c-e7bd2c7845a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "bf3f0c3d-5d2e-4d2e-90c8-a719b9abc53f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1fd1d59-5809-485c-b73a-bc2a81d820c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 24
}