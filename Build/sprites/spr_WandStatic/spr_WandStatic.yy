{
    "id": "dbb9f561-3926-43a7-95a2-d39121080820",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WandStatic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c107db96-7350-4631-b3e1-a44e0389e194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb9f561-3926-43a7-95a2-d39121080820",
            "compositeImage": {
                "id": "9a7d3b74-4106-4dac-af74-0bf5d6c4153b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c107db96-7350-4631-b3e1-a44e0389e194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5add01de-6945-46d6-bfed-320855f92e3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c107db96-7350-4631-b3e1-a44e0389e194",
                    "LayerId": "929a827e-7b68-487e-a8a3-3aa14676357e"
                }
            ]
        },
        {
            "id": "b846919b-34fd-4bea-89a2-2f6cba62cc05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbb9f561-3926-43a7-95a2-d39121080820",
            "compositeImage": {
                "id": "93573748-2c42-44a2-ba35-4f3c1227ab39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b846919b-34fd-4bea-89a2-2f6cba62cc05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc1c898-5ae9-4dbc-9d85-3b6991a380c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b846919b-34fd-4bea-89a2-2f6cba62cc05",
                    "LayerId": "929a827e-7b68-487e-a8a3-3aa14676357e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "929a827e-7b68-487e-a8a3-3aa14676357e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbb9f561-3926-43a7-95a2-d39121080820",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 0,
    "yorig": 64
}