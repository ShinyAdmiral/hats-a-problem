{
    "id": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae68d323-8c8f-480b-b497-071dc404110c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "d843fd8f-899f-4d7f-84b4-88ea5b2ef98f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae68d323-8c8f-480b-b497-071dc404110c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a49feec1-dec5-45cf-a19f-69374dbfa76a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae68d323-8c8f-480b-b497-071dc404110c",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "6c06aa67-3571-4219-a2bb-0ba2841c5c67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "3d964f99-2219-4d8c-b36b-f549f4f9d974",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c06aa67-3571-4219-a2bb-0ba2841c5c67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30077851-7d72-4c31-8026-fbc6e0449f1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c06aa67-3571-4219-a2bb-0ba2841c5c67",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "e252ed51-b2b2-470f-9e6f-d29b5cf30008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "499650b2-fe88-4ddb-a474-18d9ced18386",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e252ed51-b2b2-470f-9e6f-d29b5cf30008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6814c4-cf7d-4421-9485-a2af10981d18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e252ed51-b2b2-470f-9e6f-d29b5cf30008",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "4b1ec531-eb17-4f72-ab56-19cdcdb017a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "64fb1822-281e-4170-a2b7-6ae1619dc38a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b1ec531-eb17-4f72-ab56-19cdcdb017a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35af477d-6a9d-4cf0-9b7f-6b98f188f122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b1ec531-eb17-4f72-ab56-19cdcdb017a0",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "4a2b0084-fea7-4ed2-a925-26397cbdc03b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "43391944-94a0-45dd-95c9-f393619e3426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a2b0084-fea7-4ed2-a925-26397cbdc03b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c48d10fc-0e07-4633-93e2-c08052eb8fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a2b0084-fea7-4ed2-a925-26397cbdc03b",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "89098ce5-18a3-449c-9689-9b1e01663f26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "691aed43-c530-4e18-940f-c53f28036d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89098ce5-18a3-449c-9689-9b1e01663f26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6a77707-6193-4649-8581-22dc65179fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89098ce5-18a3-449c-9689-9b1e01663f26",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "9545984f-b78c-4fb1-8b8b-92d4457534c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "cc70b526-ad2e-4f68-a639-76962055eeeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9545984f-b78c-4fb1-8b8b-92d4457534c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc5c023-eb1f-432a-9a34-8a409ad4e59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9545984f-b78c-4fb1-8b8b-92d4457534c3",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "811838e9-ef5a-46c5-aec9-f5daa1d6f71c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "5990202d-e80c-4fc3-84fa-515d27d46b54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811838e9-ef5a-46c5-aec9-f5daa1d6f71c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb64354-542f-4779-87d0-f98de1d73848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811838e9-ef5a-46c5-aec9-f5daa1d6f71c",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "57d504a2-945f-48e8-8f14-860e7d3b8257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "0776c927-c705-4ec1-be94-bfeb0b023ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57d504a2-945f-48e8-8f14-860e7d3b8257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01185e04-ede1-4665-a3ab-22a93c79e5ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57d504a2-945f-48e8-8f14-860e7d3b8257",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "3cf09052-24e1-43da-999f-ff4931c2fad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "aa6fadc0-20ec-4d35-814c-ab9876810c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf09052-24e1-43da-999f-ff4931c2fad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88c6aa3f-0b99-45ef-9737-e128da1dd7a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf09052-24e1-43da-999f-ff4931c2fad8",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "61014a74-44b7-4b0a-8677-b1cd243dd5c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "44189316-35ea-40d0-be48-672ce01ef6ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61014a74-44b7-4b0a-8677-b1cd243dd5c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b543225-b79c-45ae-84d3-70ef53b32d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61014a74-44b7-4b0a-8677-b1cd243dd5c2",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "7f6f14cc-ee19-4b11-933b-f0475621cd8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "37d75073-1eec-432c-bf85-7b6c0dfc83ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f6f14cc-ee19-4b11-933b-f0475621cd8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b194d4c6-de4c-4fe6-b70a-9ff9a92a3742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f6f14cc-ee19-4b11-933b-f0475621cd8e",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "4631c9fa-461d-4b45-8623-ac26954c03df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "4bdb295a-224a-47d4-a9f5-39aa975f90f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4631c9fa-461d-4b45-8623-ac26954c03df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "255f88ca-a8d7-4202-8459-291d90abdfc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4631c9fa-461d-4b45-8623-ac26954c03df",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "1826094d-fe54-470d-b12b-27588bb6cd27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "40559fd0-7388-417a-9e7e-13362969ee4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1826094d-fe54-470d-b12b-27588bb6cd27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4062038e-4cd4-43b9-bf6a-71150738bdd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1826094d-fe54-470d-b12b-27588bb6cd27",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "7e731a33-b36c-4b2d-8148-cc25c6d45128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "054f9fa8-412c-45a9-8796-73d3da69cfd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e731a33-b36c-4b2d-8148-cc25c6d45128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aef0ca0-cba9-457c-bc15-9e9e9060b509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e731a33-b36c-4b2d-8148-cc25c6d45128",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "401c4c69-be57-4223-a2f2-f86a23686cc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "612029ed-3118-443e-990d-586bf4bfc66c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401c4c69-be57-4223-a2f2-f86a23686cc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f1db22a-6d6f-47dc-b63b-72ebb9039b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401c4c69-be57-4223-a2f2-f86a23686cc6",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "2ee3d6c7-f723-48de-9614-79c9f70c6102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "9ece38e6-1b3a-43ef-9dde-8db41d95418f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee3d6c7-f723-48de-9614-79c9f70c6102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6af1c27-9fe8-48e8-9055-70499b431cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee3d6c7-f723-48de-9614-79c9f70c6102",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "03fd5347-df50-4f5d-a78f-26392fe45a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "cbe7c917-9e16-4d37-b830-0b565eec5333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03fd5347-df50-4f5d-a78f-26392fe45a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b72332-0c8d-4345-9c00-f62a2bd92dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03fd5347-df50-4f5d-a78f-26392fe45a72",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "b6ff7eb3-dd59-4311-83de-43a96d3b0f83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "1f14071c-71ff-4825-b8df-0438248e8acf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6ff7eb3-dd59-4311-83de-43a96d3b0f83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5bf59d-2a30-4410-9a34-a92a83eba46a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6ff7eb3-dd59-4311-83de-43a96d3b0f83",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "2493199e-f2c3-43a6-9fec-f10223cddfef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "f515eae4-e6a4-4d45-9fc4-66f66a4392f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2493199e-f2c3-43a6-9fec-f10223cddfef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e64a89ce-fe5f-473d-8542-f57f4f6dc4c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2493199e-f2c3-43a6-9fec-f10223cddfef",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "c66365b9-7c5b-4cd7-818c-11b895e64245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "87d63edf-769e-4c46-9e1d-838688bbaf24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66365b9-7c5b-4cd7-818c-11b895e64245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b15ac951-b0e5-49c1-9420-234280c6848b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66365b9-7c5b-4cd7-818c-11b895e64245",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        },
        {
            "id": "c70241a4-855d-4132-a88f-8e2a06a6665a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "compositeImage": {
                "id": "2a521685-e9bc-4739-b1e4-b5c0a38ac73b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70241a4-855d-4132-a88f-8e2a06a6665a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79db609-be78-415d-94cb-a9d5a6838664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70241a4-855d-4132-a88f-8e2a06a6665a",
                    "LayerId": "9b98f7dd-44f8-4056-ab3c-7203889d490a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9b98f7dd-44f8-4056-ab3c-7203889d490a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}