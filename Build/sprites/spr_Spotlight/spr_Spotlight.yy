{
    "id": "7cbc8f7c-8104-48f2-9c9b-36a01c59287a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Spotlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 77,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "631bccfb-b9ac-435b-8946-35b4d4553717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cbc8f7c-8104-48f2-9c9b-36a01c59287a",
            "compositeImage": {
                "id": "b64f366f-89d4-42a9-bcd8-52a351efb5dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "631bccfb-b9ac-435b-8946-35b4d4553717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61de24d1-59c1-4503-bc87-ded962f00d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "631bccfb-b9ac-435b-8946-35b4d4553717",
                    "LayerId": "2033398e-527e-4e46-b1e6-f9a9e36b3abc"
                }
            ]
        },
        {
            "id": "678eae68-3df5-4015-9b55-8a37e75ce53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cbc8f7c-8104-48f2-9c9b-36a01c59287a",
            "compositeImage": {
                "id": "cde9d33f-0474-4c93-868b-c3fade4b812b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "678eae68-3df5-4015-9b55-8a37e75ce53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a8dd47d-deff-4d0f-a513-9139dbc8c535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "678eae68-3df5-4015-9b55-8a37e75ce53d",
                    "LayerId": "2033398e-527e-4e46-b1e6-f9a9e36b3abc"
                }
            ]
        },
        {
            "id": "064705a7-1eb5-49c2-9510-097deeacf140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cbc8f7c-8104-48f2-9c9b-36a01c59287a",
            "compositeImage": {
                "id": "fb3bdc9c-4034-4e85-89e1-98ba9efe3181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "064705a7-1eb5-49c2-9510-097deeacf140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dafb6a0a-6ba1-42f0-975d-239517ef09ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "064705a7-1eb5-49c2-9510-097deeacf140",
                    "LayerId": "2033398e-527e-4e46-b1e6-f9a9e36b3abc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "2033398e-527e-4e46-b1e6-f9a9e36b3abc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cbc8f7c-8104-48f2-9c9b-36a01c59287a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 39,
    "yorig": 24
}