{
    "id": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4d3ddd7-9407-404d-873f-abe29c4945d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "5d14438e-5213-4e95-a93a-de4b94d306ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d3ddd7-9407-404d-873f-abe29c4945d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbadea78-3598-48c3-b96f-def435d06716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d3ddd7-9407-404d-873f-abe29c4945d3",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        },
        {
            "id": "2b0e0560-cfbf-402f-97c4-355c9182b684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "8bb3696c-69e2-403d-845a-3c58082b8469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b0e0560-cfbf-402f-97c4-355c9182b684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7357db3f-bd2b-4912-8ebb-864e054d8ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0e0560-cfbf-402f-97c4-355c9182b684",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        },
        {
            "id": "d1e0c760-13a6-48be-ad2a-4299fcc46590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "3e8bac35-8c0c-40c5-b086-2148d6196001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e0c760-13a6-48be-ad2a-4299fcc46590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fa50abe-9308-48ad-b406-991ccdf8a2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e0c760-13a6-48be-ad2a-4299fcc46590",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        },
        {
            "id": "84023586-8dc1-42f4-b8fb-d627873dee1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "8bee8dc0-3954-49ab-a20f-1e909d910355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84023586-8dc1-42f4-b8fb-d627873dee1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84d650a5-a7fa-4e21-9373-8044079203be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84023586-8dc1-42f4-b8fb-d627873dee1a",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        },
        {
            "id": "d60fb9c1-56c8-49c6-adc2-65e7e12d5040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "d4c8e3d9-9740-449e-9da9-c1f6e63a8760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d60fb9c1-56c8-49c6-adc2-65e7e12d5040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "850c4842-4032-466d-8b81-3364c97a6792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d60fb9c1-56c8-49c6-adc2-65e7e12d5040",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        },
        {
            "id": "663c6bcb-95eb-43b1-9ecf-36cfd2d62058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "89534486-efb0-458f-aa05-8d59b5d930fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "663c6bcb-95eb-43b1-9ecf-36cfd2d62058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d986c76-e6aa-457c-8b86-013424f1373d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "663c6bcb-95eb-43b1-9ecf-36cfd2d62058",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        },
        {
            "id": "ba62ccc3-2f18-4a30-85d8-d1f990f21de2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "fc00b08f-e7ab-4616-b208-78db3c737e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba62ccc3-2f18-4a30-85d8-d1f990f21de2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2b6943-3125-4a43-ada4-f78949f60e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba62ccc3-2f18-4a30-85d8-d1f990f21de2",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        },
        {
            "id": "0d4e139e-a7d7-490b-99bc-dddffb260e5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "compositeImage": {
                "id": "56e081fa-7ba6-464b-9f92-b3ea5e58b91f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d4e139e-a7d7-490b-99bc-dddffb260e5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676cf0fa-357d-4617-b5b1-0f9aeae73c1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d4e139e-a7d7-490b-99bc-dddffb260e5f",
                    "LayerId": "9b0c6430-a561-471d-968d-032571d58540"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9b0c6430-a561-471d-968d-032571d58540",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b48144b-acf5-48bc-8e8f-c35b5aa6d631",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}