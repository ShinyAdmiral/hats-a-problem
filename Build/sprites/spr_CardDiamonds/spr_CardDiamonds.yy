{
    "id": "845cf763-f956-4bdb-bbb5-712386e110a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardDiamonds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a32137ea-d921-47d6-bb84-fe48f724773c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "845cf763-f956-4bdb-bbb5-712386e110a9",
            "compositeImage": {
                "id": "853bbadf-f21c-42d0-80e7-5a736532a780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a32137ea-d921-47d6-bb84-fe48f724773c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d497f7ba-f44d-411d-ae59-92c460eceb1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a32137ea-d921-47d6-bb84-fe48f724773c",
                    "LayerId": "53b107b8-d57b-453a-9c8d-dea1e716573e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "53b107b8-d57b-453a-9c8d-dea1e716573e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "845cf763-f956-4bdb-bbb5-712386e110a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 6
}