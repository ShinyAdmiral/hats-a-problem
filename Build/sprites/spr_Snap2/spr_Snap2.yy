{
    "id": "b3ac278d-4558-4f17-b07e-7de24575c5be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Snap2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 43,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "125f8047-541b-4900-99a5-1efaa03ed7f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "1cc332ef-c280-4e79-b840-410fff965b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "125f8047-541b-4900-99a5-1efaa03ed7f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e20ebfcb-cd4b-4781-9724-143453cedb24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "125f8047-541b-4900-99a5-1efaa03ed7f5",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "35628770-80c6-479f-8d6b-58233bd86291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "02187a57-4c15-44f4-97f0-a26fc65b5a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35628770-80c6-479f-8d6b-58233bd86291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be9009cc-469c-4503-85ab-2d417b948020",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35628770-80c6-479f-8d6b-58233bd86291",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "e719d270-63ef-4dac-a006-895d76585e96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "0a220130-2259-44bb-8d52-216cf601c248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e719d270-63ef-4dac-a006-895d76585e96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5868609e-a29e-4b2b-9777-9679400c50a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e719d270-63ef-4dac-a006-895d76585e96",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "5e24c896-dcc5-494f-b39a-dcc3d003711a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "3c23a79c-d84a-4022-8991-e25a89a85e55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e24c896-dcc5-494f-b39a-dcc3d003711a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b6e75e-9b17-4cad-b3b0-40d6e31740b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e24c896-dcc5-494f-b39a-dcc3d003711a",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "63d4e3d0-b7c7-49aa-83ac-9692a0e34f1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "104fb271-d91f-49ba-9004-eb32e1797382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63d4e3d0-b7c7-49aa-83ac-9692a0e34f1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5418723-4bf2-4aa3-b901-4ef18c8d4b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63d4e3d0-b7c7-49aa-83ac-9692a0e34f1a",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "3f821204-76d5-4d01-ab90-4b46c0423f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "81c84162-0859-499f-80ac-2ff24910aad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f821204-76d5-4d01-ab90-4b46c0423f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4900fd82-d9ce-4dec-8883-931283aeacca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f821204-76d5-4d01-ab90-4b46c0423f6f",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "aa7e8fc4-73db-4685-b7f3-a9f63b300520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "7a5e191d-93dd-4b76-bb30-fd21fdf7681e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7e8fc4-73db-4685-b7f3-a9f63b300520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf9ae5dc-d379-47b6-bcaf-abd290f969f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7e8fc4-73db-4685-b7f3-a9f63b300520",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "dcdb4068-6444-42f5-83d0-a36dabf1b091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "6cd320c9-b95f-46e4-b5dc-fb2ab2bffdb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcdb4068-6444-42f5-83d0-a36dabf1b091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e6abc6a-0044-4aa4-be52-9a1e87e7093a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcdb4068-6444-42f5-83d0-a36dabf1b091",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "85817fba-00ca-479f-be8c-728a93787ef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "13c955ed-eafa-4c76-a3b0-2e3c5eefe910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85817fba-00ca-479f-be8c-728a93787ef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c153a47-3ad4-4b0f-9129-9d34d48589d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85817fba-00ca-479f-be8c-728a93787ef8",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "4ffe57c7-87eb-4529-a39a-7071848ba0be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "cb0e1fa4-0faf-4778-86ca-753f81712530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ffe57c7-87eb-4529-a39a-7071848ba0be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c3c698-36cb-4c78-8790-ae89bee655ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ffe57c7-87eb-4529-a39a-7071848ba0be",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "0ac0c92a-ac5f-42bd-97d5-76034fa1c068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "c59c77e6-7153-4544-bce1-b19dff9b4169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ac0c92a-ac5f-42bd-97d5-76034fa1c068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6558e70-28d1-441d-afd0-6de2151e300f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ac0c92a-ac5f-42bd-97d5-76034fa1c068",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "9e45f3ec-5564-4dbb-95d6-4adbb472107b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "18592375-e825-4fa9-812d-364f03aa3c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e45f3ec-5564-4dbb-95d6-4adbb472107b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5fd5555-f2be-4adb-8fd5-83613ea0a317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e45f3ec-5564-4dbb-95d6-4adbb472107b",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "6ce9a2f2-c040-4150-8968-c664f91eb17c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "7f77a7e0-a8e7-470e-a9ac-2fe85be767df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ce9a2f2-c040-4150-8968-c664f91eb17c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45833547-a707-4314-b333-f17c4a16afb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ce9a2f2-c040-4150-8968-c664f91eb17c",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "e9338b6f-6a67-4a78-b157-3bb8cc9a67eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "1ef919b1-26a6-43fd-a0a8-68202f716f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9338b6f-6a67-4a78-b157-3bb8cc9a67eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "751f5fea-7cd2-43d9-9ff6-9ace279e4431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9338b6f-6a67-4a78-b157-3bb8cc9a67eb",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "89dc1780-b87b-4d87-9599-e7babf9975f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "d64674c5-02b7-4e50-bb13-5d7ab4620853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89dc1780-b87b-4d87-9599-e7babf9975f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50ea199f-c673-460b-9efb-6583a952e238",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89dc1780-b87b-4d87-9599-e7babf9975f0",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "ecd9fa71-4ebc-42b5-ace9-1bf79cc8f4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "afc4c0a8-4a06-478b-b7e9-01a82158f56f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecd9fa71-4ebc-42b5-ace9-1bf79cc8f4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f58576-2ecb-45b4-886a-42f8018281da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd9fa71-4ebc-42b5-ace9-1bf79cc8f4b0",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "8359584d-b5ea-4849-a653-1db2f8db1149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "5f1ea304-3e79-4d0a-a711-2c73b3a6f264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8359584d-b5ea-4849-a653-1db2f8db1149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09541ae0-ebe0-412b-b6c6-a2ed820493f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8359584d-b5ea-4849-a653-1db2f8db1149",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "0cf88078-228c-4473-b218-e7e76e9a6bf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "7a9ac76c-b3b6-4263-b0cd-8fe580c3dec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cf88078-228c-4473-b218-e7e76e9a6bf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8c74478-5f1c-42b1-9a7b-ca6a2b5aacd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cf88078-228c-4473-b218-e7e76e9a6bf0",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "1b328791-3ee0-4b73-baeb-cb6175084875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "0ae736fa-aa47-4893-8e37-9741668fdacc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b328791-3ee0-4b73-baeb-cb6175084875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1d7291-12e8-48ce-878f-55ce92a4c4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b328791-3ee0-4b73-baeb-cb6175084875",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "3215b435-0fec-4aa0-b4f4-10390633c2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "96829536-e659-4cc4-94c4-af900cf88b9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3215b435-0fec-4aa0-b4f4-10390633c2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3129d348-1f38-4f4c-8f22-bdbc92cd7a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3215b435-0fec-4aa0-b4f4-10390633c2f4",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "ea8d2102-92e9-46f4-b2d3-bce54b8eb5ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "03ea2a85-5438-4efe-974f-465d7771e977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea8d2102-92e9-46f4-b2d3-bce54b8eb5ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "141b8b83-bb66-488f-84a4-82b351e6bdf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea8d2102-92e9-46f4-b2d3-bce54b8eb5ca",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "cdeec25b-9564-4f1d-b3b2-7bf908e45236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "50794f02-9075-43b3-812f-12b0396ef087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdeec25b-9564-4f1d-b3b2-7bf908e45236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94ab9ede-76c6-4e2a-9c51-2aac0d15e51d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdeec25b-9564-4f1d-b3b2-7bf908e45236",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "60676021-5a10-406b-a3ae-5af58346bbeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "3798b964-0c0d-4179-ba77-860c00377327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60676021-5a10-406b-a3ae-5af58346bbeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494b02f2-72b0-4cf2-a2ee-244ec76a92ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60676021-5a10-406b-a3ae-5af58346bbeb",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "c5b380b9-1404-4f05-a81c-a174b6260633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "cb1033c4-0f99-4b36-8807-c629b36f39f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b380b9-1404-4f05-a81c-a174b6260633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2005eef6-6347-4590-9c1f-d7e65eb86eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b380b9-1404-4f05-a81c-a174b6260633",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "504ebdc4-8fc8-4e1f-9f1a-d24c6c69c07e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "26c61eb2-531c-45fd-915b-ad325b40e815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "504ebdc4-8fc8-4e1f-9f1a-d24c6c69c07e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24d6ac7-ac4c-473a-9a4a-4c72557b82fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "504ebdc4-8fc8-4e1f-9f1a-d24c6c69c07e",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        },
        {
            "id": "b0ea5c88-0892-44ce-9e80-53ded974670b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "compositeImage": {
                "id": "47dfd4bb-1b13-4d7b-9cab-dfed309ba02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ea5c88-0892-44ce-9e80-53ded974670b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9de1db1-1b23-4d9f-80f8-dbbce61d41de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ea5c88-0892-44ce-9e80-53ded974670b",
                    "LayerId": "012fae94-02c9-4a45-a939-fae2b3d0803b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "012fae94-02c9-4a45-a939-fae2b3d0803b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3ac278d-4558-4f17-b07e-7de24575c5be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 24,
    "yorig": 24
}