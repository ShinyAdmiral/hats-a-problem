{
    "id": "f3bdb9b9-8270-4d3b-9672-d4d56190beec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BorderGame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6550d42d-93b6-4852-bf75-92739465fa5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3bdb9b9-8270-4d3b-9672-d4d56190beec",
            "compositeImage": {
                "id": "33046540-f0b9-4388-bce1-bcf080ec2a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6550d42d-93b6-4852-bf75-92739465fa5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02afea0c-772a-4558-81c0-00b5984da3fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6550d42d-93b6-4852-bf75-92739465fa5b",
                    "LayerId": "e9c0a33d-307d-470e-bccb-ad4d470d859b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "e9c0a33d-307d-470e-bccb-ad4d470d859b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3bdb9b9-8270-4d3b-9672-d4d56190beec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}