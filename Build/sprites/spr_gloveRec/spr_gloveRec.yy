{
    "id": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gloveRec",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 8,
    "bbox_right": 51,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ffc140a-3f53-46da-bb3a-e06b3b737f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "7650c7df-ea03-446a-876f-f59cea451ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ffc140a-3f53-46da-bb3a-e06b3b737f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f19719-6f7a-444d-aff1-da4e4b7a543e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ffc140a-3f53-46da-bb3a-e06b3b737f38",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "68c1fdfe-5a2a-4bd5-aa0e-31653942e01f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "fb33d3f7-1f92-4d8d-af6c-f98591774a14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68c1fdfe-5a2a-4bd5-aa0e-31653942e01f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70baab92-b545-45c1-8a10-5306ac88c51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68c1fdfe-5a2a-4bd5-aa0e-31653942e01f",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "86138e0f-462e-4548-a02b-cce73c861b70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "811317f4-3878-45ca-af19-11f4179be13b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86138e0f-462e-4548-a02b-cce73c861b70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca651f8a-31f2-4d64-96f1-3bf7ce936fc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86138e0f-462e-4548-a02b-cce73c861b70",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "0cfe4f56-fdd7-4092-b655-0c1c2965499a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "71a0fab6-d178-45e3-8278-fc14a0fdd7e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cfe4f56-fdd7-4092-b655-0c1c2965499a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "046133cf-ab80-4701-a877-c420e15b7395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cfe4f56-fdd7-4092-b655-0c1c2965499a",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "c1dd4373-a8cc-4019-8468-90eca2dd9e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "fc2fd6c2-b5df-4420-91ae-186b8031d498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1dd4373-a8cc-4019-8468-90eca2dd9e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "644027ad-0744-470b-a1e9-3486ef6687b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1dd4373-a8cc-4019-8468-90eca2dd9e32",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "46c581a1-0be4-4a28-9bad-8dc18b8f267b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "61119642-bc8b-4aa4-ad3d-071a4095be99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c581a1-0be4-4a28-9bad-8dc18b8f267b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fecdec2a-6adc-4954-aa83-1916a2a8809e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c581a1-0be4-4a28-9bad-8dc18b8f267b",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "e34b7132-2d0d-4a6a-a57d-aa1fe355ae9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "eef5af0a-a1a3-4fc5-aadb-fdac652a91c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e34b7132-2d0d-4a6a-a57d-aa1fe355ae9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3423c73a-2f00-4629-93f3-8eb6ba446c47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e34b7132-2d0d-4a6a-a57d-aa1fe355ae9c",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "d1229669-a317-4c7d-9782-4c86f118663b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "7048819f-6e67-4b3e-b809-e090e99301ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1229669-a317-4c7d-9782-4c86f118663b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63867e25-512c-40b1-89f9-5d63e982d2b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1229669-a317-4c7d-9782-4c86f118663b",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "e4a38cdf-5289-433a-959b-26a4dd4a0bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "3cf9fb10-34e4-477e-ad4b-86254473970e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a38cdf-5289-433a-959b-26a4dd4a0bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36d50479-2d42-4ac3-a4cb-8b187f949520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a38cdf-5289-433a-959b-26a4dd4a0bb1",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "97d930b2-e718-47a4-b457-b729e82f55a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "e33fe111-8292-44bd-a1a4-43bddeb22c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97d930b2-e718-47a4-b457-b729e82f55a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09dc6e5f-e40d-44b5-a908-af2a196ac535",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97d930b2-e718-47a4-b457-b729e82f55a8",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "5c890031-e9a7-4879-9071-5a991d36af1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "71a169e4-1b4d-4407-8af6-865f5e66d82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c890031-e9a7-4879-9071-5a991d36af1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30b69926-39c9-4c20-88ea-640c6069535c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c890031-e9a7-4879-9071-5a991d36af1e",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "3746cb8e-cf5f-440b-b907-a42b555efcac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "8bad4024-3da1-48c1-ae21-3bfec056b877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3746cb8e-cf5f-440b-b907-a42b555efcac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693925e1-6680-4b55-b854-f7a686152ada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3746cb8e-cf5f-440b-b907-a42b555efcac",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "bf19b224-98c6-44db-ba60-9cd732b31de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "9a1052d6-ba39-481a-a2c5-83750fb5efd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf19b224-98c6-44db-ba60-9cd732b31de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f18db586-f1a4-4b00-bc2c-368af90006d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf19b224-98c6-44db-ba60-9cd732b31de7",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "a2f22f99-7725-4cfc-9088-3002b1d0d359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "6b7979df-9fdd-4026-bf80-e83ce42fa0a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f22f99-7725-4cfc-9088-3002b1d0d359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c6d5c6c-1646-40b4-b6f6-08785bad57e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f22f99-7725-4cfc-9088-3002b1d0d359",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "49406b9f-82a5-4534-b99c-9e1d39b68993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "409a2559-ff29-4200-8618-f15f9d035a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49406b9f-82a5-4534-b99c-9e1d39b68993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00ca5009-902f-4d27-b15c-de11ffbd3547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49406b9f-82a5-4534-b99c-9e1d39b68993",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "5c23f31a-c7fd-48b7-a378-558aee37c5a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "6e376df3-8dff-4a84-9889-1645ad0bbec6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c23f31a-c7fd-48b7-a378-558aee37c5a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1627ef2-2464-483a-897c-f9c4f0dc2ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c23f31a-c7fd-48b7-a378-558aee37c5a9",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        },
        {
            "id": "9b60a0fd-2073-4ba1-820c-c03050226777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "compositeImage": {
                "id": "36cc15b1-584b-4b68-89d4-b2e1005e32e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b60a0fd-2073-4ba1-820c-c03050226777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c032937-a443-401d-b772-70879cac778e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b60a0fd-2073-4ba1-820c-c03050226777",
                    "LayerId": "7bf7204a-265e-4a10-9164-210f5cc8a68a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7bf7204a-265e-4a10-9164-210f5cc8a68a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aa59f0f-67c4-4d36-afe8-ad0a68413800",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 15,
    "yorig": 31
}