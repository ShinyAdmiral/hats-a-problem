{
    "id": "570618a6-6087-412a-a6e2-28ed745e5c81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WandSwing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0dacf3c-21c2-418d-8483-a1e60c507b0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "0e02acaa-4018-4e61-93fe-891cd9837170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0dacf3c-21c2-418d-8483-a1e60c507b0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "479d2871-ba8f-4a5d-b96f-d9f1d637fcf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0dacf3c-21c2-418d-8483-a1e60c507b0b",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "0759565f-86d2-4ba6-9bc0-e8f27b3938fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "73c5819f-006b-4fd3-b675-f07f1ace3760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0759565f-86d2-4ba6-9bc0-e8f27b3938fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d152a58a-e518-48f2-96eb-9561c39533df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0759565f-86d2-4ba6-9bc0-e8f27b3938fb",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "95ad2328-ed29-467d-a2ca-19f98c4dfa40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "b941cdce-01cb-4fcb-9fe7-0e3d6f786a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95ad2328-ed29-467d-a2ca-19f98c4dfa40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce3e1b2-0495-4bc1-80bf-94b622d3cce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95ad2328-ed29-467d-a2ca-19f98c4dfa40",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "abf1103e-4fd4-4621-9492-cc4058761ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "f519409f-7b40-49b0-a53e-db5691273b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf1103e-4fd4-4621-9492-cc4058761ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c08eec-65ea-4e92-be9e-5e32d5fe734c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf1103e-4fd4-4621-9492-cc4058761ea9",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "ab43c119-284e-4130-af1a-dab7e02d2798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "031a662d-6667-4986-8c2f-ce98ac218785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab43c119-284e-4130-af1a-dab7e02d2798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1cb799-26c8-44d3-9581-54e6d6fc7672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab43c119-284e-4130-af1a-dab7e02d2798",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "a638de4a-6d31-4f6a-9664-9cf4893a4450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "80e1687c-9cd5-43b4-a31b-f9bb44200ad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a638de4a-6d31-4f6a-9664-9cf4893a4450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "110dd005-22b7-49b6-8c3b-9b0f0ea7cb09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a638de4a-6d31-4f6a-9664-9cf4893a4450",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "95eec5ea-2ef7-49ab-baf2-d5b517eb02bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "a40c7d47-4061-408f-a5ec-47a1a46d7590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95eec5ea-2ef7-49ab-baf2-d5b517eb02bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a3f54dd-7e7f-40c5-b278-6c8a16c5ec59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95eec5ea-2ef7-49ab-baf2-d5b517eb02bb",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "81a642fa-d281-453c-8830-b88bec120192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "74558814-d0c6-458c-b7d2-c5bbaeeefe1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81a642fa-d281-453c-8830-b88bec120192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fae08055-c159-45c6-b5be-9d2aebd882a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81a642fa-d281-453c-8830-b88bec120192",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "416a9ddd-079a-4eeb-bf90-735b4912b93b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "62cd7d7a-3579-4932-a03a-956e628e6ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416a9ddd-079a-4eeb-bf90-735b4912b93b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9829e09a-1e31-4376-8d95-9782ebcf602e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416a9ddd-079a-4eeb-bf90-735b4912b93b",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "ecbbe925-ff11-4aaa-9afe-42054eef1068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "05f86b75-c43f-4eb5-9e99-2400aa522654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecbbe925-ff11-4aaa-9afe-42054eef1068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ee8581f-7265-48c4-81ab-9f592f995676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecbbe925-ff11-4aaa-9afe-42054eef1068",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "bb9c74db-35ab-4be0-8ba3-2de7ddac800e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "1a81a6ec-75f5-4a9f-8a93-15482a416520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb9c74db-35ab-4be0-8ba3-2de7ddac800e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eba05419-3eb0-4ef5-b86a-ff4f854c201c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9c74db-35ab-4be0-8ba3-2de7ddac800e",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "80c4a306-0111-4e8b-ae91-ea9cbb2f5a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "bd17463d-7013-481e-81a6-2a3c230570c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80c4a306-0111-4e8b-ae91-ea9cbb2f5a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87023ca-eb39-43bd-87b8-b87680f78f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80c4a306-0111-4e8b-ae91-ea9cbb2f5a2a",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "9deba075-8202-4d12-8fc2-540a125a1de6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "ded67a7b-aa35-4afd-8d9d-ce107b01a86c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9deba075-8202-4d12-8fc2-540a125a1de6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dca1381-0125-420c-8e0b-c20f056fb2d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9deba075-8202-4d12-8fc2-540a125a1de6",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "09dde1a5-9aa0-4974-8ba6-5b4b5ce3208e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "7fe49c52-edab-4ad0-b70c-edc54513f071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09dde1a5-9aa0-4974-8ba6-5b4b5ce3208e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b394dc5-aaa0-4b30-a5eb-b98ff6740e1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09dde1a5-9aa0-4974-8ba6-5b4b5ce3208e",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "c37bbd3c-de3d-43e2-ac5b-ca274c019ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "10856302-3ff4-48be-a7de-5fcf24ec7620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37bbd3c-de3d-43e2-ac5b-ca274c019ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5943f17-2ebd-4361-b6d9-917db9eef08b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37bbd3c-de3d-43e2-ac5b-ca274c019ee1",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "2bebe68d-c7a8-41a2-a87f-1f667790cba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "8804cdd1-cf01-4594-9ff3-0fa7b621baaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bebe68d-c7a8-41a2-a87f-1f667790cba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d73757-aff6-4ab5-9640-6ada5ba568d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bebe68d-c7a8-41a2-a87f-1f667790cba2",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "2dbe0358-e3d7-4dcf-8a17-23a01f1084d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "77571384-007b-4759-9dae-f441f02f4a6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dbe0358-e3d7-4dcf-8a17-23a01f1084d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb126c2b-0d78-4e44-91bd-2854ee46d431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dbe0358-e3d7-4dcf-8a17-23a01f1084d3",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "db045f19-1910-49b2-bd73-24f93d88e910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "835d0f34-f607-4258-b871-2ffd838ee8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db045f19-1910-49b2-bd73-24f93d88e910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff50da39-bbbf-4d5e-b9eb-37c87189721e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db045f19-1910-49b2-bd73-24f93d88e910",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        },
        {
            "id": "539f9c33-64f5-4b17-9944-e0ae198ef6c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "compositeImage": {
                "id": "0a3bf449-47af-4bcd-bfb6-fdd34e3b3de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "539f9c33-64f5-4b17-9944-e0ae198ef6c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e483918d-1883-4e51-b72d-f23a84aef812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "539f9c33-64f5-4b17-9944-e0ae198ef6c9",
                    "LayerId": "968085ac-611b-4198-ab11-2eeb0b608828"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "968085ac-611b-4198-ab11-2eeb0b608828",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "570618a6-6087-412a-a6e2-28ed745e5c81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 0,
    "yorig": 64
}