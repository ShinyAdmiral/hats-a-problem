{
    "id": "d0d1aa4d-90d0-416a-bbb7-3fb527e3099a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardHearts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdce08aa-d7e7-4b53-807b-6a185f1d6982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0d1aa4d-90d0-416a-bbb7-3fb527e3099a",
            "compositeImage": {
                "id": "4a9cd5c9-1ff2-47af-bdf0-d8dbdaf35593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdce08aa-d7e7-4b53-807b-6a185f1d6982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "798be0bb-6450-40ae-841b-4728a19a6772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdce08aa-d7e7-4b53-807b-6a185f1d6982",
                    "LayerId": "bf117a7f-91be-4102-be27-ac9094924664"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "bf117a7f-91be-4102-be27-ac9094924664",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0d1aa4d-90d0-416a-bbb7-3fb527e3099a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 6
}