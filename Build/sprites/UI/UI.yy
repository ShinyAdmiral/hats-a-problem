{
    "id": "eaafde3b-1ba9-45d0-a3c9-1200c6c7bbac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "UI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5810c837-9089-4a12-97b3-b844d1a181ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaafde3b-1ba9-45d0-a3c9-1200c6c7bbac",
            "compositeImage": {
                "id": "afd82d75-64d7-49d9-8a13-da5fdf118911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5810c837-9089-4a12-97b3-b844d1a181ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "939d79c2-5a4c-444a-bf65-40c7a4405c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5810c837-9089-4a12-97b3-b844d1a181ae",
                    "LayerId": "5851a1f8-7f49-429d-bef5-68cfc54fb338"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "5851a1f8-7f49-429d-bef5-68cfc54fb338",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eaafde3b-1ba9-45d0-a3c9-1200c6c7bbac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}