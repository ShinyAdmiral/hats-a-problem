{
    "id": "ce1aebd6-db17-4628-9d73-3940b40897f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite58",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d161aed8-ab51-4541-bdd6-7d13547ccd44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce1aebd6-db17-4628-9d73-3940b40897f1",
            "compositeImage": {
                "id": "8b2fa18f-6cc1-4793-b17c-cf56816035c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d161aed8-ab51-4541-bdd6-7d13547ccd44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0841dae-db1e-4dce-a444-4b9ca3bc1315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d161aed8-ab51-4541-bdd6-7d13547ccd44",
                    "LayerId": "45797ceb-f8a7-4db5-9b38-2053851926cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "45797ceb-f8a7-4db5-9b38-2053851926cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce1aebd6-db17-4628-9d73-3940b40897f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}