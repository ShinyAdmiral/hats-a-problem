{
    "id": "277f7053-0c0f-4a12-8e13-ebe52ef53a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardRose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8e5e396-e24e-4244-9aa0-71bdbeddbcba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277f7053-0c0f-4a12-8e13-ebe52ef53a5c",
            "compositeImage": {
                "id": "85553a52-2c0e-4ce6-9aad-6e8d45505786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8e5e396-e24e-4244-9aa0-71bdbeddbcba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09892eca-3e06-4a4b-a85a-c81e07280aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8e5e396-e24e-4244-9aa0-71bdbeddbcba",
                    "LayerId": "98f2f3bb-c449-4c76-bc42-77c4b21c2c26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "98f2f3bb-c449-4c76-bc42-77c4b21c2c26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "277f7053-0c0f-4a12-8e13-ebe52ef53a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}