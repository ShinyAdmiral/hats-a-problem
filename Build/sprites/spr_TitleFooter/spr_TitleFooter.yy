{
    "id": "dd420bbb-6afb-4f14-a4ac-f9bf74673092",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TitleFooter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad8b6b67-a135-42a0-902c-2f749b09d274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd420bbb-6afb-4f14-a4ac-f9bf74673092",
            "compositeImage": {
                "id": "66d16253-5b03-4890-88de-7da32f1982d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8b6b67-a135-42a0-902c-2f749b09d274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3d829a6-91dd-4096-8fc5-b0716b95ab19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8b6b67-a135-42a0-902c-2f749b09d274",
                    "LayerId": "58729b01-f074-409a-8bfe-add7b55397a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "58729b01-f074-409a-8bfe-add7b55397a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd420bbb-6afb-4f14-a4ac-f9bf74673092",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 0,
    "yorig": 0
}