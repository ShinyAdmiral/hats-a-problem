{
    "id": "c8f963f4-1f0d-46dc-be41-da5d639b002b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_MenuLogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc340543-e42e-412a-a041-79408f0d8475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8f963f4-1f0d-46dc-be41-da5d639b002b",
            "compositeImage": {
                "id": "16f8cec1-8dbd-416e-8956-a9b9d88e744f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc340543-e42e-412a-a041-79408f0d8475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c6ad4e-f75e-4e17-82c3-33b701176713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc340543-e42e-412a-a041-79408f0d8475",
                    "LayerId": "752579dd-8929-42ea-b10a-46a497f1959c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "752579dd-8929-42ea-b10a-46a497f1959c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8f963f4-1f0d-46dc-be41-da5d639b002b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 0,
    "yorig": 0
}