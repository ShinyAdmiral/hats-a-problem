{
    "id": "5c994ad4-b9c1-44e3-85ea-762a8c59bbbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardKnife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cefa1b8f-54ee-411e-8aa1-c0833b6a20e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c994ad4-b9c1-44e3-85ea-762a8c59bbbb",
            "compositeImage": {
                "id": "5aa315e8-f197-40df-852d-c86edfe20190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cefa1b8f-54ee-411e-8aa1-c0833b6a20e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac56238-b2ca-49f8-a310-4b9ca13ab067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cefa1b8f-54ee-411e-8aa1-c0833b6a20e6",
                    "LayerId": "7af91576-92df-4e99-8146-4e6c48a318f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7af91576-92df-4e99-8146-4e6c48a318f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c994ad4-b9c1-44e3-85ea-762a8c59bbbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}