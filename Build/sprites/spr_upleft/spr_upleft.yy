{
    "id": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_upleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ad451e7-f55f-40f0-b008-3049d4f59507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "118dcabe-b32a-4fe0-88ff-dfca3d06966f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad451e7-f55f-40f0-b008-3049d4f59507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eca9dff-ce1c-431a-97aa-7b3a3cec839f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad451e7-f55f-40f0-b008-3049d4f59507",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        },
        {
            "id": "fd965d14-cc68-4d8f-9ff2-768da356e1d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "fd5ce282-8194-4e9b-916e-c498500348aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd965d14-cc68-4d8f-9ff2-768da356e1d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2ccdf20-1fcc-4dec-b14e-bc9e1826bda9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd965d14-cc68-4d8f-9ff2-768da356e1d0",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        },
        {
            "id": "bbbb1690-5424-4e36-9cf3-1db1ecea4647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "8985aa69-d78c-4a86-832f-5d1cc0f33e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbbb1690-5424-4e36-9cf3-1db1ecea4647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7cc1c4a-b265-445b-83a9-28182970fc83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbbb1690-5424-4e36-9cf3-1db1ecea4647",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        },
        {
            "id": "38ac25de-b18b-467d-90bc-e7a52e290758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "823a78cf-a289-4e9a-83f0-134e8b484297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ac25de-b18b-467d-90bc-e7a52e290758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3ada31-c7d0-4f69-a513-126b06cf01c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ac25de-b18b-467d-90bc-e7a52e290758",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        },
        {
            "id": "21ea5f42-5106-4915-9f31-357ee97b217c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "9fd5aeae-0148-4c0b-9853-5a6aa4804e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21ea5f42-5106-4915-9f31-357ee97b217c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "982c4ce6-53f2-47ba-9e2e-66cfadd30e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21ea5f42-5106-4915-9f31-357ee97b217c",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        },
        {
            "id": "bbf3213e-b30d-457f-9248-a99b141c49e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "3eb6cacf-1a40-4fdf-826c-7e374f103fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf3213e-b30d-457f-9248-a99b141c49e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "346c2111-89b9-4956-8106-0202993f81bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf3213e-b30d-457f-9248-a99b141c49e7",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        },
        {
            "id": "2474338f-81f9-4d6a-9e19-d60e92943bdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "fec55f84-6c11-48cf-a486-2a941eaedc83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2474338f-81f9-4d6a-9e19-d60e92943bdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888285e9-f598-454b-b599-861bb99b03f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2474338f-81f9-4d6a-9e19-d60e92943bdf",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        },
        {
            "id": "f194da3d-154e-4729-bab6-b2d5637aae37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "compositeImage": {
                "id": "a11ded26-f1ae-4ed8-aff9-a9437fdaeae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f194da3d-154e-4729-bab6-b2d5637aae37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24e3990f-56a8-466d-a922-03fffa3a4afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f194da3d-154e-4729-bab6-b2d5637aae37",
                    "LayerId": "f9ac6091-1809-41c6-9469-858a773ee14d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f9ac6091-1809-41c6-9469-858a773ee14d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25f73866-cc20-4c46-882b-c0e2e75f09c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}