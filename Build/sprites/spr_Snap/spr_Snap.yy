{
    "id": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Snap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 10,
    "bbox_right": 51,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8eef1ac-1c3a-4a56-8c7c-c34760845b54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "95d7a435-0543-42d2-b7da-8a6d705cd5e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8eef1ac-1c3a-4a56-8c7c-c34760845b54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c12c512b-0fa7-4072-98ba-47464c7dcbf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8eef1ac-1c3a-4a56-8c7c-c34760845b54",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "f9fc5fcb-2552-4db8-81ba-7af081981300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "a9a61ba7-f8a9-4ce8-8429-8430172003c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9fc5fcb-2552-4db8-81ba-7af081981300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d0c152-1521-4590-b8db-3213e6da0781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9fc5fcb-2552-4db8-81ba-7af081981300",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "7be95f4e-0379-47b8-9b8f-f9370f26cb9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "a8e1dac0-63e7-4b3f-8a74-cdf7f8359542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7be95f4e-0379-47b8-9b8f-f9370f26cb9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecf0834b-5395-43cf-bfd6-b744bec39c28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7be95f4e-0379-47b8-9b8f-f9370f26cb9b",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "1aa6f7f6-0451-4581-92ce-7e67f8c51dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "9ab1f56d-7389-49dc-89c6-4b7479338d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aa6f7f6-0451-4581-92ce-7e67f8c51dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac204aa-120a-4c4c-9c1d-49464ad6a9e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aa6f7f6-0451-4581-92ce-7e67f8c51dc7",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "d107b7e6-cac2-4557-affa-780e605afe27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "66a2c78c-6db2-490f-b088-9b1aa8e42331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d107b7e6-cac2-4557-affa-780e605afe27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2722d286-0913-4bdf-8822-697fcbed9c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d107b7e6-cac2-4557-affa-780e605afe27",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "01f76793-e885-477b-b7a6-a54b0752c7ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "aca87964-0d22-4187-90ae-f394e271349b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01f76793-e885-477b-b7a6-a54b0752c7ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be572cf-3be8-4f10-bb54-5d8443a87ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01f76793-e885-477b-b7a6-a54b0752c7ec",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "3f9b8513-b750-4486-926e-5276b06089ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "43a7f0ee-f2bf-49f1-8af2-e44f33f2a401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f9b8513-b750-4486-926e-5276b06089ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12185f6e-dd48-4730-b193-f316bbec5b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f9b8513-b750-4486-926e-5276b06089ec",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "237bc46e-d721-4c65-b2c2-f0fca0a43355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "ce8e4c30-75dd-4d5a-92ff-ad0b26b83545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237bc46e-d721-4c65-b2c2-f0fca0a43355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7577f1-43a1-4635-a669-8c751a0aaf40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237bc46e-d721-4c65-b2c2-f0fca0a43355",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "7c12b71e-30cf-4b15-9dac-5148fc21c291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "8fee7a14-505e-45ff-ad6c-3a844a2d71b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c12b71e-30cf-4b15-9dac-5148fc21c291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc471bf-67b3-45ad-933a-638246d7f776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c12b71e-30cf-4b15-9dac-5148fc21c291",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "208cb811-af86-4e11-9921-6ec5623a8921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "6cf1a851-e91d-46ea-8b22-b9668b1e4f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208cb811-af86-4e11-9921-6ec5623a8921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d3abff7-5c13-4ee5-b780-df1daf9ad751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208cb811-af86-4e11-9921-6ec5623a8921",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "980c1a2b-3cb4-4a92-891c-5b03f941cfe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "64d89b44-b7a5-47d2-b6e7-af9afbd224b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "980c1a2b-3cb4-4a92-891c-5b03f941cfe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab4400eb-7af7-42bb-b523-cd1a538606cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "980c1a2b-3cb4-4a92-891c-5b03f941cfe6",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "2be89a39-de8c-48ed-a96d-b391f002e9ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "3ddf0b16-a6e4-4b13-9d99-2a7904ab52d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be89a39-de8c-48ed-a96d-b391f002e9ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0522a558-bc93-496f-b00f-a80e581b7cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be89a39-de8c-48ed-a96d-b391f002e9ef",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "8c4ff2bc-cad8-4bc9-bf5c-5b5d955d8ec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "945a284e-c87a-4572-b966-f3ae155a0b66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4ff2bc-cad8-4bc9-bf5c-5b5d955d8ec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4c40af-a777-450c-a58d-9fd72c70f2a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4ff2bc-cad8-4bc9-bf5c-5b5d955d8ec7",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "3fac7598-51cd-443b-800a-f54adeb8994e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "cb47ca5e-64ec-4048-a864-071d38c8db5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fac7598-51cd-443b-800a-f54adeb8994e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc4c4a4-4bbe-494b-8819-606c4e2ec3bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fac7598-51cd-443b-800a-f54adeb8994e",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "9ab7893c-0ad5-48f9-81c6-b693cf0c6942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "7ae08f0f-7179-4a05-9b9e-4446ad915d29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab7893c-0ad5-48f9-81c6-b693cf0c6942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc50b3a5-0755-4e53-b954-44b2300918d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab7893c-0ad5-48f9-81c6-b693cf0c6942",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "b38bad6d-6cab-4dc6-ab32-0e4a19750f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "8b91d70e-980e-4a97-9634-694c4d719fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b38bad6d-6cab-4dc6-ab32-0e4a19750f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed3e6f48-f36c-45db-8b37-4be470c33f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38bad6d-6cab-4dc6-ab32-0e4a19750f6e",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "53c08a64-a482-420c-9984-e6b53cfb0fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "04137cf9-552a-45e4-ad5b-e7a491d0954c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53c08a64-a482-420c-9984-e6b53cfb0fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d3b5c2-eb16-4d67-a946-f8820a498dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53c08a64-a482-420c-9984-e6b53cfb0fca",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "2c7b69d6-bdc4-450e-85e5-d305e5415010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "745f5d45-fc23-4c0c-9ef8-448ae984012a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c7b69d6-bdc4-450e-85e5-d305e5415010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80bb7648-d878-449d-938e-25d04adc1180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c7b69d6-bdc4-450e-85e5-d305e5415010",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "ad500267-af4b-48b8-b01f-8ccc204f8c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "c952e972-1bd5-42d8-8076-af6e07fe6618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad500267-af4b-48b8-b01f-8ccc204f8c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e925ed1-e199-4898-8a67-f35905622484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad500267-af4b-48b8-b01f-8ccc204f8c97",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "e9773bdc-5088-4ea6-8a97-3d03bf32c5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "d64cb273-2280-4997-aa29-9602e794367b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9773bdc-5088-4ea6-8a97-3d03bf32c5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fef155e-7293-408f-8d6e-df674d55aff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9773bdc-5088-4ea6-8a97-3d03bf32c5ed",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "265f4e6e-e5d6-4c73-b9cc-d76e86fbf01e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "4f68af6b-ce59-4b9a-bf00-4f630d5803d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "265f4e6e-e5d6-4c73-b9cc-d76e86fbf01e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bee00a05-a92b-4c2a-a6ea-016371b2e8f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "265f4e6e-e5d6-4c73-b9cc-d76e86fbf01e",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "bf995bcb-013b-4561-ae27-70463398f421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "08da636b-7ca2-43ca-a163-e4c2c90b68fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf995bcb-013b-4561-ae27-70463398f421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c800d24d-4083-4f44-9831-5b491dda27d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf995bcb-013b-4561-ae27-70463398f421",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "b8cef062-7f83-4a8b-a0dd-f3b400054bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "c2990269-0d07-4c6f-9366-39ee88fff643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8cef062-7f83-4a8b-a0dd-f3b400054bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fae9989-d7f7-4813-9788-54877cebb564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8cef062-7f83-4a8b-a0dd-f3b400054bed",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "e1a61697-e378-4264-b9ff-3a20fe8bead3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "6cbfad06-5706-4336-8a44-59729480b017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1a61697-e378-4264-b9ff-3a20fe8bead3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93853029-a5ed-492c-8988-f3e94897772e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1a61697-e378-4264-b9ff-3a20fe8bead3",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "7aa8e823-c536-4d2e-be0d-48d854a29b39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "dd8ecd31-a1d6-4881-9142-0d5b2968b962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa8e823-c536-4d2e-be0d-48d854a29b39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8253d62-9276-4ae7-83a0-335f7819b902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa8e823-c536-4d2e-be0d-48d854a29b39",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "03cdf8cc-6d0a-40ac-af00-b20248b36da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "4ada971c-5bdb-422b-a23f-4fa3922bb085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03cdf8cc-6d0a-40ac-af00-b20248b36da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7382b4-9860-4612-8bdf-0cbdab071887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03cdf8cc-6d0a-40ac-af00-b20248b36da3",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "ec5b7501-0bb1-4db9-aa25-fddcc592ffb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "215d3e4e-e56b-4a2f-af74-d6d34beb1c01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec5b7501-0bb1-4db9-aa25-fddcc592ffb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b1f34b-9cb2-4c30-ac4d-85840468e667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec5b7501-0bb1-4db9-aa25-fddcc592ffb7",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "7c61e517-0cdd-4a37-b2fc-ab4eae76442a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "7cb72cc2-51ac-4f5d-9c15-0943ce2bae7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c61e517-0cdd-4a37-b2fc-ab4eae76442a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb9b555c-78b2-4648-992b-b9e4bae06017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c61e517-0cdd-4a37-b2fc-ab4eae76442a",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "afe0d20f-5df3-4eea-bb65-c273ca71096f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "0a1f7227-fe5f-4a9f-8519-163a9fc09b7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe0d20f-5df3-4eea-bb65-c273ca71096f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3555619-7dd2-4d7f-9946-3d15fdcb8769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe0d20f-5df3-4eea-bb65-c273ca71096f",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "40a65e2e-558a-469e-9f84-f2b44c7dbbd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "8f6134dd-38b1-4163-a591-a6e598cd90ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40a65e2e-558a-469e-9f84-f2b44c7dbbd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9902ec-9f7f-431b-a72a-9ec108ff97ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40a65e2e-558a-469e-9f84-f2b44c7dbbd1",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "883b7bbe-9b5d-4688-88a9-f33e6f25a768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "5dc64377-961d-45a1-9535-bf005f45445c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883b7bbe-9b5d-4688-88a9-f33e6f25a768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350dd995-9e89-4506-8704-433f847e6e3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883b7bbe-9b5d-4688-88a9-f33e6f25a768",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "81aa31d5-060b-4e8b-b006-3247d12f0445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "31cbcd5f-3a3f-40bf-ac7d-ba3181c92fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81aa31d5-060b-4e8b-b006-3247d12f0445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "970ce765-8329-4da5-bd53-4ed21b9252b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81aa31d5-060b-4e8b-b006-3247d12f0445",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "2ab7a069-65b7-4160-9180-fa2a3d44c901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "b9036707-2c08-418f-b16d-b1c3f5d60c0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab7a069-65b7-4160-9180-fa2a3d44c901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02eed658-02a5-41d7-b635-f0f46c72b44f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab7a069-65b7-4160-9180-fa2a3d44c901",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "38b7f958-7989-49a7-bcc2-8c25b5acee63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "52274e3a-3aaf-49b6-b185-b5e49b703859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b7f958-7989-49a7-bcc2-8c25b5acee63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361de610-e522-456c-9e2d-210f9ae7f523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b7f958-7989-49a7-bcc2-8c25b5acee63",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "0ba616f1-c9b7-4436-84cf-63e12aa323af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "7ded2f09-4ae3-4ede-a1fd-f509f8e15f29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba616f1-c9b7-4436-84cf-63e12aa323af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c10f2db-16b9-4ed8-9c7d-4ec928192098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba616f1-c9b7-4436-84cf-63e12aa323af",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "502677b1-55f9-4192-8a6d-8ac74f7e3e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "629c3aa1-8763-4c7e-b173-0c583b021326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "502677b1-55f9-4192-8a6d-8ac74f7e3e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8287f7ba-a3ed-46ca-9404-33f28fc8a47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "502677b1-55f9-4192-8a6d-8ac74f7e3e04",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "c242d0be-70ad-46ee-9ee2-f5587a1a7ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "f3e1cd4f-57ab-4877-b061-c378fd64d817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c242d0be-70ad-46ee-9ee2-f5587a1a7ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e429117-1816-4552-b359-49991c9e857e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c242d0be-70ad-46ee-9ee2-f5587a1a7ca1",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "307d6939-3143-4d02-a136-e7b97400fe95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "c7a59a9d-c08d-43a5-a023-078e21fa8709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307d6939-3143-4d02-a136-e7b97400fe95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbab42cf-5e9a-4cb5-8189-f900e3dd1d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307d6939-3143-4d02-a136-e7b97400fe95",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "ffb46882-43d4-4545-8ed9-a14ff5b2c446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "9a178011-e327-4350-8eb4-cf3005ffd0fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb46882-43d4-4545-8ed9-a14ff5b2c446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6718e42-75d1-4a2b-9418-9f800491023c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb46882-43d4-4545-8ed9-a14ff5b2c446",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        },
        {
            "id": "c391f7ea-f667-4f73-b5cf-69639294304d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "compositeImage": {
                "id": "fd911f33-9c13-4bad-b42e-01b831f76517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c391f7ea-f667-4f73-b5cf-69639294304d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ce475d3-49f4-4e4d-8b5d-32b71ec05cb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c391f7ea-f667-4f73-b5cf-69639294304d",
                    "LayerId": "c1c59ca2-d38c-4be9-9953-30dfec4194bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c1c59ca2-d38c-4be9-9953-30dfec4194bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 24,
    "yorig": 24
}