{
    "id": "a015fdde-8113-4424-945d-0cc7506c7899",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b075108e-906c-4fd8-a3f6-5a5a61bbb61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "961ecdc4-a337-44a7-8eae-fbf606776bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b075108e-906c-4fd8-a3f6-5a5a61bbb61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4c64cf-b1e5-4b38-bc7a-f4896af7276c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b075108e-906c-4fd8-a3f6-5a5a61bbb61a",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "4c5f484d-cf5a-4062-b28b-4700a971cfb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "d14440d5-b03c-48cb-b8bd-76c2d7031524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5f484d-cf5a-4062-b28b-4700a971cfb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8099b68f-f701-4aef-ab13-a52e331e2eae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5f484d-cf5a-4062-b28b-4700a971cfb5",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "e43932c6-d8c6-4cb7-ad22-699a82745d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "a2e91246-13b9-4867-9157-5044c80f19fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e43932c6-d8c6-4cb7-ad22-699a82745d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b989cbee-e468-47bf-9752-873249fd0e7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e43932c6-d8c6-4cb7-ad22-699a82745d6b",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "75205c5f-981b-43ca-b1db-dca7238475b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "dceae740-02ca-497e-bc90-0bc1a1f10445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75205c5f-981b-43ca-b1db-dca7238475b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "092f054c-864e-4012-a605-5d6bd2abdb2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75205c5f-981b-43ca-b1db-dca7238475b5",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "92343e21-e31d-421c-9756-a9b37d3faa21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "6dab8ab5-8d58-47d3-baa2-bcef62a76b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92343e21-e31d-421c-9756-a9b37d3faa21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d32b9f-d03a-429e-af1a-ccead6aee7aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92343e21-e31d-421c-9756-a9b37d3faa21",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "aad933f4-65d2-4a46-8292-3a8748a1f51b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "4248a194-cfaa-42f3-bd5c-edc0d2edca4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aad933f4-65d2-4a46-8292-3a8748a1f51b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ba93f2-820d-467e-ae6d-60007ef30300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aad933f4-65d2-4a46-8292-3a8748a1f51b",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "8756f6b8-bd27-4784-805a-c53eb5eeaf0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "2c2b649f-9cf2-41be-a52d-ceb2befd4431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8756f6b8-bd27-4784-805a-c53eb5eeaf0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d396da-63be-492f-a044-0e8bfff3ad9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8756f6b8-bd27-4784-805a-c53eb5eeaf0e",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "18091511-87ac-4dd9-b789-4be5e3f244e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "d59821a5-dc18-45d9-9551-fa7e0e935a04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18091511-87ac-4dd9-b789-4be5e3f244e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94667a9a-28e6-4a4b-8e06-82bf964f4f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18091511-87ac-4dd9-b789-4be5e3f244e7",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "af613c19-f16b-4aea-b0b8-1e54e683e56c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "5ed7290a-1009-4452-ab5d-41e7202316f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af613c19-f16b-4aea-b0b8-1e54e683e56c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35227daa-b5aa-4782-9eb7-f0e9c3ec3169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af613c19-f16b-4aea-b0b8-1e54e683e56c",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "6467783f-b2f8-4c92-aa8b-5b80d8f75d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "83ca4527-7bbd-47ae-87c2-76a460ec8261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6467783f-b2f8-4c92-aa8b-5b80d8f75d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dfff0d9-8a99-4087-aa45-bcac6fe1f769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6467783f-b2f8-4c92-aa8b-5b80d8f75d4d",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "3230fb8f-bbef-419d-9842-35215cef59d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "9567e38f-14a1-4804-bd37-61721bfac0a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3230fb8f-bbef-419d-9842-35215cef59d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b006364-f859-47c6-b72a-d8ec06a66b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3230fb8f-bbef-419d-9842-35215cef59d0",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "05c92cf3-d417-41c5-bded-d886449537b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "e9b3e5f4-1e29-4599-8c92-520c8169b6b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c92cf3-d417-41c5-bded-d886449537b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed0e9831-b4f4-423a-81e7-2b2a7c2bfbb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c92cf3-d417-41c5-bded-d886449537b0",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "9ad4b221-77c4-45e9-af08-4ea1404d051c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "809455d7-5db8-444c-be3f-42c60af32b5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad4b221-77c4-45e9-af08-4ea1404d051c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85081028-d47d-431e-8c34-1282a632ed60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad4b221-77c4-45e9-af08-4ea1404d051c",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "e9d74c9e-8a08-4ca6-8c14-43d0d3ab49f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "836ca809-6003-43dc-8395-394614da00f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d74c9e-8a08-4ca6-8c14-43d0d3ab49f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54ce33c0-8abe-4b7b-bf6a-1b0931f0260f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d74c9e-8a08-4ca6-8c14-43d0d3ab49f5",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "067b0c55-0f3b-492a-b34d-1d729c851f2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "a8d488ac-4c55-4071-9754-52b620b7ec12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "067b0c55-0f3b-492a-b34d-1d729c851f2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561873c4-11b9-4917-8571-f4e3749b4a8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "067b0c55-0f3b-492a-b34d-1d729c851f2a",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "4482de6a-0e98-47be-8612-fd42f0823d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "b1a88e42-3906-456b-bff0-8c856214c147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4482de6a-0e98-47be-8612-fd42f0823d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e540b54-4843-491e-8272-211f50f1afab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4482de6a-0e98-47be-8612-fd42f0823d97",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "150a987f-d938-412a-9777-bc6c150b57ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "9ab22da2-5d45-4366-b4c5-d9c61e944657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "150a987f-d938-412a-9777-bc6c150b57ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7d8a97-fd1d-450d-9e5f-f90a5ce4d9a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "150a987f-d938-412a-9777-bc6c150b57ce",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "9f3a5ef1-89b8-4821-835d-e2602533d3fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "e893f988-7e35-4637-835e-a499086e5cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f3a5ef1-89b8-4821-835d-e2602533d3fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d2cfa88-94fb-4dc0-942e-799ab69891a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f3a5ef1-89b8-4821-835d-e2602533d3fa",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "e1abab57-a619-4f71-bbf9-f2adccd9d608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "5e3732bd-05f7-4746-9174-13e70df1da28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1abab57-a619-4f71-bbf9-f2adccd9d608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80d5b2f7-d73f-4228-b394-b5d569779007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1abab57-a619-4f71-bbf9-f2adccd9d608",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "666f8d37-bbd4-4827-be88-75d41f5eacc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "462e8310-bd29-404d-a9ea-8e2d60776958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666f8d37-bbd4-4827-be88-75d41f5eacc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d940ea65-aa75-4503-bcdf-e37566f23e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666f8d37-bbd4-4827-be88-75d41f5eacc5",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "6b48eb26-f7cb-4559-bcf5-fcda38f2a52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "ad575d02-841e-4033-be40-04c8a0a6730e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b48eb26-f7cb-4559-bcf5-fcda38f2a52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "232ed967-5754-466f-8081-05bd70cd3023",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b48eb26-f7cb-4559-bcf5-fcda38f2a52e",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "11cad848-d389-4bea-a84b-693052b07a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "e74369e1-19a3-4ff9-8fa1-1765684c9446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11cad848-d389-4bea-a84b-693052b07a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e169849-9482-4c58-9614-1074ee0b44c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11cad848-d389-4bea-a84b-693052b07a6c",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "5b125746-723f-45aa-818c-3664cc1e2584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "1bdda8ef-b95a-4479-a323-4da7939c2ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b125746-723f-45aa-818c-3664cc1e2584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491d37dc-8814-4e29-9117-1ded0338e0ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b125746-723f-45aa-818c-3664cc1e2584",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "8b56d55e-6415-4717-aae8-749292f6e339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "c57337b0-5847-4971-a868-1c157a9b5aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b56d55e-6415-4717-aae8-749292f6e339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83fa5c6a-0b0f-4f76-a55e-a97d8832adc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b56d55e-6415-4717-aae8-749292f6e339",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "521eb03a-6574-403f-89e8-bd3b2046b254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "ffcfc58c-643d-4b35-9814-06a30bddc812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "521eb03a-6574-403f-89e8-bd3b2046b254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea18f15b-8492-44ba-bde6-658955880789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "521eb03a-6574-403f-89e8-bd3b2046b254",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "edb67d77-f37b-4dc2-b102-b9792e1d6e19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "d3f2e26f-524c-49e6-9105-3316541fe777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edb67d77-f37b-4dc2-b102-b9792e1d6e19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd98f0af-bb67-4231-a6f8-35d556d496c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edb67d77-f37b-4dc2-b102-b9792e1d6e19",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "c95a125d-47c6-46bb-80a3-3497e99b8def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "a79e8ce9-3667-49c7-be82-2b9044b58b00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c95a125d-47c6-46bb-80a3-3497e99b8def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa158e7-b43d-4203-a2d4-6b2f532cf141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c95a125d-47c6-46bb-80a3-3497e99b8def",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "c3afad20-c250-4804-bd88-bab5034b4d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "fd553410-1737-45de-aa8e-89d8a487c741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3afad20-c250-4804-bd88-bab5034b4d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97e23569-78fc-4d73-9e2b-42b30a0199ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3afad20-c250-4804-bd88-bab5034b4d7a",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "a5e3f1ae-14d4-4069-85f5-cab51fb8c7e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "84d5c883-ef0b-47e7-822f-9269feac5f68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5e3f1ae-14d4-4069-85f5-cab51fb8c7e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee49ee3-11b7-4dbb-afb8-a6ce6aef1b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5e3f1ae-14d4-4069-85f5-cab51fb8c7e1",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "9e2bbbf4-d755-428a-8179-52100be7272a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "9e6363f2-6939-4764-bda6-d17f3a5dfe47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2bbbf4-d755-428a-8179-52100be7272a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208e4331-6a1b-471f-a6da-2d00edb62511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2bbbf4-d755-428a-8179-52100be7272a",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "21cdd2d1-b8a6-4e84-8975-9682a99590c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "5065efe9-403b-4dfe-a938-d0be7895d399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21cdd2d1-b8a6-4e84-8975-9682a99590c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe487d9-2d7e-4536-beac-65ee04537b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21cdd2d1-b8a6-4e84-8975-9682a99590c2",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "6e447d05-65b7-4516-8e80-4bdce644762c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "908bda7c-b996-4986-a970-50adbf05bc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e447d05-65b7-4516-8e80-4bdce644762c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9069e31e-dfe3-4d29-814d-3016e302f5cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e447d05-65b7-4516-8e80-4bdce644762c",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "c9f2e532-d75d-4f36-953f-bbffaed1a82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "ccea4402-7dc3-4879-b9ad-70126f6bfff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9f2e532-d75d-4f36-953f-bbffaed1a82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d92c23df-04b1-435b-b9d5-ed3a1646e0e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9f2e532-d75d-4f36-953f-bbffaed1a82c",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "e1f06dd6-0d13-405f-943b-92ca02d00082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "9c969c15-8edb-4122-a5ac-57b83e6124a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f06dd6-0d13-405f-943b-92ca02d00082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ba6a4f-4e40-4910-b306-09943129d920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f06dd6-0d13-405f-943b-92ca02d00082",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "9b40960e-ddf7-4f64-b03d-752a304dc734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "853c6faf-1395-4323-acac-b066ff73042e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b40960e-ddf7-4f64-b03d-752a304dc734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0505170f-a6aa-4c4b-b289-294518903415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b40960e-ddf7-4f64-b03d-752a304dc734",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "477ef0c5-b961-481b-9811-7eaaf6bd14b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "610738ff-1f0f-4d69-bacd-efd758b2430d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "477ef0c5-b961-481b-9811-7eaaf6bd14b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46711700-bdce-41c3-88f4-7c0a4ea6603b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "477ef0c5-b961-481b-9811-7eaaf6bd14b9",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        },
        {
            "id": "ae949707-022a-477f-a099-bfe8e8d29bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "compositeImage": {
                "id": "cb38333b-8d86-4a23-8539-2d906a8a9eaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae949707-022a-477f-a099-bfe8e8d29bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dd3b897-09fd-4ade-8814-45dd68a436a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae949707-022a-477f-a099-bfe8e8d29bb1",
                    "LayerId": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0f3c86ed-b629-4f5a-9e57-fffa24b386ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a015fdde-8113-4424-945d-0cc7506c7899",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}