{
    "id": "4cfc4a8f-f542-4f72-993f-52f116b250ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardHeal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2542e01-e226-4d23-9129-e5c5f889d5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cfc4a8f-f542-4f72-993f-52f116b250ad",
            "compositeImage": {
                "id": "e9929321-ddc3-4814-937c-2355bb5cb935",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2542e01-e226-4d23-9129-e5c5f889d5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4fea7bc-0f43-4f5a-8499-940d75f0fcee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2542e01-e226-4d23-9129-e5c5f889d5f2",
                    "LayerId": "695f33d3-f9b4-4255-b334-a01299dc6cc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "695f33d3-f9b4-4255-b334-a01299dc6cc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cfc4a8f-f542-4f72-993f-52f116b250ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}