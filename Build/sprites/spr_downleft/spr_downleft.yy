{
    "id": "32c73789-1718-4595-9dab-c1ea830917b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_downleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0df236d4-7408-4f13-b835-57bfcf2dc96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "c364b0b1-c21a-45e2-a1f2-d18bcc41a1d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0df236d4-7408-4f13-b835-57bfcf2dc96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfd3358-6790-45d9-87a9-419334ad17e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0df236d4-7408-4f13-b835-57bfcf2dc96e",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        },
        {
            "id": "1afb9f4e-35ef-4229-a062-90682a3fc2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "f753c87a-f840-4fdb-ba9c-93d9fb6778b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1afb9f4e-35ef-4229-a062-90682a3fc2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2979d13-15e1-4ade-93ac-ce27a8e586de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1afb9f4e-35ef-4229-a062-90682a3fc2fc",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        },
        {
            "id": "d8df23dd-42a9-46f7-ac03-e897e73a944b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "ff3c7c48-9830-4241-8489-1289e7fa5759",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8df23dd-42a9-46f7-ac03-e897e73a944b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98c80d87-d882-45b5-af47-575a316d3202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8df23dd-42a9-46f7-ac03-e897e73a944b",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        },
        {
            "id": "756ace7a-13b9-43b0-9223-bed31e9b9914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "d26b261d-eeaf-4066-ab8f-a125b789be64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "756ace7a-13b9-43b0-9223-bed31e9b9914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8275f0a-57dc-44bb-b425-6e8c5957ddda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "756ace7a-13b9-43b0-9223-bed31e9b9914",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        },
        {
            "id": "19ff657c-c739-4ed6-9185-58fc56610d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "662dd29a-ccdf-47e1-b9e3-f2980464bd1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ff657c-c739-4ed6-9185-58fc56610d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d40dc12-b687-4d79-8aed-44ca1dfd226d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ff657c-c739-4ed6-9185-58fc56610d18",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        },
        {
            "id": "92174212-83f2-4e83-944d-9be8a75b10a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "0c273897-7892-4e69-aeaa-8ded50136c54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92174212-83f2-4e83-944d-9be8a75b10a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f94a766-6cbe-46ba-9140-6d4cb6313845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92174212-83f2-4e83-944d-9be8a75b10a0",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        },
        {
            "id": "c02ca1cc-c4b6-43d3-811e-af0708d696fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "1653359b-7acb-467e-8b4d-38a75cb58567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c02ca1cc-c4b6-43d3-811e-af0708d696fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a57e26d-c6f7-4ac2-b7ef-49a2d6fa173f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c02ca1cc-c4b6-43d3-811e-af0708d696fa",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        },
        {
            "id": "11e46e43-5ec6-4f86-8ca5-7026321881ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "compositeImage": {
                "id": "0c1e497f-1936-4d05-b7a9-4106d27b14e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e46e43-5ec6-4f86-8ca5-7026321881ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ec52b3-586a-4023-a1ff-0efd287524f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e46e43-5ec6-4f86-8ca5-7026321881ed",
                    "LayerId": "d05f64dd-a02e-456e-a74c-a49eebcec67a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d05f64dd-a02e-456e-a74c-a49eebcec67a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32c73789-1718-4595-9dab-c1ea830917b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}