{
    "id": "f8f8cb02-8f06-448f-b991-f2eab27996f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50427536-d39f-445d-a8c4-fae84d62658e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8f8cb02-8f06-448f-b991-f2eab27996f5",
            "compositeImage": {
                "id": "57d21db4-2923-4f0e-ac70-8a3d9d4a1d63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50427536-d39f-445d-a8c4-fae84d62658e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90dec129-d4c9-4124-905f-e55527548a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50427536-d39f-445d-a8c4-fae84d62658e",
                    "LayerId": "c5b3960c-ca49-40a6-a6d5-1816c0d143c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "c5b3960c-ca49-40a6-a6d5-1816c0d143c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8f8cb02-8f06-448f-b991-f2eab27996f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 3
}