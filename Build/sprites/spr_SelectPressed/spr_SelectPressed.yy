{
    "id": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SelectPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b71a264f-daae-4833-a553-64e1952c4071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "c28ec44d-ba16-4c7b-8619-ae9ddb3d8c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71a264f-daae-4833-a553-64e1952c4071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed9456ce-8584-448f-834a-3080bd125500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71a264f-daae-4833-a553-64e1952c4071",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "397c5451-1523-4985-8d54-a1f4c6affdff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "db935dd5-f247-4968-bb9e-f73b7b9b4a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "397c5451-1523-4985-8d54-a1f4c6affdff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "432c3fd9-c666-4b85-8bc8-eaccfe6b28d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "397c5451-1523-4985-8d54-a1f4c6affdff",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "b4a30a37-37fb-4648-be0c-4515bd147818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "1b2b0bc7-d916-40e5-905c-f63200f7c4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a30a37-37fb-4648-be0c-4515bd147818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d70e9d3-4f2b-43d3-a08d-27b6331c8781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a30a37-37fb-4648-be0c-4515bd147818",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "2f81bed4-376e-46af-b1cf-6f488ae66eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "f28fd990-f01b-4e19-8692-7b789aedc0b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f81bed4-376e-46af-b1cf-6f488ae66eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501a6826-9bcf-4895-981a-a90aab81721c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f81bed4-376e-46af-b1cf-6f488ae66eba",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "63a236ca-bd00-4cc3-991a-acf1e0a6ce1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "4151a9bd-5f1d-4679-bc56-2e71df0ac586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63a236ca-bd00-4cc3-991a-acf1e0a6ce1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "094f04ea-f12e-40f0-a141-72a15d6004d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63a236ca-bd00-4cc3-991a-acf1e0a6ce1a",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "1c7ef231-249e-44a0-8cb4-2b20fab4a971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "9e6331a2-9b3d-4604-aeb5-5dc5b52ea0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c7ef231-249e-44a0-8cb4-2b20fab4a971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956b6062-81ec-450b-a29c-63c3f5fd8c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c7ef231-249e-44a0-8cb4-2b20fab4a971",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "eefe8e1d-f332-460e-a22c-794e86beae3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "fad89107-95cc-4ad5-98f6-7bf14817aa31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eefe8e1d-f332-460e-a22c-794e86beae3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b62a374-a760-4bd4-8b2b-dbcb40303a62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eefe8e1d-f332-460e-a22c-794e86beae3a",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "c56c2d54-727d-4928-9728-7d0c8655ac42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "cb4141de-a90d-4277-8ab2-3f6e0f70d51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c56c2d54-727d-4928-9728-7d0c8655ac42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b74d9ddd-55bf-40cc-80cc-cf3773f4f136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c56c2d54-727d-4928-9728-7d0c8655ac42",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "998323d9-4c87-403a-926c-8069b3381adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "9f173977-72a2-4a80-a323-0029776f294c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "998323d9-4c87-403a-926c-8069b3381adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73490ce5-1d4b-4bd2-9525-da7f792b4b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "998323d9-4c87-403a-926c-8069b3381adb",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "ba950c25-99e2-497e-8fda-baa28eff3c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "2efcc11f-7fe0-4f64-a9e1-4ecc2f7037ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba950c25-99e2-497e-8fda-baa28eff3c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce7bce5-3411-4b7f-be61-2582fd939c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba950c25-99e2-497e-8fda-baa28eff3c72",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "ad258249-aae7-4395-970f-e012741b790d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "44443e96-b4bd-4272-aa85-89779600d2f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad258249-aae7-4395-970f-e012741b790d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16317198-c2a0-4c7c-9118-0ca9806fe244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad258249-aae7-4395-970f-e012741b790d",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "53c8ea0f-f888-493a-b116-b82fb9b84f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "9800db55-3ad9-42da-a3ab-5e6456c592e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53c8ea0f-f888-493a-b116-b82fb9b84f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "869567f8-4e0e-4b13-9449-35af46c263f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53c8ea0f-f888-493a-b116-b82fb9b84f09",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "87642ca8-507c-49d1-a277-6c89967d0a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "1596e8b5-f25f-469f-91d4-f174179ae3dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87642ca8-507c-49d1-a277-6c89967d0a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c7488e-8b7f-4673-8cd0-8f6ba11d8a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87642ca8-507c-49d1-a277-6c89967d0a4c",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "a0e45cfb-1977-4874-8494-79ee3eb32478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "8b656aa3-884e-419f-87c7-b09dc436eea6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e45cfb-1977-4874-8494-79ee3eb32478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfa3e641-c912-4953-aad6-61d066d714de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e45cfb-1977-4874-8494-79ee3eb32478",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "bd22dea6-c84f-4f88-84d4-efbc8b6f0cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "b360ccd5-4664-415f-ae67-085d6b73c52b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd22dea6-c84f-4f88-84d4-efbc8b6f0cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b736b3e8-348e-4550-a712-cda5c7bb3191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd22dea6-c84f-4f88-84d4-efbc8b6f0cda",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "67d6d45f-1ed9-49b8-a9ef-f57b0762b96c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "bfc44bc8-4314-4b2a-85f9-b883894120cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67d6d45f-1ed9-49b8-a9ef-f57b0762b96c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a6e776-9cd6-408d-8745-e92fe58fac6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d6d45f-1ed9-49b8-a9ef-f57b0762b96c",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "7011ecf5-bd7e-49fb-92d5-87d8795788c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "b3a1b9be-64a7-4a1f-bd6c-646fa0bce68d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7011ecf5-bd7e-49fb-92d5-87d8795788c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd50f02-ce68-4410-aeef-3306245c2d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7011ecf5-bd7e-49fb-92d5-87d8795788c4",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "058f7359-1911-4969-8dd3-d26e72d50108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "af2a9a4e-ebcc-4962-937f-9b1f6bdf1a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "058f7359-1911-4969-8dd3-d26e72d50108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c562942c-512b-4155-bafa-d98c9f4560df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "058f7359-1911-4969-8dd3-d26e72d50108",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "d1dcb3c6-0fd0-4e68-aa2b-f3898894ef40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "835d2244-a8f6-4017-b88e-27c3307798c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1dcb3c6-0fd0-4e68-aa2b-f3898894ef40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3db472a-eed4-495f-9943-01780c67a40e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1dcb3c6-0fd0-4e68-aa2b-f3898894ef40",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "4fc93686-c4ad-47c9-8cbd-1d89c15f2ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "5f736b92-7f69-49f6-a581-3490f5aa50f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fc93686-c4ad-47c9-8cbd-1d89c15f2ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ffffabe-f368-4dd2-877f-84663d45b4c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc93686-c4ad-47c9-8cbd-1d89c15f2ee6",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "a5a0c172-12e8-4a65-a9ed-1fd34e4db62a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "93441fd7-7b4e-4ef2-94fd-f381717f97aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5a0c172-12e8-4a65-a9ed-1fd34e4db62a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2778ee-706f-43ac-86d1-e4d86b322d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5a0c172-12e8-4a65-a9ed-1fd34e4db62a",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "26cb7d9a-cd22-47fd-b0fd-3eee9e10c70d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "356332c4-1fc4-4d1d-a319-d088a7dfe1ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26cb7d9a-cd22-47fd-b0fd-3eee9e10c70d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ce8d1ab-b08a-4a4b-a86b-96cee97a439c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26cb7d9a-cd22-47fd-b0fd-3eee9e10c70d",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "0e04e61b-9f19-426c-bf72-e2b7e9c52101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "4fd9c687-d56a-463e-8e45-b23012d54fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e04e61b-9f19-426c-bf72-e2b7e9c52101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f02fd9e-b509-4c24-82e4-b6e540c1edd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e04e61b-9f19-426c-bf72-e2b7e9c52101",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "4c1cd893-e2f7-4497-8139-fd65611fbf85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "6c3b44bb-77de-4915-95ff-2410dd50d2a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c1cd893-e2f7-4497-8139-fd65611fbf85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d825758-1515-4c35-8fca-818e1d4e0b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c1cd893-e2f7-4497-8139-fd65611fbf85",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "75fea1dc-b267-4040-b3e6-19f61b913843",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "a71da610-e221-4fd0-b039-ad31a40c3017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75fea1dc-b267-4040-b3e6-19f61b913843",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9542cad-033f-4968-ba5e-3fc63cbe96cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75fea1dc-b267-4040-b3e6-19f61b913843",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "3d3cd813-4181-4174-a32d-ccf5ae6f33bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "b3480e15-f4a4-48ea-8fc5-4a7adfb6b829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d3cd813-4181-4174-a32d-ccf5ae6f33bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b73c334-b1ab-47e3-8f32-bd84519742c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d3cd813-4181-4174-a32d-ccf5ae6f33bd",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "f5e51f5e-3959-413d-8eed-191d79f43f2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "5f798ca4-5c33-4073-b485-eaaa26927278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e51f5e-3959-413d-8eed-191d79f43f2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "124bccbc-3359-497a-9df4-94b851b9c0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e51f5e-3959-413d-8eed-191d79f43f2b",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "4b157b61-290c-4b99-b0aa-eb994142c3cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "26579d13-db49-4d26-8877-af667bd18e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b157b61-290c-4b99-b0aa-eb994142c3cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e44162a-9838-4199-a183-0a5514c9b451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b157b61-290c-4b99-b0aa-eb994142c3cc",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "fbd0b1af-0d97-4a8a-b4f1-f14604845355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "25eb0234-668f-4772-98e4-c9ba4ea1ece4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbd0b1af-0d97-4a8a-b4f1-f14604845355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "409ac480-e9af-46e6-a87c-893f54063660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd0b1af-0d97-4a8a-b4f1-f14604845355",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "f53d96f6-39b9-4350-8006-4523c4a660ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "6a930b81-96fc-459f-96f3-2a8a6f975243",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53d96f6-39b9-4350-8006-4523c4a660ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "611b2578-3793-4709-b86a-3f473c733f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53d96f6-39b9-4350-8006-4523c4a660ba",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "6b958cfb-91df-4bd1-8fe1-ac5022f1b67b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "da0fd4af-b1f9-4315-9ab2-2e9bfd6b9e4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b958cfb-91df-4bd1-8fe1-ac5022f1b67b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d55c0f72-ac38-416f-8193-09d0da1e60d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b958cfb-91df-4bd1-8fe1-ac5022f1b67b",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "c895d1cd-9642-4b17-a7d9-31dcd412332a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "5745ee6b-bf71-4fef-af03-c88f5bc3c216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c895d1cd-9642-4b17-a7d9-31dcd412332a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80267f48-30b9-419f-a02e-e35ada8572ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c895d1cd-9642-4b17-a7d9-31dcd412332a",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "8c2b47d6-124a-42d1-99b3-c682bc76a7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "029d2cdd-9f52-4258-a5fc-d66e6bc15289",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c2b47d6-124a-42d1-99b3-c682bc76a7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "060d0834-a7d0-487d-8178-82de9b464817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2b47d6-124a-42d1-99b3-c682bc76a7b4",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "d1d08633-d362-466d-99de-3b0853783b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "265848da-64cd-42d1-95f7-a7c5e85fceb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1d08633-d362-466d-99de-3b0853783b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea5dbcf-89ab-4958-8165-7ad5e10e0bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1d08633-d362-466d-99de-3b0853783b09",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "62dd3b4b-0397-4ffc-b5e1-9942510907da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "ad29dca7-5a93-426d-8d8e-109396ab60ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62dd3b4b-0397-4ffc-b5e1-9942510907da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e577b34f-4b1b-4c35-a9dd-d9a6e7b68e83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62dd3b4b-0397-4ffc-b5e1-9942510907da",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "b2ca7faa-65c2-416b-89e6-9a5f89c65776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "5f861134-adb4-404d-ab3d-bc68b7ea4a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ca7faa-65c2-416b-89e6-9a5f89c65776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe57d08b-a652-4911-8c4c-b33ff154f118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ca7faa-65c2-416b-89e6-9a5f89c65776",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "6fdf7c98-8cb2-4121-8ebe-afe17b5e66b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "2154082d-ce51-4c93-b27f-a1efdb4c27db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fdf7c98-8cb2-4121-8ebe-afe17b5e66b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44aec225-95a6-41dc-a9a9-f420ebd81e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fdf7c98-8cb2-4121-8ebe-afe17b5e66b3",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "607b1b45-339c-47a8-a552-79f9f4f79bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "2a855d10-b05d-4ea2-bcdd-80f02594a5fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "607b1b45-339c-47a8-a552-79f9f4f79bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52221494-4616-4b40-8eca-f01a0dcf330a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "607b1b45-339c-47a8-a552-79f9f4f79bbc",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "c791f46c-091a-4070-8852-bab1e3ae9455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "28ec211d-dd97-4506-aacb-eb0cb032a97d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c791f46c-091a-4070-8852-bab1e3ae9455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a58061-bb2b-4713-b25a-c23daafd1fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c791f46c-091a-4070-8852-bab1e3ae9455",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "f6867869-8f26-405e-ac57-0df7e9e32893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "1c8efa60-a18f-4e35-9db7-385d6f5c5da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6867869-8f26-405e-ac57-0df7e9e32893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c90bb9a6-e3b8-4eba-a3c2-2a1813e0d8a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6867869-8f26-405e-ac57-0df7e9e32893",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "3922ecf4-ce64-4b3d-88a6-c330283c19b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "540b2ac2-60bc-453e-9178-e560dec9e06a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3922ecf4-ce64-4b3d-88a6-c330283c19b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5069fac7-40c9-446e-a8e2-c62c3d9f2fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3922ecf4-ce64-4b3d-88a6-c330283c19b0",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        },
        {
            "id": "965ed38e-7c2f-4359-816e-ec017528342c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "compositeImage": {
                "id": "1bb746bd-b3b3-4c3c-b7ac-35e73ed41372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "965ed38e-7c2f-4359-816e-ec017528342c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45dabd7b-47f3-4b69-8f71-24094c7b42a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "965ed38e-7c2f-4359-816e-ec017528342c",
                    "LayerId": "e79e0a4b-7b37-45af-a729-2a45a12784d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "e79e0a4b-7b37-45af-a729-2a45a12784d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a9b1445-5968-4851-9aa3-27d6c8e109f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}