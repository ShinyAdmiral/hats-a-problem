{
    "id": "8af1e7d4-6bbe-42e5-b885-44c487ed9a72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d18b405-85c8-47ca-bff1-9b62a535ff3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8af1e7d4-6bbe-42e5-b885-44c487ed9a72",
            "compositeImage": {
                "id": "c0369252-0f81-4a24-85e3-098cbdc9e821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d18b405-85c8-47ca-bff1-9b62a535ff3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53cfe2a6-d593-4b04-95a4-8c320275dcb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d18b405-85c8-47ca-bff1-9b62a535ff3b",
                    "LayerId": "1773df2f-6251-4c5e-a4dd-d8dcb653f59d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "1773df2f-6251-4c5e-a4dd-d8dcb653f59d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8af1e7d4-6bbe-42e5-b885-44c487ed9a72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 5
}