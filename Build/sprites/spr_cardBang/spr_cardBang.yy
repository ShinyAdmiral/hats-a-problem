{
    "id": "8360abae-6b2d-4971-950d-3b0c481fa2d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardBang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e27b12b4-5788-464a-a719-b7659926263c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8360abae-6b2d-4971-950d-3b0c481fa2d5",
            "compositeImage": {
                "id": "4445afcb-907f-4f08-aa44-f46c4a3b0def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e27b12b4-5788-464a-a719-b7659926263c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de250e1-0547-4204-a59d-0ea25c669283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e27b12b4-5788-464a-a719-b7659926263c",
                    "LayerId": "442296a6-fb39-4207-8dbd-d8ab92285bf9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "442296a6-fb39-4207-8dbd-d8ab92285bf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8360abae-6b2d-4971-950d-3b0c481fa2d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}