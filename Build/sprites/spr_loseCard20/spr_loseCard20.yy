{
    "id": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loseCard20",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf31e87c-5156-436a-be36-6ad12921185b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "95fc6bf9-7371-42ca-a3b1-75cd3fc980b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf31e87c-5156-436a-be36-6ad12921185b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84754bb7-c2b1-4fab-90bd-aee3366adc06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf31e87c-5156-436a-be36-6ad12921185b",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "d2da787a-de53-4a6a-9aca-a62f796336e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "f9c5bf5d-fd73-45ca-a2dd-8aade792f975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2da787a-de53-4a6a-9aca-a62f796336e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d98a95-6c7a-42e1-abc8-92a4576c3ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2da787a-de53-4a6a-9aca-a62f796336e6",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "a2c82bbb-0bdc-49d2-9101-f2d07e6095dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "b5106fe7-e162-4542-a870-043d56b29cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c82bbb-0bdc-49d2-9101-f2d07e6095dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73aecbd-8a01-48ba-b2ed-0141331597dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c82bbb-0bdc-49d2-9101-f2d07e6095dd",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "8f0ad491-515d-4670-9594-7a996471ab68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "cc8a3568-cce0-4af8-a6b6-22d528d4eba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f0ad491-515d-4670-9594-7a996471ab68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20278de1-a10e-4268-8133-01885cefdf09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0ad491-515d-4670-9594-7a996471ab68",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "ac461b65-ba59-4229-85cb-8d7d8ba4dd1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "96aae1cb-32c7-443f-800a-06d9f1e85da4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac461b65-ba59-4229-85cb-8d7d8ba4dd1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a63231fb-fcd0-40a6-87e3-959e8ba151c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac461b65-ba59-4229-85cb-8d7d8ba4dd1b",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "918fc74f-027b-4651-b6f3-3892da8b4885",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "4e0786a1-357c-4eb1-a58c-25fc7e4fabb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "918fc74f-027b-4651-b6f3-3892da8b4885",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8063456-fc96-454c-8acf-80b0c219332d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918fc74f-027b-4651-b6f3-3892da8b4885",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "3e5ca8f6-2f51-4188-9af4-ad0b9b08f3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "08a9d279-cc74-4d1a-98db-0e7bb1bc2df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e5ca8f6-2f51-4188-9af4-ad0b9b08f3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6dae128-68a8-4787-81aa-887bf8aba93e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e5ca8f6-2f51-4188-9af4-ad0b9b08f3be",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "8986fe44-3d5f-443b-935a-c94ab154ffa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "5d8dbeae-bac1-4fd3-bd9b-7c9906247206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8986fe44-3d5f-443b-935a-c94ab154ffa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28254f6d-ec87-44e7-b8da-23e4647f447a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8986fe44-3d5f-443b-935a-c94ab154ffa9",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "3209694f-d377-4f2c-9022-4d81f85610aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "ad67daae-a756-493f-9dda-4ecd159507b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3209694f-d377-4f2c-9022-4d81f85610aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871919db-bbcf-4d87-85d8-2c278d9c87ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3209694f-d377-4f2c-9022-4d81f85610aa",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "771facc8-590f-49c8-9fc1-69f4bf39982d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "234ae06d-0097-4973-a56a-48ed642e7690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771facc8-590f-49c8-9fc1-69f4bf39982d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3310a81-af2d-42d7-a31c-bc750c6f840a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771facc8-590f-49c8-9fc1-69f4bf39982d",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "574ea17e-7276-419a-baff-94b50809ed39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "b5bf0a55-5524-47e2-91b7-75af882e1706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "574ea17e-7276-419a-baff-94b50809ed39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bd0486d-4658-4fd7-b31e-c9ab39bdd8f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "574ea17e-7276-419a-baff-94b50809ed39",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "30f67986-c100-49f6-b52f-e9594641b9ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "ecfd0a29-086a-4b8a-9bbf-757de3973784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f67986-c100-49f6-b52f-e9594641b9ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc64d64-611f-47d3-9f55-33ca4fe04cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f67986-c100-49f6-b52f-e9594641b9ba",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "9a060bb0-dea9-49ca-a961-f9c144a024f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "febb7bf3-9ec3-4464-9d87-664dc48ab140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a060bb0-dea9-49ca-a961-f9c144a024f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861f9b39-ae93-4ce7-a6eb-86e900aee7e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a060bb0-dea9-49ca-a961-f9c144a024f6",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "6f936ba7-c573-416d-a74f-896e4f2bf85c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "e2807b32-11c6-4c1f-8618-d497a81a6c6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f936ba7-c573-416d-a74f-896e4f2bf85c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6bd320d-be21-4204-89bb-2e8dec6f1035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f936ba7-c573-416d-a74f-896e4f2bf85c",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "af957e54-c884-4fca-aeb6-730a1d3c54e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "38a57557-067b-4774-bcf1-5b40f8751ad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af957e54-c884-4fca-aeb6-730a1d3c54e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0291c261-ca80-47a4-9b66-7b7e8b5b5a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af957e54-c884-4fca-aeb6-730a1d3c54e7",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "f1bc7d30-8401-447a-a04e-18c3ec207483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "ea967dc2-37d3-4243-bcdd-f2274292d80c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1bc7d30-8401-447a-a04e-18c3ec207483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ff667a-0e98-420f-9da4-27bddfc22456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1bc7d30-8401-447a-a04e-18c3ec207483",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "a1c4685b-6335-4928-9fe9-6ca4532f5d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "3e02f0d5-6d61-41ee-bad7-a77891ed6d53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1c4685b-6335-4928-9fe9-6ca4532f5d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b83fab-a9a6-42e2-8249-5478d2e2f43e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1c4685b-6335-4928-9fe9-6ca4532f5d72",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "36a28b3d-3a43-49b1-bf91-a03aebf0afd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "6440492b-3607-4c05-8c50-02c19b6460d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a28b3d-3a43-49b1-bf91-a03aebf0afd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "359569d8-235c-4036-885c-cba145f5d500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a28b3d-3a43-49b1-bf91-a03aebf0afd4",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "3bbbe6a2-a51e-47d9-9c47-ef83e2857368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "cd4d9458-2084-4adc-932d-a61ecf1c4814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bbbe6a2-a51e-47d9-9c47-ef83e2857368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "503323f8-20e2-47ee-84da-fb7c7295ce1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bbbe6a2-a51e-47d9-9c47-ef83e2857368",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "0685dca6-9aea-45fc-ae44-e04d482402ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "9afd43f8-5da0-4db3-a550-5014babc1609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0685dca6-9aea-45fc-ae44-e04d482402ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fed85a4-ad9a-45d2-a69f-fd6596ec5b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0685dca6-9aea-45fc-ae44-e04d482402ab",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "0acdd8df-a757-44a8-9a3b-ce5362564cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "d79d1d40-568e-4ea4-8133-62b81e6c1ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0acdd8df-a757-44a8-9a3b-ce5362564cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04a3b11c-7dde-4e75-bdad-60d6c6a950c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0acdd8df-a757-44a8-9a3b-ce5362564cc4",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "d3423a2a-8240-4cb4-bca8-0e87cf3d108d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "b2f57f91-8263-49e0-a17c-bac27f043f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3423a2a-8240-4cb4-bca8-0e87cf3d108d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545c0334-feb3-4d56-8507-1419e15cd8fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3423a2a-8240-4cb4-bca8-0e87cf3d108d",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "f1d92a89-b68d-4fce-aeb8-92abcee242fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "d1786820-18db-4437-a9b8-1a0931d9ff90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d92a89-b68d-4fce-aeb8-92abcee242fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61e3bbaa-626d-412e-b73c-85fdd0583094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d92a89-b68d-4fce-aeb8-92abcee242fa",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "d9345111-f8a8-4d83-8268-d59ed6aa1ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "d88faa02-89eb-4ba7-8926-29e4d710fa56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9345111-f8a8-4d83-8268-d59ed6aa1ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca879b33-e6eb-4ee5-8f99-981c6ee99ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9345111-f8a8-4d83-8268-d59ed6aa1ab5",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "e593f458-9501-446f-a680-1256ca79e090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "b596f7a3-1a64-47a5-9fec-b6605d5c48cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e593f458-9501-446f-a680-1256ca79e090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b0b5c4f-243c-4387-8e0c-896d7f2c7eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e593f458-9501-446f-a680-1256ca79e090",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "60e12f8d-e72c-4185-9227-01088a5e85fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "55a3a61e-06c0-4099-9021-562607bf30f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60e12f8d-e72c-4185-9227-01088a5e85fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e26af5-3557-4a02-8c37-088171f532c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60e12f8d-e72c-4185-9227-01088a5e85fa",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "58b66da7-6993-45ac-aa03-3db99ece2499",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "66392ffa-620b-4d73-a469-7f6aa9c1cac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b66da7-6993-45ac-aa03-3db99ece2499",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d18b57c-d2eb-49ea-9291-805c9d4b1456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b66da7-6993-45ac-aa03-3db99ece2499",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "bb1717ee-aee3-4b47-b207-a032741df8df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "acbd4a78-97b3-4b9d-93f5-a4f2d4c66350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1717ee-aee3-4b47-b207-a032741df8df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f7aea2-0fed-497b-aed8-6a95c7aa3d24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1717ee-aee3-4b47-b207-a032741df8df",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "554c989e-6fca-4bbe-aa9d-ee00604857a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "eddddf4d-7c8f-4369-af65-cfc1fde024b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554c989e-6fca-4bbe-aa9d-ee00604857a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e77455b-ec30-4c69-8eaf-dbdcaa10fe91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554c989e-6fca-4bbe-aa9d-ee00604857a4",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "adf2dee0-0fe9-4385-a075-e54db5ea7384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "ea5a2995-5376-4547-b232-350fd7b0e566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf2dee0-0fe9-4385-a075-e54db5ea7384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "790bc643-e363-47fc-9c81-75d25674ceb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf2dee0-0fe9-4385-a075-e54db5ea7384",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "39ad277b-e3b4-4818-892f-0c32f32538b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "e1f47799-276f-4f8f-8df4-483a403dc1ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ad277b-e3b4-4818-892f-0c32f32538b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802c9355-01d5-4011-9f93-0a30a279a4db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ad277b-e3b4-4818-892f-0c32f32538b0",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "7e8867bd-9cb6-4ed0-b62e-b23ed6d8c02e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "3cc3e03d-35b6-4164-8973-c7b8e75f5fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8867bd-9cb6-4ed0-b62e-b23ed6d8c02e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec1c3380-b86e-48e6-bc17-33200837474e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8867bd-9cb6-4ed0-b62e-b23ed6d8c02e",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "2fc77464-5882-4af5-98a9-45b3b0f87f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "712db95a-4e15-48cf-8897-f23beff72a67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fc77464-5882-4af5-98a9-45b3b0f87f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc93b610-1f69-4e54-88e2-c63535d293b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fc77464-5882-4af5-98a9-45b3b0f87f8d",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "e350dcee-0fc9-46b7-9f8e-8333e64cad5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "5c66f487-4c65-4b6f-9a15-dc8c32ace439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e350dcee-0fc9-46b7-9f8e-8333e64cad5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b6c18f-f371-4df1-b7a5-b80da236d5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e350dcee-0fc9-46b7-9f8e-8333e64cad5b",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "2897cb85-f8b7-436b-8e13-1421c4478882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "7bf9c572-3fc3-48b5-b7d8-336af97e1268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2897cb85-f8b7-436b-8e13-1421c4478882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1348fd-7f5e-4705-aa77-c8b2254b0b21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2897cb85-f8b7-436b-8e13-1421c4478882",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "9cee1a35-647c-4c80-a292-70563310470c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "22e46cf9-c821-4469-81b5-aee7e2a1ee81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cee1a35-647c-4c80-a292-70563310470c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e271ccd-3674-4e84-ba90-81e83669bb84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cee1a35-647c-4c80-a292-70563310470c",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "2ea3e2e8-44f0-4ee7-b4d4-7a076bcfd333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "10308e3f-525f-4b1a-93e3-ef08d82fdbce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ea3e2e8-44f0-4ee7-b4d4-7a076bcfd333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eae3ddb-a69b-432a-83c3-65d54e49f95e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ea3e2e8-44f0-4ee7-b4d4-7a076bcfd333",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "a2c48749-1884-4820-b72d-bdd47027350d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "1837f256-5e4c-41f2-9635-786bb253120c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c48749-1884-4820-b72d-bdd47027350d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b487311-f288-419e-9044-95d88480021c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c48749-1884-4820-b72d-bdd47027350d",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "a2520604-02d6-4ce5-9b1b-9e20b84658d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "913f77f7-320a-49dc-8d5e-a698916b833f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2520604-02d6-4ce5-9b1b-9e20b84658d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "362e91e0-579b-4800-a90c-08ddd5bc769e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2520604-02d6-4ce5-9b1b-9e20b84658d0",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "c9560252-d33a-4d8a-9c7c-2e048667022d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "0a4651b1-ce38-4fc9-af30-b6ffd7cacf03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9560252-d33a-4d8a-9c7c-2e048667022d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fd6df73-16f2-42cc-bc6e-a3c2cd5baf1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9560252-d33a-4d8a-9c7c-2e048667022d",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "3a4df2c9-793d-4b4f-a4cf-3d4e7dd7cfc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "471490b3-4a5d-407a-91fb-83de882b3e25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a4df2c9-793d-4b4f-a4cf-3d4e7dd7cfc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae05f006-0b2f-4d10-9c45-cbc3a3efc77c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a4df2c9-793d-4b4f-a4cf-3d4e7dd7cfc1",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "43f26bb8-e83f-4802-94db-95b6c7013b74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "fbed037d-da04-465b-8dde-f3e8e63f3381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f26bb8-e83f-4802-94db-95b6c7013b74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15b21b9-acd1-4435-834f-0873ae2d2c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f26bb8-e83f-4802-94db-95b6c7013b74",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        },
        {
            "id": "b4abb0f3-b424-4600-a052-ef1946dca56a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "compositeImage": {
                "id": "41b7c331-5e84-44f2-8a39-59808f0dde18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4abb0f3-b424-4600-a052-ef1946dca56a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6aa58b-3990-4112-ad16-5fbe2763b3e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4abb0f3-b424-4600-a052-ef1946dca56a",
                    "LayerId": "b075c90c-0865-4249-a420-9dea919bbce7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b075c90c-0865-4249-a420-9dea919bbce7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e756001-b1c8-403f-b28d-f85a6cd4c110",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}