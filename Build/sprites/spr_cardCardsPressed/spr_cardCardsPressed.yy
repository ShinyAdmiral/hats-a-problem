{
    "id": "d0ae102d-0a80-41f9-a69e-2ae3947c7684",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cardCardsPressed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a57f44c-b875-41ad-b3be-9a67c6813bd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0ae102d-0a80-41f9-a69e-2ae3947c7684",
            "compositeImage": {
                "id": "f72a6806-832d-4594-9572-ac78a611cd31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a57f44c-b875-41ad-b3be-9a67c6813bd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee52282-553a-47b6-b3bd-3502521de6a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a57f44c-b875-41ad-b3be-9a67c6813bd4",
                    "LayerId": "68889377-9949-4541-806d-f7c1909fcb3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "68889377-9949-4541-806d-f7c1909fcb3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0ae102d-0a80-41f9-a69e-2ae3947c7684",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}