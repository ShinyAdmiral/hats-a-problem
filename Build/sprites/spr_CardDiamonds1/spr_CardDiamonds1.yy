{
    "id": "a16b21e4-70be-4b2f-8519-d67d22181bd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardDiamonds1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "208c664b-cb97-4fb8-9716-5a117011a529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a16b21e4-70be-4b2f-8519-d67d22181bd8",
            "compositeImage": {
                "id": "20129905-89c4-4c16-9ec1-a4aedb9b0eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208c664b-cb97-4fb8-9716-5a117011a529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d610280-78ad-4793-8318-f5e4c241389d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208c664b-cb97-4fb8-9716-5a117011a529",
                    "LayerId": "18bf1a2b-c4e1-478d-a944-b6ab12037d59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "18bf1a2b-c4e1-478d-a944-b6ab12037d59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a16b21e4-70be-4b2f-8519-d67d22181bd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}