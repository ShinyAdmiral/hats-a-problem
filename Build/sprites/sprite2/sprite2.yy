{
    "id": "b9ba2a74-feae-4c7e-a8c1-a9c6b9d96459",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 4,
    "bbox_right": 73,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e71630ce-9624-4a87-a132-7e71a2c5a640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9ba2a74-feae-4c7e-a8c1-a9c6b9d96459",
            "compositeImage": {
                "id": "0c9afc68-f9db-4926-9452-735cc2982583",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71630ce-9624-4a87-a132-7e71a2c5a640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b40563d-0b52-4224-b545-cf6f5ee2b4ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71630ce-9624-4a87-a132-7e71a2c5a640",
                    "LayerId": "5105b1f1-e462-4e39-aa7f-bb2bafcc6950"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5105b1f1-e462-4e39-aa7f-bb2bafcc6950",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9ba2a74-feae-4c7e-a8c1-a9c6b9d96459",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 39,
    "yorig": 32
}