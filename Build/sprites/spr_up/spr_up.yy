{
    "id": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3148cd0-4ef0-4d9c-b824-990d9d8cc315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "70819235-e585-4d2f-b90b-04fc3e487d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3148cd0-4ef0-4d9c-b824-990d9d8cc315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf41758-19ce-43bb-886c-72c4f1f1bf41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3148cd0-4ef0-4d9c-b824-990d9d8cc315",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        },
        {
            "id": "8e1bf4e9-2d41-4a1f-ba63-061c9288e7fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "72cf6591-26ed-49ed-a806-56db2a9cd6ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e1bf4e9-2d41-4a1f-ba63-061c9288e7fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "122ddd59-b9e8-45a6-becf-d57a8285a272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e1bf4e9-2d41-4a1f-ba63-061c9288e7fa",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        },
        {
            "id": "32b2092c-17a8-4135-8437-778e1bf39eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "e7c3fbf0-c958-4eee-a379-8f3c7062989d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32b2092c-17a8-4135-8437-778e1bf39eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8543b849-313a-47cf-a68d-f562d80214b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32b2092c-17a8-4135-8437-778e1bf39eda",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        },
        {
            "id": "86ae12a3-e414-4425-977b-1529765a8ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "1572642e-00ee-473f-a15c-5fc1f67e305b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86ae12a3-e414-4425-977b-1529765a8ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2bb6167-5855-42f8-a72c-059f14632132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86ae12a3-e414-4425-977b-1529765a8ee6",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        },
        {
            "id": "2c93bc3d-c0f1-4749-888b-60473115eaa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "5b22822d-ee93-4d56-a863-98f8a36a5f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c93bc3d-c0f1-4749-888b-60473115eaa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7249f672-c4a0-41f2-b48d-9ea77e054c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c93bc3d-c0f1-4749-888b-60473115eaa3",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        },
        {
            "id": "f907acd6-3ef2-4f6b-b1d7-d13a2be452a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "b66305f7-60c2-45c9-8c0e-385c6c7f9e7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f907acd6-3ef2-4f6b-b1d7-d13a2be452a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10c93668-c542-40ec-bb2f-9eca1405a8a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f907acd6-3ef2-4f6b-b1d7-d13a2be452a6",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        },
        {
            "id": "aab1a3b6-e181-42b2-8cd8-99eccc094902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "5787858d-5580-4b84-8849-270efa6deb3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab1a3b6-e181-42b2-8cd8-99eccc094902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd11b61a-bffd-41dc-be72-549e36dffb41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab1a3b6-e181-42b2-8cd8-99eccc094902",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        },
        {
            "id": "509363d5-87c1-4189-86ab-a32d1024fcd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "compositeImage": {
                "id": "1f989161-a8fa-4afd-a3e1-b9d7d781914c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "509363d5-87c1-4189-86ab-a32d1024fcd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ae9809-6b1b-4ffb-b8dc-9e6e59175c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "509363d5-87c1-4189-86ab-a32d1024fcd4",
                    "LayerId": "61182224-fbe3-4066-a311-6c6d2d372e84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "61182224-fbe3-4066-a311-6c6d2d372e84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}