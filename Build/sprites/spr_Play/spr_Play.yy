{
    "id": "271f983b-9a1f-436d-8e7a-cb145e9ad604",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Play",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acf29c85-ee6c-4ec0-bc3b-f2cd8cbaf0f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "271f983b-9a1f-436d-8e7a-cb145e9ad604",
            "compositeImage": {
                "id": "f73b9c66-5cf1-4de5-89e4-98bc5746cdfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acf29c85-ee6c-4ec0-bc3b-f2cd8cbaf0f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bdb6920-6767-4eec-8745-bb0084d9ace6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acf29c85-ee6c-4ec0-bc3b-f2cd8cbaf0f4",
                    "LayerId": "0f34982f-96aa-49b8-89a2-64147a9a54c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0f34982f-96aa-49b8-89a2-64147a9a54c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "271f983b-9a1f-436d-8e7a-cb145e9ad604",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}