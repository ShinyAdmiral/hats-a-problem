{
    "id": "b654c9f8-a6ec-4ea5-a6ab-b8034f8ce2cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5f31438-8ef3-477e-b78a-9c78c40c9f9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b654c9f8-a6ec-4ea5-a6ab-b8034f8ce2cd",
            "compositeImage": {
                "id": "333d1863-e671-4d74-a19b-f7fd38fbd2a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5f31438-8ef3-477e-b78a-9c78c40c9f9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "360ccc90-cfcf-4f65-b1fb-914cd5c5b897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5f31438-8ef3-477e-b78a-9c78c40c9f9e",
                    "LayerId": "be8b42dd-3bf1-4c22-8b13-7eacb7c83641"
                }
            ]
        },
        {
            "id": "fd4040f3-9a7d-4b8e-a7d5-73d5d85dbc53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b654c9f8-a6ec-4ea5-a6ab-b8034f8ce2cd",
            "compositeImage": {
                "id": "07c8bed5-01e9-4a89-b74a-57052e7e8bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd4040f3-9a7d-4b8e-a7d5-73d5d85dbc53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cde0239-e3fa-411b-a312-0bd80a193e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd4040f3-9a7d-4b8e-a7d5-73d5d85dbc53",
                    "LayerId": "be8b42dd-3bf1-4c22-8b13-7eacb7c83641"
                }
            ]
        },
        {
            "id": "6b14d956-825d-4c5e-9a72-2b86030deede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b654c9f8-a6ec-4ea5-a6ab-b8034f8ce2cd",
            "compositeImage": {
                "id": "96f35138-8708-46ed-9573-517d18708399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b14d956-825d-4c5e-9a72-2b86030deede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f16cc246-9993-4864-b366-aa507967eb11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b14d956-825d-4c5e-9a72-2b86030deede",
                    "LayerId": "be8b42dd-3bf1-4c22-8b13-7eacb7c83641"
                }
            ]
        },
        {
            "id": "26c410eb-ad25-4236-af7e-4ca3f8af8feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b654c9f8-a6ec-4ea5-a6ab-b8034f8ce2cd",
            "compositeImage": {
                "id": "cee75091-50d4-42d4-96a8-e8c58feb6b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c410eb-ad25-4236-af7e-4ca3f8af8feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7963d35-42a6-4b74-9510-4ed6a634eb5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c410eb-ad25-4236-af7e-4ca3f8af8feb",
                    "LayerId": "be8b42dd-3bf1-4c22-8b13-7eacb7c83641"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "be8b42dd-3bf1-4c22-8b13-7eacb7c83641",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b654c9f8-a6ec-4ea5-a6ab-b8034f8ce2cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}