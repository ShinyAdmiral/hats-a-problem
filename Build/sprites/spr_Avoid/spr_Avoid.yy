{
    "id": "62b96513-bbe7-41f8-a6bb-572f29cb8792",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Avoid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc0ce11b-d3be-48d2-b07c-eaff75735a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62b96513-bbe7-41f8-a6bb-572f29cb8792",
            "compositeImage": {
                "id": "6e0420a5-0d21-449f-8858-e9ce09618355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc0ce11b-d3be-48d2-b07c-eaff75735a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66d91087-2c5a-425a-80cd-00f9cd42867b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc0ce11b-d3be-48d2-b07c-eaff75735a93",
                    "LayerId": "bf7d50ea-05d7-4ef8-9188-1ead56b54f24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "bf7d50ea-05d7-4ef8-9188-1ead56b54f24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62b96513-bbe7-41f8-a6bb-572f29cb8792",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}