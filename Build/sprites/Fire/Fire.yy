{
    "id": "d0052b80-ffb6-439d-9e0f-a49d3f3b439a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed7a0ddb-7d36-4751-b912-4aec6cc82ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0052b80-ffb6-439d-9e0f-a49d3f3b439a",
            "compositeImage": {
                "id": "4285d969-2579-4c13-9357-bd1067cb1b6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed7a0ddb-7d36-4751-b912-4aec6cc82ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9734ddd-bc1d-47fe-9462-1f3208cc47aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed7a0ddb-7d36-4751-b912-4aec6cc82ed2",
                    "LayerId": "bd68cb58-7d41-4511-9fff-37b0ce993e9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bd68cb58-7d41-4511-9fff-37b0ce993e9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0052b80-ffb6-439d-9e0f-a49d3f3b439a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}