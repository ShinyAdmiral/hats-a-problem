{
    "id": "d9b6dbd9-2a01-4141-80f4-1dbb544a0d48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96d4631a-b46e-44cd-b053-ef921715fb53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9b6dbd9-2a01-4141-80f4-1dbb544a0d48",
            "compositeImage": {
                "id": "ab4accb9-4057-4789-a26e-eec639c88659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d4631a-b46e-44cd-b053-ef921715fb53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99c5830a-25d7-4fea-b455-01a7d072d25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d4631a-b46e-44cd-b053-ef921715fb53",
                    "LayerId": "3814d3f2-dcac-4d34-95fb-a2ca334b671d"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 16,
    "layers": [
        {
            "id": "3814d3f2-dcac-4d34-95fb-a2ca334b671d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9b6dbd9-2a01-4141-80f4-1dbb544a0d48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 3
}