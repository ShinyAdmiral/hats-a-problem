{
    "id": "4efc0cc4-5e90-430d-8012-2af3ad80e9b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TextBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2e478bb-eb27-486a-81b1-27a37f08bbee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4efc0cc4-5e90-430d-8012-2af3ad80e9b5",
            "compositeImage": {
                "id": "ec269c38-918f-4907-89dc-9babbe9aee67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2e478bb-eb27-486a-81b1-27a37f08bbee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1103a7-6cc5-4217-858e-a4250ebf065b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2e478bb-eb27-486a-81b1-27a37f08bbee",
                    "LayerId": "edc0e007-c4d3-4818-83a7-cc1b21baaf17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "edc0e007-c4d3-4818-83a7-cc1b21baaf17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4efc0cc4-5e90-430d-8012-2af3ad80e9b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 0,
    "yorig": 0
}