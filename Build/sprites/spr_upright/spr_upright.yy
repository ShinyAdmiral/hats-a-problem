{
    "id": "07f062ce-3160-40c0-a613-20797a06f956",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_upright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44b438ba-767c-47a1-b501-a4801f2ba709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "68857bb6-de7f-436f-9adb-f8c38ec39fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b438ba-767c-47a1-b501-a4801f2ba709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41d90d1b-424e-4339-b996-3af4be6cbca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b438ba-767c-47a1-b501-a4801f2ba709",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        },
        {
            "id": "d49adbfb-ec2f-4685-98f9-811601bf603f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "8fb9471c-cd90-4e59-b6fe-be7fa20d5929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49adbfb-ec2f-4685-98f9-811601bf603f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d19bd31-16b0-43e4-8f7e-41b8f9ae976f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49adbfb-ec2f-4685-98f9-811601bf603f",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        },
        {
            "id": "eca89df5-ddb4-4eb5-aa2e-392960c8eb43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "abfb5449-f18d-440d-91cc-253877de445b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eca89df5-ddb4-4eb5-aa2e-392960c8eb43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "263be801-338c-4306-a4a5-b70e7a867aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eca89df5-ddb4-4eb5-aa2e-392960c8eb43",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        },
        {
            "id": "79e6de69-b072-4f9a-b3f6-42aad2328eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "46ef0691-4d46-41f1-b828-90c0f6844900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79e6de69-b072-4f9a-b3f6-42aad2328eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95060e0e-bbf2-42c6-9ad6-6936e98f82cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79e6de69-b072-4f9a-b3f6-42aad2328eef",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        },
        {
            "id": "ec42be29-9cbb-421b-8736-23ca31ccd130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "03d4b572-3cb2-4dc9-b60f-d0764c7a7308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec42be29-9cbb-421b-8736-23ca31ccd130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c7cb86b-105b-4a51-996d-e7475b3c5daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec42be29-9cbb-421b-8736-23ca31ccd130",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        },
        {
            "id": "9f9003f3-b74a-45d4-9695-4c857d567951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "e6aad2f0-3923-4cc6-878d-2e688d9dde26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9003f3-b74a-45d4-9695-4c857d567951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "274a837a-616e-4b4f-a3bf-dca933658f0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9003f3-b74a-45d4-9695-4c857d567951",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        },
        {
            "id": "058fe477-c32c-4d08-ae6e-d520e5fadab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "a97cb45a-29ed-4758-ad98-f7fa5962d229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "058fe477-c32c-4d08-ae6e-d520e5fadab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9061c6cd-9bd0-4f21-9369-19eac3976e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "058fe477-c32c-4d08-ae6e-d520e5fadab0",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        },
        {
            "id": "f7ddd282-d4fa-4f86-b9c1-16337157eccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "compositeImage": {
                "id": "a3161532-1f45-4e2c-bd3e-4c01949dcaf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ddd282-d4fa-4f86-b9c1-16337157eccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c10c5b6b-d994-4288-9513-128a45bc1f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ddd282-d4fa-4f86-b9c1-16337157eccb",
                    "LayerId": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9e2dc49a-3adc-4ee2-8ca9-abe32bd84da0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07f062ce-3160-40c0-a613-20797a06f956",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}