{
    "id": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_leftHands",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 9,
    "bbox_right": 44,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c6bf93d-39cc-4805-948c-fba7b5e01df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "32f01585-2a71-4e4a-8195-256010b92e79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6bf93d-39cc-4805-948c-fba7b5e01df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84c25b6b-0146-452c-93ab-d9b58129869d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6bf93d-39cc-4805-948c-fba7b5e01df1",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "6ba237bc-f68a-4089-8cf1-69a0f7c66725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "8ff1975a-26b8-4293-b8dc-b1cded538f77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba237bc-f68a-4089-8cf1-69a0f7c66725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d19d7b5f-1fa6-44af-a521-9bdd054bdce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba237bc-f68a-4089-8cf1-69a0f7c66725",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "b2375039-eea5-40f8-8373-fd187d3b2fd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "170bd1de-f738-482b-a5be-f4352265af70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2375039-eea5-40f8-8373-fd187d3b2fd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06d900d7-db74-4802-a0bf-16c9342acce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2375039-eea5-40f8-8373-fd187d3b2fd4",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "9ba50532-34de-499f-94b0-277ffdc91912",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "33759c67-7a49-48ca-aa81-9aab831352a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba50532-34de-499f-94b0-277ffdc91912",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d171151-0008-4bef-b9e6-2f9b497f24d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba50532-34de-499f-94b0-277ffdc91912",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "a046f6e3-cabb-44c2-aed5-2d5a8e42452d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "befd47d8-23fa-49e6-84c8-e1a1ab3577f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a046f6e3-cabb-44c2-aed5-2d5a8e42452d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c02aa28-b6bd-40a7-a88b-3054565afb37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a046f6e3-cabb-44c2-aed5-2d5a8e42452d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "598fb9b9-9b92-411c-ae38-418096e1a544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "6f945804-0749-4961-838d-e6bbad5b5875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "598fb9b9-9b92-411c-ae38-418096e1a544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "783eb325-647a-45d1-a75e-da3fea0ee6f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "598fb9b9-9b92-411c-ae38-418096e1a544",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "da550847-4f39-4511-8102-0f284639ecbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "03387bce-e26d-4427-bfd5-a07b01855ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da550847-4f39-4511-8102-0f284639ecbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d406598f-8fd7-48bf-b8b0-a710bbe50f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da550847-4f39-4511-8102-0f284639ecbd",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "cea59395-f1f1-41f6-9922-12898dd9b6d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "a8192809-25a4-4283-a773-16bae0394fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea59395-f1f1-41f6-9922-12898dd9b6d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45a13fe3-243f-4bb2-97f4-94cd79bf633a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea59395-f1f1-41f6-9922-12898dd9b6d7",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "5e5758dc-437a-49aa-abbe-9bcb1e337c1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "971b24aa-2ddc-413d-b01c-2e1e44ab4071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e5758dc-437a-49aa-abbe-9bcb1e337c1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2578ea07-f52f-480c-836d-f1f0e02e6aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e5758dc-437a-49aa-abbe-9bcb1e337c1d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "71a4dbaf-392e-4db6-96ff-c66b18ff50a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "f5dc80ba-b5b5-4914-bf42-a3be39a19409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a4dbaf-392e-4db6-96ff-c66b18ff50a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac58552-fcd7-4b1f-9045-cf971ce14cd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a4dbaf-392e-4db6-96ff-c66b18ff50a1",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "e9f41504-c750-408c-855c-f063a768dd8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "e945e811-5f65-49f0-92f5-887b8289e810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f41504-c750-408c-855c-f063a768dd8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0b3deb-2950-4d6d-8965-31dcd0a227ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f41504-c750-408c-855c-f063a768dd8d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "6f0c6cf0-7902-43cf-a1f2-eaea5173306e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "95ef22f8-3b13-4088-9578-4923397accff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f0c6cf0-7902-43cf-a1f2-eaea5173306e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf00dcdb-afc2-49dc-ad53-7fb717ec9643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f0c6cf0-7902-43cf-a1f2-eaea5173306e",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "b454d4e0-43bf-482d-9fe7-2841002a057e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "02dd7347-cfb4-4563-9d52-5173aa598244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b454d4e0-43bf-482d-9fe7-2841002a057e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c6ae58-9244-4e15-bfc5-e443128d9a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b454d4e0-43bf-482d-9fe7-2841002a057e",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "7db6d109-e8dc-482a-94f1-1cb712e8b83f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "3f00df44-9772-432f-9459-ef76d10c10c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db6d109-e8dc-482a-94f1-1cb712e8b83f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e101c092-3fbe-481e-924d-4050ad9d68ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db6d109-e8dc-482a-94f1-1cb712e8b83f",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "16b3ddae-f218-4170-af9e-6b1c9046865d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "5eb4a153-fc82-41dc-8543-29497d775948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16b3ddae-f218-4170-af9e-6b1c9046865d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "173e613c-5b39-4051-9281-622fde1413df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16b3ddae-f218-4170-af9e-6b1c9046865d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "a8326b84-4d4b-4dae-a9cc-ee2d6c949dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "118d25e0-a0c2-4295-a5e5-8815bcdbf0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8326b84-4d4b-4dae-a9cc-ee2d6c949dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "359f668e-53ba-425e-b6a5-fccf25cb826d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8326b84-4d4b-4dae-a9cc-ee2d6c949dfe",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "15a39801-ebef-4fd3-aa00-20a58c5d5114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "84868c18-d74b-4db6-a9b1-2856206d69f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a39801-ebef-4fd3-aa00-20a58c5d5114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1711f6d5-8325-45d9-ac8a-8a54a2b5eb78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a39801-ebef-4fd3-aa00-20a58c5d5114",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "6dd482a4-ac52-4397-942d-4da5c986bc8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "49c63e2d-e972-4fe5-92c5-c20d8171b60c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dd482a4-ac52-4397-942d-4da5c986bc8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb76283f-bb13-439a-ae6a-fa60ffc2bad0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dd482a4-ac52-4397-942d-4da5c986bc8c",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "82719692-99c3-4de1-8d3f-c6c99a6aff3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "8b481fbc-19d2-4dc1-8c8e-083f057e47ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82719692-99c3-4de1-8d3f-c6c99a6aff3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fec7fa15-a774-403f-b2e5-d30b4df74116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82719692-99c3-4de1-8d3f-c6c99a6aff3b",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "0e59a0db-bb16-492b-b168-010a8293c367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "2738d4d6-0169-4123-a1db-02613caf41a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e59a0db-bb16-492b-b168-010a8293c367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9648d635-30d6-4086-b9a0-fb248c49286c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e59a0db-bb16-492b-b168-010a8293c367",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "78407a70-819f-4e1a-b577-6b7fab591409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "f27b8898-a924-4adb-8a08-2fb0f2608ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78407a70-819f-4e1a-b577-6b7fab591409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32eb9614-ff05-4b3b-b4a6-57a5dd2f22af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78407a70-819f-4e1a-b577-6b7fab591409",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "a1f1773d-9ea8-4bf1-8aa5-bdc9ab3b73f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "87e73f63-c4eb-4f42-b92b-d032c9246005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f1773d-9ea8-4bf1-8aa5-bdc9ab3b73f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1871ecb2-0ca7-48ae-a488-06969c3af2d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f1773d-9ea8-4bf1-8aa5-bdc9ab3b73f9",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "34f87a2d-6c30-4e4c-8a24-e46d0d8a9d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "7189a129-6aa6-427d-a7e4-f4ec6cd62461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f87a2d-6c30-4e4c-8a24-e46d0d8a9d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74d5b325-eb83-46bb-ad83-6d8db18494d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f87a2d-6c30-4e4c-8a24-e46d0d8a9d61",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "9aae7547-9661-475b-8eea-f73b0c7211ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "aa6e934d-004c-426e-bca3-b5b3c790d5aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aae7547-9661-475b-8eea-f73b0c7211ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d97fd711-40ed-4196-a790-898b73dabdc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aae7547-9661-475b-8eea-f73b0c7211ef",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "d63d1c8f-f152-48d1-bb1d-223dec3eacb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "6680f154-c5fd-4612-a9f5-e4d9ba97aec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63d1c8f-f152-48d1-bb1d-223dec3eacb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2368df7f-98c8-4104-8648-59e369381ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63d1c8f-f152-48d1-bb1d-223dec3eacb3",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "da920579-e793-43e4-9ba0-36b23cfb03d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "4443e06d-f1be-48c8-8c5f-11c5eff33daf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da920579-e793-43e4-9ba0-36b23cfb03d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afef6fee-d5ad-4aa0-92fb-06b2fb7c804a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da920579-e793-43e4-9ba0-36b23cfb03d1",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "8984b015-7af0-49b2-88b9-2bdd65cd1709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "53cb849b-bb71-4645-bb92-45b1eab06f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8984b015-7af0-49b2-88b9-2bdd65cd1709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01bd9155-c857-476d-922d-35b799e850db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8984b015-7af0-49b2-88b9-2bdd65cd1709",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "018ad822-2077-490a-b8a2-7e0f120d8b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "00577253-e3f3-4a8b-97cb-4f350934a94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "018ad822-2077-490a-b8a2-7e0f120d8b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491d373d-4def-486f-9e2e-24f0c3c4c248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "018ad822-2077-490a-b8a2-7e0f120d8b09",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "0e4fa184-fd0a-4240-ac5b-7f07e55d0c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "1bdf258a-16c9-434a-8ae8-5d94fc338c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4fa184-fd0a-4240-ac5b-7f07e55d0c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6edf20f-8db5-40a9-935d-10bb18ff5016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4fa184-fd0a-4240-ac5b-7f07e55d0c01",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "e3512035-77c2-475e-b0a7-127655c9de1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "57af0e33-09e4-40bf-847e-76a0439b3fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3512035-77c2-475e-b0a7-127655c9de1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87e39e8-3a64-464c-a5c7-54512b29a44b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3512035-77c2-475e-b0a7-127655c9de1e",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "12dca8a7-2c0a-4d97-bc28-78ccda88b85f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "e09b9627-0068-427c-b7ce-8a05d6f9d85d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12dca8a7-2c0a-4d97-bc28-78ccda88b85f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6486655-e7e1-4032-86c6-1ba84c47838f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12dca8a7-2c0a-4d97-bc28-78ccda88b85f",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "3a8caf0f-b364-404e-9616-94dc36bc3748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "cb5acc43-807f-4c6d-a058-3dd4668526b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8caf0f-b364-404e-9616-94dc36bc3748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0149844b-ffbd-47fc-b1c6-9a705ae7afdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8caf0f-b364-404e-9616-94dc36bc3748",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "24093017-a1b0-42eb-884c-eeb07e020cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "b69028ff-1c0f-454c-9d27-ff8bb7e23d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24093017-a1b0-42eb-884c-eeb07e020cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7645462e-1c50-4fea-9cff-7224d1fc74f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24093017-a1b0-42eb-884c-eeb07e020cd1",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "cb3b2140-9505-45c6-bb9e-f0a95ca450dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "164f27dd-941d-41a3-8901-c9f6f2cfd9b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb3b2140-9505-45c6-bb9e-f0a95ca450dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adde588a-6a4c-4228-94f5-1041e3194ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb3b2140-9505-45c6-bb9e-f0a95ca450dc",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "76720b6b-ec40-454b-b6e8-4c3f7b53699a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "7c9338a0-eb05-42fb-b9dd-e3907a3ca558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76720b6b-ec40-454b-b6e8-4c3f7b53699a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7a35be-0e52-48b6-af93-546cfd339ac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76720b6b-ec40-454b-b6e8-4c3f7b53699a",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "f4a90343-2161-4698-8890-17f88d28c9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "17a7459f-0a3e-4689-9a50-0e1c19919552",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a90343-2161-4698-8890-17f88d28c9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2148a8a0-349f-4392-853d-25d278136c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a90343-2161-4698-8890-17f88d28c9d2",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "d0310fd0-c62e-4d26-828c-c9bb8e8af6ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "4eedf74d-f764-4c40-a9c8-3e62316bca37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0310fd0-c62e-4d26-828c-c9bb8e8af6ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c90b9a44-37a8-4a88-bb4c-deda9cc295d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0310fd0-c62e-4d26-828c-c9bb8e8af6ac",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "13993a09-516a-47ef-8c00-84a34d37bab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "5f059553-f9f7-43d3-837c-79a1726e117c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13993a09-516a-47ef-8c00-84a34d37bab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "537a2fe2-7639-41b3-90dc-d0e749fb924d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13993a09-516a-47ef-8c00-84a34d37bab5",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "de086a45-b346-4b81-8733-83fa13d17515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "f558cc6b-fdbf-4568-8783-3a4c5e8b8781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de086a45-b346-4b81-8733-83fa13d17515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04407e9f-438b-48d6-bd48-d3a725bce96e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de086a45-b346-4b81-8733-83fa13d17515",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "acd75b6b-5026-4b07-8208-b2ae9ff97a0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "38439ceb-2175-4d6b-9817-bf98664d7660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acd75b6b-5026-4b07-8208-b2ae9ff97a0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fc909a5-92fe-4953-98bc-3a338d5f9165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acd75b6b-5026-4b07-8208-b2ae9ff97a0b",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "f0990f57-40e3-46ae-bedf-5c9fc97c5032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "4e54138f-2186-4772-92bb-7be424e18546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0990f57-40e3-46ae-bedf-5c9fc97c5032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d8a0ca-9eba-4d48-be7d-eb706e48cef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0990f57-40e3-46ae-bedf-5c9fc97c5032",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "2c2d481c-431c-43e9-a33a-ff7a641dd99b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "450bc879-7850-465d-918f-7b4bab00aad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c2d481c-431c-43e9-a33a-ff7a641dd99b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81902aa6-6591-4fd1-9125-c97d764394c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c2d481c-431c-43e9-a33a-ff7a641dd99b",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "4f287066-62fb-4b89-a4c5-43915e5727cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "0fc446cd-2900-4527-a2f1-1816a83c5413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f287066-62fb-4b89-a4c5-43915e5727cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac176af-05c0-49e3-9961-f225521c5c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f287066-62fb-4b89-a4c5-43915e5727cf",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "eacb1fd2-a62d-4d3b-83b7-992346e4153e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "5f6113da-3170-4d40-858b-6b3a85468671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eacb1fd2-a62d-4d3b-83b7-992346e4153e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c72a3d5-7024-49ca-9374-ec26995a1f64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eacb1fd2-a62d-4d3b-83b7-992346e4153e",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "f6d77a68-3e61-4b68-be49-e1159d6898d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "fab1d675-b8db-42b4-8bb7-95328f79293e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d77a68-3e61-4b68-be49-e1159d6898d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1924655d-7e54-40cc-92ba-6bc9400c198e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d77a68-3e61-4b68-be49-e1159d6898d4",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "b0b5d07a-75bd-4053-bc4c-efaa35e68f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "58e5ea7c-89c0-430d-a119-c88c7d1c7e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b5d07a-75bd-4053-bc4c-efaa35e68f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac80ad9-ab18-4fbc-b2d2-10e9ee63f9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b5d07a-75bd-4053-bc4c-efaa35e68f9b",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "f7fe3e80-9297-44a3-91ab-f9948f45ec43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "0236a28d-2b8c-4a34-9960-c4d3a0db0659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7fe3e80-9297-44a3-91ab-f9948f45ec43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ecd8c42-9b3d-4f2c-9452-ea7a9587985c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7fe3e80-9297-44a3-91ab-f9948f45ec43",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "17471561-a6eb-4137-84ed-af6193b4195b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "e8801ff1-c7b0-4cae-be50-a56a2a7f951f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17471561-a6eb-4137-84ed-af6193b4195b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935e701d-c8ec-46ab-8eaf-f0d38ca2018a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17471561-a6eb-4137-84ed-af6193b4195b",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "24a02fcc-1e16-4649-84aa-f748c82ce6c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "67561ba9-5f8c-4303-b069-7f45fe001494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a02fcc-1e16-4649-84aa-f748c82ce6c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f92a37b-62b8-497e-89df-b4b18d8d90af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a02fcc-1e16-4649-84aa-f748c82ce6c8",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "676754d7-8608-4e47-a3b9-a9daf5b2bbfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "7e3db408-7f58-4a31-946c-0411ec330f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676754d7-8608-4e47-a3b9-a9daf5b2bbfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b402371c-7e57-48db-a92f-7947fbc19697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676754d7-8608-4e47-a3b9-a9daf5b2bbfe",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "f6f23aa3-54dc-4476-91fc-1d560ed7fcb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "7237d94e-ffd9-41e0-b3ab-bb815ff89f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6f23aa3-54dc-4476-91fc-1d560ed7fcb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0769ee3-2008-4d21-8588-9424e06fc063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6f23aa3-54dc-4476-91fc-1d560ed7fcb5",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "877c0bc3-cd79-4719-b83b-c5d65cb03668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "808559af-65a4-4ef0-be44-222fc0ecf90a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "877c0bc3-cd79-4719-b83b-c5d65cb03668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d344c12-4dac-4f18-a3a1-2bf10a719f3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "877c0bc3-cd79-4719-b83b-c5d65cb03668",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "d7be29ac-65b8-475f-90f6-4e605361051c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "6df414bf-cb0c-484a-9546-8f3277f0086d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7be29ac-65b8-475f-90f6-4e605361051c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a46ee7d-0dda-4125-ae24-21f22ae02ffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7be29ac-65b8-475f-90f6-4e605361051c",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "f3617451-6022-4bff-a38e-add09adbfd42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "5bd0a097-52eb-4aff-964d-cf18bde28b8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3617451-6022-4bff-a38e-add09adbfd42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7c0510a-4272-4df1-b22d-7c247983f4a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3617451-6022-4bff-a38e-add09adbfd42",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "321a502b-be94-4526-a1cd-543bae6a6373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "62c1947a-6491-4cb9-9026-6a3df204a494",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "321a502b-be94-4526-a1cd-543bae6a6373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac75b7d7-e9cc-4fd7-b05c-c18bacf6bf53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321a502b-be94-4526-a1cd-543bae6a6373",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "0ad11402-e801-4eac-ba6b-a20be545a183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "7efa6d66-17b5-4b2b-b4bb-40eec97f9a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad11402-e801-4eac-ba6b-a20be545a183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "228fe1cb-e82b-41f8-9db5-4315ec576a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad11402-e801-4eac-ba6b-a20be545a183",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "f02a5fbd-9d3e-41b6-99d7-b96bb46c0c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "2ecb72aa-c853-4030-926f-d7d470216c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f02a5fbd-9d3e-41b6-99d7-b96bb46c0c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eccab9a-2577-4194-8465-0536bbe64bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f02a5fbd-9d3e-41b6-99d7-b96bb46c0c09",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "25afd2a1-45a1-4276-9524-b2b3c2b4380e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "905fc25a-cf45-44df-80a6-b07ffa9cf2d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25afd2a1-45a1-4276-9524-b2b3c2b4380e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b004c7-6284-49aa-b066-a9cc264ec1d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25afd2a1-45a1-4276-9524-b2b3c2b4380e",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "6c7b62f0-a646-4622-a223-5ccc2ba50f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "ff53b9c2-0c1f-4130-a946-6f503a667954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7b62f0-a646-4622-a223-5ccc2ba50f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb8695c-c8f9-47f3-b9a9-7ad6e1b2e219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7b62f0-a646-4622-a223-5ccc2ba50f0d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "7cb24b97-92a6-422b-88ae-4aab02498356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "e0e9c524-c94e-4b51-8f75-685a9ee42d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb24b97-92a6-422b-88ae-4aab02498356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5151090f-19bf-4c47-bf76-27b2c24cbde0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb24b97-92a6-422b-88ae-4aab02498356",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "830b5940-40af-4bca-938c-b7b8c7282093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "41b00bef-4601-4318-8a54-9f125bad339a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "830b5940-40af-4bca-938c-b7b8c7282093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a282cf2-69ac-4944-88c2-2378bb492096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "830b5940-40af-4bca-938c-b7b8c7282093",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "98aecf69-dcb8-4b62-8b7c-1e933260732b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "a59b046d-e3e5-4098-9f98-0b0d96db8b55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98aecf69-dcb8-4b62-8b7c-1e933260732b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9746b3e-4726-4f73-8717-835abbbc8df9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98aecf69-dcb8-4b62-8b7c-1e933260732b",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "fe02df05-e6ad-4e8f-a150-f3e4cc5eac1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "c464ad31-6596-44e1-929f-4591830ee57e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe02df05-e6ad-4e8f-a150-f3e4cc5eac1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99139bd5-94ff-496b-be7f-63e3d2b3fc03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe02df05-e6ad-4e8f-a150-f3e4cc5eac1d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "76ce828e-fea2-433d-a1b6-5f759bf5b4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "c26fb794-45dd-4ae5-bbb0-7261f82a8f1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ce828e-fea2-433d-a1b6-5f759bf5b4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b82883d-fd64-4655-92b9-d86dd2887409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ce828e-fea2-433d-a1b6-5f759bf5b4e0",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "1619a58a-6e07-4b3c-9b18-42bbfdbd02ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "5006c33b-ad02-4309-9caa-aac32315da36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1619a58a-6e07-4b3c-9b18-42bbfdbd02ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84d8fc68-bb08-4cc0-bb68-dbd5d4a77b85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1619a58a-6e07-4b3c-9b18-42bbfdbd02ec",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "4766d13a-5c04-4d5a-a98d-33e10c7b46b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "4b08d60a-1c8b-4303-bfdd-1a1203ccf10a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4766d13a-5c04-4d5a-a98d-33e10c7b46b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ed96d1-6457-4ce5-8542-d8aa3045d47c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4766d13a-5c04-4d5a-a98d-33e10c7b46b3",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "2c236b62-e906-4f5c-8ce3-9eb9ef20b2dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "adf79730-c6fb-4130-80d7-e45453f006e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c236b62-e906-4f5c-8ce3-9eb9ef20b2dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96884c38-a37d-443b-a47a-4699d39f4685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c236b62-e906-4f5c-8ce3-9eb9ef20b2dd",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "7c0be043-3cfa-4175-bd2b-38f1b2b4a17d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "4a067e0f-6daa-45d6-bdae-42a683864e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0be043-3cfa-4175-bd2b-38f1b2b4a17d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "932044ab-6df7-405b-b79d-ba91ddfbd2c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0be043-3cfa-4175-bd2b-38f1b2b4a17d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "53caadb7-1937-49cc-962a-28bc1b2a04be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "cb2aeecb-b2b6-4157-bca0-ceb5d4208a93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53caadb7-1937-49cc-962a-28bc1b2a04be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ba4da1-9d92-43c3-8e14-ed31d8a408b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53caadb7-1937-49cc-962a-28bc1b2a04be",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "b5b733e5-a823-4c83-93c9-8101f4f62d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "3f5abdde-571f-4d3f-9f93-45ebe6c01c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5b733e5-a823-4c83-93c9-8101f4f62d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37f8d3e3-5e2c-44c3-8243-a96709d1654a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5b733e5-a823-4c83-93c9-8101f4f62d86",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "9ad6df52-8381-4f54-931b-c625b1ae3a17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "4f8e43e9-36ac-4d6e-a984-261125025401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad6df52-8381-4f54-931b-c625b1ae3a17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b3fadd7-6baa-4446-b1fb-8f3fc88dcebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad6df52-8381-4f54-931b-c625b1ae3a17",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "21efe10a-4c85-42b7-a45a-6f8d51ee88e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "16fd57cf-36f0-49ea-b7d7-90604e950624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21efe10a-4c85-42b7-a45a-6f8d51ee88e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00476d5-5fa7-45b5-be78-e833e5ef0ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21efe10a-4c85-42b7-a45a-6f8d51ee88e8",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "3637fc1b-c9a9-4f49-8f0c-62dc0283af38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "61fffbd2-ff84-49af-a8ff-64cc1a87cb72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3637fc1b-c9a9-4f49-8f0c-62dc0283af38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9150dfe-1715-47fe-8db2-4d106488878b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3637fc1b-c9a9-4f49-8f0c-62dc0283af38",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "6c87dabe-0b12-4920-8175-70df7a648042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "2c6dfb7f-3c83-4114-90d0-ad6bd6aa9d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c87dabe-0b12-4920-8175-70df7a648042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae620e57-2814-4b87-9704-30aa326fe506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c87dabe-0b12-4920-8175-70df7a648042",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "842139d5-f9bc-4251-ad22-f9d76d3e2918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "249aa186-f3db-456b-b578-ce8021a8983a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "842139d5-f9bc-4251-ad22-f9d76d3e2918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdefb253-7950-424f-956d-57d04b3a57b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "842139d5-f9bc-4251-ad22-f9d76d3e2918",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "9eed1f8f-fbab-46a7-956a-45e5690979ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "8d656280-bc93-4699-875e-3801d1e9e831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eed1f8f-fbab-46a7-956a-45e5690979ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5edcda21-baec-4e4c-93a8-495581a5f6f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eed1f8f-fbab-46a7-956a-45e5690979ba",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "74df956d-7946-4dc4-81e9-c2fe29b9396d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "0ab8c53e-c38d-40ce-ac43-fbd0297fb1c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74df956d-7946-4dc4-81e9-c2fe29b9396d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05ebcf1f-6d30-48a3-9d9c-4e129ca2adab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74df956d-7946-4dc4-81e9-c2fe29b9396d",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "a8552631-2306-4bcf-a78f-f1eef7c0b083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "d230b66b-80af-46bf-bcc3-44874d24bc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8552631-2306-4bcf-a78f-f1eef7c0b083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9de1e6ef-5ef4-4019-960a-03905c5577d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8552631-2306-4bcf-a78f-f1eef7c0b083",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "fa523f34-e447-46ed-a064-9a4f6abc44ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "53bdc200-1c7e-4e5a-a9de-003d61a3e6fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa523f34-e447-46ed-a064-9a4f6abc44ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a793c493-de10-4bfb-b745-f53659aa169c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa523f34-e447-46ed-a064-9a4f6abc44ce",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "3404a54a-eca0-46f3-abc7-2a9d0ec508d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "d03c130b-bf9c-43ed-b868-bac542fb96f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3404a54a-eca0-46f3-abc7-2a9d0ec508d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8da7cba-d816-485d-bee1-f9caa0e8578b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3404a54a-eca0-46f3-abc7-2a9d0ec508d7",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "8b91470b-1b90-437e-9a91-6547c1a2a30f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "8384ac14-59c4-4f4f-8dd1-346415d09bac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b91470b-1b90-437e-9a91-6547c1a2a30f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08cc93d8-9567-412b-97e6-1f9931b44a09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b91470b-1b90-437e-9a91-6547c1a2a30f",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "a1e0560a-83ac-42ef-95a6-f226df1b2675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "f53781d9-9c76-48d2-ad40-2668ccdb22ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e0560a-83ac-42ef-95a6-f226df1b2675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7880456-da43-4691-af2f-557236f6c8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e0560a-83ac-42ef-95a6-f226df1b2675",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "3dfda21a-cdd2-464f-a17b-da6606fb1c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "5351498a-a5f7-4dea-b188-7d849822ad73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dfda21a-cdd2-464f-a17b-da6606fb1c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "631816cb-b626-4713-9c98-53b341206a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dfda21a-cdd2-464f-a17b-da6606fb1c03",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "0cffcc0f-24d9-4568-9519-8d378f877c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "57d32347-acea-4ba3-8622-7f72b2c703e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cffcc0f-24d9-4568-9519-8d378f877c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7f0ffb-5bd9-423b-b118-644a828c3d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cffcc0f-24d9-4568-9519-8d378f877c53",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "4d1b6bc3-7007-45b6-864b-7ac66134e259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "f925dd03-d132-49b7-8e45-40f913e68370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d1b6bc3-7007-45b6-864b-7ac66134e259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "315bc9e5-3344-4850-b9e2-cace4106d3d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d1b6bc3-7007-45b6-864b-7ac66134e259",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "496d8b88-4e27-47c7-a3fe-272ec8c2dc28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "ffc6c068-5c01-49ba-9079-6723d233da55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "496d8b88-4e27-47c7-a3fe-272ec8c2dc28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e62ad884-3316-44f8-af16-107a3e018612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "496d8b88-4e27-47c7-a3fe-272ec8c2dc28",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        },
        {
            "id": "e8b4deb7-2263-44c2-87c7-078c051c9347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "compositeImage": {
                "id": "deffa741-432d-48f5-aa77-e7bbe8daaa79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b4deb7-2263-44c2-87c7-078c051c9347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7cc79b0-a7ef-4821-86ec-ec6a7784898f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b4deb7-2263-44c2-87c7-078c051c9347",
                    "LayerId": "7a870829-02ff-4c43-a231-5915b4375198"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7a870829-02ff-4c43-a231-5915b4375198",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}