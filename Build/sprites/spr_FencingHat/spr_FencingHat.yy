{
    "id": "26cf1759-2c43-4283-ba80-30f8b98f5926",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_FencingHat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09333bf6-e8e8-4674-9d98-7401ab083336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26cf1759-2c43-4283-ba80-30f8b98f5926",
            "compositeImage": {
                "id": "4bccc768-7b75-4936-80c5-3a57453d4001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09333bf6-e8e8-4674-9d98-7401ab083336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c8d54e4-8126-4e05-8ebc-5c6416321da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09333bf6-e8e8-4674-9d98-7401ab083336",
                    "LayerId": "09e9a44c-91e0-4e3e-80a9-29d8c5fba1a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "09e9a44c-91e0-4e3e-80a9-29d8c5fba1a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26cf1759-2c43-4283-ba80-30f8b98f5926",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 0
}