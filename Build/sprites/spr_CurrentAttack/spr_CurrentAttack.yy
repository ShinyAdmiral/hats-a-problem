{
    "id": "b855efea-1406-435c-bb11-6ed1544c259d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CurrentAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0adaa0d-5934-441d-a721-7ffeacc5b27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b855efea-1406-435c-bb11-6ed1544c259d",
            "compositeImage": {
                "id": "638af30b-c447-4169-b980-719864c99068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0adaa0d-5934-441d-a721-7ffeacc5b27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a4539e3-eda4-47cf-81be-a256ff7b8579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0adaa0d-5934-441d-a721-7ffeacc5b27f",
                    "LayerId": "ba96dbe4-505a-4eee-a85e-8eebfd3a2e07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "ba96dbe4-505a-4eee-a85e-8eebfd3a2e07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b855efea-1406-435c-bb11-6ed1544c259d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 27
}