{
    "id": "928deec2-4933-4697-adfd-fd55ada7ba62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CardSpades",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7f4fa30-e316-47aa-9798-d20c919852f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "928deec2-4933-4697-adfd-fd55ada7ba62",
            "compositeImage": {
                "id": "b08b35f3-a4fb-40c2-9601-31c3867889ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f4fa30-e316-47aa-9798-d20c919852f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82d9647-2757-4720-961f-8beabe1113af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f4fa30-e316-47aa-9798-d20c919852f9",
                    "LayerId": "69ab9401-1934-409a-8b4e-4031f00728f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "69ab9401-1934-409a-8b4e-4031f00728f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "928deec2-4933-4697-adfd-fd55ada7ba62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 6
}