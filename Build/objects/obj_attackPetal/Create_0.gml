/// @description start hell

bufferTime = 30

bulletSpeed = 2

randomize();

var a = instance_create_layer(obj_Enemy.x, obj_Enemy.y, "Bullets", obj_Petal)
with a
{
	randomize();
	scr_getRandomPath();
	path_start(path, obj_attackPetal.bulletSpeed, 0, true);
}

var b = instance_create_layer(obj_Enemy.x, obj_Enemy.y, "Bullets", obj_Petal)
with b
{
	randomize();
	scr_getRandomPath();
	path_start(path, obj_attackPetal.bulletSpeed, 0, true);
}

alarm[0] = bufferTime;

alarm[1] = room_speed * 8;