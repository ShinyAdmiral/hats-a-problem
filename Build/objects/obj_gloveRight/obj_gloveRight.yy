{
    "id": "f3f9a8c8-57c3-447f-9dc9-838cb38fca63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gloveRight",
    "eventList": [
        {
            "id": "ccc3c48a-341e-4f7d-a618-4dedcf86bae0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3f9a8c8-57c3-447f-9dc9-838cb38fca63"
        },
        {
            "id": "7cc14108-b663-4789-8d2f-b1a5d72ba638",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f3f9a8c8-57c3-447f-9dc9-838cb38fca63"
        },
        {
            "id": "d7b8b7af-1e66-451c-9a2c-fec3d1d32fa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f3f9a8c8-57c3-447f-9dc9-838cb38fca63"
        },
        {
            "id": "ecd6c3e3-b36c-4bef-a6d5-2d5762e38f83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f3f9a8c8-57c3-447f-9dc9-838cb38fca63"
        },
        {
            "id": "4941d4c5-123d-46fc-941f-2c192d862230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f3f9a8c8-57c3-447f-9dc9-838cb38fca63"
        },
        {
            "id": "24e8ce26-2b71-4ba4-bce6-a7cd39139ebf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "f3f9a8c8-57c3-447f-9dc9-838cb38fca63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7bf71c0c-4aea-4a23-802c-330e0b24a0af",
    "visible": true
}