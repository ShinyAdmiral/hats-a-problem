/// @description Set up and Despawn
//set up
if (!obj_handSpawner.finish)
{
	//is the hand fully visible?
	if (image_alpha >= alpha)
	{
		//run this section once to start attack
		if (start)
		{
			vspeed = glovespeed;
			sprite_index = spr_glove1;
			start = false;
			alarm[0] = 40;
			alarm[3] = 80;
		}
		else
		{
			//following player?
			if(follow_player)
			{
				//stop moving
				if (!follow)
				{
					vspeed = 0;
					follow = true;
				}
				if (instance_exists(obj_player))
				{
					y = lerp(y, obj_player.y, 0.05)
				}
			}
			else
			{
				//move again once going randomly
				if (follow)
				{
					vspeed = choose(-1,1);
					follow = false;
				}
				//change direction when the hand reaches limit
				if (y >= obj_sumoCircle.y + 21 && vspeed > 0)
				vspeed = -vspeed;
				if (y <= obj_sumoCircle.y - 23 && vspeed < 0)
				vspeed = -vspeed;
			}
		}
	}

	//When the hand is not fully visable...
	else
	{
		//does the hand exist next to the enemy?
		if (!instance_exists(obj_RightGlove))
		{
			//fade in more
			image_alpha += 0.035;
		}
	}
}

//despawn
else
{
	//run through this code once
	if (!done)
	{
		//change sprite
		sprite_index = spr_gloveRec;
		//this stops all other actions
		done = true;
		vspeed = 0;
	}
	
	//fade out and destroy self
	image_alpha -= 0.025;
	if (image_alpha <= 0)
	{
		//place hands next to enemy
		instance_create_layer(176, 64, "Hands", obj_RightGlove);
		//destroy self
		instance_destroy();
	}
}

//keep in range
y = clamp(y,obj_sumoCircle.y - 23 , obj_sumoCircle.y + 21)