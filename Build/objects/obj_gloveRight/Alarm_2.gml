/// @description Start Moving again
if (!done)
{
	//start moving again
	vspeed = glovespeed;
	//redy up an new shot
	alarm[0] = 40
	sprite_index = spr_glove1;
	
	//prevent glove from getting stuck
	if(!follow_player && vspeed = 0)
	{
		vspeed = choose(-1,1);
	}
}