/// @description set up
//alpha set up
alpha = image_alpha;
image_alpha = 0;

//sprite change
sprite_index = spr_ShootReady;

//boolean for one liners
start = true;
done = false;
follow = false;

//boolean for following player
follow_player = true;

//spawn at a random location
randomize();
ran = irandom_range(-19,19);
y = obj_sumoCircle.y + ran;

//variable for speed
glovespeed = 1;