/// @description shoot
if (!done)
{
	//create bullet
	instance_create_layer(x,y,"bullets", obj_bullet);
	//gamefeel
	sprite_index = spr_gloveShoot;
	ScreenShake(2,3);
	audio_play_sound(sfx_GunShot, 10, false);
	//start moving
	alarm[2] = 25;
}