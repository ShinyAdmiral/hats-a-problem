{
    "id": "ca59c569-b18c-4333-9d99-ef67bc0652ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_glovesLeft",
    "eventList": [
        {
            "id": "a02bb8e3-be70-4461-bdfb-d990ace431de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca59c569-b18c-4333-9d99-ef67bc0652ee"
        },
        {
            "id": "bcee5b6a-89df-4bbb-b81d-587db02091bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca59c569-b18c-4333-9d99-ef67bc0652ee"
        },
        {
            "id": "297953d8-0017-44c9-b866-694f6493a8da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ca59c569-b18c-4333-9d99-ef67bc0652ee"
        },
        {
            "id": "6ad2089b-c0d2-4302-af28-75024c714705",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ca59c569-b18c-4333-9d99-ef67bc0652ee"
        },
        {
            "id": "0477fc4b-8f01-4aec-8691-935bbe3b0a7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "ca59c569-b18c-4333-9d99-ef67bc0652ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7bf71c0c-4aea-4a23-802c-330e0b24a0af",
    "visible": true
}