/// @description pls read
//refer to other hand gun object to understand code. The only difference with this is the image scaling
//and lack of following player event

audio_play_sound(sfx_Teleport, 10, false);
alpha = image_alpha;
image_alpha = 0;
sprite_index = spr_ShootReady;
image_xscale = -1;
start = true;
done = false;
follow = false;

randomize();
ran = irandom_range(-19,19);
y = obj_sumoCircle.y + ran;

glovespeed = -1;