/// @description shoot
if (!done)
{
	instance_create_layer(x,y,"bullets", obj_bullet);
	sprite_index = spr_gloveShoot;
	ScreenShake(2,3);
	alarm[2] = 25;
	audio_play_sound(sfx_GunShot, 10, false);
}