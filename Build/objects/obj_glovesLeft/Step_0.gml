/// @description Insert description here
// You can write your code in this editor
if (!obj_handSpawner.finish)
{
	if (image_alpha >= alpha)
	{
		if (start)
		{
			vspeed = glovespeed;
			sprite_index = spr_glove1;
			image_xscale = -1;
			start = false;
			alarm[0] = 20;
		}
		else
		{
			if(obj_gloveRight.follow_player)
			{
				if (follow)
				{
					vspeed = choose(-1,1);
					follow = false;
				}
				if (y >= obj_sumoCircle.y + 21  && vspeed>0)
				vspeed = -vspeed;
				if (y <= obj_sumoCircle.y - 23 && vspeed<0)
				vspeed = -vspeed;
			}
			else
			{
				if (!follow)
				{
					vspeed = 0;
					follow = true;
				}
				if (instance_exists(obj_player))
				{
					y = lerp(y, obj_player.y, 0.05)
				}
			}
		}
	}

	else
	{
		if (!instance_exists(obj_RightGlove))
		{
			image_alpha = image_alpha + 0.035;
		}
	}
}

else
{
	if (!done)
	{
		sprite_index = spr_gloveRec;
		image_xscale = -1;
		done = true;
		vspeed = 0;
	}
	image_alpha = image_alpha - 0.025;
	if (image_alpha <= 0)
	{
		instance_create_layer(64, 64, "Hands", obj_LeftGlove);
		instance_destroy();
	}
}

y = clamp(y,obj_sumoCircle.y - 23 , obj_sumoCircle.y + 21)