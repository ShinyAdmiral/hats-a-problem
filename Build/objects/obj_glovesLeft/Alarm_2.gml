/// @description Start Moving again
if (!done)
{
	vspeed = glovespeed;
	alarm[0] = 40;
	sprite_index = spr_glove1;
	image_xscale = -1;
	
	if(follow && vspeed = 0)
	{
		vspeed = choose(-1,1);
	}
}