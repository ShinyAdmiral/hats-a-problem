/// @description start hell

bufferTime = 40

bulletSpeed = 2

randomize();

var a = instance_create_layer(obj_Enemy.x, obj_Enemy.y, "Bullets", obj_Bullet)
with a
{
	randomize();
	scr_getRandomPath();
	path_start(path, obj_attack1.bulletSpeed, 0, true);
}

var b = instance_create_layer(obj_Enemy.x, obj_Enemy.y, "Bullets", obj_Bullet)
with b
{
	randomize();
	scr_getRandomPath();
	path_start(path, obj_attack1.bulletSpeed, 0, true);
}

alarm[0] = bufferTime;