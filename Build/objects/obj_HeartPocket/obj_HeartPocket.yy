{
    "id": "e8014074-d76e-4f7c-b446-cf59f656e7ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HeartPocket",
    "eventList": [
        {
            "id": "730e7976-05c0-4222-a3e3-253cb32b3a04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8014074-d76e-4f7c-b446-cf59f656e7ab"
        },
        {
            "id": "bc4507ea-09ae-4c20-9d5e-bbb4c3c6f2ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1666d05c-e222-4d64-976a-c0d9465b1dd3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e8014074-d76e-4f7c-b446-cf59f656e7ab"
        },
        {
            "id": "b00305c9-62d2-456a-b753-ddd0a120d9d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8014074-d76e-4f7c-b446-cf59f656e7ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d8292820-a3df-4b96-9c27-7dc4b6bbffe0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e96f988-2921-43bc-9421-4d2f6c438197",
    "visible": true
}