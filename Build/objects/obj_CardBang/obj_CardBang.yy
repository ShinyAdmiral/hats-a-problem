{
    "id": "170adcce-417c-47fb-80cd-3f41ea31d583",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardBang",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8d8b6742-5f05-4542-a14a-dc8244fe4dba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b4f1cb7-40fe-40de-a3af-f5ec6fc2fe27",
    "visible": true
}