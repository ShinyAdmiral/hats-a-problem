{
    "id": "e8f2c0c4-05eb-405a-b0ea-bdcc4d448810",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ThrowingClub",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "28cffc8f-c118-4c90-9a4e-061c7bbb82b0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "928deec2-4933-4697-adfd-fd55ada7ba62",
    "visible": true
}