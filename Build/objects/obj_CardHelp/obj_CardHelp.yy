{
    "id": "f2448f11-8a61-450b-b773-f72b6c3ff415",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardHelp",
    "eventList": [
        {
            "id": "bcecccfc-0bfc-49a7-b426-991cefaa3eba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        },
        {
            "id": "60d483f5-4ada-476b-b0e0-b29b9f583f94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        },
        {
            "id": "cb3c9361-0678-49cc-98e5-4ac0db4bc0f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        },
        {
            "id": "19f7f9cc-7387-4865-851a-21328fda7acc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        },
        {
            "id": "ac76aa47-7a11-4fe7-8a98-6c6b63e7d91c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        },
        {
            "id": "a770507a-e6b6-4762-a8ad-4733c063e640",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        },
        {
            "id": "8a18b1ef-f764-42c2-b189-fbbb8c0644e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        },
        {
            "id": "25c0f789-ea6c-4369-bd28-57b0e798b38f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f2448f11-8a61-450b-b773-f72b6c3ff415"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b7d40029-733d-4e96-9551-10fcdde5d3fe",
    "visible": true
}