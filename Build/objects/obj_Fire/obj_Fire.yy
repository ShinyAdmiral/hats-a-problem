{
    "id": "d72f8fcc-ca3e-453d-a7aa-7b56be8b66be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Fire",
    "eventList": [
        {
            "id": "f66af72b-fe14-4125-81af-60643686a828",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d72f8fcc-ca3e-453d-a7aa-7b56be8b66be"
        },
        {
            "id": "13deb97e-dd1e-4e6c-af90-86a1071229f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d72f8fcc-ca3e-453d-a7aa-7b56be8b66be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6faa0b80-f54e-402c-9abe-66a6cfd45313",
    "visible": true
}