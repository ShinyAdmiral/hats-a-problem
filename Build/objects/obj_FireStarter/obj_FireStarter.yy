{
    "id": "ff51fa2e-5765-4de6-bc6f-d5a697f90f82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_FireStarter",
    "eventList": [
        {
            "id": "1266987f-1882-4113-832d-f73781d9c0a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff51fa2e-5765-4de6-bc6f-d5a697f90f82"
        },
        {
            "id": "5d4afed5-5c68-49b2-ad57-ce6ad40acf12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ff51fa2e-5765-4de6-bc6f-d5a697f90f82"
        },
        {
            "id": "518bb449-1b3b-4eb9-aea7-558516a3b593",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ff51fa2e-5765-4de6-bc6f-d5a697f90f82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd68b79e-35b7-473f-927d-ac50657d090a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}