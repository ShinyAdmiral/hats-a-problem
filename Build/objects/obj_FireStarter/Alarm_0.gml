/// @description spawn fire
for(i = 0; i < 5; i++;)
{
	//random range
	ranx = irandom_range(obj_sumoCircle.x - range_x, obj_sumoCircle.x + range_x);
	rany = irandom_range(obj_sumoCircle.y - range_y, obj_sumoCircle.y + range_y);
	//spawn
	instance_create_layer(ranx, rany, "Bullets", obj_FireWarning);
}

//sound que
audio_play_sound(sfx_FballThrow, 10, false);

//repeat
alarm[0] = 45;