{
    "id": "6bcc9aa1-8647-429b-89da-0fbe77c55b1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_SpadePocket",
    "eventList": [
        {
            "id": "1512fa7c-2703-49bb-8370-d0c185b95ff2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6bcc9aa1-8647-429b-89da-0fbe77c55b1a"
        },
        {
            "id": "bd5ce4d2-c60f-4482-bb3b-dc2797b06e00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6bcc9aa1-8647-429b-89da-0fbe77c55b1a"
        },
        {
            "id": "c78c9ef7-f5a6-47a0-975a-1fed93bb72f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6bcc9aa1-8647-429b-89da-0fbe77c55b1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6bcc9aa1-8647-429b-89da-0fbe77c55b1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d8292820-a3df-4b96-9c27-7dc4b6bbffe0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9f905b9e-20b0-4cc6-bd6b-48a7cc4ea866",
    "visible": true
}