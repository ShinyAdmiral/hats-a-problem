{
    "id": "44bc909d-91e3-4c6e-be5c-67c5615d9438",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Petal",
    "eventList": [
        {
            "id": "3add12e2-87af-494d-a17b-e1f29299a074",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44bc909d-91e3-4c6e-be5c-67c5615d9438"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3414fed8-c57e-424e-9d0d-77b14f12228c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4018a9f-b4b1-4d2f-94ea-f8f6468acd17",
    "visible": true
}