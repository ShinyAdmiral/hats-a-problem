{
    "id": "6bd4febb-debb-4841-a6ce-3b7bbc7ea610",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_knifeWarning",
    "eventList": [
        {
            "id": "955cf883-6e11-4d7e-ae14-83503fd9a472",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6bd4febb-debb-4841-a6ce-3b7bbc7ea610"
        },
        {
            "id": "c2af8bbc-b489-4b8b-b95e-485165c0699a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "6bd4febb-debb-4841-a6ce-3b7bbc7ea610"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24ded579-ca2a-4209-96b2-b2fad86fdd17",
    "visible": true
}