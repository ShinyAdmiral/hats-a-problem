{
    "id": "0a6f2bc2-0e50-460c-ab47-aaa50914cf57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueRose",
    "eventList": [
        {
            "id": "ffe691b0-995f-46ce-a673-50682cc78139",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0a6f2bc2-0e50-460c-ab47-aaa50914cf57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "277f7053-0c0f-4a12-8e13-ebe52ef53a5c",
    "visible": true
}