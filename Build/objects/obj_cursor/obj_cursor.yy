{
    "id": "4d1d6285-dce0-4071-aac5-da2c260127e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cursor",
    "eventList": [
        {
            "id": "8e5eafad-154b-468c-8679-5f8c19273786",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d1d6285-dce0-4071-aac5-da2c260127e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d9b6dbd9-2a01-4141-80f4-1dbb544a0d48",
    "visible": true
}