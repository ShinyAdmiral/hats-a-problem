{
    "id": "5e1924de-d01f-42d1-abae-dfcd98b6c22b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardFireBlast",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8d8b6742-5f05-4542-a14a-dc8244fe4dba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0114301-42a1-46fa-b083-b52d3d9e35d7",
    "visible": true
}