{
    "id": "e8bbc1e3-f92f-44ae-8a9b-aca45dfa38bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_LeftGlove",
    "eventList": [
        {
            "id": "579b733a-3a99-4a06-a6e1-92043350935d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8bbc1e3-f92f-44ae-8a9b-aca45dfa38bb"
        },
        {
            "id": "c6427545-ee15-4913-bb87-e777979eb9a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8bbc1e3-f92f-44ae-8a9b-aca45dfa38bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "81eefcb4-fee3-4e46-b156-6332c3b3cd47",
    "visible": true
}