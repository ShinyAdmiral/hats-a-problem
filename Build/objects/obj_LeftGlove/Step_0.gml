//follow enemy in place
x = obj_Enemy.x - 56;
y = obj_Enemy.y

#region fade out and destroy if move is being used
if (instance_exists(obj_handSpawner))
{
	image_alpha = image_alpha - .05;
	if (image_alpha <=0)
	{
		instance_destroy(self);
	}
}
#endregion

#region if move is not being used, Then fade into existance
else 
{
	if (!instance_exists(obj_handSpawner))
	{
		if (image_alpha < alpha)
		{
			image_alpha = image_alpha + .02;
		}
	}
}
#endregion