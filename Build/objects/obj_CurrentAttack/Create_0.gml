//variables for the start
image_xscale = 0;
image = true;
draw = true;
flash = false;
start = true;
alarm[1] = room_speed * (15/60);

//i is the index for arrays
i = 0;

//physical attacks
instance[0] = obj_hurt;
instance[1] = obj_hurt;
instance[2] = obj_hurt;

//instance of attack
attack[0] = false;
attack[1] = false;
attack[2] = false;

//used for start and end of all three attacks
breaks = false;
start_attack = false;