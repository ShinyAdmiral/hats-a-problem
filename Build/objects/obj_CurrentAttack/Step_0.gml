#region Expand to be visable for when the Attack Que is Begining

if (start)
{
	image_xscale += room_speed * (0.1/60);
	if (image_xscale >= 1)
	{
		image_xscale = 1;
		image_yscale = 1;	
		flash = true;
		audio_play_sound(sfx_RoundAlarm, 10, false);
		alarm[2] = room_speed * (50/60);
		start = false;
	}
}

#endregion

#region Move the indicator down
//check for the instance of attack
if (attack[i])
{
	//does the physical attack still exists
	if(!instance_exists(instance[i]))
	{
		//have three attacks gone through
		if (i<2)
		{
			//move indicator to macth card
			if (image)
			{
				vspeed = 1;
				//is card in correct location
				if (y >= 64 + (56*(i+1)))
				{
					//stop card and start attack
					vspeed = 0;
					y = 64 + (56*(i+1))
					image = false;
				}
			}
			
			//show flashing indicatin and spawwn next attack
			if (!image)
			{
				flash = true;
				audio_play_sound(sfx_RoundAlarm, 10, false);
				attack[i] = false;
				//this spawns the next attack
				alarm[2] = room_speed * (50/60);
				i++;
			}
		}
		//if the three attacks are done, destroy all cards and set up new hand
		else
		{
			vspeed = -2;
			breaks = true;
		}
	}
}
#endregion