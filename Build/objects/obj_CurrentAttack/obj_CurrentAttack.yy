{
    "id": "a006c9f3-c6e6-4e93-a58c-885697b7874d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CurrentAttack",
    "eventList": [
        {
            "id": "32002768-7db0-4f59-a0b8-a4d0e8c52a79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        },
        {
            "id": "957812c6-c0d0-4cb2-9004-e9e21361b80c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        },
        {
            "id": "ef7483dc-72aa-4883-b2d1-26d91baa50a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        },
        {
            "id": "a25d4905-f001-49c7-9a06-847c299c1f1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        },
        {
            "id": "f667ac54-5c9d-41db-b771-81640b648ef1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        },
        {
            "id": "4dcc79bc-1656-4c13-9192-b141610c6b70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        },
        {
            "id": "82dc9333-1027-4fd1-8a97-d1cd1fd0223c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        },
        {
            "id": "39e9f1f9-f090-45dc-abbf-cac3ada13b02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "a006c9f3-c6e6-4e93-a58c-885697b7874d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b855efea-1406-435c-bb11-6ed1544c259d",
    "visible": true
}