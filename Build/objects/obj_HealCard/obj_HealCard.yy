{
    "id": "f289f89c-6562-4cd3-a69d-4c426bb6e8bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HealCard",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b6a76188-3c4d-4477-9f99-b79449bf294b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0d1aa4d-90d0-416a-bbb7-3fb527e3099a",
    "visible": true
}