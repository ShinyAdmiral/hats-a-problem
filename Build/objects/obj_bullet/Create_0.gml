/// @description Shoot

bulletSpeed = 1.5

if (x > obj_sumoCircle.x)
{
	hspeed = -bulletSpeed;
	image_xscale = -1;
}

if (x < obj_sumoCircle.x)
{
	hspeed = bulletSpeed;
}
