{
    "id": "eb021333-023b-4479-a210-79ab9191d3da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueFire",
    "eventList": [
        {
            "id": "fa3d5005-94c1-41ab-b8ad-f99af9c6f4d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "eb021333-023b-4479-a210-79ab9191d3da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9294cc2f-5a27-4446-b468-20eed2e7bda9",
    "visible": true
}