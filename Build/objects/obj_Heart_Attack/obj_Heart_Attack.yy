{
    "id": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Heart_Attack",
    "eventList": [
        {
            "id": "5eae2963-1480-4906-b3b2-b1f83268a923",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "9bb3083c-44b3-4095-8855-140798a3fb66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "bc606c15-a27b-4c01-9a02-91b439dc49f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "32719d58-f87f-4fd8-bcad-d98d030330d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "5a985ae5-aebd-454e-9178-4e69f8995b66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "b5954788-dc80-43f4-a549-20b23ca7cb9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "66c1a809-c0c3-4587-9ae7-8950ccaf79c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "80eecfed-58e9-4a2f-9648-4355df6790e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "3dc78bfe-6fb3-449d-9bc4-e907d9db6847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "7e02f910-78d2-4975-b0fc-22e7bf544bcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "2371bfc5-9085-41b0-a39e-54e7a883a145",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 9,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        },
        {
            "id": "caa17ca9-73b0-4dc8-be3e-8eeb6e0a70fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "15b7bc38-b86a-4a36-87d1-fdd5e5d7e249"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd68b79e-35b7-473f-927d-ac50657d090a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}