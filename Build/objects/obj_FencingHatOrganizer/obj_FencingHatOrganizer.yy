{
    "id": "c5f6cdd0-2a09-4880-be42-1f8e298eddec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_FencingHatOrganizer",
    "eventList": [
        {
            "id": "b19cd9cb-8afd-44cb-bb70-a2219580671e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "7256f2d3-c28d-4244-ab62-f162f24df88b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "d68cf1b8-9625-4261-b1be-dbe667f1182f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "9bf105b7-645b-4282-9730-7974d08c541d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "e3118dd5-fb72-4dbd-9f03-943cb80af5df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "b534d922-aa48-45ab-b076-8628a30fb898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "74626e0d-1437-4144-838b-af3d7132e897",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "94153a27-6c57-4f6d-a292-f46f7267a1fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        },
        {
            "id": "ae98c1e5-6c91-4737-96b4-20b9656d72bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c5f6cdd0-2a09-4880-be42-1f8e298eddec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}