if (!set_up)
{
	obj_player.x = lerp(obj_player.x, obj_sumoCircle.x, 0.05);
	obj_player.y = lerp(obj_player.y, obj_sumoCircle.y, 0.05);
	if ((obj_player.x >= obj_sumoCircle.x - offset) && (obj_player.x <= obj_sumoCircle.x + offset))
	{
		if (obj_player.y >= obj_sumoCircle.y - offset) && (obj_player.y <= obj_sumoCircle.y + offset)
		{
			obj_player.x = obj_sumoCircle.x;
			obj_player.y = obj_sumoCircle.y;
			set_up = true;
		}
	}
}

if (set_up && visable)
{
	obj_player.image_alpha -= .05;
	obj_shadow.image_alpha -= .05;
	if (obj_player.image_alpha <= 0)
	{
		visable = false;
		obj_player.image_alpha = 0;
		obj_shadow.image_alpha = 0;
	}
}

if (!visable && !start)
{
	instance_create_layer(obj_sumoCircle.x, room_height/2 +20,"VFX",obj_HatIn);
	event_user(0);
	start = true;
}