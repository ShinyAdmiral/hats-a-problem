if (!set_up)
{
	obj_player.x = lerp(obj_player.x, obj_sumoCircle.x, 0.05);
	obj_player.y = lerp(obj_player.y, obj_sumoCircle.y, 0.05);
	if ((obj_player.x >= obj_sumoCircle.x - offset) && (obj_player.x <= obj_sumoCircle.x + offset))
	{
		if (obj_player.y >= obj_sumoCircle.y - offset) && (obj_player.y <= obj_sumoCircle.y + offset)
		{
			set_up = true;
			
			var a = instance_create_depth(-30,0,0,obj_HeartPocket);
			
			distx[0] = obj_sumoCircle.x - (obj_HeartPocket.sprite_width * 2);
			distx[1] = obj_sumoCircle.x + (obj_HeartPocket.sprite_width * 2);
			distx[2] = obj_sumoCircle.x - (obj_HeartPocket.sprite_width * 4);
			distx[3] = obj_sumoCircle.x + (obj_HeartPocket.sprite_width * 4);
			
			disty = room_height - (obj_sumoCircle.sprite_height + obj_HeartPocket.sprite_height);
			disty /= 8;
			
			instance_destroy(a);
			
			randomize();
			var i = choose(0,1,2,3);
			
			randomize();
			var b = choose(1,3);
			
			instance_create_layer(distx[i], disty * (b+3), "Fire", obj_HeartPocket);
			i++
			if (i>3)
			i = 0;
			instance_create_layer(distx[i], disty * (b+3), "Fire", obj_DiamondPocket);
			i++
			if (i>3)
			i = 0;
			if (b>2)
			b = 1;
			else
			b = 3
			instance_create_layer(distx[i], disty * (b+3), "Fire", obj_ClubPocket);
			i++
			if (i>3)
			i = 0;
			instance_create_layer(distx[i], disty * (b+3), "Fire", obj_SpadePocket);
			
			instance_create_layer(x, y, "Fire", obj_cursor);
			alarm[0] = room_speed * 10;
		}
	}
}

if ((set_up) && !finish)
{
	if (!cooldown)
	{
		randomize();
		i = choose(0,1,2,3);
		
		if (i = 0)
		instance_create_layer(obj_player.x, obj_player.y, "Fire", obj_ThrowingClub)
		else if (i = 1)
		instance_create_layer(obj_player.x, obj_player.y, "Fire", obj_ThrowingSpade)
		else if (i = 2)
		instance_create_layer(obj_player.x, obj_player.y, "Fire", obj_ThrowingDiamond)
		else if (i = 3)
		instance_create_layer(obj_player.x, obj_player.y, "Fire", obj_ThrowingHeart)
		
		cooldown = true
	}
}

if (finish)
{
	if (!instance_exists(obj_HeartPocket))
		if (!instance_exists(obj_DiamondPocket))
			if (!instance_exists(obj_ClubPocket))
				if (!instance_exists(obj_SpadePocket))
				{
					instance_destroy(obj_cursor);
					obj_player.start = true;
					instance_destroy(self);
				}
}