{
    "id": "83f7d749-2f22-492d-9410-5bbfb2a67aed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_RazorCards",
    "eventList": [
        {
            "id": "aaaf38e9-194a-419a-a190-ef53c55be356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83f7d749-2f22-492d-9410-5bbfb2a67aed"
        },
        {
            "id": "7f95fcb7-24a4-42b0-bb1a-4cc0841d8cab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "83f7d749-2f22-492d-9410-5bbfb2a67aed"
        },
        {
            "id": "1beadf98-ddf2-4c24-b05a-90697b21009e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "83f7d749-2f22-492d-9410-5bbfb2a67aed"
        },
        {
            "id": "115a9580-d467-43bd-883d-a7317a6f777f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "83f7d749-2f22-492d-9410-5bbfb2a67aed"
        },
        {
            "id": "7a4310f8-b766-4f36-979c-6b2934b12834",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "83f7d749-2f22-492d-9410-5bbfb2a67aed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}