{
    "id": "7b5863b2-e444-43a1-b074-063a8bc7446b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HatIn",
    "eventList": [
        {
            "id": "7b9c53ba-291c-452b-9bf1-b13d20ad3e55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7b5863b2-e444-43a1-b074-063a8bc7446b"
        },
        {
            "id": "059b57f9-8fa2-451c-bff5-0efd18123066",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b5863b2-e444-43a1-b074-063a8bc7446b"
        },
        {
            "id": "d512ae71-b9e5-4026-aa76-355cd6a4d66f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "35d1dafd-f32e-4581-a620-c64aa919aadf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b5863b2-e444-43a1-b074-063a8bc7446b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7f13ff0-457e-46a4-a60c-6c6f7d391901",
    "visible": true
}