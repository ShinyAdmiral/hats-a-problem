{
    "id": "030a8f2e-2f4c-4e9b-883e-2b5a10aaa7cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCamera",
    "eventList": [
        {
            "id": "6682889d-7c34-4b8a-866b-cdb05a281663",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "030a8f2e-2f4c-4e9b-883e-2b5a10aaa7cb"
        },
        {
            "id": "7ee53005-126b-47de-858c-417da73719d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "030a8f2e-2f4c-4e9b-883e-2b5a10aaa7cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "0ecfb2a8-444d-4ed6-b209-818019f4d4ac",
    "visible": false
}