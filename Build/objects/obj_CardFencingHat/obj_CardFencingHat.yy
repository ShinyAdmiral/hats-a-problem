{
    "id": "f565b343-06ab-423f-a512-2964466f269c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardFencingHat",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8d8b6742-5f05-4542-a14a-dc8244fe4dba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90e99d88-dfd8-4d87-99d6-77402963b9c9",
    "visible": true
}