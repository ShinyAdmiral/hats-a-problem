{
    "id": "13543413-2c40-4dd8-a93c-fa0988dbd6ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_BannanaRang",
    "eventList": [
        {
            "id": "f9e82180-d779-4836-8acb-97e0e4b20884",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "13543413-2c40-4dd8-a93c-fa0988dbd6ab"
        },
        {
            "id": "678836d8-6913-4a95-a9be-ebf7e0a3ac1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "13543413-2c40-4dd8-a93c-fa0988dbd6ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ce1aebd6-db17-4628-9d73-3940b40897f1",
    "visible": true
}