{
    "id": "f22abb58-4af3-4918-a93b-52dcace64326",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ThrowingSpade",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "28cffc8f-c118-4c90-9a4e-061c7bbb82b0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57d375b0-9b27-452f-9fd1-8d851b1fd023",
    "visible": true
}