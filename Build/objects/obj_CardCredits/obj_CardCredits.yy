{
    "id": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardCredits",
    "eventList": [
        {
            "id": "69c7893f-5312-4ce0-a9ae-5f1c8ee01e55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        },
        {
            "id": "0d191dbe-5774-46c5-a85a-12bc3f88104d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        },
        {
            "id": "56a9d87e-b2f0-4470-9414-265b091115b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        },
        {
            "id": "4233c83b-b3e5-4204-9b3a-313bd5d6c2e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        },
        {
            "id": "d1f6103d-9608-4bab-8d12-19317080f31a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        },
        {
            "id": "ccdddda9-0d53-43a8-b241-7585e43fde59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        },
        {
            "id": "add92c30-bf41-4a94-8779-f15a0c6590bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        },
        {
            "id": "23909e06-148e-453c-9090-773834a31aa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4787788b-18a5-4ce3-be37-ad1b77c54d95",
    "visible": true
}