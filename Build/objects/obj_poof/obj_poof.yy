{
    "id": "e745ba24-b246-4435-a9f4-c2f4245194e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_poof",
    "eventList": [
        {
            "id": "c9702c13-aaa8-4923-a99e-f935d1bc9994",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e745ba24-b246-4435-a9f4-c2f4245194e0"
        },
        {
            "id": "46dde05f-c5d5-4e3f-8bc9-4ff938f7954d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e745ba24-b246-4435-a9f4-c2f4245194e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f93f5318-ec6e-4846-bc1e-7298b5724d9d",
    "visible": true
}