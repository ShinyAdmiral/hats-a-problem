{
    "id": "bac23a22-86c6-49e9-a9f5-0b697dba68d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_AttackOrganizer",
    "eventList": [
        {
            "id": "e395894c-9506-42bb-8e4f-4946a984dccb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bac23a22-86c6-49e9-a9f5-0b697dba68d2"
        },
        {
            "id": "61d65f2d-904e-4a03-a735-a8f6638fc0b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bac23a22-86c6-49e9-a9f5-0b697dba68d2"
        },
        {
            "id": "debc8256-8f41-4947-9380-a74181eeb8d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bac23a22-86c6-49e9-a9f5-0b697dba68d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}