#region scan card and throw in que

//Was card deleted?
if (instance[i] != "obj_card")
{
	//cut name of object and add the que card prefix to it.
	length_string = string_length(instance[i]) - 8;
	instance[i] = string_copy(instance[i], 9, length_string);
	attack_string = "obj_cardQue" + instance[i];
	
	//get object by searching name and spawn it
	attack_object = asset_get_index(attack_string);
	attack[i] = instance_create_layer(248, 240, "Cards", attack_object);
	
	//send it up the UI
	attack[i].vspeed = -5;
	i++;
}

#endregion

#region place card in correct locatin

if (attack[0] != "obj_cardque")
{
	with attack[0] if (y <= 64)
		{
			vspeed = 0;
			y = 64;
		}
}

if (attack[1] != "obj_cardque")
{
	with attack[1] if (y <= 120)
		{
			vspeed = 0;
			y = 120;
		}
}

if (attack[2] != "obj_cardque")
{
	with attack[2] if (y <= 176)
		{
			vspeed = 0;
			y = 176;
		}
}

#endregion

#region Start Attack Line
if (attack[2] != "obj_cardque")
{
	if ((attack[0].speed = 0) && (attack[1].speed = 0) && (attack[2].speed = 0))
	{
		instance_create_layer(248, 64, "Instances", obj_CurrentAttack);
		instance_destroy(obj_cursor);
		instance_destroy(self);
	}
}

#endregion