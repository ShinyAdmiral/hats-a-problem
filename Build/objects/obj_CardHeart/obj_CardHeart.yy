{
    "id": "0281a5b4-4b02-470b-976b-c9f4254245f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardHeart",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8d8b6742-5f05-4542-a14a-dc8244fe4dba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec712919-200f-483f-b6ae-6480465fcc1e",
    "visible": true
}