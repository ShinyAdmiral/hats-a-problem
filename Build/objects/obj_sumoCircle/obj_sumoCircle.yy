{
    "id": "9dbce774-538e-4da6-8660-1f695177b60d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sumoCircle",
    "eventList": [
        {
            "id": "ee6ae6aa-2695-45bb-a872-b00f8a7f8211",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9dbce774-538e-4da6-8660-1f695177b60d"
        },
        {
            "id": "8952e6bc-50a1-4d1a-9916-2cc6c42278f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9dbce774-538e-4da6-8660-1f695177b60d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7cbc8f7c-8104-48f2-9c9b-36a01c59287a",
    "visible": true
}