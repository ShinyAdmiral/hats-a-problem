{
    "id": "0c04028f-2d1a-4f41-a417-ec31e358b4d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueFireBlast",
    "eventList": [
        {
            "id": "c5d9c843-70b5-4a39-97b0-35bc3d261d89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0c04028f-2d1a-4f41-a417-ec31e358b4d5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0114301-42a1-46fa-b083-b52d3d9e35d7",
    "visible": true
}