{
    "id": "2532bf57-8e7d-4a1f-b374-e65247802b1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueBang",
    "eventList": [
        {
            "id": "308aa583-cec5-4eb1-af0c-7419902d478d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2532bf57-8e7d-4a1f-b374-e65247802b1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8360abae-6b2d-4971-950d-3b0c481fa2d5",
    "visible": true
}