{
    "id": "842a979b-c3c1-4c4c-96cc-a0f4afd34cd8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadow",
    "eventList": [
        {
            "id": "386bf86c-e742-4d9f-a601-9106ba24568c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "842a979b-c3c1-4c4c-96cc-a0f4afd34cd8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9cff30ed-12e6-40a3-a920-f10ff7fb3445",
    "visible": true
}