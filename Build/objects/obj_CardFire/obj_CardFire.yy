{
    "id": "4a157da0-e3fc-479f-bc08-a9602c027d8a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardFire",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8d8b6742-5f05-4542-a14a-dc8244fe4dba",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "847e7a94-c323-4e00-bd95-16f4f5500694",
    "visible": true
}