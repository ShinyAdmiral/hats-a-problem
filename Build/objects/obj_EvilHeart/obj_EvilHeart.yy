{
    "id": "3ba51aba-5e0f-4b27-9561-f3f77fc6b0ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_EvilHeart",
    "eventList": [
        {
            "id": "ed0fb1fb-6c93-4dcd-8756-794b5f36c30c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3ba51aba-5e0f-4b27-9561-f3f77fc6b0ef"
        },
        {
            "id": "8b554648-dd08-4c41-bef9-f40a61137e5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3ba51aba-5e0f-4b27-9561-f3f77fc6b0ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3414fed8-c57e-424e-9d0d-77b14f12228c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75fe7679-d8cf-4229-8f9f-e0c389a8204e",
    "visible": true
}