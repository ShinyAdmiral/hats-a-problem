{
    "id": "f647250c-7af6-4aa1-9d09-a5791b2bba26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Click",
    "eventList": [
        {
            "id": "a366363a-afdc-44dc-9f69-6445eb163848",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        },
        {
            "id": "26c7c8d3-4edb-4e5d-8596-6e5711d72f7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        },
        {
            "id": "5735ab88-01ed-4c44-b7e5-26df08d17534",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        },
        {
            "id": "446c3d56-188c-4feb-91c9-fa3d869d7000",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        },
        {
            "id": "e70a004b-3291-4e8f-8fec-8bc39a6ab86c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        },
        {
            "id": "4105c0a3-1220-4cd4-8f4c-923f48719ee3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        },
        {
            "id": "2341ca73-9734-48b8-9797-f74c3172ab6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        },
        {
            "id": "c61d0a5c-a6fe-4330-bc63-a1eae3f73c04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f647250c-7af6-4aa1-9d09-a5791b2bba26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3191e4f8-0ec3-449e-bc1a-2a76a1d4cef6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "857d7c4e-2e3a-485c-944e-9c427755bcda",
    "visible": true
}