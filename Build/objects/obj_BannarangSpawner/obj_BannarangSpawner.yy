{
    "id": "c0421d2a-88bf-44b6-8517-3cbb907666a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_BannarangSpawner",
    "eventList": [
        {
            "id": "fc1a0891-6ef1-4051-a09b-db9dcf3124bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0421d2a-88bf-44b6-8517-3cbb907666a9"
        },
        {
            "id": "cfd85dbf-789a-47cc-84be-e63398c496a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c0421d2a-88bf-44b6-8517-3cbb907666a9"
        },
        {
            "id": "88959ac7-cda9-479b-9e53-f156e49910b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0421d2a-88bf-44b6-8517-3cbb907666a9"
        },
        {
            "id": "109d60f2-4721-4416-97f6-56a15b17bd88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c0421d2a-88bf-44b6-8517-3cbb907666a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd68b79e-35b7-473f-927d-ac50657d090a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}