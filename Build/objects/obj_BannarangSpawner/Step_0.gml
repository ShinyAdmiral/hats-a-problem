if (back)
{
	if (throw)
	{
		if (!instance_exists(a))
		{
			throw = false;
			alarm[1] = 30;
			back = false;
		}
	}
}

if (!back)
{
	if (throw)
	{
		if (!instance_exists(b))
		{
			throw = false;
			alarm[0] = 30;
			back = true;
		}
	}
}

if(count >= 4)
{
	instance_destroy();
}