{
    "id": "6e90e1fe-8a9f-4147-8b25-850586575a38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueKnife",
    "eventList": [
        {
            "id": "07b42786-c7bb-4474-b812-27e9c40ff675",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6e90e1fe-8a9f-4147-8b25-850586575a38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c994ad4-b9c1-44e3-85ea-762a8c59bbbb",
    "visible": true
}