{
    "id": "5cf22777-ec28-41bc-b12a-42ffe4bc273f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueHeart",
    "eventList": [
        {
            "id": "0181a9c2-0627-4d8c-844d-bb2a9a168f69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5cf22777-ec28-41bc-b12a-42ffe4bc273f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4cfc4a8f-f542-4f72-993f-52f116b250ad",
    "visible": true
}