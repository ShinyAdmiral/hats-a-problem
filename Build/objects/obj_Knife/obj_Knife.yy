{
    "id": "1e2a243e-1217-4da8-80d2-db367c844071",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Knife",
    "eventList": [
        {
            "id": "47f08d8d-e851-48b8-9aec-e764db182253",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e2a243e-1217-4da8-80d2-db367c844071"
        },
        {
            "id": "ec09ee7d-adba-4adb-8be4-ff535eb9b641",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1e2a243e-1217-4da8-80d2-db367c844071"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3414fed8-c57e-424e-9d0d-77b14f12228c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8d21fbbf-2955-4f0b-ba04-186d33ddf890",
    "visible": true
}