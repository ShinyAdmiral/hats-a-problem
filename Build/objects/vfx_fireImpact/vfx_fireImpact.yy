{
    "id": "29f9f7d1-fa3f-4efc-af72-18cbaa82aaf0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vfx_fireImpact",
    "eventList": [
        {
            "id": "843f2e5c-8e1b-4409-90b9-906d4a7d0db3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "29f9f7d1-fa3f-4efc-af72-18cbaa82aaf0"
        },
        {
            "id": "2836aafc-3af0-4b70-8acf-2b1244f6cb67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29f9f7d1-fa3f-4efc-af72-18cbaa82aaf0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d64abd9-cb9d-4acf-b2c2-7caef8c1b10e",
    "visible": true
}