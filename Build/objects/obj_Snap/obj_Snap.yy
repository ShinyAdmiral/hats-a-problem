{
    "id": "9ef09a57-2f9d-4132-baa2-588f976b3c9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Snap",
    "eventList": [
        {
            "id": "afd096e6-c7a0-44e5-8b51-a092d865b6e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9ef09a57-2f9d-4132-baa2-588f976b3c9f"
        },
        {
            "id": "ba365da9-aeb1-426a-bf94-b8e06a7e5af1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9ef09a57-2f9d-4132-baa2-588f976b3c9f"
        },
        {
            "id": "3606db9e-a362-46d7-95b7-867db947baf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ef09a57-2f9d-4132-baa2-588f976b3c9f"
        },
        {
            "id": "ac5c4764-a3a7-4148-85b7-a8d70aa188ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9ef09a57-2f9d-4132-baa2-588f976b3c9f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5f83bd7d-aa79-4f2f-98a6-f522f1aba5e9",
    "visible": true
}