//follow enemy in place
x = obj_Enemy.x - 56;
y = obj_Enemy.y

//when the enemy snaps
if (half)
{
	//check for snap frame
	if image_index >= 40
	{
		//freeze frame
		sprite_index = spr_Snap1;
		
		//alarm to set back hand
		alarm[0] = 30;
		
		//spawn cards
		audio_play_sound(sfx_snap, 10, false);
		scr_SpawnCards()
		instance_create_layer(x,y,"Fire", obj_cursor);
		half = false;
	}
}