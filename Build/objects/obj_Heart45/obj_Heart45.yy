{
    "id": "5119b736-69f4-4e7b-b018-13e841c09405",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Heart45",
    "eventList": [
        {
            "id": "80017b67-3694-4097-a7a3-6f9127bebbb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5119b736-69f4-4e7b-b018-13e841c09405"
        },
        {
            "id": "3f2ad4d4-ac54-4f7b-970f-cbf3fce2fdc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5119b736-69f4-4e7b-b018-13e841c09405"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b6a76188-3c4d-4477-9f99-b79449bf294b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1f23a773-310e-4a8c-8560-4df0b9d0aad0",
    "visible": true
}