if (!done)
{
	if (x > obj_sumoCircle.x)
	{
		if (image_xscale < 1)
		{
			image_xscale = image_xscale + 0.1;
			image_yscale = image_yscale + 0.05;
		}
		else
		{
			image_xscale = 1;
			image_yscale = 1;
			done = true;
		}
	}

	else
	{
		if (image_xscale > -1)
		{
			image_xscale = image_xscale - 0.1;
			image_yscale = image_yscale + 0.1;
		}
		else
		{
			image_xscale = -1;
			image_yscale = 1;
			done = true;
		}
	}
}