{
    "id": "4c4da93b-8c8f-47ee-b2ef-74c10ea66a79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_SharpCard",
    "eventList": [
        {
            "id": "298b250c-9adb-4271-9def-0fb8be62c1fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c4da93b-8c8f-47ee-b2ef-74c10ea66a79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3414fed8-c57e-424e-9d0d-77b14f12228c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57d375b0-9b27-452f-9fd1-8d851b1fd023",
    "visible": true
}