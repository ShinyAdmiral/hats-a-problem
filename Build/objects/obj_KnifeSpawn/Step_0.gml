//ready up when attack starts
if (instance_exists(obj_knifeWarning))
{
	ready = true;
}

//spawn attack again once the knives are gone
if (ready)
{
	if (!instance_exists(obj_knifeWarning))
	{
		ready = false;
		alarm[0] = (room_speed/2);
	}
}

//struck 3 times?
if (count >= 3)
{
	if (!instance_exists(obj_Knife))
	{
		instance_destroy(self);
	}
}