{
    "id": "75b81443-2e08-4de0-85cd-4f41208cbad9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_KnifeSpawn",
    "eventList": [
        {
            "id": "700ce6cc-72a2-4511-a94e-e9c060377535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75b81443-2e08-4de0-85cd-4f41208cbad9"
        },
        {
            "id": "de6e3be6-b50c-4c39-9c2b-d88a8d60eabc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "75b81443-2e08-4de0-85cd-4f41208cbad9"
        },
        {
            "id": "887cfd70-a157-48c7-9074-1a64f2a8044e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75b81443-2e08-4de0-85cd-4f41208cbad9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd68b79e-35b7-473f-927d-ac50657d090a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}