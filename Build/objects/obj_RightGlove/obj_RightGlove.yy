{
    "id": "1ad6aada-b3e2-4997-b6b8-e2f32b5d6d88",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_RightGlove",
    "eventList": [
        {
            "id": "96163a70-9790-4ada-bc66-71139fe3fd51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1ad6aada-b3e2-4997-b6b8-e2f32b5d6d88"
        },
        {
            "id": "3a5ec35a-4ee4-48b0-b747-6a084508bcfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ad6aada-b3e2-4997-b6b8-e2f32b5d6d88"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3921d363-2017-4120-8903-b0827b9d4cb0",
    "visible": true
}