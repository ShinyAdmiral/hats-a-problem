{
    "id": "e657ebbf-ef22-4f4b-af32-a3394f46281b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HatOut",
    "eventList": [
        {
            "id": "895b242f-336a-4c8c-bc28-067e0f61131f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e657ebbf-ef22-4f4b-af32-a3394f46281b"
        },
        {
            "id": "06647740-2f54-4556-bd5c-7487e644127f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e657ebbf-ef22-4f4b-af32-a3394f46281b"
        },
        {
            "id": "c1159e6b-a3ff-43e0-9edb-8f43c36b54db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e657ebbf-ef22-4f4b-af32-a3394f46281b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26cf1759-2c43-4283-ba80-30f8b98f5926",
    "visible": true
}