{
    "id": "1666d05c-e222-4d64-976a-c0d9465b1dd3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ThrowingHeart",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "28cffc8f-c118-4c90-9a4e-061c7bbb82b0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cac94bc8-91e2-475b-918f-4b0501843b7d",
    "visible": true
}