{
    "id": "ccc4e22f-8b6b-4b9e-9c54-4c07a3ba87b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_credits",
    "eventList": [
        {
            "id": "2bb13a8a-96f3-48e7-9a23-1d88a1833bfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ccc4e22f-8b6b-4b9e-9c54-4c07a3ba87b3"
        },
        {
            "id": "6c820c4e-e584-43db-bf08-ce26c22a09aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "ccc4e22f-8b6b-4b9e-9c54-4c07a3ba87b3"
        },
        {
            "id": "548b87ab-bcba-4bd2-97d8-d7acc93cd718",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "ccc4e22f-8b6b-4b9e-9c54-4c07a3ba87b3"
        },
        {
            "id": "606a4932-97f8-4954-9bce-a7e30f42b426",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 6,
            "m_owner": "ccc4e22f-8b6b-4b9e-9c54-4c07a3ba87b3"
        },
        {
            "id": "3b61faca-5366-41b7-ba63-d9ad2ef14453",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ccc4e22f-8b6b-4b9e-9c54-4c07a3ba87b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c15663a7-747e-4989-b90f-efc00a88c2c2",
    "visible": true
}