/// @description Step
if (start)
{
	//Gather Input
	up = keyboard_check(ord("W")) || keyboard_check(vk_up);
	left = keyboard_check(ord("A")) || keyboard_check(vk_left);
	right = keyboard_check(ord("D")) || keyboard_check(vk_right);
	down = keyboard_check(ord("S")) || keyboard_check(vk_down);

	#region Calc Movement
	//if going diaganol
	if ((right && up) ||  (right && down) || (left && up) || (left && down))
	{
		hor = (right - left) * moveSpeed;
		ver = (down - up) * moveSpeed;
	
		hor = hor/sqrt(2);
		ver = ver/sqrt(2);
	}
	//if going straight
	else 
	{
		hor = (right - left) * moveSpeed;
		ver = (down - up) * moveSpeed;
	}
	#endregion

	//collisions
	stadium_collision(obj_sumoCircle, 30);
	
	//move Player
	hspeed = hor;
	vspeed = ver;
	
	#region Sprite Changes for Facing
	if (vspeed < 0 && hspeed = 0)
	{
		sprite_index = spr_up;
	}
	
	if (vspeed > 0 && hspeed = 0)
	{
		sprite_index = spr_down;
	}

	if (vspeed = 0 && hspeed > 0)
	{
		sprite_index = spr_right
	}
	
	if (vspeed = 0 && hspeed < 0)
	{
		sprite_index = spr_left
	}

	if (vspeed > 0 && hspeed < 0)
	{
		sprite_index = spr_downleft
	}

	if (vspeed < 0 && hspeed > 0)
	{
		sprite_index = spr_upright
	}

	if (vspeed > 0 && hspeed > 0)
	{
		sprite_index = spr_downright
	}

	if (vspeed < 0 && hspeed < 0)
	{
		sprite_index = spr_upleft
	}
	#endregion
}