/// @description Take Damge If Hit
if (start)
{
	//Target active?
	if (other.hit)
	{
		//On CoolDown?
		if (!cool)
		{
			//Take Damage
			obj_Enemy.alarm[0] = 1;
			hp--;
			//take Damage and start cooldown
			cool = true;
			alarm[0] = 80;
			
			//game feel
			ScreenShake(25,10);
			flash = 1;
			
			//die if 0 hp
			if (hp <=0)
			{
				audio_stop_all();
				start = false;
				audio_play_sound(sfx_PlayerDeath, 10, false);
				instance_destroy(self);
			}
			
			//sound effect if low on health
			if (hp = 1)
			{
				audio_play_sound(sfx_LowHealth, 10, false);
			}
		}
	}
}