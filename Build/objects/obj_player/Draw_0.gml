//draw boolean when taking damage
if (draw)
{
	draw_self();
}

//flash when hit
if (flash > 0)
{
	flash -=0.05;
	shader_set(shd_hurt);
	shadeAlpha = shader_get_uniform(shd_hurt, "alpha")
	shader_set_uniform_f(shadeAlpha, flash);
	draw_self();
	shader_reset();
}