//clamp health between 0 and max
hp = clamp(hp, 0, maxhp)

//draw health to match player's health
var i
for (i = 0; i<hp; i++)
{
	if (instance_exists(obj_sumoCircle))
		draw_sprite(spr_heart, 0, 36, 44 + offset * i);
}