/// @description variables

//used to calc movement
hor = 0;
ver = 0;
moveSpeed = 1.75;

//used to calc hp and show it
hp = 4;
maxhp = 7;
offset = 36;
cool = false;

//game feel when hurt
draw = true;
alarm[1] = 1;
flash = 0;

//give player control when ready
start = false;