{
    "id": "b505d829-d327-41dd-98fa-b83639408842",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "3a178e3f-26e3-4bbf-b14a-d8f12f3ba831",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "02c41490-38f4-4690-b3f7-eac59a4c6073",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "fdf06c22-9535-4e53-9188-23000f15003f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3414fed8-c57e-424e-9d0d-77b14f12228c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "2fb27fb9-fa93-43a5-9dec-5ab25f3c0f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "5a5780ed-dbe6-4962-9aac-2faadea8ab74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "dc727103-f74a-4609-bd2c-36669cf2a7f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "324e399f-7b3a-47c5-b358-90cd9dc32dbc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "dad4b488-e4e3-4692-afbd-fc6910705002",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "450517c4-86a3-424f-b5e7-41f862c62c68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b6a76188-3c4d-4477-9f99-b79449bf294b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "761f46f9-63a7-459f-ac38-451e5af72ed4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "06eb192a-c0ce-4b72-ae2a-77fe1edafdbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "cdceed19-6555-47d6-b1bc-816027f9e57a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        },
        {
            "id": "5a600074-ada6-479c-980c-cc0536ef47f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ec01439f-c983-4665-aced-51968f4ec024",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b505d829-d327-41dd-98fa-b83639408842"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dfc8d22b-3a30-4ecd-a4bd-4371cbbd363b",
    "visible": true
}