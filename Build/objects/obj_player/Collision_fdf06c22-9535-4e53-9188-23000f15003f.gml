/// @description Get Hurt
if (start)
{
	//on cool down?
	if (!cool)
	{
		//take Damage and start cooldown
		obj_Enemy.alarm[0] = 1;
		instance_destroy(other);
		hp--;
		cool = true;
		
		//game feel
		alarm[0] = 80;
		ScreenShake(25,10);
		audio_play_sound(sfx_PlayerHit, 10, false);
		flash = 1;
		
		//die if 0 hp
		if (hp <=0)
		{
			audio_stop_all();
			start = false;
			audio_play_sound(sfx_PlayerDeath, 10, false);
			instance_destroy(self);
		}
		
		//sound effect if low on health
		if (hp = 1)
		{
			audio_play_sound(sfx_LowHealth, 10, false);
		}
	}
}