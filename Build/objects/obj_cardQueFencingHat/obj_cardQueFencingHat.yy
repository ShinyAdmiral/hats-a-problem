{
    "id": "9c6b2e73-5c7b-4258-8084-df8bfe94c8fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueFencingHat",
    "eventList": [
        {
            "id": "77cdf744-2c5f-4203-b1d0-9b1e68c3b0e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9c6b2e73-5c7b-4258-8084-df8bfe94c8fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90e99d88-dfd8-4d87-99d6-77402963b9c9",
    "visible": true
}