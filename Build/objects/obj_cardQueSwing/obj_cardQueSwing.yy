{
    "id": "08a98552-8db7-4d4c-9bc2-e3592c709875",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cardQueSwing",
    "eventList": [
        {
            "id": "5970ff38-b42f-43cf-8024-8c6f915a176a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "08a98552-8db7-4d4c-9bc2-e3592c709875"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a57990a-c266-46f8-977e-825f66b0c564",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "62e2e793-9284-48ba-a5f2-7871a4af2ed6",
    "visible": true
}