if (!obj_RazorCards.finish)
{
	inst = collision_rectangle(x - sprite_width/1.5, y - sprite_height/1.5, x + sprite_width/1.5, y + sprite_height/1.5, obj_ThrowingClub, false, true)
	if (inst != noone)
	{
		with inst instance_destroy();
	}

	inst2 = collision_rectangle(x - sprite_width/1.5, y - sprite_height/1.5, x + sprite_width/1.5, y + sprite_height/1.5, obj_ThrowingHeart, false, true)
	if (inst2 != noone)
	{
		with inst2 instance_destroy();
	}

	inst3 = collision_rectangle(x - sprite_width/1.5, y - sprite_height/1.5, x + sprite_width/1.5, y + sprite_height/1.5, obj_ThrowingSpade, false, true)
	if (inst3 != noone)
	{
		with inst3 instance_destroy();
	}
}

if (obj_RazorCards.finish)
{
	if !instance_exists(obj_ThrowingCard)
	{
		hurlSpeed *= 2;
		vspeed = -5;
	}
}
