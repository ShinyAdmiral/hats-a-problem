{
    "id": "16cca7a2-a67a-4b04-ad8f-8cfd6ab4d290",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DiamondPocket",
    "eventList": [
        {
            "id": "1e283e20-ba84-4c89-b3f8-281aa5a0488f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16cca7a2-a67a-4b04-ad8f-8cfd6ab4d290"
        },
        {
            "id": "289c98fa-46a8-4b11-984c-50b25b2506de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16cca7a2-a67a-4b04-ad8f-8cfd6ab4d290"
        },
        {
            "id": "c739cfdb-39ac-4f3d-862e-d55fae3c1ebc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dc3a01c6-6e0f-4808-9cf7-48d15af773a8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "16cca7a2-a67a-4b04-ad8f-8cfd6ab4d290"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d8292820-a3df-4b96-9c27-7dc4b6bbffe0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a16b21e4-70be-4b2f-8519-d67d22181bd8",
    "visible": true
}