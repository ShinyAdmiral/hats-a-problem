{
    "id": "14c0afc7-c005-44e4-a465-cbc5b1013778",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ClubPocket",
    "eventList": [
        {
            "id": "e1bedd74-581a-4844-aa2f-a4f6908afa9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14c0afc7-c005-44e4-a465-cbc5b1013778"
        },
        {
            "id": "b288aef6-5667-434b-8648-485db7999e15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "14c0afc7-c005-44e4-a465-cbc5b1013778"
        },
        {
            "id": "9c8b6246-4aeb-4efe-a5b1-5c171114663c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "14c0afc7-c005-44e4-a465-cbc5b1013778",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "14c0afc7-c005-44e4-a465-cbc5b1013778"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d8292820-a3df-4b96-9c27-7dc4b6bbffe0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d02ab7a5-8309-453b-a41f-350408944ccd",
    "visible": true
}