/// @description Start
//Fade into existance
audio_play_sound(sfx_Teleport, 10, false);
alpha = image_alpha;
image_alpha = 0;

//boolean for boss theme
sound = true;

//spawn in hands
instance_create_layer(176, 64, "Hands", obj_RightGlove);
instance_create_layer(64, 64, "Hands", obj_LeftGlove);

//start movement path
path_start(path14, 0.15, path_action_restart, true);

//set booleans
drawCards = false;
phantom = true;