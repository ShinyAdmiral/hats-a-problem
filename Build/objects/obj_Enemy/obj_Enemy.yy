{
    "id": "63f81e14-cef4-438c-b0db-3b3434bde4e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Enemy",
    "eventList": [
        {
            "id": "1eb51f61-d452-439e-87cc-a9228f95af60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63f81e14-cef4-438c-b0db-3b3434bde4e6"
        },
        {
            "id": "83294d98-2b80-474b-8d53-81a244e3045f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "63f81e14-cef4-438c-b0db-3b3434bde4e6"
        },
        {
            "id": "9fad23e4-d110-4226-855c-0725e0bef609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "63f81e14-cef4-438c-b0db-3b3434bde4e6"
        },
        {
            "id": "adf3a903-2cec-4eb6-b5c5-ae994bafd345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "63f81e14-cef4-438c-b0db-3b3434bde4e6"
        },
        {
            "id": "017e68a0-dd15-4547-bdbe-3e66084f1e01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "63f81e14-cef4-438c-b0db-3b3434bde4e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c24c53d0-dbca-4c5e-8238-13022ad13b4f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f91ef0f0-2e1e-43d1-a1c0-366626d29d79",
    "visible": true
}