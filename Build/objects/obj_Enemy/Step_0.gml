//make snappinng hands appear to spawn cards
if(drawCards)
{
	if (obj_LeftGlove.image_index >= 68 || obj_LeftGlove.image_index <= 13 )
	{
		//spawn snapping hands
		instance_create_layer(obj_LeftGlove.x, obj_LeftGlove.y, "Hands", obj_Snap);
		instance_destroy(obj_LeftGlove)
		drawCards = false;
	}
	//start boss music
	//maybe move this to the if above when intro is made
	if (sound)
	{
		audio_play_sound(sfx_BossMusic, 20, true)
		sound = false;
	}
}

//fade into existance at the start
if (phantom)
{
	image_alpha = image_alpha + 0.01;
	if (image_alpha = alpha)
	{
		drawCards = true;
		phantom = false;
	}
}

//end laugh when at the end of animation of laugh
if (sprite_index = spr_laugh)
{
	//laugh infinitly when dead
	if (instance_exists(obj_player))
	{
		if (image_index >= 44)
		{
			sprite_index = spr_HatBoss;	
		}
	}
}