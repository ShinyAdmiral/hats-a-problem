{
    "id": "b87a39b0-10b7-4d62-a209-6b56a14f0fa2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_handSpawner",
    "eventList": [
        {
            "id": "0154a4f4-22fc-4a29-8d45-3d9cf764c2ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b87a39b0-10b7-4d62-a209-6b56a14f0fa2"
        },
        {
            "id": "6bffb602-b3c7-4a29-bcc7-9dda7e9518ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b87a39b0-10b7-4d62-a209-6b56a14f0fa2"
        },
        {
            "id": "fdf5bcab-02c9-45d4-9a2b-196fb46d39c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b87a39b0-10b7-4d62-a209-6b56a14f0fa2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd68b79e-35b7-473f-927d-ac50657d090a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}