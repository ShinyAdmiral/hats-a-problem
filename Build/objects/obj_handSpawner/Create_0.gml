///@description start the attack
//spawn in both gloves
instance_create_layer(64, 188, "Fire", obj_glovesLeft);
instance_create_layer(172, 176, "Fire", obj_gloveRight);

//timer for hands to be active
alarm[0] = room_speed * 10; 

//boolean for finishing the attack
finish = false;