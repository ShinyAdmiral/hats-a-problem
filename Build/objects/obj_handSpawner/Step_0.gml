///@description Destroy self when gloves are gone
if (finish)
{
	if (!instance_exists(obj_glovesLeft))
	{
		if (!instance_exists(obj_gloveRight))
		{
			instance_destroy(self);	
		}
	}	
}