/// @description set up
//play audio when spawning in
audio_play_sound(sfx_Warp, 10, false);

//start faded out
image_alpha = 0;
fade = false;

//initiallize move as speed variable
move = 0.5;

//depending on location of swing, face the correct direction
if (x < 120)
{
	move *= 1;
}
if (x > 120)
{
	move *= -1;
}

//variables for one time functions
swing = true;
finish = false;
audio = true;

//destroy it's self after 18 ticks
//alarm[0] = room_speed*(18/60);