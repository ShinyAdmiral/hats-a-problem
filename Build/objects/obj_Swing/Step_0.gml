/// @description swing
//fade in
image_alpha += (room_speed*(.03/60));

//once faded in, start swing
if (image_alpha >= 1)
{
	//swing only once
	if (swing)
	{
		sprite_index = spr_WandSwing;	
		hspeed = move;
		swing = false;
		finish = true;
		alarm[1] = room_speed * (25/60);
	}
}

//when the swing has reached the middle, stop
if (x = 156)
{
hspeed = 0
}

//fade away once animation ends (check animation event)
if (fade)
{
	image_alpha -= (room_speed*(.1/60));
}