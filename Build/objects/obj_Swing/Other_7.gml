/// @description Stop attack and start to fade
if (finish)
{
	image_index = 18;
	fade = true;
	
	//some game feel to spice things up
	ScreenShake(3,5);
	if (audio)
	{
		audio_play_sound(sfx_SwipeWand, 10, false);
		audio = false;
	}
	
}