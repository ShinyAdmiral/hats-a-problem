{
    "id": "30d196c1-7b82-4325-a66b-71c74071ae67",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Swing",
    "eventList": [
        {
            "id": "9df76bb9-5ae8-4c0b-8bf5-5c1ca14d8898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30d196c1-7b82-4325-a66b-71c74071ae67"
        },
        {
            "id": "ec0de5f8-b808-465f-807c-06c47604d1a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "30d196c1-7b82-4325-a66b-71c74071ae67"
        },
        {
            "id": "0c4b9179-1e21-4058-830e-7ff8d9075301",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "30d196c1-7b82-4325-a66b-71c74071ae67"
        },
        {
            "id": "bffb45a2-cc3e-448c-b648-2a9b9201367f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "30d196c1-7b82-4325-a66b-71c74071ae67"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ec01439f-c983-4665-aced-51968f4ec024",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dbb9f561-3926-43a7-95a2-d39121080820",
    "visible": true
}