{
    "id": "edbc856e-6a81-4784-944d-74e2b38627b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CardPlay",
    "eventList": [
        {
            "id": "1afea49e-5512-4bf3-a864-c9778ae6ba1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        },
        {
            "id": "8a600644-2da2-48c5-91f7-588da7a3d813",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        },
        {
            "id": "b0a426d7-1902-46a3-aee9-c9d7b8893bc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        },
        {
            "id": "8be5552d-5922-4ae4-90bd-65e93a8e3401",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        },
        {
            "id": "b99b62f2-6e1a-473e-a03a-8db19f2e76a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        },
        {
            "id": "41f5066d-1bee-4591-83bc-516eef02506d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        },
        {
            "id": "53b73a9c-2657-49da-8bb6-db750e9d7f23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        },
        {
            "id": "cc09f856-5bc4-4899-a544-111a71114554",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "edbc856e-6a81-4784-944d-74e2b38627b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "271f983b-9a1f-436d-8e7a-cb145e9ad604",
    "visible": true
}