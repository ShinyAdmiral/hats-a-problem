if (!heal)
{
	randomize();
	ran = irandom_range(2,-2);

	var a = instance_create_layer(x, y, "Cards", obj_HealCard)
	a.direction = 270 + (5 * ran);
	a.speed = 1;
	heal = true;
}
alarm[5] = 25;