{
    "id": "5a9b687b-34a4-4940-93eb-5adf156babe9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Card_Attack",
    "eventList": [
        {
            "id": "e60821bf-e95d-445b-b786-de8eea6285a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        },
        {
            "id": "e7fe0fb9-096a-4bb4-aab8-c68a8a03d4cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        },
        {
            "id": "995606bc-ea50-4847-a725-8ee3f699778e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        },
        {
            "id": "5e8fe238-ac61-45af-ad9c-ea5d34f6e75a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        },
        {
            "id": "447b02d0-6329-49e2-917f-c518c1fd68dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        },
        {
            "id": "aca612f1-f8bd-47f1-9301-ed7e47142d66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        },
        {
            "id": "0ae7955a-6b8f-4366-93ca-f3b37fa184fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        },
        {
            "id": "449e63e8-01f4-4963-aa26-0aca12fab3cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "5a9b687b-34a4-4940-93eb-5adf156babe9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd68b79e-35b7-473f-927d-ac50657d090a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}