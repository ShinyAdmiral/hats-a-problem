/// @description appear from the smoke
if instance_exists(obj_poof)
{
	if (obj_poof.image_index = 4)
	{
		image_alpha = 255;
	}
}

//delete self if options are taken
if (!instance_find(obj_card, 1))
{
	instance_create_layer(obj_card.x,obj_card.y,"Fire", obj_poof);
	instance_destroy(obj_card);
}