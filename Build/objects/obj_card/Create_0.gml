/// @description set Variables

//appear from the smoke
image_alpha = 0;
instance_create_layer(x, y, "Player", obj_poof);

//variables
pressed = false;
vspeed = -0.05;
mouse_over = false;

//freeze on the first frame
default_speed = image_speed;
image_speed = 0;
image_index = 0;