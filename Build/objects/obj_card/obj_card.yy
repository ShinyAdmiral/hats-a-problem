{
    "id": "8d8b6742-5f05-4542-a14a-dc8244fe4dba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_card",
    "eventList": [
        {
            "id": "5e9495d8-8961-4936-8344-6399e54037a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "7af1eb18-57a7-4c2c-83fd-a577c58c0a03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "bbf602a8-9938-4dfd-8e9d-7d4c818e8fea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "019fe806-e498-4b09-b341-2a07283a617d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "7f926e5e-5a32-4262-b1ec-29224a11514d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "79025d25-3a48-4acc-a6d0-c62eae853a1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "fba6de9b-ec00-4847-a016-425a948127ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "ee1425be-56d6-49b2-ac83-bc94babbf229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        },
        {
            "id": "29213f54-577f-42cf-9f05-da2cd64444a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8d8b6742-5f05-4542-a14a-dc8244fe4dba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0d1aa4d-90d0-416a-bbb7-3fb527e3099a",
    "visible": true
}