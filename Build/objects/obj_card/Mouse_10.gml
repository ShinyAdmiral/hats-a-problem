/// @description Show Hovering

if (!pressed)
{
	image_speed = default_speed;
	audio_play_sound(sfx_SelectHover, 10, false);
	mouse_over = true;
}