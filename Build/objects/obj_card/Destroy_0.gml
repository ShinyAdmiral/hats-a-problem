/// @description set organizer variable

if (instance_find(obj_card, 3))
{
	obj_AttackOrganizer.instance[0] = object_get_name(object_index);
}

else if (instance_find(obj_card, 2))
{
	obj_AttackOrganizer.instance[1] = object_get_name(object_index);
}

else if (instance_find(obj_card, 1))
{
	obj_AttackOrganizer.instance[2] = object_get_name(object_index);
}
