if (!set_up)
{
	obj_player.x = lerp(obj_player.x, obj_sumoCircle.x, 0.05);
	obj_player.y = lerp(obj_player.y, obj_sumoCircle.y, 0.05);
	if ((obj_player.x >= obj_sumoCircle.x - offset) && (obj_player.x <= obj_sumoCircle.x + offset))
	{
		if (obj_player.y >= obj_sumoCircle.y - offset) && (obj_player.y <= obj_sumoCircle.y + offset)
		set_up = true;
		alarm[0] = room_speed * 5;
	}
}

if ((set_up) && !finish)
{
	if(mouse_check_button_pressed(mb_left))
	{
		instance_create_layer(obj_player.x, obj_player.y, "Fire", obj_Fire_Blast_Projectile);
	}
}

if (finish)
{
	if (!instance_exists(obj_Fire_Blast_Projectile))
	{
		obj_player.start = true;
		instance_destroy(self);
	}
}