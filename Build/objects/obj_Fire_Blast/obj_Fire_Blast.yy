{
    "id": "1fe5df1d-ac3f-4a41-aa66-7e1f4307c868",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Fire_Blast",
    "eventList": [
        {
            "id": "32574788-5afc-4449-8571-d1876d53acec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1fe5df1d-ac3f-4a41-aa66-7e1f4307c868"
        },
        {
            "id": "c37614b9-09f7-4d97-8f9a-97a603bc7d92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1fe5df1d-ac3f-4a41-aa66-7e1f4307c868"
        },
        {
            "id": "2187e2f2-5f6b-43d8-b0c5-513b3c6aceac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1fe5df1d-ac3f-4a41-aa66-7e1f4307c868"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}