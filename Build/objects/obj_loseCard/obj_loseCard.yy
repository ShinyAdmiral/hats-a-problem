{
    "id": "d44bb881-90d5-46ba-acfd-aa82938996d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loseCard",
    "eventList": [
        {
            "id": "dad8996d-a797-4f9e-a82b-7ee3e24febc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        },
        {
            "id": "0cfb63ec-1fb4-4e9a-af33-34735908897c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        },
        {
            "id": "e39c3bb9-a518-4c49-8d1f-308e92286058",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        },
        {
            "id": "798052bc-730d-4173-99a3-1fff4c3b0d99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        },
        {
            "id": "647e1ee8-10ce-435b-a7a0-f412ceac634f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        },
        {
            "id": "5e38ea23-72a5-4195-853d-ae506f0941a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        },
        {
            "id": "515d0662-e590-48ee-a69a-8c4758019b93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        },
        {
            "id": "362ad286-3946-443f-98a1-798c37215057",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d44bb881-90d5-46ba-acfd-aa82938996d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1908a605-3e07-45f4-a6b5-7adb2f0ca014",
    "visible": true
}