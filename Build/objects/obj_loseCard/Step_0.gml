if (!place)
{
	if (y >= 128)
	{
		vspeed = 0;
		place = true;
		sprite_index = spr_loseCardDeath;
		vspeed = -0.05;
		obj_Enemy.alarm[2] = 1;

		if (audio)
		{
			if (obj_scoreHolder.points < 20)
			{
				audio_play_sound(sfx_lowscore, 10, true);
				audio = false;
			}
			else
			{
				audio_play_sound(sfx_highscore, 10, true);
				audio = false;
			}
		}
	}
}