{
    "id": "324e399f-7b3a-47c5-b358-90cd9dc32dbc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_FireWarning",
    "eventList": [
        {
            "id": "a5629ac5-9876-46cf-be72-e2195bba8d62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "324e399f-7b3a-47c5-b358-90cd9dc32dbc"
        },
        {
            "id": "b3e9f7e2-d990-4dc5-9618-260459c39c22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "324e399f-7b3a-47c5-b358-90cd9dc32dbc"
        },
        {
            "id": "60f43f6a-d6a2-45d2-bc05-df15422da84a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "324e399f-7b3a-47c5-b358-90cd9dc32dbc"
        },
        {
            "id": "8bc67363-5370-43f2-9d38-f05838a22437",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "324e399f-7b3a-47c5-b358-90cd9dc32dbc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8af1e7d4-6bbe-42e5-b885-44c487ed9a72",
    "visible": true
}