/// @description Set Up

//Check and replace if in invalid spot
if(!place_meeting(x,y,obj_sumoCircle) || place_meeting(x,y, obj_FireWarning))
{
	ranx = irandom_range(obj_sumoCircle.x - obj_FireStarter.range_x, obj_sumoCircle.x + obj_FireStarter.range_x);
	rany = irandom_range(obj_sumoCircle.y - obj_FireStarter.range_y, obj_sumoCircle.y + obj_FireStarter.range_y);
	instance_create_layer(ranx, rany, "Bullets", obj_FireWarning);
	instance_destroy(self);
}

//get small to grow
image_xscale = 0;
image_yscale = 0;

//booleans for one liners
fire = false;
hit = false;
activate = false;

//variable for fire to prevent missteps
var a;