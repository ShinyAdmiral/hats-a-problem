/// @description Activation

//ready to fire?
if (!fire)
{
	//grow if small
	if (image_xscale < 1 && image_yscale < 1)
	{
		image_xscale = image_xscale + .02;
		image_yscale = image_yscale + .02;
	}
	
	//shoot fire when big enough
	else
	{
		image_xscale = 1
		image_yscale = 1
		alarm[0] = 20; 
		fire = true;
	}
}

if (activate)
{
	if instance_exists(a)
	{
		if place_meeting(x,y,a)
		{
			hit = true;
			alarm[1] = 1;
		}
	}
}