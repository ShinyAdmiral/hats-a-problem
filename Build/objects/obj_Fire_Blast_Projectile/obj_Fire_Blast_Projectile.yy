{
    "id": "c725eb5f-4830-40e6-a093-31caa851fd61",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Fire_Blast_Projectile",
    "eventList": [
        {
            "id": "76bbd52c-308c-424a-a6d2-0bf292e9a98e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c725eb5f-4830-40e6-a093-31caa851fd61"
        },
        {
            "id": "d1d23d0d-314f-4014-8dd3-e4e5f3cf3e93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c725eb5f-4830-40e6-a093-31caa851fd61"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d8292820-a3df-4b96-9c27-7dc4b6bbffe0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d0052b80-ffb6-439d-9e0f-a49d3f3b439a",
    "visible": true
}