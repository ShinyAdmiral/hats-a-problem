{
    "id": "f2b340d6-8791-4cd4-a0af-a8852f870fe3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_EvilHeart45",
    "eventList": [
        {
            "id": "03adadcf-6d49-49e7-b1e1-573d0de80a06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f2b340d6-8791-4cd4-a0af-a8852f870fe3"
        },
        {
            "id": "c756285d-e538-47a9-9a75-bb16f5fd2e21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f2b340d6-8791-4cd4-a0af-a8852f870fe3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3414fed8-c57e-424e-9d0d-77b14f12228c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6bfd22f7-a79e-4f34-9b0f-d4423ef846b6",
    "visible": true
}