if (!moving)
{
	if (mouse_check_button_pressed(mb_left))
	{
		speed = 7;
		dir = point_direction(obj_player.x, obj_player.y, obj_cursor.x, obj_cursor.y);
		direction = dir;
		image_angle = 90 + dir;
		obj_RazorCards.alarm[1] = 30;
		moving = true;
	}
}

if (obj_RazorCards.finish)
{
	if (speed = 0)
	{
		instance_destroy(self);
	}
}