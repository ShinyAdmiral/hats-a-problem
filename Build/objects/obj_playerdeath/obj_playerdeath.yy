{
    "id": "f1eb481f-21ee-47ab-abfe-fbebc059d92e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playerdeath",
    "eventList": [
        {
            "id": "b9031033-ccf0-4609-8206-c2b1d0e0dfbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f1eb481f-21ee-47ab-abfe-fbebc059d92e"
        },
        {
            "id": "c1b8e6c5-764c-48ad-8202-09fbabe6f24c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f1eb481f-21ee-47ab-abfe-fbebc059d92e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9ac85808-56b9-4f36-9e8e-8a63a7e73a04",
    "visible": true
}