{
    "id": "76d22e1c-81fb-4ec3-9c50-23b3f2738bdf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Heart",
    "eventList": [
        {
            "id": "bc0ea17f-4211-4325-b6ab-29177bbcb5b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76d22e1c-81fb-4ec3-9c50-23b3f2738bdf"
        },
        {
            "id": "3cbe3f09-def2-43c0-af65-f0382729ede1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76d22e1c-81fb-4ec3-9c50-23b3f2738bdf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b6a76188-3c4d-4477-9f99-b79449bf294b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cc42f23d-d0dc-4b09-b151-6bc8eb7b0c78",
    "visible": true
}