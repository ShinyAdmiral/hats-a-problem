{
    "id": "35d1dafd-f32e-4581-a620-c64aa919aadf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HurdlingPlayer",
    "eventList": [
        {
            "id": "5eaf1410-8b25-404c-9019-238280ec80af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "35d1dafd-f32e-4581-a620-c64aa919aadf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd9e5dfb-2947-4b54-a673-b532f1f88d55",
    "visible": true
}