{
    "id": "0a2200c2-b808-4a8d-89df-2d187d4938c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bulletShreddar",
    "eventList": [
        {
            "id": "48dffc8f-d8e0-4fb7-8075-db41f19baadf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3414fed8-c57e-424e-9d0d-77b14f12228c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "66306fe4-2e3a-461b-8151-9c6ec8d20419",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8d8b6742-5f05-4542-a14a-dc8244fe4dba",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "ef9d5ccf-030d-45fe-96af-6bee4c1daf9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a006c9f3-c6e6-4e93-a58c-885697b7874d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "92cd65ec-abeb-4bdf-bfb8-21e8b518356f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "76d22e1c-81fb-4ec3-9c50-23b3f2738bdf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "948ca5eb-66ff-4cfa-88fe-d058cc214925",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5119b736-69f4-4e7b-b018-13e841c09405",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "d46bddf8-6e60-4f81-86bb-72d95e0eb1c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "edbc856e-6a81-4784-944d-74e2b38627b3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "ce7f4d3c-7274-446b-a20d-70ee493a42c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "774cb5e0-12ff-4616-af3e-a0a3c0aa7f29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d44bb881-90d5-46ba-acfd-aa82938996d2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "6339009e-a88e-4f8a-b4d8-5311a586b2da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0166635f-cdf1-4ad2-9225-0a5581a0b9e2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "8a6e7d5f-1412-4e44-99ab-00be8dca7ab5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f2448f11-8a61-450b-b773-f72b6c3ff415",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "bb008061-ea59-4957-9031-53a74b411db9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3191e4f8-0ec3-449e-bc1a-2a76a1d4cef6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "a339f9d7-59d4-4a2c-bca7-aca895a9c98e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8292820-a3df-4b96-9c27-7dc4b6bbffe0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        },
        {
            "id": "a1dd8e5e-847e-40e5-a69c-bed15b47d7a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "28cffc8f-c118-4c90-9a4e-061c7bbb82b0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0a2200c2-b808-4a8d-89df-2d187d4938c0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de61c415-e8f0-415d-969b-7e9a94dccf2c",
    "visible": true
}