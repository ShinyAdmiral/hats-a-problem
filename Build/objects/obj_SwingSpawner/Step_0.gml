/// @description Spawn in swing once the previous is done
if (!instance_exists(obj_RightGlove))
{
	if (!instance_exists(obj_Swing))
	{
		//left or right?
		randomize();
		ran = choose(-1,1)
		
		//right spawn
		if (ran > 0)
		{
			if (right <= (left-3))
			{
				instance_create_layer(128, 156, "Fire", obj_Swing)
				//adding a point to control randomness
				right ++;
				left = 0;
			}
			//rng control
			else
			{
				var a = instance_create_layer(112, 156, "Fire", obj_Swing)
				a.image_xscale = -1
				//adding a point to control randomness
				left ++;
				right = 0;
			}
		}
		//sleft spawn
		else
		{
			if (left <= (right-3))
			{
				var a = instance_create_layer(112, 156, "Fire", obj_Swing)
				a.image_xscale = -1
				//adding a point to control randomness
				left ++;
				right = 0;
			}
			//rng control
			else
			{
				instance_create_layer(128, 156, "Fire", obj_Swing)
				//adding a point to control randomness
				right ++;
				left = 0;
			}
		}
	}
}