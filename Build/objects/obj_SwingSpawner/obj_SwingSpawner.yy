{
    "id": "bdf3df84-cabb-4c1e-a6cf-d066a129bfb3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_SwingSpawner",
    "eventList": [
        {
            "id": "afe44da6-73b5-43f9-b677-61e53969ebe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdf3df84-cabb-4c1e-a6cf-d066a129bfb3"
        },
        {
            "id": "a4cca08b-7a28-4bae-b4cc-d2d93ef7af5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdf3df84-cabb-4c1e-a6cf-d066a129bfb3"
        },
        {
            "id": "0487b118-3df1-4887-881e-0f040351536b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bdf3df84-cabb-4c1e-a6cf-d066a129bfb3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd68b79e-35b7-473f-927d-ac50657d090a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}