{
    "id": "f00bb396-87e8-46df-b32f-5044118ad1a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Avoid",
    "eventList": [
        {
            "id": "b1b33c6a-a8bf-4745-b12c-19704a389afc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        },
        {
            "id": "7c91c28a-889f-4dd3-a387-a7b20a89989a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        },
        {
            "id": "789e9764-7c21-45df-9f63-9e0479cb3772",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        },
        {
            "id": "173bb710-c865-40f1-b59f-e2bc52c8e4c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        },
        {
            "id": "cab85669-b0b3-4aa0-a8bc-1279f51bad5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        },
        {
            "id": "daf5d7da-d88b-4168-8565-0d1612cc61f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        },
        {
            "id": "c72240bc-3ddd-4fa3-b9f0-64d8dc814f3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        },
        {
            "id": "409250aa-22cb-41ad-bd2a-f06e561555e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f00bb396-87e8-46df-b32f-5044118ad1a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3191e4f8-0ec3-449e-bc1a-2a76a1d4cef6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "62b96513-bbe7-41f8-a6bb-572f29cb8792",
    "visible": true
}