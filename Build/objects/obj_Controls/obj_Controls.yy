{
    "id": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Controls",
    "eventList": [
        {
            "id": "fcb5b607-7519-4f46-ba0b-ea7e9f9314b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        },
        {
            "id": "cad8ea8e-3e79-4188-ab48-3f0a9c64f323",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        },
        {
            "id": "89b904ab-fb73-4af6-83b2-ded62a667e3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        },
        {
            "id": "e55f7db9-99c6-4bf5-ab3d-93ce57034ab1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        },
        {
            "id": "9bbd1de7-05f1-4033-a5c0-c4f74cecdb67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        },
        {
            "id": "7448776e-6b50-4849-b4dc-e38b22edad71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        },
        {
            "id": "a813a007-a55a-4111-b73d-d730624c069e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        },
        {
            "id": "41d12f9e-e6da-4e89-b162-2b61df732647",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a40cc435-3ad0-4bf2-a1ff-59f61641cae5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3191e4f8-0ec3-449e-bc1a-2a76a1d4cef6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "50fd291a-06e8-488a-91d6-0864554a4eab",
    "visible": true
}