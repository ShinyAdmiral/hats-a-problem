{
    "id": "682d7c70-4e41-4ba9-9783-a52dd42b89cf",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path14",
    "closed": true,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "4ca8fa8a-3b2b-464d-9df9-25f3df71378d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 120.529434,
            "y": 64.04411,
            "speed": 100
        },
        {
            "id": "ca6b8a31-1cc9-4562-93b7-2679d61a3aa8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 104.985321,
            "y": 67.45588,
            "speed": 100
        },
        {
            "id": "228305f7-cb03-40f8-8ba5-863e0747ddc6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 106.5294,
            "y": 72.75,
            "speed": 100
        },
        {
            "id": "59cdcae4-c950-4d17-8e4d-d76bbad99e8f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 120.5007,
            "y": 72.38874,
            "speed": 100
        },
        {
            "id": "f7b1fb9e-c8a6-47dc-b8b4-59ea9a1126dc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128.5787,
            "y": 72.50931,
            "speed": 100
        },
        {
            "id": "830b492e-8f87-4f9b-8b0c-74cc9897bd06",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 135.058853,
            "y": 73.1323547,
            "speed": 100
        },
        {
            "id": "7420af10-8548-4b12-8b6a-c5a7bc9bb18f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 136.294144,
            "y": 67.47059,
            "speed": 100
        },
        {
            "id": "8a9c3c3f-2c26-4a9d-b9c4-e97d52fcefde",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 131.706375,
            "y": 65.58732,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}