***There might be an attempt to block the file from downloading and running, just bypass those.***
* Run the executable and hit more info and run anyways (if windows tries to block it)
* Press the escape key to leave the game
* There are instructions in the game on how to play

--------------------------------------------------------------------------------------------------------
Our team has decided to take a hiatus on Hat's a Problem for an unknown amount of time, mainly because we've lost motivation in it for the time being. 
We took a month-long break recently, and other scheduling kept us from working on it actively. As a team, we're making the decision to start a new 
project that will spark our creativity and motivation again, and be managed in a better manner. With the addition and loss of team members over time, 
we've created a team lineup that we are happy with, and all of us are excited to work together on a new project. We hope you understand our thinking 
and support us in our next endeavour. This might not be the last time you see Hat's a Problem, so stay tuned for the new projects and any updates on this one.
--------------------------------------------------------------------------------------------------------

1.1 Patch Notes - 2/15/2020
New features
- You can now select 3 cards from a hand of four
- Cool Lookin Cursor was implemented
- A pretty background

New Unavailable Features: these features were removed because they are not fully developed and we are moving away from the project for now.
- The player can now select attack cards
- A Fire Attack was added
- A Hat Trampoline Attack was added
- A Razor Card Attack was added
- A Slot Machine Attack was added

Fixes
- The boss will not be restarted laughing animation
- Cleaned up code (game jams get sloppy)
- Fireballs don't randomly despawn

Improvements
- Smooth collisions along the sides of the spotlight
- Sprite fixes on the hands, wands, cards, and outlines
- Nerfed Swing (Swings slower)
- Swing randomization is controlled
- Hand Guns attempt to follow you
- Petals hit all spots on the spotlight
- Knife randomization controlled
- Cards spin back when unselected